<!DOCTYPE html>
<html ng-app="myApp">
<!-- web app capri  www.capri-agence.com -->
<head>
<meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-1">

  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width,initial-scale=1">
  <title>Admin</title>
  <!-- Angular -->

  <script  type = "text/javascript"  src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyBQE8-oT6QrHhk5T3hWtcaPVnTAOH5QtjA&libraries=places" ></script>
  <script src="js/angular-googlemap-location-picker.js"></script>
  </head>
  <body ng-cloak="">
    <div ng-controller="navabarCtrl" >
      <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#/listing">Administration</a>
    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <div ng-controller="authCtrl" >
              <ul class="nav navbar-nav">
              <li ng-class="{ active: isActive('/users') }">
          <a class="nav-link" ng-show="isUserLoggedIn" style="color:#FFFFFF" href="#/users">Utlisateurs</a>
        </li>

        <li ng-class="{ active: isActive('/listing') }"> <a ng-show="isUserLoggedIn" style="color:#FFFFFF" href="#/listing"> Questions/Réponses </a></li>
        </li>
        <li ng-class="{ active: isActive('/listingquestion') }"> <a ng-show="isUserLoggedIn" style="color:#FFFFFF" href="#/listingquestion"> Questions </a></li>
        </li>
        <li ng-class="{ active: isActive('/category') }">
          <a class="nav-link" ng-show="isUserLoggedIn" style="color:#FFFFFF" href="#/category">Catégories</a>
        </li>
      
    </ul>

      <ul class="nav navbar-nav navbar-right" ng-show="isUserLoggedIn">
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{name}}  <span class="caret"></span></a>
          <ul class="dropdown-menu">
          <li>  <a  ng-show="isUserLoggedIn"  data-title="Delete" data-toggle="modal" href="#/compte" role="button">Mon compte</a></li>

            <li role="separator" class="divider"></li>
            <li>  <a  ng-show="isUserLoggedIn"  data-title="Delete" data-toggle="modal" data-target="#close" role="button">Déconnexion</a></li>
          </ul>
        </li>
      </ul>
      </div>
          </div>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
  <div>
    <div class="container-fluid" style="margin-top:20px;">
      <div data-ng-view="" id="ng-view" class="slide-animation"></div>
    </div>
    <div class="bg-faded container-fluid"  style="color:#FFFFFF;background-color: #414141;">
      <div class="footer pt-3 pb-3">
        <div class="container ">
          <div class="row">
            <div class="col-md-4">
              <h6>&copy; 2018 - création <a style="color:#FFFFFF;" href="http://www.capri-pro.com/">  Capri   </a> </h6>
            </div>
            <div class="col-md-4">
            </div>
            <div class="col-md-4">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <toaster-container toaster-options="{'time-out': 3}"></toaster-container>
  <!-- Libs -->
  <script src="js/angular-route.min.js"></script>
  <script src="js/angular-animate.min.js" ></script>
  <script src="js/ng-map.min.js" ></script>
  <script rel="stylesheet" src="css/custom.css" ></script>
  <script rel="stylesheet" src="js/ng-image-input-with-preview.css" ></script>
  <script type="text/javascript" src="js/ng-image-input-with-preview.js"></script>
  <!-- vsGoogleAutocomplete -->
  <script src="js/vs-google-autocomplete.js"></script>
  <script src="js/toaster.js"></script>
  <script src="app/app.js"></script>
  <script src="app/listing.js"></script>
  <script src="app/data.js"></script>
  <script src="app/directives.js"></script>

  <!-- modal-content -->
  <div class="modal fade" id="close" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
          <h4 class="modal-title custom_align" id="Heading">Deconnexion</h4>
        </div>
        <div class="modal-body">
          <div class="alert alert-danger">Voulez-vous vraiment vous déconnecter ? </div>
        </div>
        <div ng-controller="authCtrl" >
          <div class="modal-footer ">
            <button type="button" class="btn btn-success" data-title="close" data-toggle="modal" ng-click="logout()"><span class="glyphicon glyphicon-ok-sign"></span> Oui </button>
            <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Non </button>
          </div>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
  </br>
</br>
</br>
</div>
</html>
