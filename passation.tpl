<?php echo $header; ?>
<div class="container">

  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
	<div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
		<div class="row">
			<div class="col-sm-6">
				<div class="">
					<h2>Bienvenue dans l’espace des passations</h2>
				</div>
			</div>
			<div class="col-sm-6 ">
			<div class="well">
				<p>Vous trouverez ci-dessous la liste des examens en cours.<br>Sélectionnez votre examen, saisissez le code d&#8217;accès puis appuyez sur le bouton <i>Valider</i>.</p>
				<br>
				<?php if ($error_warning) { ?>
					<div class="alert alert-danger">
						<i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?> 
					</div>
				<?php } ?>
			</div>
			</div>
		</div>
		<div class="row">	
			<div class="col-sm-12 ">
				<p></p>
			</div>
		</div>
		
		<hr>
		
	  <br> 
	   <h2>Passations</h2>
   	     
      <br />
      <div class="row">
	  
        <div class="product-layout product-list col-xs-12">
          <div class="product-thumb">
           <div class="image"><img src="https://www.selectionetconseils.ch/examen/image/cache/catalog/PCGE-150x150.jpg" class="img-responsive"></div>
            <div>
				<div class="caption">
					<div class="row">
						<div class="col-sm-10 text-left"><h3>Police Cantonale de Genève</h3></div>
						<div class="col-sm-2 text-right"></div>
						<div class="col-sm-12 text-left"><span style="font-family: arial, helvetica, sans-serif; font-size: 12pt; color: #000000;">Sélection et Conseils vous souhaite la bienvenue aux tests&nbsp;d'évaluation.</span></div>
					</div>		
				</div>	
				<div class="button-group">
					<div class="col-sm-4 text-left"></div>
					<div class="col-sm-8 text-right">
							<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">	
								<div class="form-group" style="margin-top:15px;">
									Saisir ici le code d&#8217;accès à l&#8217;examen
									<input type="input" name="passation_code" value="" id="input-mdp" autocomplete="off" class="form-control" style="display:inline; width:20%;"/>
									<input type="submit" value="Valider" class="btn btn-primary" />
								</div>	
								<input type="hidden" name="passation_num" value="1" />
							</form>
										</div>	
				</div>	
            </div>
          </div>
        </div>
      </div>
     

      <?php echo $content_bottom; ?>
	</div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>
