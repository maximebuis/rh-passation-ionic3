import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, MenuController, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Login } from '../pages/login/login';
import { Diagnostic } from '@ionic-native/diagnostic';
import { HomePage } from '../pages/home/home';
import { AuthService } from '../providers/AuthService';
import { PassationsPage } from '../pages/passations/passations';
import { TranslateService } from '@ngx-translate/core';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage: any;
  pages: Array<{title: string, component: any, icon:any}>;
  pages2: { title: string; component: any, icon:any,logsOut: boolean; }[];
  constructor( private translate: TranslateService,public loadingController:LoadingController,public authService: AuthService,public diagnostic:  Diagnostic ,
    public menu: MenuController, public storage: Storage,public platform: Platform) {
    translate.addLangs(['en','gr', 'fr']);
    if(this.authService.authenticated()) {
      this.rootPage = PassationsPage;
    } else {
      this.rootPage = PassationsPage;
    }
  
    this.pages2 = [
      { title: 'Déconnexion', component: HomePage, icon: 'log-out', logsOut: true }
    ];
    this.menu.enable (false); 
     

    this.initializeApp();
      
    }

  initializeApp() {
    this.platform.ready().then(() => {
     
     });
     this.initTranslate();
  }
  initTranslate() {
    console.log("translate ini");
    // Set the default language for translation strings, and the current language.
    this.translate.setDefaultLang('fr');


    if (this.translate.getBrowserLang() !== undefined) {
        this.translate.use(this.translate.getBrowserLang());
    } else {
        this.translate.use('fr'); // Set your language here
    }

}

  dologout() {
    this.authService.login()
    this.menu.enable (false); 
    this.nav.setRoot(Login);
  }
  
  isActive(page: any) {
    if (this.nav.getActive() && this.nav.getActive().component === page.component) {
      return 'light';
    }
    return 'secondary';
  }
  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
