import { BrowserModule, SafeHtml } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { Diagnostic } from '@ionic-native/diagnostic';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Login } from '../pages/login/login';
import { Forgotpassword } from '../pages/forgotpassword/forgotpassword';
import { SettingsPage } from '../pages/settings/settings';
import { HttpModule } from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';
import {RedditService} from "../providers/reddit-service";
import { Geolocation } from '@ionic-native/geolocation';
import { FormsModule } from '@angular/forms';
import { Edit } from '../pages/edit/edit';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { FileTransfer } from '@ionic-native/file-transfer';
import { FilePath } from '@ionic-native/file-path';
import { File } from '@ionic-native/file';
import { Transfer } from '@ionic-native/transfer';
import { AuthService } from '../providers/AuthService';
import {NgxWigModule} from 'ngx-wig';
import { TestsPage } from '../pages/tests/tests';
import { QuestionsPage } from '../pages/questions/questions';
import { PassationsPage } from '../pages/passations/passations';
import { OrdertestPage } from '../pages/ordertest/ordertest';
import { DragulaModule } from 'ng2-dragula';
import { OrderpassationPage } from '../pages/orderpassation/orderpassation';
import { StarttestPage } from '../pages/starttest/starttest';
import { ValeursPage } from '../pages/valeurs/valeurs';
import { EndtestPage } from '../pages/endtest /endtest';
import { SessionsPage } from '../pages/sessions/sessions';
import { QuestionnairePage } from '../pages/questionnaire/questionnaire';
import { ScientifiquePage } from '../pages/scientifique/scientifique';
import { LogiquePage } from '../pages/logique/logique';
import { VerbaldefinitionPage } from '../pages/verbaldefinition/verbaldefinition';
import { VerbaleliminationPage } from '../pages/verbalelimination/verbalelimination';
import { MemoirePage } from '../pages/memoire/memoire';
import { EspacePage } from '../pages/espace/espace';
import { StressPage } from '../pages/stress/stress';
import { ImaginationPage } from '../pages/imagination/imagination';
import { JugementPage } from '../pages/jugement/jugement';
import { FacialePage } from '../pages/faciale/faciale';
import { VerbalsynonymePage } from '../pages/verbalsynonyme/verbalsynonyme';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import {TranslateHttpLoader} from "@ngx-translate/http-loader";

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    Login,
    Forgotpassword,
    SettingsPage,
    Edit,
    SessionsPage,
    TestsPage,
    QuestionsPage,
    PassationsPage,
    StarttestPage,
    EndtestPage,
    OrdertestPage,
    OrderpassationPage,
    ValeursPage,
    QuestionnairePage,
    ScientifiquePage, 
    LogiquePage,
    VerbaldefinitionPage,
    VerbaleliminationPage,
    MemoirePage,
    EspacePage,
    StressPage,
    ImaginationPage,
    JugementPage,
    FacialePage,
    VerbalsynonymePage,
  ],
  imports: [
    
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
        HttpModule ,
        IonicStorageModule.forRoot(),
        NgxWigModule,
        DragulaModule, 
        TranslateModule.forRoot({
          loader: {
              provide: TranslateLoader,
              useFactory: (HttpLoaderFactory),
              deps: [HttpClient]
          }
      }),

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    Login,
    SessionsPage,
    Forgotpassword,
    SettingsPage,
    Edit,
    TestsPage,
    QuestionsPage,
    PassationsPage,
    StarttestPage,
    EndtestPage,
    OrdertestPage,
    OrderpassationPage,
    ValeursPage,
    QuestionnairePage,
    ScientifiquePage,
    LogiquePage,
    VerbaldefinitionPage,
    VerbaleliminationPage,
    MemoirePage,
    EspacePage,
    StressPage,
    ImaginationPage,
    JugementPage,
    FacialePage,
    VerbalsynonymePage
  ],
  providers: [

    File,
    Transfer,
    FilePath,
    FileTransfer,
    Geolocation,
    StatusBar,
    BrowserModule,
    FormsModule,
    SplashScreen,
    RedditService,
    AuthService,
    Diagnostic,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
  
  ]
})
export class AppModule {}

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}
