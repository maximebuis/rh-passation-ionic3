import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Component, NgZone} from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation';
import { RedditService } from "../../providers/reddit-service";
import {Storage} from '@ionic/storage';

import {NavController,  NavParams,  ActionSheetController, ToastController, Platform, LoadingController, Loading } from 'ionic-angular';
import { Md5 } from 'ts-md5';


@Component({
selector: 'page-edit',
templateUrl: 'edit.html',
})
export class Edit {
  item: any;
  phone: any;
  cp: any;
  city: any;
  address: any;
  email: any;
  firstname: any;
  lastname: any;
  birthday: any;
  genre: any;
  diplome: any;
  certificat: any;
  parameter1: any;
  data: any;
  time: any;
  date: any;
  password: any;
  password2: any;

  constructor( 
    public loadingController:LoadingController,  public actionSheetCtrl: ActionSheetController, 
    public toastCtrl: ToastController, public platform: Platform, public loadingCtrl: LoadingController,
    public navCtrl: NavController, public storage: Storage, public redditService:RedditService, 
    public navParams: NavParams, public http: Http, public Geolocation: Geolocation, public zone: NgZone) {
    this.parameter1 = navParams.get('param1');
    console.log(  this.parameter1 );
  }
  
ionViewWillEnter(){
    this.redditService.getDetail(this.parameter1).subscribe(data=>{

      this.data=data.items;
      this.birthday=data.items[0].birthday;
      this.date=data.items[0].date;
      this.genre=data.items[0].genre;
      this.diplome=data.items[0].diplome;
      this.time=data.items[0].time;
      this.certificat=data.items[0].certificat;
    })
    }
doSave(){    
var data = JSON.stringify({ 
userid:this.parameter1,
lastname: this.lastname,
firstname: this.firstname,
email: this.email,
address: this.address,
city: this.city,
cp: this.cp,
genre:this.genre, 
diplome:this.diplome,
time:this.time,
certificat:this.certificat,
birthday:this.birthday, 
});

this.redditService.updateuser(data)  
.toPromise()
.then((response) =>
{
console.log(response);
})

}

doSavepassword(){    

  this.password2=Md5.hashStr(this.password);
  var data = JSON.stringify({ 
  userid:this.parameter1,
  password: this.password2,
  });
  this.redditService.updateuserpassword(data)  
  .toPromise()
  .then((response) =>
  {
  console.log(response);
  })


  setTimeout(() => { 
    this.navCtrl.push(Edit, {
      param1: this.parameter1,
    } );    
  }, 500);}
 
}








