import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Component, NgZone} from '@angular/core';
import { RedditService } from "../../providers/reddit-service";
import {Storage} from '@ionic/storage';
import {NavController,  NavParams,  ActionSheetController, ToastController, Platform, LoadingController, Loading } from 'ionic-angular';
import { StarttestPage } from '../starttest/starttest';
import { HomePage } from '../home/home';
import { Login } from '../login/login';
import { AuthService } from '../../providers/AuthService';
import { TranslateService } from '@ngx-translate/core';


@Component({
selector: 'page-endtest',
templateUrl: 'endtest.html',
})
export class  EndtestPage {
  
  question_label: any;
  q_prop1: any;
  q_prop2: any;
  q_prop3: any;
  q_prop4: any;
  q_prop5: any;
  q_prop6: any;
  q_img1: any;
  q_img2: any;
  q_img3: any;
  q_img4: any;
  q_img5: any;
  q_img6: any;
  son: any;
  parameter1: any;
  data: any;
  type: any;
  description2: any;
  types: any;
  format_num: any;
  test_label: any;
  test_titre: any;
  test_duree: any;
  test_nav: any;
  test_into: any;
  test_final: any;
  test_tiers: any;
  test_intro: any;
  test_category: any;
  parameter2: number;
  questions: any;
  lenghtquestions: any;
  parameter3: any;
  numpassation: any;
  sessionorder: any;
  customer_id: any;
  idsession: any;
  longueurpassation: any;
  idtest: any;
  lenghttest: any;
  test: any;
  position: any;
  lg: string;

  constructor( 
    public loadingController:LoadingController,public authService: AuthService,  public actionSheetCtrl: ActionSheetController, 
    public toastCtrl: ToastController, public platform: Platform, public loadingCtrl: LoadingController,
    public navCtrl: NavController, public storage: Storage, public redditService:RedditService, 
    public navParams: NavParams, public http: Http, public zone: NgZone,private translate: TranslateService) {
    this.parameter1 = navParams.get('param1');
    this.position = navParams.get('param2');
    
    this.storage.get('customer_id').then((userid) => {
      this.customer_id=userid;
      console.log(this.numpassation)
    })

    this.storage.get('numpassation').then((numpass) => {
      this.numpassation=numpass;
      console.log(this.numpassation)
    })
    this.storage.get('longueurpassation').then((longpass) => {
      this.longueurpassation=longpass;
      console.log("----------  longueur passation -------");
      console.log(this.longueurpassation);
    })


    this.storage.get('idsession').then((sesid) => {
      this.idsession=sesid;
      console.log(  this.idsession);
    })

    this.lg=translate.currentLang;
  
    switch(this.lg) { 
      case this.lg="fr": { 
        this.parameter2=2;
         break; 
      } 
      case this.lg="de": { 
        console.log("allemand break")
        this.parameter2=3;
         break; 
      } 
      case this.lg="en": { 
        console.log("english break")
        this.parameter2=1;
        break; 
     } 
    } 

}
  
ionViewWillEnter(){ 
  this.redditService.getTest(this.parameter1).subscribe(data=>{
    console.log(data);
    this.data=data.items; 
    this.test_final=data.items[0].test_final_fr;
  })
}



saveSession() {
  console.log("Longueur passation");
  console.log(this.longueurpassation);
  console.log("Session  NUMERO ACTUEL !!!!!!!!!!!");
  console.log( this.position);
 if (this.longueurpassation >=  this.position) {
  this.position= this.position+1;
  var data = JSON.stringify({ 
      idsession:this.idsession,
      sessionorder: this.position,
      finished:0,
      }); 
      this.redditService.updatesession(data)  
      .toPromise()
      .then((response) =>
      {
  if(response.status == 'success') {
  console.log("session enregistré - test suivant ");

this.redditService.getFkpass(  this.numpassation).subscribe(data=>{
  this.test=data.listing;
  this.lenghttest=data.length;
  this.storage.set('longueurpassation', this.lenghttest);
 this.idtest=this.test[this.position].idtest;

 console.log("id nouveau test");
    this.navCtrl.push(StarttestPage, {
      param1: this.idtest,
      param2: this.lenghttest,
      param6:this.position,
    });
});



  

}})
}else{
  console.log("FIN DE PASSATION BRAVO ");
  var data = JSON.stringify({ 
      idsession:this.idsession,
      sessionorder:this.sessionorder,
      finished:1,
      }); 
      this.redditService.updatesession(data)  
      .toPromise()
      .then((response) =>
      {
  if(response.status == 'success') {



    this.navCtrl.push(HomePage);
}})

}}


logOut() {
  this.authService.logout()
  this.navCtrl.push(Login);
}
}