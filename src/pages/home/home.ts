import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Login } from '../login/login';
import { AuthService } from '../../providers/AuthService';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController, public authService: AuthService) {}

  Logout(){    
   this.navCtrl.push(Login);
  
}


dologout() {
  this.authService.login()
  this.navCtrl.push(Login);
}
}
