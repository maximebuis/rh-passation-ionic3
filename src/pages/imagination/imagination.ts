import 'rxjs/add/operator/map';
import { Component} from '@angular/core';
import { RedditService } from "../../providers/reddit-service";
import {Storage} from '@ionic/storage';
import {NavController,  NavParams, LoadingController,reorderArray } from 'ionic-angular';
import { DragulaService} from 'ng2-dragula';
import { EndtestPage } from '../endtest /endtest';
import { AuthService } from '../../providers/AuthService';
import { Login } from '../login/login';
import { FormGroup, FormBuilder, AbstractControl } from '@angular/forms';
import { LogiquePage } from '../logique/logique';


@Component({
  providers: [DragulaService],
  selector: 'page-imagination',
  templateUrl: 'imagination.html',
})
export class ImaginationPage {
  form: FormGroup;
  parameter1: any;
  questions: any;
  data: any;
  squestion: any;
  questionid: any;
  editing: boolean = true;
  parameter2: number;
  lenghtquestions: any;
  question: any;
  q: any;
  questionlist: any;
  question_label: any;
  q_prop1: any;
  q_prop2: any;
  q_prop3: any;
  q_prop4: any;
  q_prop5: any;
  testlenght: any;
  idtest: any;
  dataq  = [];
  nbquestionbytest: any;
  testcategory: any;
  testduree: any;
  test_nav: any;
  q_h: any;
  q_f: any;
  position: any;
  propositions: any;
  reponses: number[];
  indextest:number=0;
  indexdisplay:number=1;
  posts2 = [];
  checked: boolean;
  checked1: boolean;
  checked2: boolean;
  checked3: boolean;
  checked4: boolean;
  checked5: boolean;
  checkedIdx=-1;
  options = [];
  reponse = [];
  reponsesave= [];
  reponsebyid: number;
  datasave: number;
  timeInSeconds: any;
  time: any;
  hasStarted: boolean;
  runTimer: boolean;
  remainingTime: any;
  hasFinished: boolean;
  displayTime: string;
  q_img1: any;
  q_img2: any;
  q_img3: any;
  q_img5: any;
  q_img4: any;
  urlimage="https://www.selectionetconseils.ch/examen/image/";
  question_num: any;
  reponseimg: any;

  idresponse: any;
  responsezero: any;
  responsestring: string="";
  datareponse=[];
  response1: any;
  constructor( fb: FormBuilder,
    public loadingController:LoadingController,public authService: AuthService,  private dragulaService: DragulaService,  public loadingCtrl: LoadingController,
    public navCtrl: NavController, public storage: Storage, public redditService:RedditService, 
    public navParams: NavParams) {

    this.idtest = navParams.get('param1');
    this.testlenght = navParams.get('param2');
    this.testcategory= navParams.get('param4');
    this.timeInSeconds= navParams.get('param5');
    this.test_nav= navParams.get('param6');
    this.position= navParams.get('param7');

    this.form = fb.group({
      cbox: [false, LogiquePage.mustBeTruthy]
    });


    this.storage.get('idresponse').then((idresponse) => {
      this.idresponse=idresponse;
    })
    

  }
  ionViewWillEnter(){ 
    if (this.timeInSeconds>0){this.startTimer();}
    this.indexdisplay=1;

    //Données i=0
    this.storage.get("questions").then( users => {
      this.dataq = JSON.parse(users);
      console.log(this.dataq);
      this.nbquestionbytest=this.dataq.length;
      this.question_label=this.dataq[0].question_label;
      this.q_prop1=this.dataq[0].q_prop1;
      this.q_prop2=this.dataq[0].q_prop2;
      this.q_prop3=this.dataq[0].q_prop3;
      this.q_prop4=this.dataq[0].q_prop4;
      this.q_prop5=this.dataq[0].q_prop5;
      this.q_img1=this.urlimage +this.dataq[0].q_img1;
      this.q_img2=this.urlimage + this.dataq[0].q_img2;
      this.q_img3=this.urlimage + this.dataq[0].q_img3;
      this.q_img4=this.urlimage +this.dataq[0].q_img4;
      this.q_img5=this.urlimage +this.dataq[0].q_img5;
      this.q_h=this.dataq[0].q_h;
      this.q_f=this.dataq[0].q_f;
      this.question_num=this.dataq[0].question_num;
      this.options= [this.q_img1];
      this.checkedIdx=-1;
      this.reponseimg="";
      //Array reponse
      for(let i=0;i<this.nbquestionbytest;i++){
        this.reponsesave[i]="";
      }
   console.log(this.reponsesave);
  })
  }

next(){

  this.reponsesave[this.indextest]=this.reponseimg;
  // new question
  this.indextest=this.indextest+1;

  // toutes les réponses
  console.log("réponse");
  console.log(this.reponsesave);
this.reponseimg=this.reponsesave[this.indextest];
  if (  this.indextest < this.nbquestionbytest ){
    this.indexdisplay=this.indexdisplay+1;
    this.retrieveQuestions();
  }else{
    this.pauseTimer();
  ////// SAVE DATA
  console.log(this.testlenght);
  for(let i=0;i<this.testlenght;i++){


    this.response1=this.reponsesave[i];
    console.log( this.response1);


    this.responsestring=this.responsestring+this.response1+";";
 
 console.log(this.responsestring);
  }
  /////


  var data = JSON.stringify({ 
    name: "InterpretationA", 
    idresponse:this.idresponse,
    resp: this.responsestring, 
    });

console.log(data);

  this.redditService.addresponses(data)  
  .toPromise()
  .then((response) =>
  {console.log(response);
  })


    this.navCtrl.push(EndtestPage, {
      param1: this.idtest,
      param2: this.position,
    });
  }
}

prev(){

  if (  this.indextest > 0 ){
    this.indextest=this.indextest-1;
    this.indexdisplay=this.indexdisplay-1;
    this.reponseimg=this.reponsesave[this.indextest];
    this.reviewQuestions();
  }else{
 
  }
}

retrieveQuestions() {
  this.storage.get("questions").then( users => {
      this.dataq = JSON.parse(users);
      this.question_label=this.dataq[this.indextest].question_label;
      this.propositions=this.dataq[this.indextest];
      this.q_prop1=this.dataq[this.indextest].q_prop1;
      this.q_prop2=this.dataq[this.indextest].q_prop2;
      this.q_prop3=this.dataq[this.indextest].q_prop3;
      this.q_prop4=this.dataq[this.indextest].q_prop4;
      this.q_prop5=this.dataq[this.indextest].q_prop5;

      this.q_img1=this.urlimage +this.dataq[this.indextest].q_img1;
      this.q_img2=this.urlimage + this.dataq[this.indextest].q_img2;
      this.q_img3=this.urlimage + this.dataq[this.indextest].q_img3;
      this.q_img4=this.urlimage +this.dataq[this.indextest].q_img4;
      this.q_img5=this.urlimage +this.dataq[this.indextest].q_img5;
      this.q_h=this.dataq[this.indextest].q_h;
      this.q_f=this.dataq[this.indextest].q_f;
      this.reponseimg=this.reponsesave[this.indextest];
      this.options= [this.q_img1];
      this.question_num=this.dataq[this.indextest].question_num;
  })
}


reviewQuestions() {
  this.storage.get("questions").then( users => {
      this.dataq = JSON.parse(users);
      this.question_label=this.dataq[this.indextest].question_label;
      this.propositions=this.dataq[this.indextest];
      this.q_prop1=this.dataq[this.indextest].q_prop1;
      this.q_prop2=this.dataq[this.indextest].q_prop2;
      this.q_prop3=this.dataq[this.indextest].q_prop3;
      this.q_prop4=this.dataq[this.indextest].q_prop4;
      this.q_prop5=this.dataq[this.indextest].q_prop5;
      this.q_img1=this.urlimage + this.dataq[this.indextest].q_img1;
      this.q_img2=this.urlimage + this.dataq[this.indextest].q_img2;
      this.q_img3=this.urlimage + this.dataq[this.indextest].q_img3;
      this.q_img4=this.urlimage +this.dataq[this.indextest].q_img4;
      this.q_img5=this.urlimage +this.dataq[this.indextest].q_img5;
      this.q_h=this.dataq[this.indextest].q_h;
      this.q_f=this.dataq[this.indextest].q_f;
      this.reponseimg=this.reponsesave[this.indextest];
      this.options= [this.q_img1];
      this.question_num=this.dataq[this.indextest].question_num;
  })
}

static mustBeTruthy(c: AbstractControl): { [key: string]: boolean } {
  let rv: { [key: string]: boolean } = {};
  if (!c.value) {
    rv['notChecked'] = true;
  }
  return rv;
}
reorderData(indexes: any) {
  this.posts2 = reorderArray(this.posts2, indexes);
  console.log(this.posts2);
  console.log(indexes);
//this.dataq[this.indextest] = reorderArray(this.dataq[this.indextest], indexes);
 // console.log(this.dataq[this.indextest]);
// console.log(this.dataq[this.indextest]);
}


logOut() {
  this.authService.logout()
  this.navCtrl.push(Login);
}

ngOnInit() {
  this.initTimer();
}

initTimer() {
  this.time = this.timeInSeconds;
  this.runTimer = false;
  this.hasStarted = false;
  this.hasFinished = false;
  this.remainingTime = this.timeInSeconds;
  
  this.displayTime = this.getSecondsAsDigitalClock(this.remainingTime);
}

startTimer() {
   this.runTimer = true;
  this.hasStarted = true;
  this.timerTick();
}

pauseTimer() {
  this.runTimer = false;
}

resumeTimer() {
  this.startTimer();
}

timerTick() {
  setTimeout(() => {

    if (!this.runTimer) { return; }
    this.remainingTime--;
    this.displayTime = this.getSecondsAsDigitalClock(this.remainingTime);
    if (this.remainingTime > 0) {
      this.timerTick();
    }
    else {
      this.hasFinished = true;
      this.navCtrl.push(EndtestPage, {
        param1: this.idtest,
        param2: this.position,
      });
    }
  }, 1000);
}

getSecondsAsDigitalClock(inputSeconds: number) {
  var sec_num = parseInt(inputSeconds.toString(), 10); // don't forget the second param
  var hours = Math.floor(sec_num / 3600);
  var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
  var seconds = sec_num - (hours * 3600) - (minutes * 60);
  var hoursString = '';
  var minutesString = '';
  var secondsString = '';
  hoursString = (hours < 10) ? "0" + hours : hours.toString();
  minutesString = (minutes < 10) ? "0" + minutes : minutes.toString();
  secondsString = (seconds < 10) ? "0" + seconds : seconds.toString();
  return hoursString + ':' + minutesString + ':' + secondsString;
}
}