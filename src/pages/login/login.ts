import { Component } from '@angular/core';
import { NavController, NavParams, MenuController,  AlertController } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import {Storage} from '@ionic/storage';
import {RedditService} from "../../providers/reddit-service";
import { LoadingController } from 'ionic-angular';
import { Forgotpassword} from '../forgotpassword/forgotpassword';
import { Md5 } from 'ts-md5';
import { AuthService } from '../../providers/AuthService';
import {FormBuilder, FormGroup, Validators, AbstractControl, FormControl} from '@angular/forms';
import { StarttestPage } from '../starttest/starttest';
import { SessionsPage } from '../sessions/sessions';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class Login {
validations_form: FormGroup;
uid: string;
lastname: string;
firstname: string;
status: string;
response: any;
user: string;
data: any;
password2: any;
  loginData: any;

  email: string = "";
  password: string = "";
  diplome: any;
  genre: any;
  cp: any;
  city: any;
  address: any;
  parameter1: any;
  certificat: any;
  birthday: any;
  numpassation: any;
  test: any;
  lenghttest: any;
  idtest: any;
  emailr: any;
  passwordr: any;
  lg: any;

constructor( private translate: TranslateService,public loadingController:LoadingController,public authService: AuthService,public http: Http,public menu: MenuController,
   private alertCtrl: AlertController,public navCtrl: NavController, public navParams: NavParams, public redditService:RedditService,     
   public formbuilder:FormBuilder, public formBuilder: FormBuilder, public storage: Storage) {
 
    this.storage.get('numpassation').then((numpass) => {
      this.numpassation=numpass;
    })
   this.lg=translate.currentLang;
  }
  ionViewWillEnter () { 
  this.menu.enable (false); 

    this.validations_form = this.formBuilder.group({
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
    });
  this.storage.get('email').then((email) => {
    this.email=email;
  })
  this.storage.get('password').then((password) => {
    this.password=password;
  })
  }

validation_messages = {
  'email': [
    { type: 'required', message: 'Email est requis.' },
    { type: 'pattern', message: 'Entrez un email valide' }
  ],
 
};


doLogin(){
  this.Login(this.email, this.password);
}

 Login(email,password){

  this.password2=Md5.hashStr(this.password);
  var data = JSON.stringify({ 
    email:this.email,
    password: this.password2,
  });
  this.storage.set('email',this.email);
  this.storage.set('password',this.password);
  this.redditService.login(data)  
  .toPromise()
  .then((response) =>
  {
    console.log(response);
    if(response.status == 'success') {
    let loading = this.loadingController.create({content : "Chargement..."});
    loading.present();
    setTimeout(() => {
          this.storage.set('customer_id', response.data[0].customer_id);
          this.storage.set('firstname', response.data[0].firstname);
          this.storage.set('lastname', response.data[0].lastname);
          this.storage.set('genre', response.data[0].genre);
          this.storage.set('birthday', response.data[0].birthday);
          console.log("------NUM PASSATION-------");
          console.log(this.numpassation);
          this.authService.login();
          this.navCtrl.push(SessionsPage);
          loading.dismissAll();
      }, 3000);
      } else if (response.status == 'error') {
        let alert = this.alertCtrl.create({
          title: 'Erreur',
          subTitle: 'Identifiant ou mot de passe incorrect .',
          buttons: ['Fermer']
        });
        alert.present();
      }
    
    })
  };


  doSave(){
    var data = JSON.stringify({ 
    email:this.emailr,
  });
  console.log(data);
  this.redditService.userexist(data)  
  .toPromise()
  .then((response) =>
  {
  console.log(response);
  if (response.nbrow >=1){
  
    let alert = this.alertCtrl.create({
      title: 'Attention !',
      subTitle: 'L utilisateur existe dejà, vous devez choisir une autre adresse email.',
      buttons: ['Ok']
    });
    alert.present();
  }else {
    this.adduser()
  }
  })
  .catch((error) =>
  { console.log(error)});
  }
      

  adduser(){
    this.password2=Md5.hashStr(this.passwordr);
var data = JSON.stringify({ 
userid:this.parameter1,
lastname: this.lastname,
firstname: this.firstname,
email: this.emailr,
address: this.address,
city: this.city,
cp: this.cp,
genre:this.genre, 
diplome:this.diplome,
time:0,
certificat:this.certificat,
birthday:this.birthday, 
password:this.password2,
});


console.log(data);

this.redditService.register(data)  
.toPromise()
.then((response) =>
{if(response.status == 'success') {

  this.Login(this.emailr, this.passwordr);
}
})


}

doForgotpassword() {
  this.navCtrl.push( Forgotpassword );
}
changeLanguage(){
  this.translate.use(this.lg);
 }
}