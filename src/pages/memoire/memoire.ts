import 'rxjs/add/operator/map';
import { Component} from '@angular/core';
import { RedditService } from "../../providers/reddit-service";
import {Storage} from '@ionic/storage';
import {NavController,  NavParams, LoadingController,reorderArray } from 'ionic-angular';
import { DragulaService} from 'ng2-dragula';
import { EndtestPage } from '../endtest /endtest';
import { AuthService } from '../../providers/AuthService';
import { Login } from '../login/login';
import { FormGroup, FormBuilder, AbstractControl } from '@angular/forms';


@Component({
  providers: [DragulaService],
  selector: 'page-memoire',
  templateUrl: 'memoire.html',
})
export class MemoirePage {
  form: FormGroup;
  parameter1: any;
  questions: any;
  data: any;
  squestion: any;
  questionid: any;
  editing: boolean = true;
  parameter2: number;
  lenghtquestions: any;
  question: any;
  q: any;
  questionlist: any;
  question_label: any;
  q_prop1: any;
  q_prop2: any;
  q_prop3: any;
  q_prop4: any;
  q_prop5: any;
  testlenght: any;
  idtest: any;
  dataq  = [];
  nbquestionbytest: any;
  testcategory: any;
  testduree: any;
  test_nav: any;
  q_h: any;
  q_f: any;
  position: any;
  propositions: any;
  reponses: number[];
  indextest:number=0;
  indexdisplay:number=1;
  posts2 = [];
  checked: boolean;
  checked1: boolean;
  checked2: boolean;
  checked3: boolean;
  checked4: boolean;
  checked5: boolean;
  checkedIdx=-1;
  options = [];
  reponse = [];
  reponsesave= [];
  reponsesave2= [];
  reponsesave3= [];
  reponsebyid: number;
  datasave: number;
  timeInSeconds: any;
  time: any;
  hasStarted: boolean;
  runTimer: boolean;
  remainingTime: any;
  hasFinished: boolean;
  displayTime: any;
  q_img1: any;
  q_img2: any;
  q_img3: any;
  q_img5: any;
  q_img4: any;
  hide:number=3;
  idresponse: any;
  responsezero: any;
  responsestring: string="";
  response1: any="";
  response2: any="";
  response3: any="";
  response4: any="";
  response5: any="";
  urlimage="https://api.selectionetconseils.ch/uploads/image/";
  question_num: any;
  options2: any[];
  progressPercent:any=0;
  progressTimer: number;
  testnumber:number=0;
  responsedeux: any;
  responseun: any;
  responsdeux: any;
  responsetrois: any;
  responsestring2: string;
  responsestring3: string;
  constructor( fb: FormBuilder,
    public loadingController:LoadingController,public authService: AuthService,  private dragulaService: DragulaService,  public loadingCtrl: LoadingController,
    public navCtrl: NavController, public storage: Storage, public redditService:RedditService, 
    public navParams: NavParams) {

    this.idtest = navParams.get('param1');
    this.testlenght = navParams.get('param2');
    this.testcategory= navParams.get('param4');
    this.timeInSeconds= navParams.get('param5');
    this.test_nav= navParams.get('param6');
    this.position= navParams.get('param7');


    this.storage.get('idresponse').then((idresponse) => {
      this.idresponse=idresponse;
    })

    this.form = fb.group({
      cbox: [false, MemoirePage.mustBeTruthy]
    });

  }
  ionViewWillEnter(){ 
    this.hide=1;
    if (this.timeInSeconds>0){this.startTimer();}
    this.indexdisplay=1;
   
    //Données i=0
    this.storage.get("questions").then( users => {
      this.dataq = JSON.parse(users);
      this.nbquestionbytest=this.dataq.length;
      //Array reponse
      for(let i=0;i<this.nbquestionbytest;i++){
        this.reponsesave[i]=-1;
      }    
      
      for(let i=0;i<this.nbquestionbytest;i++){
        this.reponsesave2[i]=-1;
      }  

      for(let i=0;i<this.nbquestionbytest;i++){
        this.reponsesave3[i]=-1;
      }  
  })
}


displayimages(){
      this.hide=0;
      console.log(this.hide);
      this.indexdisplay=0;
      this.indextest=-1;
      this.next();
}



displayimages2(){
  this.hide=0;
  console.log(this.hide);
  this.indexdisplay=0;
  this.indextest=-1;
  this.next3();
}

displayimage3(){
  this.hide=0;
  console.log(this.hide);
  this.indexdisplay=0;
  this.indextest=-1;
  this.next3();
}


displayintro3(){
  this.hide=7;
  console.log(this.hide);
}

next(){

  this.reponsesave[this.indextest]=this.checkedIdx;
  // new question
  this.indextest=this.indextest+1;
  if (  this.indextest < this.nbquestionbytest ){
    this.indexdisplay=this.indexdisplay+1;
    this.retrieveQuestions();

  }else{
    this.hide=2;
    this.indextest=-1;
    this.indexdisplay=0;

    this.next2()
  }
}


next2(){

  console.log(this.hide);
  this.testnumber= this.testnumber+1;
   console.log(this.testnumber);
  this.reponsesave2[this.indextest]=this.checkedIdx;
  this.indextest=this.indextest+1;
  if (  this.indextest < this.nbquestionbytest ){
    this.indexdisplay=this.indexdisplay+1;
    this.retrieveQuestions2();
  }else{
    this.hide=4;
    
for(let i=0;i<this.testlenght;i++){
  this.responseun=this.reponsesave2[i]; 

  this.responsestring= this.responsestring +this.responseun;
  console.log(this.responsestring);
}

}}

next3(){

  console.log(this.hide);
  this.testnumber= this.testnumber+1;
   console.log(this.testnumber);
  this.reponsesave2[this.indextest]=this.checkedIdx;
  this.indextest=this.indextest+1;
  if (  this.indextest < this.nbquestionbytest ){
    this.indexdisplay=this.indexdisplay+1;
    this.retrieveQuestions2();
  }else{
    this.hide=5;

for(let i=0;i<this.testlenght;i++){
  this.responsdeux=this.reponsesave2[i]; 
  this.responsestring2= this.responsestring +this.responsdeux+",";

  console.log(this.responsestring);
}}}

next5(){
  this.testnumber= this.testnumber+1;
   console.log(this.testnumber);
  this.reponsesave3[this.indextest]=this.checkedIdx;
  this.indextest=this.indextest+1;
  if (  this.indextest < this.nbquestionbytest ){
    this.indexdisplay=this.indexdisplay+1;
    this.retrieveQuestions2();
  }else{
    this.hide=6;
  ////// SAVE DATA

for(let i=0;i<this.testlenght;i++){
  this.responsetrois=this.reponsesave3[i]; 
  this.responsestring3= this.responsestring +this.responsetrois+",";
}

var data = JSON.stringify({ 
  name: "Memoire", 
  idresponse:this.idresponse,
  resp: this.responsestring+","+this.responsestring2+","+this.responsestring3+",", 
  });
console.log(data);


this.redditService.addresponses(data)  
.toPromise()
.then((response) =>
{console.log(response);
})

}

}


finish(){


  this.navCtrl.push(EndtestPage, {
    param1: this.idtest,
    param2: this.position,
  });
}


prev(){

  if (  this.indextest > 0 ){
    this.indextest=this.indextest-1;
    this.indexdisplay=this.indexdisplay-1;
    this.reviewQuestions();
  }else{
 
  }
}

retrieveQuestions() {
  this.storage.get("questions").then( users => {
      this.dataq = JSON.parse(users);
      this.question_label=this.dataq[this.indextest].question_label;
      this.propositions=this.dataq[this.indextest];
      this.q_prop1=this.dataq[this.indextest].q_prop1;
      this.q_prop2=this.dataq[this.indextest].q_prop2;
      this.q_prop3=this.dataq[this.indextest].q_prop3;
      this.q_prop4=this.dataq[this.indextest].q_prop4;
      this.q_prop5=this.dataq[this.indextest].q_prop5;

      this.q_img1=this.urlimage +this.dataq[this.indextest].q_img1;
      this.q_h=this.dataq[this.indextest].q_h;
      this.q_f=this.dataq[this.indextest].q_f;
      this.checkedIdx=this.reponsesave[this.indextest];
      this.options= [this.q_img1];
      this.options2= [this.q_prop1,this.q_prop2,this.q_prop3,this.q_prop4,this.q_prop5];
      this.question_num=this.dataq[this.indextest].question_num;
      setTimeout(() => {

        this.next();
      }, 2500);

  })
}

retrieveQuestions2() {
  this.storage.get("questions").then( users => {
      this.dataq = JSON.parse(users);
      this.question_label=this.dataq[this.indextest].question_label;
      this.propositions=this.dataq[this.indextest];
      this.q_prop1=this.dataq[this.indextest].q_prop1;
      this.q_prop2=this.dataq[this.indextest].q_prop2;
      this.q_prop3=this.dataq[this.indextest].q_prop3;
      this.q_prop4=this.dataq[this.indextest].q_prop4;
      this.q_prop5=this.dataq[this.indextest].q_prop5;

      this.q_img1=this.urlimage +this.dataq[this.indextest].q_img1;
      this.q_h=this.dataq[this.indextest].q_h;
      this.q_f=this.dataq[this.indextest].q_f;
      this.checkedIdx=this.reponsesave[this.indextest];
      this.options= [this.q_img1];
      this.options2= [this.q_prop1,this.q_prop2,this.q_prop3,this.q_prop4,this.q_prop5];
      this.question_num=this.dataq[this.indextest].question_num;
      setTimeout(() => {

        this.next2();
      }, 2500);


  })
}
reviewQuestions() {
  this.storage.get("questions").then( users => {
      this.dataq = JSON.parse(users);
      this.question_label=this.dataq[this.indextest].question_label;
      this.propositions=this.dataq[this.indextest];
      this.q_prop1=this.dataq[this.indextest].q_prop1;
      this.q_prop2=this.dataq[this.indextest].q_prop2;
      this.q_prop3=this.dataq[this.indextest].q_prop3;
      this.q_prop4=this.dataq[this.indextest].q_prop4;
      this.q_prop5=this.dataq[this.indextest].q_prop5;
      this.q_img1=this.urlimage + this.dataq[this.indextest].q_img1;
      this.q_img2=this.urlimage + this.dataq[this.indextest].q_img2;
      this.q_img3=this.urlimage + this.dataq[this.indextest].q_img3;
      this.q_img4=this.urlimage +this.dataq[this.indextest].q_img4;
      this.q_img5=this.urlimage +this.dataq[this.indextest].q_img5;
      this.q_h=this.dataq[this.indextest].q_h;
      this.q_f=this.dataq[this.indextest].q_f;
      this.checkedIdx=this.reponsesave[this.indextest];
      this.options= [this.q_img1];
      this.options2= [this.q_prop1,this.q_prop2,this.q_prop3,this.q_prop4,this.q_prop5];
      this.question_num=this.dataq[this.indextest].question_num;
  })
}

static mustBeTruthy(c: AbstractControl): { [key: string]: boolean } {
  let rv: { [key: string]: boolean } = {};
  if (!c.value) {
    rv['notChecked'] = true;
  }
  return rv;
}



logOut() {
  this.authService.logout()
  this.navCtrl.push(Login);
}

ngOnInit() {
  this.initTimer();
}

initTimer() {
  this.time = this.timeInSeconds;
  this.runTimer = false;
  this.hasStarted = false;
  this.hasFinished = false;
  this.remainingTime = this.timeInSeconds;
  this.displayTime = this.getSecondsAsDigitalClock(this.remainingTime);
}

startTimer() {
   this.runTimer = true;
  this.hasStarted = true;
  this.timerTick();
}

pauseTimer() {
  this.runTimer = false;
}

resumeTimer() {
  this.startTimer();
}

timerTick() {
  setTimeout(() => {

    if (!this.runTimer) { return; }
    this.remainingTime--;

    
    this.progressTimer=(this.remainingTime*  100) /  this.time;
    this.progressPercent=(100- this.progressTimer).toFixed(0);

    this.displayTime = this.getSecondsAsDigitalClock(this.remainingTime);
    if (this.remainingTime > 0) {
      this.timerTick();
    }
    else {
      this.hasFinished = true;
      this.navCtrl.push(EndtestPage, {
        param1: this.idtest,
        param2: this.position,
      });
    }
  }, 1000);
}

getSecondsAsDigitalClock(inputSeconds: number) {
  var sec_num = parseInt(inputSeconds.toString(), 10); // don't forget the second param
  var hours = Math.floor(sec_num / 3600);
  var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
  var seconds = sec_num - (hours * 3600) - (minutes * 60);
  var hoursString = '';
  var minutesString = '';
  var secondsString = '';
  hoursString = (hours < 10) ? "0" + hours : hours.toString();
  minutesString = (minutes < 10) ? "0" + minutes : minutes.toString();
  secondsString = (seconds < 10) ? "0" + seconds : seconds.toString();
  return hoursString + ':' + minutesString + ':' + secondsString;
}
}