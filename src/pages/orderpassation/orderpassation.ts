import 'rxjs/add/operator/map';
import { Component, OnInit} from '@angular/core';
import { RedditService } from "../../providers/reddit-service";
import {NavController,  NavParams,  LoadingController, AlertController, reorderArray } from 'ionic-angular';
import { DragulaService} from 'ng2-dragula';
import { Subscription } from 'rxjs/Subscription';

@Component({
providers: [DragulaService],
selector: 'page-orderpassation',
templateUrl: 'orderpassation.html',
})
export class  OrderpassationPage implements OnInit {
    fktest: any;
    items = [];
    posts2 = [];
    parameter1: any;
    data: any;
    page: number;
    posts: any;
    pages: any;
    currentpage: any;
    testid: any;
    idtest: any;
    idorder: any;
    idquestion: any;
    editing: boolean = true;
    songs: any[];
    qnum: any;
    questions2: any[];
    category: any;
    word: any="";
    wordid: any="";
constructor(public navCtrl: NavController, public navParams: NavParams, public loadingController:LoadingController, private dragulaService: DragulaService, private alertCtrl: AlertController, public redditService:RedditService) {
        this.parameter1 = navParams.get('param1');
        this.page=1;
        this.redditService.getPassation(this.parameter1).subscribe(data=>{
            this.data=data.items;
          })
     
          this.getFk();
}
ngOnInit() { }
ionViewWillEnter(){
    this.redditService.getAllTests(this.page).subscribe(data=>{
        this.posts=data.listing;
        this.items=data.items;
        this.pages=data.page;
        this.currentpage=data.currentpage;
        });
}
reorderData(indexes: any) {
    this.posts2 = reorderArray(this.posts2, indexes);
    console.log(this.posts2);
}
 

getFk(){
this.redditService.getFkpass( this.parameter1).subscribe(data=>{
    this.posts2=data.listing;
});}
next() {


}
}


