import 'rxjs/add/operator/map';
import { Component, OnInit} from '@angular/core';
import { RedditService } from "../../providers/reddit-service";
import {NavController,  NavParams,  LoadingController, AlertController, reorderArray } from 'ionic-angular';
import { DragulaService} from 'ng2-dragula';
import { Subscription } from 'rxjs/Subscription';

@Component({
providers: [DragulaService],
selector: 'page-ordertest',
templateUrl: 'ordertest.html',
})
export class  OrdertestPage implements OnInit {
    fktest: any;
    items = [];
    posts2 = [];
    parameter1: any;
    data: any;
    page: number;
    posts: any;
    pages: any;
    currentpage: any;
    testid: any;
    idtest: any;
    idorder: any;
    idquestion: any;
    editing: boolean = true;
    songs: any[];
    qnum: any;
    questions2: any[];
    category: any;
    word: any="";
    wordid: any="";
constructor(public navCtrl: NavController, public navParams: NavParams, public loadingController:LoadingController, private dragulaService: DragulaService, private alertCtrl: AlertController, public redditService:RedditService) {
        this.parameter1 = navParams.get('param1');
        this.category = navParams.get('param2');
        this.page=1;
        this.redditService.getTest(this.parameter1).subscribe(data=>{
            this.data=data.items;
          })
     
          this.getFk();
}
ngOnInit() { }
ionViewWillEnter(){
    this.redditService.Questioncategory(this.page, this.category).subscribe(data=>{
        this.posts=data.listing;
        this.items=data.items;
        this.pages=data.page;
        this.currentpage=data.currentpage;
        });
}
reorderData(indexes: any) {
    this.posts2 = reorderArray(this.posts2, indexes);
    console.log(this.posts2);
}
 
save() {
    let loading = this.loadingController.create({content : "Enregistrement..."});
    loading.present();
    var data2 = JSON.stringify({ 
        idfktest:this.parameter1,
        }); 
        this.redditService.deletefkidtest(data2)  
        .toPromise()
        .then((response) =>
        {
    if(response.status == 'success') {
    for (var i = 0; i < this.posts2.length; i++) {
    this.qnum=this.posts2[i].question_num
    this.addfk(this.qnum, i);
  }
  
}
setTimeout(() => {  
loading.dismissAll();
this.getFk();
}, 3000);
}) 
}



addfk(qnum, orderid) {
var data = JSON.stringify({ 
idquestion:qnum,
idtest:this.parameter1,
idorder:orderid,
}); 
this.redditService.addfktest(data)  
.toPromise()
.then((response) =>
{if(response.status == 'success') {}
})
};

add(event, item) {
var data = JSON.stringify({ 
        idquestion:item.question_num,
        idtest:this.parameter1,
        idorder:this.idorder,
    }); 
this.redditService.addfktest(data)  
.toPromise()
.then((response) =>
{if(response.status == 'success') {
this.getFk();
}}) 
};


delete(event, item) {
var data = JSON.stringify({ 
idfktest:item.idfktest,});
this.redditService.deletefktest(data)  
.toPromise()
.then((response) =>
{if(response.status == 'success') {
this.getFk();
}}) 
};


getFk(){
this.redditService.getFktest( this.parameter1).subscribe(data=>{
    this.posts2=data.listing;
});}
onInput(selectedValue: any) {
    this.page=1;
         this.redditService.seachQuestioncategoryword(this.page,this.word,this.category).subscribe(data=>{
           this.posts=data.listing;
           this.items=data.items;
           this.pages=data.page;
           this.currentpage=data.currentpage;
           });
       }
 
       onCancel(selectedValue: any) {
      this.wordid=="";
       this.word=="";
   }
   onInputid(selectedValue: any) {
    this.page=1;
         this.redditService.seachQuestioncategoryid(this.page,this.wordid,this.category).subscribe(data=>{
           this.posts=data.listing;
           this.items=data.items;
           this.pages=data.page;
           this.currentpage=data.currentpage;
           });
       }
 
       onCancelid(selectedValue: any) {
       this.wordid=="";
       this.word=="";
   }

reset(){
this.wordid=="";
this.word=="";
this.navCtrl.push(OrdertestPage, {
    param1: this.parameter1 ,
    param2: this.category,
  });
}




next() {
    if  (this.page<this.pages){
         this.page = this.page +1 ;
         if  (this.word=="" && this.wordid==""){   
          this.redditService.Questioncategory(this.page, this.category).subscribe(data=>{
           this.posts=data.listing;
           this.items=data.items;
           this.pages=data.page;
           this.currentpage=data.currentpage;
           });
         }else if (this.word!=="" ) {
           this.redditService.seachQuestioncategoryword(this.page,this.word, this.category).subscribe(data=>{
             this.posts=data.listing;
             this.items=data.items;
             this.pages=data.page;
             this.currentpage=data.currentpage;
             });
         }else if (this.wordid!=="") {
           this.redditService.seachQuestioncategoryid(this.page,this.wordid,this.category).subscribe(data=>{
             this.posts=data.listing;
             this.items=data.items;
             this.pages=data.page;
             this.currentpage=data.currentpage;
            });
        }
       
     }}
    
    
      prev() {
    
        if  (this.page>1){
             this.page = this.page -1;
             if  (this.word=="" && this.wordid==""){    
              this.redditService.Questioncategory(this.page, this.category).subscribe(data=>{
               this.posts=data.listing;
               this.items=data.items;
               this.pages=data.page;
               this.currentpage=data.currentpage;
               });
             }else if (this.word!=="" ) {
               this.redditService.seachQuestioncategoryword(this.page,this.word,this.category).subscribe(data=>{
                 this.posts=data.listing;
                 this.items=data.items;
                 this.pages=data.page;
                 this.currentpage=data.currentpage;
                 });
             }else if (this.wordid!=="") {
               this.redditService.seachQuestioncategoryid(this.page,this.wordid,this.category).subscribe(data=>{
                 this.posts=data.listing;
                 this.items=data.items;
                 this.pages=data.page;
                 this.currentpage=data.currentpage;
                 });
             }
            
          }}
}




/*
this.posts2.forEach((data) => {
console.log(data.question_num);
this.qnum=data.question_num;
console.log(data.length);
    this.addfk(this.qnum, 4);
});*/