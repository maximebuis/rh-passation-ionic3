import { Component } from '@angular/core';
import { MenuController, NavController, NavParams, ViewController, PopoverController, AlertController  } from 'ionic-angular';
import {RedditService} from "../../providers/reddit-service";
import { LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { StarttestPage } from '../starttest/starttest';
import { AuthService } from '../../providers/AuthService';
import { Login } from '../login/login';
import {TranslateService} from "@ngx-translate/core";
@Component({
  selector: 'page-passations',
  templateUrl: 'passations.html'
})
export class PassationsPage {

pages: any;
items: any;
posts: any;
page:number;
category: string;
region: string;
currentpage: any;
word: any="";
wordid: any="";
  parameter1: any;
  test: any;
  lenghttest: number=0;
  idtest: any;
  pass: any;
urlupload="http://api.selectionetconseils.ch/uploads/";
  passw: any;
  code: any;
  language: any;
  lg: any="fr";
constructor(private translate: TranslateService,public navCtrl: NavController,public authService: AuthService, public popoverCtrl: PopoverController,private alertCtrl: AlertController, public menu: MenuController ,public loadingController:LoadingController,  public redditService:RedditService, public storage: Storage,  public navParams: NavParams, private viewCtrl: ViewController) {
this.page=1;
}


ionViewWillEnter(){
  
      this.page=1;
      this.redditService.getAllPassations(this.page).subscribe(data=>{
        console.log(data);
        this.pass=data.listing;
        });

   }
  doRefresh(refresher) {
    setTimeout(() => {
      this.page=1 ;
      this.redditService.getAllPassations(this.page).subscribe(data=>{
        this.posts=data.listing;
        console.log(this.posts);
        this.items=data.items;
        this.pages=data.page;
        this.currentpage=data.currentpage;
        });
      refresher.complete();
    }, 1100);
  }


    gotest(event, item,passw) {
    
    this.code=item.passation_mdp;
    if ( this.code == passw) {
      this.storage.set('numpassation', item.passation_num);
        this.navCtrl.push(Login);
    }
   
        

    


}

doPrompt(event, item) {

  this.translate.get(['code', 'start', 'cancel']).subscribe(text => {

  const alert = this.alertCtrl.create({
    title: text["code"],
    message: '',
    inputs: [
      {
        name: 'titre',
        placeholder: 'CODE',
        value: this.passw
      },
    ],
    buttons: [
      {
        text: text["cancel"],
        handler: (data: any) => {
        }
      },
      {
        text: text["start"],
        handler: (data: any) => {
          this.passw=data.titre;
          this.gotest(event, item,this.passw);
        }
      }
    ]
  });
  alert.present();
  });
  }

 changeLanguage(){
   this.translate.use(this.lg);
  }
}

