import 'rxjs/add/operator/map';
import { Component} from '@angular/core';
import { RedditService } from "../../providers/reddit-service";
import {Storage} from '@ionic/storage';
import {NavController,  NavParams, LoadingController,reorderArray } from 'ionic-angular';
import { DragulaService} from 'ng2-dragula';
import { EndtestPage } from '../endtest /endtest';
import { AuthService } from '../../providers/AuthService';
import { Login } from '../login/login';
import { FormGroup, FormBuilder, AbstractControl } from '@angular/forms';


@Component({
  providers: [DragulaService],
  selector: 'page-questionnaire',
  templateUrl: 'questionnaire.html',
})
export class QuestionnairePage {
  form: FormGroup;
  parameter1: any;
  questions: any;
  data: any;
  squestion: any;
  questionid: any;
  editing: boolean = true;
  parameter2: number;
  lenghtquestions: any;
  question: any;
  q: any;
  questionlist: any;
  question_label: any;
  q_prop1: any;
  q_prop2: any;
  q_prop3: any;
  q_prop4: any;
  q_prop5: any;
  testlenght: any;
  idtest: any;
  dataq  = [];
  nbquestionbytest: any;
  testcategory: any;
  testduree: any;
  test_nav: any;
  q_h: any;
  q_f: any;
  position: any;
  propositions: any;
  reponses: number[];
  indextest:number=0;
  indexdisplay:number=1;
  posts2 = [];
  checked: boolean;
  checked1: boolean;
  checked2: boolean;
  checked3: boolean;
  checked4: boolean;
  checked5: boolean;
  checkedIdx=-1;
  options = [];
  reponse = [];
  reponsesave= [];
  reponsebyid: number;
  datasave: number;
  timeInSeconds: any;
  time: any;
  hasStarted: boolean;
  runTimer: boolean;
  remainingTime: any;
  hasFinished: boolean;
  displayTime: string;
  question_num: any;

  idresponse: any;
  responsezero: any;
  responsestring: string="";
  response1: any="";
  response2: any="";
  response3: any="";
  response4: any="";
  response5: any="";
  progressTimer: number;
  progressPercent:any=0;
  constructor( fb: FormBuilder,
    public loadingController:LoadingController,public authService: AuthService,  private dragulaService: DragulaService,  public loadingCtrl: LoadingController,
    public navCtrl: NavController, public storage: Storage, public redditService:RedditService, 
    public navParams: NavParams) {

    this.idtest = navParams.get('param1');
    this.testlenght = navParams.get('param2');
    this.testcategory= navParams.get('param4');
    this.timeInSeconds= navParams.get('param5');
    this.test_nav= navParams.get('param6');
    this.position= navParams.get('param7');


    this.storage.get('idresponse').then((idresponse) => {
      this.idresponse=idresponse;
    })
    

    this.form = fb.group({
      cbox: [false, QuestionnairePage.mustBeTruthy]
    });

  }
  ionViewWillEnter(){ 
    if (this.timeInSeconds>0){this.startTimer();}
    this.indexdisplay=1;

    //Données i=0
    this.storage.get("questions").then( users => {
      this.dataq = JSON.parse(users);
      this.nbquestionbytest=this.dataq.length;
      this.question_label=this.dataq[0].question_label;
      this.q_prop1=this.dataq[0].q_prop1;
      this.q_prop2=this.dataq[0].q_prop2;
      this.q_prop3=this.dataq[0].q_prop3;
      this.q_prop4=this.dataq[0].q_prop4;
      this.q_prop5=this.dataq[0].q_prop5;
      this.q_h=this.dataq[0].q_h;
      this.q_f=this.dataq[0].q_f;
      this.options= [this.q_prop1,this.q_prop2,this.q_prop3,this.q_prop4,this.q_prop5];
      this.question_num=this.dataq[0].question_num;
      this.checkedIdx=-1;
      //Array reponse
      for(let i=0;i<this.nbquestionbytest;i++){
        this.reponsesave[i]=-1;
      }
   
  })
  }

next(){

  this.reponsesave[this.indextest]=this.checkedIdx;
  // new question
  this.indextest=this.indextest+1;

  // toutes les réponses
  console.log("réponse");
  console.log(this.reponsesave);

  if (  this.indextest < this.nbquestionbytest ){
    this.indexdisplay=this.indexdisplay+1;
    this.retrieveQuestions();
  }else{
    this.pauseTimer();
  ////// SAVE DATA

for(let i=0;i<this.testlenght;i++){

  this.responsezero=this.reponsesave[i]; 
  this.responsestring= this.responsestring +this.responsezero;
}


/////


var data = JSON.stringify({ 
  name: "questionnaire", 
  idresponse:this.idresponse,
  resp: this.responsestring, 
  });
console.log(data);


this.redditService.addresponses(data)  
.toPromise()
.then((response) =>
{console.log(response);
})


  this.navCtrl.push(EndtestPage, {
    param1: this.idtest,
    param2: this.position,
  });
}
}

prev(){

  if (  this.indextest > 0 ){
    this.indextest=this.indextest-1;
    this.indexdisplay=this.indexdisplay-1;
    this.reviewQuestions();
  }else{
 
  }
}

retrieveQuestions() {
  this.storage.get("questions").then( users => {
      this.dataq = JSON.parse(users);
      this.question_label=this.dataq[this.indextest].question_label;
      this.propositions=this.dataq[this.indextest];
      this.q_prop1=this.dataq[this.indextest].q_prop1;
      this.q_prop2=this.dataq[this.indextest].q_prop2;
      this.q_prop3=this.dataq[this.indextest].q_prop3;
      this.q_prop4=this.dataq[this.indextest].q_prop4;
      this.q_prop5=this.dataq[this.indextest].q_prop5;
      this.q_h=this.dataq[this.indextest].q_h;
      this.q_f=this.dataq[this.indextest].q_f;
      this.checkedIdx=this.reponsesave[this.indextest];
      this.question_num=this.dataq[this.indextest].question_num;
  })
}


reviewQuestions() {
  this.storage.get("questions").then( users => {
      this.dataq = JSON.parse(users);
      this.question_label=this.dataq[this.indextest].question_label;
      this.propositions=this.dataq[this.indextest];
      this.q_prop1=this.dataq[this.indextest].q_prop1;
      this.q_prop2=this.dataq[this.indextest].q_prop2;
      this.q_prop3=this.dataq[this.indextest].q_prop3;
      this.q_prop4=this.dataq[this.indextest].q_prop4;
      this.q_prop5=this.dataq[this.indextest].q_prop5;
      this.q_h=this.dataq[this.indextest].q_h;
      this.q_f=this.dataq[this.indextest].q_f;
      this.checkedIdx=this.reponsesave[this.indextest];
      this.question_num=this.dataq[this.indextest].question_num;
  
  })
}

static mustBeTruthy(c: AbstractControl): { [key: string]: boolean } {
  let rv: { [key: string]: boolean } = {};
  if (!c.value) {
    rv['notChecked'] = true;
  }
  return rv;
}
reorderData(indexes: any) {
  this.posts2 = reorderArray(this.posts2, indexes);
  console.log(this.posts2);
  console.log(indexes);
//this.dataq[this.indextest] = reorderArray(this.dataq[this.indextest], indexes);
 // console.log(this.dataq[this.indextest]);
// console.log(this.dataq[this.indextest]);
}


logOut() {
  this.authService.logout()
  this.navCtrl.push(Login);
}

ngOnInit() {
  this.initTimer();
}

initTimer() {
  this.time = this.timeInSeconds;
  this.runTimer = false;
  this.hasStarted = false;
  this.hasFinished = false;
  this.remainingTime = this.timeInSeconds;


  
  this.displayTime = this.getSecondsAsDigitalClock(this.remainingTime);
}

startTimer() {
   this.runTimer = true;
  this.hasStarted = true;
  this.timerTick();
}

pauseTimer() {
  this.runTimer = false;
}

resumeTimer() {
  this.startTimer();
}

timerTick() {
  setTimeout(() => {

    if (!this.runTimer) { return; }
    this.remainingTime--;
    this.displayTime = this.getSecondsAsDigitalClock(this.remainingTime);


    this.progressTimer=(this.remainingTime*  100) /  this.time;
    this.progressPercent=(100- this.progressTimer).toFixed(0);
    if (this.remainingTime > 0) {
      this.timerTick();
    }
    else {
      this.hasFinished = true;
      this.navCtrl.push(EndtestPage, {
        param1: this.idtest,
        param2: this.position,
      });
    }
  }, 1000);
}

getSecondsAsDigitalClock(inputSeconds: number) {
  var sec_num = parseInt(inputSeconds.toString(), 10); // don't forget the second param
  var hours = Math.floor(sec_num / 3600);
  var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
  var seconds = sec_num - (hours * 3600) - (minutes * 60);
  var hoursString = '';
  var minutesString = '';
  var secondsString = '';
  hoursString = (hours < 10) ? "0" + hours : hours.toString();
  minutesString = (minutes < 10) ? "0" + minutes : minutes.toString();
  secondsString = (seconds < 10) ? "0" + seconds : seconds.toString();
  return hoursString + ':' + minutesString + ':' + secondsString;
}
}