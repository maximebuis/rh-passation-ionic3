import 'rxjs/add/operator/map';
import { Component} from '@angular/core';
import { RedditService } from "../../providers/reddit-service";
import {Storage} from '@ionic/storage';
import {NavController,  NavParams, LoadingController,reorderArray } from 'ionic-angular';
import { DragulaService} from 'ng2-dragula';
import { EndtestPage } from '../endtest /endtest';
import { AuthService } from '../../providers/AuthService';
import { Login } from '../login/login';
import { FormGroup, FormBuilder, AbstractControl } from '@angular/forms';


@Component({
  providers: [DragulaService],
  selector: 'page-scientifique',
  templateUrl: 'scientifique.html',
})
export class ScientifiquePage {
  form: FormGroup;
  parameter1: any;
  questions: any;
  data: any;
  squestion: any;
  questionid: any;
  editing: boolean = true;
  parameter2: number;
  lenghtquestions: any;
  question: any;
  q: any;
  questionlist: any;
  question_label: any;
  q_prop1: any;
  q_prop2: any;
  q_prop3: any;
  q_prop4: any;
  q_prop5: any;
  testlenght: any;
  idtest: any;
  dataq  = [];
  nbquestionbytest: any;
  testcategory: any;
  testduree: any;
  test_nav: any;
  q_h: any;
  q_f: any;
  position: any;
  propositions: any;
  reponses: number[];
  indextest:number=0;
  indexdisplay:number=1;
  posts2 = [];
  nbchecked: number=0;
  checked: boolean;
  checked1: boolean;
  checked2: boolean;
  checked3: boolean;
  checked4: boolean;
  checked5: boolean;
  checkedIdx=-1;
  options = [];
  reponse = [];
  reponsesave= [];
  reponsebyid: number;
  datasave: number;
  timeInSeconds: any;
  time: any;
  hasStarted: boolean;
  runTimer: boolean;
  remainingTime: any;
  hasFinished: boolean;
  displayTime: string;
  question_num: any;
  myDataArray= [];
  checkedItems: any;
  datareponse=[];
  questiondata: { id: string; prop: any; isChecked: boolean; }[];
  datarep: any;
  reponsecheck: any;
  idresponse: any;
  responsezero: any;
  responsestring: string="";
  response1: any="";
  response2: any="";
  response3: any="";
  response4: any="";
  response5: any="";

  progressTimer: number;
  progressPercent:any=0;
  constructor( 
    public loadingController:LoadingController,public authService: AuthService,  private dragulaService: DragulaService,  public loadingCtrl: LoadingController,
    public navCtrl: NavController, public storage: Storage, public redditService:RedditService, 
    public navParams: NavParams) {
    this.idtest = navParams.get('param1');
    this.testlenght = navParams.get('param2');
    this.testcategory= navParams.get('param4');
    this.timeInSeconds= navParams.get('param5');
    this.test_nav= navParams.get('param6');
    this.position= navParams.get('param7');



    this.storage.get('idresponse').then((idresponse) => {
      this.idresponse=idresponse;
    })
    

    this.storage.get("questions").then( users => {
      this.dataq = JSON.parse(users);
      for(let i=0;i<this.dataq.length;i++){
        this.question_label=this.dataq[i].question_label;
        this.propositions=this.dataq[i];
        this.q_prop1=this.dataq[i].q_prop1;
        this.q_prop2=this.dataq[i].q_prop2;
        this.q_prop3=this.dataq[i].q_prop3;
        this.q_prop4=this.dataq[i].q_prop4;
        this.q_prop5=this.dataq[i].q_prop5;
        this.q_h=this.dataq[i].q_h;
        this.q_f=this.dataq[i].q_f;
        this.checkedIdx=this.reponsesave[i];
        this.question_num=this.dataq[i].question_num;      
        this.options= [{id: '1', prop: this.q_prop1,isChecked:false},{id: '2', prop: this.q_prop2,isChecked:false},
        {id: '3', prop: this.q_prop3,isChecked:false},{id: '4', prop: this.q_prop4,isChecked:false},{id: '5', prop: this.q_prop5,isChecked:false}]; 
        this.datareponse.push(this.options);
     
      }
  })
  }

  ionViewWillEnter(){ 
    if (this.timeInSeconds>0){this.startTimer();}
    this.indexdisplay=1;
    //Données i=0
    this.storage.get("questions").then( users => {
      this.dataq = JSON.parse(users);
      this.nbquestionbytest=this.dataq.length;
      this.question_label=this.dataq[0].question_label;
      this.q_h=this.dataq[0].q_h;
      this.q_f=this.dataq[0].q_f;
      this.question_num=this.dataq[0].question_num;
      this.datarep=this.datareponse[0];
  })
}



next(){
for(let i=0;i<this.datareponse[this.indextest];i++){
  this.datareponse.push(this.datareponse[this.indextest]);
console.log(this.datareponse[this.indextest])
}
console.log(  this.datareponse);

  /*this.datareponse[this.indextest] =  this.datarep.filter(value => {

    console.log(value.isChecked );
    return value.isChecked && !value.isChecked ;
  });
*/
  console.log("enregistrement des questions");
  console.log(this.datareponse);

  // new question
  this.indextest=this.indextest+1;

  if (  this.indextest < this.nbquestionbytest ){
    this.indexdisplay=this.indexdisplay+1;
    this.retrieveQuestions();
  }else{
    this.pauseTimer();

////// SAVE DATA

for(let i=0;i<this.testlenght;i++){
  if(this.datareponse[i][0].isChecked==true){
     this.response1=this.datareponse[i][0].id;
  }else if (this.datareponse[i][0].isChecked==false){ this.response1=""; }
  if(this.datareponse[i][1].isChecked==true){
    this.response2=this.datareponse[i][1].id;
 }else if (this.datareponse[i][1].isChecked==false) { this.response2=""; }
 if(this.datareponse[i][2].isChecked==true){
  this.response3=this.datareponse[i][2].id;
}else if (this.datareponse[i][2].isChecked==false){ this.response3=""; }
if(this.datareponse[i][3].isChecked==true){
  this.response4=this.datareponse[i][3].id;
}else if (this.datareponse[i][3].isChecked==false) { this.response4=""; }
if(this.datareponse[i][4].isChecked==true){
  this.response5=this.datareponse[i][4].id;
}else if (this.datareponse[i][4].isChecked==false) { this.response5=""; }


    this.responsezero=this.response1 + this.response2 + this.response3 +this.response4+",";
  this.responsestring= this.responsestring +this.responsezero;
}
/////


var data = JSON.stringify({ 
  name: "ConnSci6RepMult", 
  idresponse:this.idresponse,
  resp: this.responsestring, 
  });



this.redditService.addresponses(data)  
.toPromise()
.then((response) =>
{console.log(response);
})


  this.navCtrl.push(EndtestPage, {
    param1: this.idtest,
    param2: this.position,
  });
}
}


prev(){

  if (  this.indextest > 0 ){
    this.indextest=this.indextest-1;
    this.indexdisplay=this.indexdisplay-1;
    this.reviewQuestions();
  }else{
 
  }
}

retrieveQuestions() {
  this.storage.get("questions").then( users => {
      this.dataq = JSON.parse(users);
      this.question_label=this.dataq[this.indextest].question_label;
      this.q_h=this.dataq[this.indextest].q_h;
      this.q_f=this.dataq[this.indextest].q_f;
      this.question_num=this.dataq[this.indextest].question_num;
      this.datarep=this.datareponse[this.indextest];

      // nb true reponse
      this.nbchecked=0;
      for(let i=0;i<5;i++){
        if (this.datarep[i].isChecked==true){
          this.nbchecked=this.nbchecked +1;
        } 
      }
   
    })
}


reviewQuestions() {
  this.storage.get("questions").then( users => {
      this.dataq = JSON.parse(users);
      this.question_label=this.dataq[this.indextest].question_label;
      this.q_h=this.dataq[this.indextest].q_h;
      this.q_f=this.dataq[this.indextest].q_f;
      this.question_num=this.dataq[this.indextest].question_num;
      this.datarep=this.datareponse[this.indextest];
       // nb true reponse
       this.nbchecked=0;
       for(let i=0;i<5;i++){
         if (this.datarep[i].isChecked==true){
           this.nbchecked=this.nbchecked +1;
         } 
       }
      
  })
}

updateCheck($event) {
  console.log($event.checked); 
  if ($event.checked==true){
    this.nbchecked=this.nbchecked +1;
    console.log(this.nbchecked);
  } else if ($event.checked==false){
  this.nbchecked= this.nbchecked -1;
  console.log(this.nbchecked);
    } 
}

static mustBeTruthy(c: AbstractControl): { [key: string]: boolean } {
  let rv: { [key: string]: boolean } = {};
  if (!c.value) {
    rv['notChecked'] = true;
  }
  return rv;
}
reorderData(indexes: any) {
  this.posts2 = reorderArray(this.posts2, indexes);
  console.log(this.posts2);
  console.log(indexes);
//this.dataq[this.indextest] = reorderArray(this.dataq[this.indextest], indexes);
 // console.log(this.dataq[this.indextest]);
// console.log(this.dataq[this.indextest]);
}


logOut() {
  this.authService.logout()
  this.navCtrl.push(Login);
}

ngOnInit() {
  this.initTimer();
}

initTimer() {
  this.time = this.timeInSeconds;
  this.runTimer = false;
  this.hasStarted = false;
  this.hasFinished = false;
  this.remainingTime = this.timeInSeconds;
  
  this.displayTime = this.getSecondsAsDigitalClock(this.remainingTime);
}

startTimer() {
   this.runTimer = true;
  this.hasStarted = true;
  this.timerTick();
}

pauseTimer() {
  this.runTimer = false;
}

resumeTimer() {
  this.startTimer();
}

timerTick() {
  setTimeout(() => {

    if (!this.runTimer) { return; }
    this.remainingTime--;
    this.displayTime = this.getSecondsAsDigitalClock(this.remainingTime);

    this.progressTimer=(this.remainingTime*  100) /  this.time;
    this.progressPercent=(100- this.progressTimer).toFixed(0);

    if (this.remainingTime > 0) {
      this.timerTick();
    }
    else {
      this.hasFinished = true;
      this.navCtrl.push(EndtestPage, {
        param1: this.idtest,
        param2: this.position,
      });
    }
  }, 1000);
}

getSecondsAsDigitalClock(inputSeconds: number) {
  var sec_num = parseInt(inputSeconds.toString(), 10); // don't forget the second param
  var hours = Math.floor(sec_num / 3600);
  var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
  var seconds = sec_num - (hours * 3600) - (minutes * 60);
  var hoursString = '';
  var minutesString = '';
  var secondsString = '';
  hoursString = (hours < 10) ? "0" + hours : hours.toString();
  minutesString = (minutes < 10) ? "0" + minutes : minutes.toString();
  secondsString = (seconds < 10) ? "0" + seconds : seconds.toString();
  return hoursString + ':' + minutesString + ':' + secondsString;
}
}