import { Component } from '@angular/core';
import { MenuController, NavController, NavParams, ViewController, PopoverController, AlertController  } from 'ionic-angular';
import {RedditService} from "../../providers/reddit-service";
import { LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { StarttestPage } from '../starttest/starttest';
import { AuthService } from '../../providers/AuthService';
import { Login } from '../login/login';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'page-sessions',
  templateUrl: 'sessions.html'
})
export class SessionsPage {

pages: any;
items: any;
posts: any;
page:number;
category: string;
region: string;
currentpage: any;
word: any="";
wordid: any="";
  parameter1: any;
  test: any;
  lenghttest: number=0;
  idtest: any;
  pass: any;
urlupload="http://api.selectionetconseils.ch/uploads/";
  passw: any;
  code: any;
  numpassation: any;
  userid: any;
  session: any;
  i: any;
  customer_id: any;
  idsession: any;
  birthday: any;
  genre: any;
  lastname: any;
  firstname: any;
  idresponse: any;
constructor(private translate: TranslateService,public navCtrl: NavController,public authService: AuthService, public popoverCtrl: PopoverController,private alertCtrl: AlertController, public menu: MenuController ,public loadingController:LoadingController,  public redditService:RedditService, public storage: Storage,  public navParams: NavParams, private viewCtrl: ViewController) {
this.page=1;

  


this.storage.get('customer_id').then((userid) => {
  this.customer_id=userid;
})

this.storage.get('lastname').then((lastname) => {
  this.lastname=lastname;
})

this.storage.get('firstname').then((firstname) => {
  this.firstname=firstname;
})

this.storage.get('birthday').then((birthday) => {
  this.birthday=birthday;
})

this.storage.get('genre').then((genre) => {
  this.genre=genre;
})



this.storage.get('numpassation').then((numpass) => {
  this.numpassation=numpass;
  console.log("------Numero de passation sauvegarde -----------")
  console.log(this.numpassation)
})
}


ionViewWillEnter(){
      this.page=1;
      this.storage.get('customer_id').then((userid) => {
        this.userid=userid;
      this.redditService.getSession(this.userid).subscribe(data=>{
        this.session=data.listing;
        console.log(this.session);
        });
      
      })
      this.redditService.getAllPassations(this.page).subscribe(data=>{
        console.log(data);
        this.pass=data.listing;
        });

   }
  doRefresh(refresher) {
    setTimeout(() => {
      this.page=1 ;
      this.redditService.getAllPassations(this.page).subscribe(data=>{
        this.posts=data.listing;
        console.log(this.posts);
        this.items=data.items;
        this.pages=data.page;
        this.currentpage=data.currentpage;
        });
      refresher.complete();
    }, 1100);
  }


  goRestore(event, item) {

////get session order =i

console.log(item.sessionorder);
    this.i=item.sessionorder;
    this.storage.set('sessionorder', 0);
    this.idsession=item.idsession;
    this.storage.set('idsession', this.idsession);
    this.parameter1=item.passation_num;
    this.redditService.getFkpass(  this.numpassation).subscribe(data=>{
   
      console.log(data);
      this.test=data.listing;
      this.lenghttest=data.length;
      this.storage.set('longueurpassation', this.lenghttest);
     this.idtest=this.test[this.i].idtest
        this.navCtrl.push(StarttestPage, {
          param1: this.idtest,
          param2: this.lenghttest,
        });
  });
}


    gotest(event, item) {

      this.parameter1=item.passation_num;
      this.redditService.getFkpass(  this.numpassation).subscribe(data=>{
        this.test=data.listing;
        this.lenghttest=data.length;
        console.log("longeur passation");
        console.log( this.lenghttest);

        this.idtest=this.test[0].idtest
   
            var data3 = JSON.stringify({ 
              firstname:this.firstname,
              lastname: this.lastname,
              idpassation: this.numpassation,
              genre:this.genre,
              birthday:this.birthday,
            }); 
            this.redditService.addnewresponse(data3)  
            .toPromise()
            .then((response) =>
            {
              this.idresponse=response.id;
              this.storage.set('idresponse', this.idresponse);
              
              console.log(response.id);
              var data2 = JSON.stringify({ 
                iduser:this.customer_id,
                idpassation: this.numpassation,
                sessionorder:0,
                finished:0,
                idresponse:this.idresponse,
                }); 
              this.redditService.addsession(data2)  
              .toPromise()
              .then((response) =>
              {
                this.storage.set('idsession', response.id);
             
              if(response.status == 'success') {
               this.navCtrl.push(StarttestPage, {
              param1: this.idtest,
              param2: this.lenghttest,
              param6: 0,
            });  
        }})
            })
      
    });
}



logOut() {
  this.authService.logout()
  this.navCtrl.push(Login);
}

}



