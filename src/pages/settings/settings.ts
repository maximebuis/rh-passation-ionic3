import { Component } from '@angular/core';
import { NavController, NavParams }  from 'ionic-angular';
import {Storage} from '@ionic/storage';

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {


  toastCtrl: any;
  enableNotifications: any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public storage: Storage,) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingsPage');
  }

delete() {

  this.storage.clear();

  alert( "Deconnexion ");  
  
}


toggleNotifications() {
  if (this.enableNotifications) {
    this.toastCtrl.create('Notifications enabled.');
  } else {
    this.toastCtrl.create('Notifications disabled.');
  }
}




}
