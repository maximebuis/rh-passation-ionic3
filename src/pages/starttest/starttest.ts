import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Component, NgZone, EventEmitter, Input, Output} from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation';
import { RedditService } from "../../providers/reddit-service";
import {Storage} from '@ionic/storage';
import {NavController,  NavParams,  ActionSheetController, ToastController, Platform, LoadingController, Loading } from 'ionic-angular';
import { QuestionsPage } from '../questions/questions';
import { TestsPage } from '../tests/tests';
import { ValeursPage } from '../valeurs/valeurs';
import { Login } from '../login/login';
import { AuthService } from '../../providers/AuthService';
import { QuestionnairePage } from '../questionnaire/questionnaire';
import { ScientifiquePage } from '../scientifique/scientifique';
import { LogiquePage } from '../logique/logique';
import { VerbaldefinitionPage } from '../verbaldefinition/verbaldefinition';
import { VerbaleliminationPage } from '../verbalelimination/verbalelimination';
import { MemoirePage } from '../memoire/memoire';
import { EspacePage } from '../espace/espace';
import { StressPage } from '../stress/stress';
import { ImaginationPage } from '../imagination/imagination';
import { JugementPage } from '../jugement/jugement';
import { FacialePage } from '../faciale/faciale';
import { VerbalsynonymePage } from '../verbalsynonyme/verbalsynonyme';
import { TranslateService } from '@ngx-translate/core';


@Component({
selector: 'page-starttest',
templateUrl: 'starttest.html',
})
export class  StarttestPage {
  
  question_label: any;
  q_prop1: any;
  q_prop2: any;
  q_prop3: any;
  q_prop4: any;
  q_prop5: any;
  q_prop6: any;
  q_img1: any;
  q_img2: any;
  q_img3: any;
  q_img4: any;
  q_img5: any;
  q_img6: any;
  son: any;
  parameter1: any;
  data: any;
  type: any;
  description2: any;
  types: any;
  format_num: any;
  test_label: any;
  test_titre: any;
  test_duree: any;
  test_nav: any;
  test_into: any;
  test_final: any;
  test_tiers: any;
  test_intro: any;
  test_category: any;
  parameter2: number;
  questions: any;
  lenghtquestions: any;
  parameter3: any;
  lenghtpassation: any;
  testcategory: any;
  testduree: any;
  ordresession: any;
  parameter6: any;
  position: any;
  maxtime: any=30;
  charcterstic1: number=0;
  timer: number=0;
  maxTime: number=100;
  lg: string;
  constructor( 
    
    public loadingController:LoadingController, public authService: AuthService, public actionSheetCtrl: ActionSheetController, 
    public toastCtrl: ToastController, public platform: Platform, public loadingCtrl: LoadingController,
    public navCtrl: NavController, public storage: Storage, public redditService:RedditService, 
    public navParams: NavParams, public http: Http, public zone: NgZone, private translate: TranslateService) {
    this.parameter1 = navParams.get('param1');

    this.position = navParams.get('param6');
    this.lg=translate.currentLang;
  
switch(this.lg) { 
  case this.lg="fr": { 
    this.parameter2=2;
     break; 
  } 
  case this.lg="de": { 
    console.log("allemand break")
    this.parameter2=3;
     break; 
  } 
  case this.lg="en": { 
    console.log("english break")
    this.parameter2=1;
    break; 
 } 
} 
}

ionViewWillEnter(){


  console.log(this.parameter2);
  this.redditService.getTest(this.parameter1).subscribe(data=>{
    this.data=data.items;  
    console.log(data);
    this.testcategory=data.items[0].test_category;
    this.testduree=data.items[0].test_duree;
    this.test_nav=data.items[0].test_nav;
  })

  this.redditService.questionbyTest( this.parameter1, this.parameter2 ).subscribe(data=>{
    this.questions=data.listing;
    console.log("//////////LONGEUR /////////");
    console.log("//////////LONGEUR /////////");
    console.log("//////////LONGEUR /////////");
    console.log("//////////LONGEUR /////////");
    console.log("//////////LONGEUR /////////");
    this.parameter3=this.questions.length;
    console.log(this.parameter3);
    this.storeQuestions();});
/*  this.redditService.getFkpass(this.parameter1).subscribe(data=>{
    this.lenghtpassation=data.lenght;  
    console.log("--------LENGHT PASSATION--------");
    console.log(this.lenghtpassation);
})*/
}
goTest(){   

  console.log("GO test");
  console.log(this.testcategory);

  /// test choix unique
  if (this.testcategory==42) {
    console.log("Logique");
    this.navCtrl.push(LogiquePage, { param1: this.parameter1 ,param2: this.parameter3,param4: this.testcategory,param5: this.testduree,param6: this.test_nav,param7: this.position,
    });
  } 
  
  
  if (this.testcategory==725) {
    console.log(" Science");
  
    this.navCtrl.push(ScientifiquePage, { param1: this.parameter1 ,param2: this.parameter3,param4: this.testcategory,param5: this.testduree,param6: this.test_nav,param7: this.position
    });
  } 
  


  if (this.testcategory==22) {
    console.log(" Stress");
  
    this.navCtrl.push(StressPage, { param1: this.parameter1 ,param2: this.parameter3,param4: this.testcategory,param5: this.testduree,param6: this.test_nav,param7: this.position
    });
  } 
  


  if (this.testcategory==394) {
    console.log(" Memoire");
  
    this.navCtrl.push(MemoirePage, { param1: this.parameter1 ,param2: this.parameter3,param4: this.testcategory,param5: this.testduree,param6: this.test_nav,param7: this.position
    });
  } 
  
  if (this.testcategory==1654) {
    console.log(" Espace");
  
    this.navCtrl.push(EspacePage, { param1: this.parameter1 ,param2: this.parameter3,param4: this.testcategory,param5: this.testduree,param6: this.test_nav,param7: this.position
    });
  } 
 
  if (this.testcategory==1574) {
    console.log(" Verbal synomyme");
  
    this.navCtrl.push(VerbalsynonymePage, { param1: this.parameter1 ,param2: this.parameter3,param4: this.testcategory,param5: this.testduree,param6: this.test_nav,param7: this.position
    });
  } 

  if (this.testcategory==713) {
    console.log(" Verbal definition");
  
    this.navCtrl.push(VerbaldefinitionPage, { param1: this.parameter1 ,param2: this.parameter3,param4: this.testcategory,param5: this.testduree,param6: this.test_nav,param7: this.position
    });
  } 

  if (this.testcategory==142) {
    console.log(" Verbal elimination");
    this.navCtrl.push(VerbaleliminationPage, { param1: this.parameter1 ,param2: this.parameter3,param4: this.testcategory,param5: this.testduree,param6: this.test_nav,param7: this.position
    });
  } 


 
  if (this.testcategory==533) {
    console.log(" Imagination");

    this.navCtrl.push(ImaginationPage, { param1: this.parameter1 ,param2: this.parameter3,param4: this.testcategory,param5: this.testduree,param6: this.test_nav,param7: this.position
    });
  } 



  if ( this.testcategory==14) {
    console.log(" Valeurs");
    this.navCtrl.push(ValeursPage, { param1: this.parameter1 ,param2: this.parameter3,param4: this.testcategory,param5: this.testduree,param6: this.test_nav,param7: this.position,
    });
  } 
  
  if ( this.testcategory==13) {
    console.log(" Jugement");
    this.navCtrl.push(JugementPage, { param1: this.parameter1 ,param2: this.parameter3,param4: this.testcategory,param5: this.testduree,param6: this.test_nav,param7: this.position,
    });
  } 
  
  if ( this.testcategory==122) {
    console.log(" Faciale");
    this.navCtrl.push(FacialePage, { param1: this.parameter1 ,param2: this.parameter3,param4: this.testcategory,param5: this.testduree,param6: this.test_nav,param7: this.position,
    });
  } 
  


  if (this.testcategory==255 ||this.testcategory==345 ||this.testcategory==1283||this.testcategory==1388||this.testcategory==1193||this.testcategory==1013||this.testcategory==818||this.testcategory==923||this.testcategory==1103
    ||this.testcategory==1872 ){
    console.log("Choix multiples");

    this.navCtrl.push(QuestionnairePage, { param1: this.parameter1 ,param2: this.parameter3,param4: this.testcategory,param5: this.testduree,param6: this.test_nav,param7: this.position,
    });
}
}

storeQuestions() {
  let usersStringifiedObj = JSON.stringify(this.questions);
  this.storage.set("questions", usersStringifiedObj);
}



logOut() {
  this.authService.logout()
  this.navCtrl.push(Login);
}

timerprogress(){

  setTimeout(() => {
    this.charcterstic1=this.charcterstic1+1;
    console.log(this.charcterstic1);
  }, 3000);
}




  StartTimer(){

    console.log(this.charcterstic1);
   
this.timer=this.timer+1;
console.log( this.timer);
console.log( this.charcterstic1);  

          if(this.timer<=this.maxTime){
            setTimeout(function(){
              this.charcterstic1 = this.charcterstic1+1;
              this.StartTimer();
           },100);

          
             
           
           
            } else{
             
            }
          
           
         
          
         

     

  }

}


