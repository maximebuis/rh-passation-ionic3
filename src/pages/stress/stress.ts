import 'rxjs/add/operator/map';
import { Component, ViewChild, ElementRef} from '@angular/core';
import { RedditService } from "../../providers/reddit-service";
import {Storage} from '@ionic/storage';
import {NavController,  NavParams, LoadingController,reorderArray } from 'ionic-angular';
import { DragulaService} from 'ng2-dragula';
import { EndtestPage } from '../endtest /endtest';
import { AuthService } from '../../providers/AuthService';
import { Login } from '../login/login';
import { FormGroup, FormBuilder, AbstractControl } from '@angular/forms';

@Component({
  providers: [DragulaService],
  selector: 'page-stress',
  templateUrl: 'stress.html',
})
export class StressPage {
  form: FormGroup;
  parameter1: any;
  questions: any;
  data: any;
  squestion: any;
  questionid: any;
  editing: boolean = true;
  parameter2: number;
  lenghtquestions: any;
  question: any;
  q: any;
  questionlist: any;
  question_label: any;
  q_prop1: any;
  q_prop2: any;
  q_prop3: any;
  q_prop4: any;
  q_prop5: any;
  testlenght: any;
  idtest: any;
  dataq  = [];
  nbquestionbytest: any;
  testcategory: any;
  testduree: any;
  test_nav: any;
  q_h: any;
  q_f: any;
  position: any;
  propositions: any;
  reponses: number[];
  indextest:number=0;
  indexdisplay:number=1;
  posts2 = [];
  nbchecked: number=0;
  checked: boolean;
  checked1: boolean;
  checked2: boolean;
  checked3: boolean;
  checked4: boolean;
  checked5: boolean;
  checkedIdx=-1;
  options = [];
  reponse = [];
  reponsesave= [];
  reponsebyid: number;
  datasave: number;
  timeInSeconds: any;
  time: any;
  hasStarted: boolean;
  runTimer: boolean;
  remainingTime: any;
  hasFinished: boolean;
  displayTime: string;
  question_num: any;
  myDataArray= [];
  checkedItems: any;
  datareponse=[];
  questiondata: { id: string; prop: any; isChecked: boolean; }[];
  datarep: any;
  reponsecheck: any;
  canvas: HTMLCanvasElement;
 
  points= [];
  pts: any;
  square1: number;
  square2: string;
  points2: any;
  points3: number;
  result= [];

  test=[];
  pts1: any;
  jsonObj: any;
  myJSON: any;
  y: any;
  x: any;
  canvas2: any;
  pts2: any;
  canvas1: any;
  pts3: any;
  canvas3: any;
  can3=[];
xdata:number;
ydata:number;
cpt3:number=0;

progressTimer: number;
progressPercent:any=0;
  constructor( 
    public loadingController:LoadingController,public authService: AuthService,  private dragulaService: DragulaService,  public loadingCtrl: LoadingController,
    public navCtrl: NavController, public storage: Storage, public redditService:RedditService, 
    public navParams: NavParams) {
    this.idtest = navParams.get('param1');
    this.testlenght = navParams.get('param2');
    this.testcategory= navParams.get('param4');
    this.timeInSeconds= navParams.get('param5');
    this.test_nav= navParams.get('param6');
    this.position= navParams.get('param7');


    this.storage.get("questions").then( users => {
      this.dataq = JSON.parse(users);
      for(let i=0;i<this.dataq.length;i++){
        this.question_label=this.dataq[i].question_label;
        this.propositions=this.dataq[i];
        this.q_prop1=this.dataq[i].q_prop1;
        this.q_prop2=this.dataq[i].q_prop2;
        this.q_prop3=this.dataq[i].q_prop3;
        this.q_prop4=this.dataq[i].q_prop4;
        this.q_prop5=this.dataq[i].q_prop5;
        this.q_h=this.dataq[i].q_h;
        this.q_f=this.dataq[i].q_f;
        this.checkedIdx=this.reponsesave[i];
        this.question_num=this.dataq[i].question_num;      
        this.options= [{id: '1', prop: this.q_prop1,isChecked:false},{id: '2', prop: this.q_prop2,isChecked:false},
        {id: '3', prop: this.q_prop3,isChecked:false},{id: '4', prop: this.q_prop4,isChecked:false},{id: '5', prop: this.q_prop5,isChecked:false}]; 
        this.datareponse.push(this.options);
     
      }
  })
  }

  ionViewWillEnter(){ 
    this.canvas1 = <HTMLCanvasElement>document.getElementById('canvas1');
    this.canvas2 = <HTMLCanvasElement>document.getElementById('canvas2');
    this.canvas3 = <HTMLCanvasElement>document.getElementById('canvas3');
    if (this.timeInSeconds>0){this.startTimer();}
    this.indexdisplay=1;
    //Données i=0
    this.storage.get("questions").then( users => {
      this.dataq = JSON.parse(users);
      this.nbquestionbytest=this.dataq.length;
      this.question_label=this.dataq[0].question_label;
      this.q_h=this.dataq[0].q_h;
      this.q_f=this.dataq[0].q_f;
      this.q_prop1=this.dataq[0].q_prop1;
      console.log( this.q_prop1)
      this.q_prop2=this.dataq[0].q_prop2;
      this.question_num=this.dataq[0].question_num;
      this.datarep=this.datareponse[0];
      console.log(this.datareponse[0][0].prop);
      this.pts1=this.datareponse[0][0].prop;
      this.pts2=this.datareponse[0][1].prop;
      this.pts3=this.datareponse[0][2].prop;
      this.markPoint(this.pts1);
      this.markPoint2(this.pts1);
      this.markPoint3(this.pts3);
      console.log("-------------PTS2--------");
      console.log(this.pts2);

  })
}

 markPoint(pts1) {
  let context1 : CanvasRenderingContext2D = this.canvas1.getContext("2d");
  var  canvas1 = document.getElementById('canvas1');
  context1.beginPath(); 
  this.points=this.remplace(this.pts1);

  console.log("PTS 1");
    console.log(this.points);
 for (var i=0; i<21; i++) {
  this.x=this.points[i];
  this.y=this.points[i+1];
  console.log(this.x,this.y);
   i = i+1;
  context1.fillRect(this.x,this.y,5,5);}
  context1.fillStyle = "#92B901;  width: 20";
  context1.stroke();
  context1.closePath();
}

markPoint2(pts2) {
  let context2 : CanvasRenderingContext2D = this.canvas2.getContext("2d");
  var  canvas2 = document.getElementById('canvas2');
  context2.beginPath(); 

  this.pts2=this.remplace(this.pts2);
  console.log("PTS 2");
  console.log(this.pts2);
 for (var i=0; i<21; i++) {
  this.x=this.pts2[i];
  this.y=this.pts2[i+1];
  console.log(this.x,this.y);
   i = i+1;
  context2.fillRect(this.x,this.y,5,5);}
  context2.fillStyle = "#92B901;  width: 20";
  context2.stroke();
  context2.closePath();
}


markPoint3(pts3) {
  let context3 : CanvasRenderingContext2D = this.canvas3.getContext("2d");
  var  canvas3 = document.getElementById('canvas3');
  context3.beginPath(); 
  this.pts3=this.remplace(this.pts3);

  console.log("PTS 3");
  console.log(this.pts3);
 for (var i=0; i<21; i++) {
  this.x=this.pts3[i];
  this.y=this.pts3[i+1];
  context3.fillRect(this.x,this.y,5,5);}
  context3.fillStyle = "#92B901;  width: 20";
  context3.stroke();
  context3.closePath();


  canvas3.addEventListener('click', function(evt) {
    var rect = canvas3.getBoundingClientRect();
   // this.xdata = (evt.x - rect.left).toFixed(0);
  //  this.ydata = (evt.y - rect.top).toFixed(0);
 //console.log(this.ctp3);
  //  if(this.ctp3=0){
 //     console.log("pts 0");
   // context3.beginPath();
   // context3.lineWidth = 2;
   // context3.strokeStyle="#FF0000";
   // context3.lineJoin = "round";
   // context3.moveTo(evt.x - rect.left ,evt.y - rect.top); 
      
   //   this.ctp3=this.ctp3+1;
  //  }else if (this.ctp3<6 && this.ctp3>0){
  // console.log("pts");
  //    context3.lineTo(evt.x - rect.left ,evt.y - rect.top);
  //    context3.stroke();
    //  this.ctp3=this.ctp3+1;
  //  }
  

    
    
    
      });
  context3.stroke();
  context3.closePath();

}





getPosition(event){
  var rect = this.canvas3.getBoundingClientRect();
  var x = event.clientX - rect.left;
  var y = event.clientY - rect.top;
     
console.log(x);
}


remplace(data){
  let ptsarray = data;
  let r1 = /\,/gi;
  let re = /\;/gi;
  let r2 = /\[/gi;
  let r3 = /\]/gi;
  let result = ptsarray.replace(r1, ",").replace(re, ",").replace(r2, "").replace(r3, "");
  this.points= result.split(',');
 return this.points;
}



next(){
for(let i=0;i<this.datareponse[this.indextest];i++){
  this.datareponse.push(this.datareponse[this.indextest]);
console.log(this.datareponse[this.indextest])
}
console.log(  this.datareponse);

  /*this.datareponse[this.indextest] =  this.datarep.filter(value => {

    console.log(value.isChecked );
    return value.isChecked && !value.isChecked ;
  });
*/
  console.log("enregistrement des questions");
  console.log(this.datareponse);

  // new question
  this.indextest=this.indextest+1;



  if (  this.indextest < this.nbquestionbytest ){
    this.indexdisplay=this.indexdisplay+1;
    this.retrieveQuestions();
  }else{
    this.pauseTimer();
  ////// SAVE DATA
  /*console.log("réponses");
  console.log(this.reponsesave);

  for(let i=0;i<this.nbquestionbytest[i];i++){
    this.datasave=this.datasave+this.reponsesave[i];
    console.log(this.datasave);
  }*/
  /////
    this.navCtrl.push(EndtestPage, {
      param1: this.idtest,
      param2: this.position,
    });
  }
}

prev(){

  if (  this.indextest > 0 ){
    this.indextest=this.indextest-1;
    this.indexdisplay=this.indexdisplay-1;
    this.reviewQuestions();
  }else{
 
  }
}

retrieveQuestions() {
  this.storage.get("questions").then( users => {
      this.dataq = JSON.parse(users);
      this.question_label=this.dataq[this.indextest].question_label;
      this.q_h=this.dataq[this.indextest].q_h;
      this.q_f=this.dataq[this.indextest].q_f;
      this.question_num=this.dataq[this.indextest].question_num;
      this.datarep=this.datareponse[this.indextest];

      // nb true reponse
      this.nbchecked=0;
      for(let i=0;i<5;i++){
        if (this.datarep[i].isChecked==true){
          this.nbchecked=this.nbchecked +1;
        } 
      }
   
    })
}


reviewQuestions() {
  this.storage.get("questions").then( users => {
      this.dataq = JSON.parse(users);
      this.question_label=this.dataq[this.indextest].question_label;
      this.q_h=this.dataq[this.indextest].q_h;
      this.q_f=this.dataq[this.indextest].q_f;
      this.question_num=this.dataq[this.indextest].question_num;
      this.datarep=this.datareponse[this.indextest];
       // nb true reponse
       this.nbchecked=0;
       for(let i=0;i<5;i++){
         if (this.datarep[i].isChecked==true){
           this.nbchecked=this.nbchecked +1;
         } 
       }
      
  })
}

updateCheck($event) {
  console.log($event.checked); 
  if ($event.checked==true){
    this.nbchecked=this.nbchecked +1;
    console.log(this.nbchecked);
  } else if ($event.checked==false){
  this.nbchecked= this.nbchecked -1;
  console.log(this.nbchecked);
    } 
}

static mustBeTruthy(c: AbstractControl): { [key: string]: boolean } {
  let rv: { [key: string]: boolean } = {};
  if (!c.value) {
    rv['notChecked'] = true;
  }
  return rv;
}
reorderData(indexes: any) {
  this.posts2 = reorderArray(this.posts2, indexes);
  console.log(this.posts2);
  console.log(indexes);
//this.dataq[this.indextest] = reorderArray(this.dataq[this.indextest], indexes);
 // console.log(this.dataq[this.indextest]);
// console.log(this.dataq[this.indextest]);
}


logOut() {
  this.authService.logout()
  this.navCtrl.push(Login);
}

ngOnInit() {
  this.initTimer();
}

initTimer() {
  this.time = this.timeInSeconds;
  this.runTimer = false;
  this.hasStarted = false;
  this.hasFinished = false;
  this.remainingTime = this.timeInSeconds;
  
  this.displayTime = this.getSecondsAsDigitalClock(this.remainingTime);
}

startTimer() {
   this.runTimer = true;
  this.hasStarted = true;
  this.timerTick();
}

pauseTimer() {
  this.runTimer = false;
}

resumeTimer() {
  this.startTimer();
}

timerTick() {
  setTimeout(() => {

    if (!this.runTimer) { return; }
    this.remainingTime--;
    this.displayTime = this.getSecondsAsDigitalClock(this.remainingTime);

    this.progressTimer=(this.remainingTime*  100) /  this.time;
    this.progressPercent=(100- this.progressTimer).toFixed(0);
    if (this.remainingTime > 0) {
      this.timerTick();
    }
    else {
      this.hasFinished = true;
      this.navCtrl.push(EndtestPage, {
        param1: this.idtest,
        param2: this.position,
      });
    }
  }, 1000);
}

getSecondsAsDigitalClock(inputSeconds: number) {
  var sec_num = parseInt(inputSeconds.toString(), 10); // don't forget the second param
  var hours = Math.floor(sec_num / 3600);
  var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
  var seconds = sec_num - (hours * 3600) - (minutes * 60);
  var hoursString = '';
  var minutesString = '';
  var secondsString = '';
  hoursString = (hours < 10) ? "0" + hours : hours.toString();
  minutesString = (minutes < 10) ? "0" + minutes : minutes.toString();
  secondsString = (seconds < 10) ? "0" + seconds : seconds.toString();
  return hoursString + ':' + minutesString + ':' + secondsString;
}
}