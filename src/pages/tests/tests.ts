import { Component } from '@angular/core';
import { MenuController, NavController, NavParams, ViewController, PopoverController, AlertController  } from 'ionic-angular';
import {RedditService} from "../../providers/reddit-service";
import { LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { OrdertestPage } from '../ordertest/ordertest';
@Component({
  selector: 'page-tests',
  templateUrl: 'tests.html'
})
export class TestsPage {

pages: any;
items: any;
posts: any;
page:number;
category: string;
region: string;
currentpage: any;
word: any="";
wordid: any="";

constructor(public navCtrl: NavController, public popoverCtrl: PopoverController,private alertCtrl: AlertController, public menu: MenuController ,public loadingController:LoadingController,  public redditService:RedditService, public storage: Storage,  public navParams: NavParams, private viewCtrl: ViewController) {
this.page=1;
}

ionViewWillEnter(){
      this.page=1;
      this.redditService.getAllTests(this.page).subscribe(data=>{

        console.log(data);
        this.posts=data.listing;
        this.items=data.items;
        this.pages=data.page;
        this.currentpage=data.currentpage;
        });

   }
  doRefresh(refresher) {
    setTimeout(() => {
      this.page=1 ;
      this.redditService.getAllTests(this.page).subscribe(data=>{
        this.posts=data.listing;
        this.items=data.items;
        this.pages=data.page;
        this.currentpage=data.currentpage;
        });
      refresher.complete();
    }, 1100);
  }



    order(event, item) {
      this.navCtrl.push(OrdertestPage, {
        param1: item.test_num,
        param2: item.test_category,
      });
    }
    delete(event, item) {
      var data = JSON.stringify({ 
        idtest:item.test_num,
        });  

        console.log(data);
        this.redditService.deleteTest(data)  
        .toPromise()
        .then((response) =>
        {setTimeout(() => { this.navCtrl.push(TestsPage);}, 500);})
      }




    next() {
if  (this.page<this.pages){
     this.page = this.page +1 ;
     if  (this.word=="" && this.wordid==""){    
      this.redditService.getAllTests(this.page).subscribe(data=>{
       this.posts=data.listing;
       this.items=data.items;
       this.pages=data.page;
       this.currentpage=data.currentpage;
       });
     }else if (this.word!=="" ) {
       this.redditService.seachTest(this.page,this.word).subscribe(data=>{
         this.posts=data.listing;
         this.items=data.items;
         this.pages=data.page;
         this.currentpage=data.currentpage;
         });
     }else if (this.wordid!=="") {
       this.redditService.seachTestid(this.page,this.wordid).subscribe(data=>{
         this.posts=data.listing;
         this.items=data.items;
         this.pages=data.page;
         this.currentpage=data.currentpage;
         });
     }
    }}


  prev() {

    if  (this.page>1){
         this.page = this.page -1;
         if  (this.word=="" && this.wordid==""){    
          this.redditService.getAllTests(this.page).subscribe(data=>{
           this.posts=data.listing;
           this.items=data.items;
           this.pages=data.page;
           this.currentpage=data.currentpage;
           });
         }else if (this.word!=="" ) {
           this.redditService.seachTest(this.page,this.word).subscribe(data=>{
             this.posts=data.listing;
             this.items=data.items;
             this.pages=data.page;
             this.currentpage=data.currentpage;
             });
         }else if (this.wordid!=="") {
           this.redditService.seachTestid(this.page,this.wordid).subscribe(data=>{
             this.posts=data.listing;
             this.items=data.items;
             this.pages=data.page;
             this.currentpage=data.currentpage;
             });
         }
        
      }}

      forward(){

        if  (this.pages>1){
             this.page = this.pages;
             if  (this.word=="" && this.wordid==""){    
              this.redditService.getAllTests(this.page).subscribe(data=>{
               this.posts=data.listing;
               this.items=data.items;
               this.pages=data.page;
               this.currentpage=data.currentpage;
               });
             }else if (this.word!=="" ) {
               this.redditService.seachTest(this.page,this.word).subscribe(data=>{
                 this.posts=data.listing;
                 this.items=data.items;
                 this.pages=data.page;
                 this.currentpage=data.currentpage;
                 });
             }else if (this.wordid!=="") {
               this.redditService.seachTestid(this.page,this.wordid).subscribe(data=>{
                 this.posts=data.listing;
                 this.items=data.items;
                 this.pages=data.page;
                 this.currentpage=data.currentpage;
                 });
             }
            
          }}
      backward() {

        if  (this.pages>1){
             this.page =1;
             if  (this.word=="" && this.wordid==""){    
              this.redditService.getAllTests(this.page).subscribe(data=>{
               this.posts=data.listing;
               this.items=data.items;
               this.pages=data.page;
               this.currentpage=data.currentpage;
               });
             }else if (this.word!=="" ) {
               this.redditService.seachTest(this.page,this.word).subscribe(data=>{
                 this.posts=data.listing;
                 this.items=data.items;
                 this.pages=data.page;
                 this.currentpage=data.currentpage;
                 });
             }else if (this.wordid!=="") {
               this.redditService.seachTestid(this.page,this.wordid).subscribe(data=>{
                 this.posts=data.listing;
                 this.items=data.items;
                 this.pages=data.page;
                 this.currentpage=data.currentpage;
                 });
             }
            
          }}
 
     
  
      onInput(selectedValue: any) {
        this.page=1;
             this.redditService.seachTest(this.page,this.word).subscribe(data=>{


        console.log(data);
               this.posts=data.listing;
               this.items=data.items;
               this.pages=data.page;
               this.currentpage=data.currentpage;
               });
           }
     
           onCancel(selectedValue: any) {
          this.wordid=="";
           this.word=="";
       }
       onInputid(selectedValue: any) {
        this.page=1;
             this.redditService.seachTestid(this.page,this.wordid).subscribe(data=>{
               this.posts=data.listing;
               this.items=data.items;
               this.pages=data.page;
               this.currentpage=data.currentpage;
               });
           }
     
           onCancelid(selectedValue: any) {
           this.wordid=="";
           this.word=="";
       }

   reset(){
    this.wordid=="";
    this.word=="";
    this.navCtrl.setRoot(TestsPage);
   }
}



