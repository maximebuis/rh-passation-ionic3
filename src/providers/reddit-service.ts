import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http, Headers, RequestOptions} from '@angular/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';

import 'rxjs/add/observable/of';
import { FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { HttpHeaders } from '@angular/common/http';
@Injectable()
export class RedditService {

  articlesPage: string="10";
  URL = "https://api.XXxxxxxxxxxxxxxxxxx/"
  token: string ="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1N";
  language: string="EN";
  file: any;
  datatoken: { token: any; };
  constructor(public http: Http) {

   this.getToken();
  
  }

  ionViewWillEnter(){
  
    }

 
  getTestsecure() {

console.log(this.token)
    let body = 'Authorization=' + 'Bearer '+ this.token ;
    let headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
console.log(body);
    return  this.http.post( this.URL+'secure',body,  {headers: headers}) 
    .map(res=>res.json())
  

}


 getToken() {
      return  this.http.post( this.URL+'token', Headers)
      .map(res=>res.json())
  }
 
register(data) {
  console.log(data);
    return  this.http.post( this.URL+'adduser',data)
    .map(res=>res.json())
}

userexist(data) {
  console.log(data);
    return  this.http.post( this.URL+'userexist',data)
    .map(res=>res.json())
}
//////////////Console//////////////


addsession(data) {
  console.log(data);
    return  this.http.post( this.URL+'addsession',data)
    .map(res=>res.json())
}

updatesession(data) {
  console.log(data);
    return  this.http.post( this.URL+'updatesession',data)
    .map(res=>res.json())
}

//////////////Upload//////////////
upload(body) {
  console.log(body);
    return  this.http.post( this.URL+'upload/',body)
    .map(res=>res.json())
}

uploadlisting(body, id) {
    return  this.http.post( this.URL+'upload/listing/'+ id,body)
    .map(res=>res.json())
}

///////////////// ADMINISTATEURS ///////


getAllUsersadmin(page){
  return this.http.get( this.URL+'usersadmin/'+page)
  .map(res=>res.json());
}
addUseradmin(data) {
    return  this.http.post( this.URL+'adduseradmin',data)
    .map(res=>res.json())
}

deleteuseradmin(data) {
    return  this.http.post( this.URL+'deleteuseradmin',data)
    .map(res=>res.json())
}

userexistadmin(data) {
  console.log(data);
    return  this.http.post( this.URL+'userexistadmin',data)
    .map(res=>res.json())
}


loginadmin(data) {
  console.log(data);
    return  this.http.post( this.URL+'loginadmin',data)
    .map(res=>res.json())
}
login(data) {
  console.log(data);
    return  this.http.post( this.URL+'login',data)
    .map(res=>res.json())
}
loginfacebook(data) {
  console.log(data);
    return  this.http.post( this.URL+'loginfacebook',data)
    .map(res=>res.json())
}

///USER

getUserlocation(lat,lng,distance,page,category){
  console.log(distance);
  return this.http.get( this.URL+'userslocation/'+page+'?lat='+lat+'&lng='+lng+'&distance='+distance+'&category='+category)
  .map(res=>res.json());
}
getAllUsers(page){
  return this.http.get( this.URL+'users/'+page)
  .map(res=>res.json());
}

getAllUsers1(page){
  return this.http.get( this.URL+'users1/'+page)
  .map(res=>res.json());
}

getDetail(para1){
  return this.http.get(this.URL+'user/'+para1)
  .map(res=>res.json());
}
updateuser(data) {
  console.log(data);
    return  this.http.post( this.URL+'updateuser',data)
    .map(res=>res.json())
}
updateuserpassword(data) {
    return  this.http.post( this.URL+'updateuserpassword',data)
    .map(res=>res.json())
}

updateuserlocation(data) {
  console.log(data);
    return  this.http.post( this.URL+'updateuserlocation',data)
    .map(res=>res.json())
}

updateimageprofil(data) {
  console.log(data);
    return  this.http.post( this.URL+'updateimageprofil',data)
    .map(res=>res.json())
}
deleteuser(data) {
  console.log(data);
    return  this.http.post( this.URL+'deleteuser',data)
    .map(res=>res.json())
}
seachUser(page, word){
  return this.http.get( this.URL+'seachUser/'+page +'?word='+word)
  .map(res=>res.json());
}

seachUserid(page, wordid){
  return this.http.get( this.URL+'seachUserid/'+page +'?word='+wordid)
  .map(res=>res.json());
}




/// QUESTIONS


getAllQuestions(page){
  return this.http.get( this.URL+'questions/'+page)
  .map(res=>res.json());
}

getQuestion(para1){
  return this.http.get(this.URL+'question/'+para1)
  .map(res=>res.json());
}
updateQuestion(data) {
  console.log(data);
    return  this.http.post( this.URL+'updatequestion',data)
    .map(res=>res.json())
}
getLastquestion(){
  return this.http.get(this.URL+'lastquestion')
  .map(res=>res.json());
}
addQuestion(data) {
  console.log(data);
    return  this.http.post( this.URL+'addquestion',data)
    .map(res=>res.json())
}
deleteQuestion(data) {
  console.log(data);
    return  this.http.post( this.URL+'deletequestion',data)
    .map(res=>res.json())
}
seachQuestion(page, word){
  return this.http.get( this.URL+'seachQuestion/'+page +'?word='+word)
  .map(res=>res.json());
}
seachQuestionid(page, wordid){
  return this.http.get( this.URL+'seachQuestionid/'+page +'?word='+wordid)
  .map(res=>res.json());
}

//PAGE ORDER QUESTION


Questioncategory(page,category){
  console.log(page);
return this.http.get( this.URL+'Questioncategory/'+page +'?category='+category)
.map(res=>res.json());
}
seachQuestioncategoryword(page,word, category){
  console.log(page);
return this.http.get( this.URL+'seachQuestioncategoryword/'+page +'?word='+word +'&category='+category)
.map(res=>res.json());
}
seachQuestioncategoryid(page,wordid, category){
  console.log(page);
return this.http.get( this.URL+'seachQuestioncategoryid/'+page +'?word='+wordid +'&category='+category)
.map(res=>res.json());
}


/// TESTS

questionbyTest(para1, para2){
  return this.http.get(this.URL+'questionbyTest/'+para1 +'?languageid='+para2)
  .map(res=>res.json());
}

getAllTests(page){
  return this.http.get( this.URL+'tests/'+page)
  .map(res=>res.json());
}

getTest(para1){
  return this.http.get(this.URL+'test/'+para1)
  .map(res=>res.json());
}
updateTest(data) {
  console.log(data);
    return  this.http.post( this.URL+'updatetest',data)
    .map(res=>res.json())
}
deleteTest(data) {
  console.log(data);
    return  this.http.post( this.URL+'deletetest',data)
    .map(res=>res.json())
}

addTest(data) {
  console.log(data);
    return  this.http.post( this.URL+'addtest',data)
    .map(res=>res.json())
}
seachTest(page, word){
  return this.http.get( this.URL+'searchTest/'+page +'?word='+word)
  .map(res=>res.json());
}

seachTestid(page, wordid){
  return this.http.get( this.URL+'searchTestid/'+page +'?word='+wordid)
  .map(res=>res.json());
}

///FK test 


getFktest(para1){
  console.log(para1);
  return this.http.get(this.URL+'fktest/'+para1)
  .map(res=>res.json());
}

addfktest(data) {
  console.log(data);
    return  this.http.post( this.URL+'addfktest',data)
    .map(res=>res.json())
}

deletefktest(data) {
  console.log(data);
    return  this.http.post( this.URL+'deletefktest',data)
    .map(res=>res.json())
}

deletefkidtest(data) {
  console.log(data);
    return  this.http.post( this.URL+'deletefkbyidtest',data)
    .map(res=>res.json())
}
/// PASSATIONS


getAllPassations(page){
  return this.http.get( this.URL+'passations/'+page)
  .map(res=>res.json());
}

getPassation(para1){
  return this.http.get(this.URL+'passation/'+para1)
  .map(res=>res.json());
}
updatePassation(data) {
  console.log(data);
    return  this.http.post( this.URL+'updatepassation',data)
    .map(res=>res.json())
}
deletePassation(data) {
  console.log(data);
    return  this.http.post( this.URL+'deletepassation',data)
    .map(res=>res.json())
}
seachPassation(page, word){
  console.log(page);
  console.log(word);
  return this.http.get( this.URL+'searchpassation/'+page +'?word='+word)
  .map(res=>res.json());
}

seachPassationid(page, wordid){
  return this.http.get( this.URL+'searchpassationid/'+page +'?word='+wordid)
  .map(res=>res.json());
}
addpassation(data) {
  console.log(data);
    return  this.http.post( this.URL+'addpassation',data)
    .map(res=>res.json())
}

getSession(para1){
  console.log(para1);
  return this.http.get(this.URL+'session/'+para1)
  .map(res=>res.json());
}

///FK test 
getFkpass(para1){
  console.log(para1);
  return this.http.get(this.URL+'fkpass/'+para1)
  .map(res=>res.json());
}

addfkpass(data) {
  console.log(data);
    return  this.http.post( this.URL+'addfkpass',data)
    .map(res=>res.json())
}

deletefkpass(data) {
  console.log(data);
    return  this.http.post( this.URL+'deletefkpass',data)
    .map(res=>res.json())
}

deletefkidpass(data) {
  console.log(data);
    return  this.http.post( this.URL+'deletefkbyidpass',data)
    .map(res=>res.json())
}

///ADDresponses
addnewresponse(data) {
  console.log(data);
    return  this.http.post( this.URL+'addnewresponse',data)
    .map(res=>res.json())
}


///Updateresponses
addresponses(data) {
  console.log(data);
    return  this.http.post( this.URL+'addresponse',data)
    .map(res=>res.json())
}


///Listing

getListingLocation(lat,lng,distance,page,category){
  console.log(distance);
  return this.http.get( this.URL+'listinglocation/'+page+'?lat='+lat+'&lng='+lng+'&distance='+distance+'&category='+category)
  .map(res=>res.json());
}
getAllListing(page){
  return this.http.get( this.URL+'listingall/'+page)
  .map(res=>res.json());
}

getListingDetail(para1){
  return this.http.get(this.URL+'listing/'+para1)
  .map(res=>res.json());
}

addlisting(data) {
  console.log(data);
    return  this.http.post( this.URL+'addlisting',data)
    .map(res=>res.json())
}
updatelisting(data) {
  console.log(data);
    return  this.http.post( this.URL+'updatelisting',data)
    .map(res=>res.json())
}

updatelistinglocation(data) {
  console.log(data);
    return  this.http.post( this.URL+'updatelistinglocation',data)
    .map(res=>res.json())
}

updatelistingimage(data) {
  console.log(data);
    return  this.http.post( this.URL+'updatelistingimage',data)
    .map(res=>res.json())
}
deletelisting(data) {
  console.log(data);
    return  this.http.post( this.URL+'deletelisting',data)
    .map(res=>res.json())
}

seachListing(page, word){
  return this.http.get( this.URL+'seachListing/'+page +'?word='+word)
  .map(res=>res.json());
}


///CATEGORY

Test(data){

return this.http.post(this.URL+'token',data)
.map(res=>res.json());
}


getCategory(){

    var headers = new Headers()
   headers.append('Content-Type', 'application/x-www-form-urlencoded');

  let options = new RequestOptions({ headers: headers });
  return this.http.get(this.URL+'category')
  .map(res=>res.json());
}

addCategory (data){
  return this.http.post(this.URL+'addcategory ', data)
  .map(res=>res.json());
}

deleteCategory (data){
  return this.http.post(this.URL+'deletecategory ', data)
  .map(res=>res.json());
}

updateCategory (data){
  return this.http.post(this.URL+'updatecategory ', data)
  .map(res=>res.json());
}


uploadimage(data) {
  console.log(data);
    return  this.http.post( 'http://www.upload.terminalhair.ch',data)
    .map(res=>res.json())
}
///Favoris
favoris(uid){
  console.log(uid);
  return this.http.get(this.URL+'favoris/'+uid)
  .map(res=>res.json());
}

addfavoris(data) {
  console.log(data);
    return  this.http.post( this.URL+'addfavoris',data)
    .map(res=>res.json())
}

deletefavoris(data) {
  console.log(data);
    return  this.http.post( this.URL+'deletefavoris',data)
    .map(res=>res.json())
}

///Rdv
rdv(uid){
  console.log(uid);
  return this.http.get(this.URL+'rdv/'+uid)
  .map(res=>res.json());
}


detailrdv(idrdv){
  return this.http.get(this.URL+'detailrdv/'+idrdv)
  .map(res=>res.json());
}

updaterdv(data) {
  console.log(data);
    return  this.http.post( this.URL+'updaterdv',data)
    .map(res=>res.json())
}

///Rdvpro
rdvpro(uid){
  console.log(uid);
  return this.http.get(this.URL+'rdvpro/'+uid)
  .map(res=>res.json());
}

rdvproagenda(uid){
  console.log(uid);
  return this.http.get(this.URL+'rdvproagenda/'+uid)
  .map(res=>res.json());
}

addrdv(data) {
  console.log(data);
    return  this.http.post( this.URL+'addrdv',data)
    .map(res=>res.json())
}



addservice(data) {
  console.log(data);
    return  this.http.post( this.URL+'addservice',data)
    .map(res=>res.json())
}
  
deleteservice(data) {
  console.log(data);
    return  this.http.post( this.URL+'deleteservice',data)
    .map(res=>res.json())
}


}
