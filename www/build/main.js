webpackJsonp([0],{

/***/ 12:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RedditService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_catch__ = __webpack_require__(260);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_catch__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_toPromise__ = __webpack_require__(262);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_toPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_toPromise__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_observable_of__ = __webpack_require__(263);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_observable_of___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_observable_of__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var RedditService = /** @class */ (function () {
    function RedditService(http) {
        this.http = http;
        this.articlesPage = "10";
        this.URL = "https://api.selectionetconseils.ch/";
        this.token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1N";
        this.language = "EN";
        this.getToken();
    }
    RedditService.prototype.ionViewWillEnter = function () {
    };
    RedditService.prototype.getTestsecure = function () {
        console.log(this.token);
        var body = 'Authorization=' + 'Bearer ' + this.token;
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        console.log(body);
        return this.http.post(this.URL + 'secure', body, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.getToken = function () {
        return this.http.post(this.URL + 'token', __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */])
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.register = function (data) {
        console.log(data);
        return this.http.post(this.URL + 'adduser', data)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.userexist = function (data) {
        console.log(data);
        return this.http.post(this.URL + 'userexist', data)
            .map(function (res) { return res.json(); });
    };
    //////////////Console//////////////
    RedditService.prototype.addsession = function (data) {
        console.log(data);
        return this.http.post(this.URL + 'addsession', data)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.updatesession = function (data) {
        console.log(data);
        return this.http.post(this.URL + 'updatesession', data)
            .map(function (res) { return res.json(); });
    };
    //////////////Upload//////////////
    RedditService.prototype.upload = function (body) {
        console.log(body);
        return this.http.post(this.URL + 'upload/', body)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.uploadlisting = function (body, id) {
        return this.http.post(this.URL + 'upload/listing/' + id, body)
            .map(function (res) { return res.json(); });
    };
    ///////////////// ADMINISTATEURS ///////
    RedditService.prototype.getAllUsersadmin = function (page) {
        return this.http.get(this.URL + 'usersadmin/' + page)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.addUseradmin = function (data) {
        return this.http.post(this.URL + 'adduseradmin', data)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.deleteuseradmin = function (data) {
        return this.http.post(this.URL + 'deleteuseradmin', data)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.userexistadmin = function (data) {
        console.log(data);
        return this.http.post(this.URL + 'userexistadmin', data)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.loginadmin = function (data) {
        console.log(data);
        return this.http.post(this.URL + 'loginadmin', data)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.login = function (data) {
        console.log(data);
        return this.http.post(this.URL + 'login', data)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.loginfacebook = function (data) {
        console.log(data);
        return this.http.post(this.URL + 'loginfacebook', data)
            .map(function (res) { return res.json(); });
    };
    ///USER
    RedditService.prototype.getUserlocation = function (lat, lng, distance, page, category) {
        console.log(distance);
        return this.http.get(this.URL + 'userslocation/' + page + '?lat=' + lat + '&lng=' + lng + '&distance=' + distance + '&category=' + category)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.getAllUsers = function (page) {
        return this.http.get(this.URL + 'users/' + page)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.getAllUsers1 = function (page) {
        return this.http.get(this.URL + 'users1/' + page)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.getDetail = function (para1) {
        return this.http.get(this.URL + 'user/' + para1)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.updateuser = function (data) {
        console.log(data);
        return this.http.post(this.URL + 'updateuser', data)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.updateuserpassword = function (data) {
        return this.http.post(this.URL + 'updateuserpassword', data)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.updateuserlocation = function (data) {
        console.log(data);
        return this.http.post(this.URL + 'updateuserlocation', data)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.updateimageprofil = function (data) {
        console.log(data);
        return this.http.post(this.URL + 'updateimageprofil', data)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.deleteuser = function (data) {
        console.log(data);
        return this.http.post(this.URL + 'deleteuser', data)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.seachUser = function (page, word) {
        return this.http.get(this.URL + 'seachUser/' + page + '?word=' + word)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.seachUserid = function (page, wordid) {
        return this.http.get(this.URL + 'seachUserid/' + page + '?word=' + wordid)
            .map(function (res) { return res.json(); });
    };
    /// QUESTIONS
    RedditService.prototype.getAllQuestions = function (page) {
        return this.http.get(this.URL + 'questions/' + page)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.getQuestion = function (para1) {
        return this.http.get(this.URL + 'question/' + para1)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.updateQuestion = function (data) {
        console.log(data);
        return this.http.post(this.URL + 'updatequestion', data)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.getLastquestion = function () {
        return this.http.get(this.URL + 'lastquestion')
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.addQuestion = function (data) {
        console.log(data);
        return this.http.post(this.URL + 'addquestion', data)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.deleteQuestion = function (data) {
        console.log(data);
        return this.http.post(this.URL + 'deletequestion', data)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.seachQuestion = function (page, word) {
        return this.http.get(this.URL + 'seachQuestion/' + page + '?word=' + word)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.seachQuestionid = function (page, wordid) {
        return this.http.get(this.URL + 'seachQuestionid/' + page + '?word=' + wordid)
            .map(function (res) { return res.json(); });
    };
    //PAGE ORDER QUESTION
    RedditService.prototype.Questioncategory = function (page, category) {
        console.log(page);
        return this.http.get(this.URL + 'Questioncategory/' + page + '?category=' + category)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.seachQuestioncategoryword = function (page, word, category) {
        console.log(page);
        return this.http.get(this.URL + 'seachQuestioncategoryword/' + page + '?word=' + word + '&category=' + category)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.seachQuestioncategoryid = function (page, wordid, category) {
        console.log(page);
        return this.http.get(this.URL + 'seachQuestioncategoryid/' + page + '?word=' + wordid + '&category=' + category)
            .map(function (res) { return res.json(); });
    };
    /// TESTS
    RedditService.prototype.questionbyTest = function (para1, para2) {
        return this.http.get(this.URL + 'questionbyTest/' + para1 + '?languageid=' + para2)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.getAllTests = function (page) {
        return this.http.get(this.URL + 'tests/' + page)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.getTest = function (para1) {
        return this.http.get(this.URL + 'test/' + para1)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.updateTest = function (data) {
        console.log(data);
        return this.http.post(this.URL + 'updatetest', data)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.deleteTest = function (data) {
        console.log(data);
        return this.http.post(this.URL + 'deletetest', data)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.addTest = function (data) {
        console.log(data);
        return this.http.post(this.URL + 'addtest', data)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.seachTest = function (page, word) {
        return this.http.get(this.URL + 'searchTest/' + page + '?word=' + word)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.seachTestid = function (page, wordid) {
        return this.http.get(this.URL + 'searchTestid/' + page + '?word=' + wordid)
            .map(function (res) { return res.json(); });
    };
    ///FK test 
    RedditService.prototype.getFktest = function (para1) {
        console.log(para1);
        return this.http.get(this.URL + 'fktest/' + para1)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.addfktest = function (data) {
        console.log(data);
        return this.http.post(this.URL + 'addfktest', data)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.deletefktest = function (data) {
        console.log(data);
        return this.http.post(this.URL + 'deletefktest', data)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.deletefkidtest = function (data) {
        console.log(data);
        return this.http.post(this.URL + 'deletefkbyidtest', data)
            .map(function (res) { return res.json(); });
    };
    /// PASSATIONS
    RedditService.prototype.getAllPassations = function (page) {
        return this.http.get(this.URL + 'passations/' + page)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.getPassation = function (para1) {
        return this.http.get(this.URL + 'passation/' + para1)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.updatePassation = function (data) {
        console.log(data);
        return this.http.post(this.URL + 'updatepassation', data)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.deletePassation = function (data) {
        console.log(data);
        return this.http.post(this.URL + 'deletepassation', data)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.seachPassation = function (page, word) {
        console.log(page);
        console.log(word);
        return this.http.get(this.URL + 'searchpassation/' + page + '?word=' + word)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.seachPassationid = function (page, wordid) {
        return this.http.get(this.URL + 'searchpassationid/' + page + '?word=' + wordid)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.addpassation = function (data) {
        console.log(data);
        return this.http.post(this.URL + 'addpassation', data)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.getSession = function (para1) {
        console.log(para1);
        return this.http.get(this.URL + 'session/' + para1)
            .map(function (res) { return res.json(); });
    };
    ///FK test 
    RedditService.prototype.getFkpass = function (para1) {
        console.log(para1);
        return this.http.get(this.URL + 'fkpass/' + para1)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.addfkpass = function (data) {
        console.log(data);
        return this.http.post(this.URL + 'addfkpass', data)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.deletefkpass = function (data) {
        console.log(data);
        return this.http.post(this.URL + 'deletefkpass', data)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.deletefkidpass = function (data) {
        console.log(data);
        return this.http.post(this.URL + 'deletefkbyidpass', data)
            .map(function (res) { return res.json(); });
    };
    ///ADDresponses
    RedditService.prototype.addnewresponse = function (data) {
        console.log(data);
        return this.http.post(this.URL + 'addnewresponse', data)
            .map(function (res) { return res.json(); });
    };
    ///Updateresponses
    RedditService.prototype.addresponses = function (data) {
        console.log(data);
        return this.http.post(this.URL + 'addresponse', data)
            .map(function (res) { return res.json(); });
    };
    ///Listing
    RedditService.prototype.getListingLocation = function (lat, lng, distance, page, category) {
        console.log(distance);
        return this.http.get(this.URL + 'listinglocation/' + page + '?lat=' + lat + '&lng=' + lng + '&distance=' + distance + '&category=' + category)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.getAllListing = function (page) {
        return this.http.get(this.URL + 'listingall/' + page)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.getListingDetail = function (para1) {
        return this.http.get(this.URL + 'listing/' + para1)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.addlisting = function (data) {
        console.log(data);
        return this.http.post(this.URL + 'addlisting', data)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.updatelisting = function (data) {
        console.log(data);
        return this.http.post(this.URL + 'updatelisting', data)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.updatelistinglocation = function (data) {
        console.log(data);
        return this.http.post(this.URL + 'updatelistinglocation', data)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.updatelistingimage = function (data) {
        console.log(data);
        return this.http.post(this.URL + 'updatelistingimage', data)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.deletelisting = function (data) {
        console.log(data);
        return this.http.post(this.URL + 'deletelisting', data)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.seachListing = function (page, word) {
        return this.http.get(this.URL + 'seachListing/' + page + '?word=' + word)
            .map(function (res) { return res.json(); });
    };
    ///CATEGORY
    RedditService.prototype.Test = function (data) {
        return this.http.post(this.URL + 'token', data)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.getCategory = function () {
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        var options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this.URL + 'category')
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.addCategory = function (data) {
        return this.http.post(this.URL + 'addcategory ', data)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.deleteCategory = function (data) {
        return this.http.post(this.URL + 'deletecategory ', data)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.updateCategory = function (data) {
        return this.http.post(this.URL + 'updatecategory ', data)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.uploadimage = function (data) {
        console.log(data);
        return this.http.post('http://www.upload.terminalhair.ch', data)
            .map(function (res) { return res.json(); });
    };
    ///Favoris
    RedditService.prototype.favoris = function (uid) {
        console.log(uid);
        return this.http.get(this.URL + 'favoris/' + uid)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.addfavoris = function (data) {
        console.log(data);
        return this.http.post(this.URL + 'addfavoris', data)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.deletefavoris = function (data) {
        console.log(data);
        return this.http.post(this.URL + 'deletefavoris', data)
            .map(function (res) { return res.json(); });
    };
    ///Rdv
    RedditService.prototype.rdv = function (uid) {
        console.log(uid);
        return this.http.get(this.URL + 'rdv/' + uid)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.detailrdv = function (idrdv) {
        return this.http.get(this.URL + 'detailrdv/' + idrdv)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.updaterdv = function (data) {
        console.log(data);
        return this.http.post(this.URL + 'updaterdv', data)
            .map(function (res) { return res.json(); });
    };
    ///Rdvpro
    RedditService.prototype.rdvpro = function (uid) {
        console.log(uid);
        return this.http.get(this.URL + 'rdvpro/' + uid)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.rdvproagenda = function (uid) {
        console.log(uid);
        return this.http.get(this.URL + 'rdvproagenda/' + uid)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.addrdv = function (data) {
        console.log(data);
        return this.http.post(this.URL + 'addrdv', data)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.addservice = function (data) {
        console.log(data);
        return this.http.post(this.URL + 'addservice', data)
            .map(function (res) { return res.json(); });
    };
    RedditService.prototype.deleteservice = function (data) {
        console.log(data);
        return this.http.post(this.URL + 'deleteservice', data)
            .map(function (res) { return res.json(); });
    };
    RedditService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */]])
    ], RedditService);
    return RedditService;
}());

//# sourceMappingURL=reddit-service.js.map

/***/ }),

/***/ 147:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StarttestPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_reddit_service__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__valeurs_valeurs__ = __webpack_require__(267);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__login_login__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_AuthService__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__questionnaire_questionnaire__ = __webpack_require__(359);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__scientifique_scientifique__ = __webpack_require__(360);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__logique_logique__ = __webpack_require__(165);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__verbaldefinition_verbaldefinition__ = __webpack_require__(361);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__verbalelimination_verbalelimination__ = __webpack_require__(362);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__memoire_memoire__ = __webpack_require__(363);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__espace_espace__ = __webpack_require__(364);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__stress_stress__ = __webpack_require__(365);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__imagination_imagination__ = __webpack_require__(366);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__jugement_jugement__ = __webpack_require__(367);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__faciale_faciale__ = __webpack_require__(368);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__verbalsynonyme_verbalsynonyme__ = __webpack_require__(369);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__ngx_translate_core__ = __webpack_require__(53);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






















var StarttestPage = /** @class */ (function () {
    function StarttestPage(loadingController, authService, actionSheetCtrl, toastCtrl, platform, loadingCtrl, navCtrl, storage, redditService, navParams, http, zone, translate) {
        this.loadingController = loadingController;
        this.authService = authService;
        this.actionSheetCtrl = actionSheetCtrl;
        this.toastCtrl = toastCtrl;
        this.platform = platform;
        this.loadingCtrl = loadingCtrl;
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.redditService = redditService;
        this.navParams = navParams;
        this.http = http;
        this.zone = zone;
        this.translate = translate;
        this.maxtime = 30;
        this.charcterstic1 = 0;
        this.timer = 0;
        this.maxTime = 100;
        this.parameter1 = navParams.get('param1');
        this.position = navParams.get('param6');
        this.lg = translate.currentLang;
        switch (this.lg) {
            case this.lg = "fr": {
                this.parameter2 = 2;
                break;
            }
            case this.lg = "de": {
                console.log("allemand break");
                this.parameter2 = 3;
                break;
            }
            case this.lg = "en": {
                console.log("english break");
                this.parameter2 = 1;
                break;
            }
        }
    }
    StarttestPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        console.log(this.parameter2);
        this.redditService.getTest(this.parameter1).subscribe(function (data) {
            _this.data = data.items;
            console.log(data);
            _this.testcategory = data.items[0].test_category;
            _this.testduree = data.items[0].test_duree;
            _this.test_nav = data.items[0].test_nav;
        });
        this.redditService.questionbyTest(this.parameter1, this.parameter2).subscribe(function (data) {
            _this.questions = data.listing;
            console.log("//////////LONGEUR /////////");
            console.log("//////////LONGEUR /////////");
            console.log("//////////LONGEUR /////////");
            console.log("//////////LONGEUR /////////");
            console.log("//////////LONGEUR /////////");
            _this.parameter3 = _this.questions.length;
            console.log(_this.parameter3);
            _this.storeQuestions();
        });
        /*  this.redditService.getFkpass(this.parameter1).subscribe(data=>{
            this.lenghtpassation=data.lenght;
            console.log("--------LENGHT PASSATION--------");
            console.log(this.lenghtpassation);
        })*/
    };
    StarttestPage.prototype.goTest = function () {
        console.log("GO test");
        console.log(this.testcategory);
        /// test choix unique
        if (this.testcategory == 42) {
            console.log("Logique");
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_11__logique_logique__["a" /* LogiquePage */], { param1: this.parameter1, param2: this.parameter3, param4: this.testcategory, param5: this.testduree, param6: this.test_nav, param7: this.position,
            });
        }
        if (this.testcategory == 725) {
            console.log(" Science");
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_10__scientifique_scientifique__["a" /* ScientifiquePage */], { param1: this.parameter1, param2: this.parameter3, param4: this.testcategory, param5: this.testduree, param6: this.test_nav, param7: this.position
            });
        }
        if (this.testcategory == 22) {
            console.log(" Stress");
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_16__stress_stress__["a" /* StressPage */], { param1: this.parameter1, param2: this.parameter3, param4: this.testcategory, param5: this.testduree, param6: this.test_nav, param7: this.position
            });
        }
        if (this.testcategory == 394) {
            console.log(" Memoire");
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_14__memoire_memoire__["a" /* MemoirePage */], { param1: this.parameter1, param2: this.parameter3, param4: this.testcategory, param5: this.testduree, param6: this.test_nav, param7: this.position
            });
        }
        if (this.testcategory == 1654) {
            console.log(" Espace");
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_15__espace_espace__["a" /* EspacePage */], { param1: this.parameter1, param2: this.parameter3, param4: this.testcategory, param5: this.testduree, param6: this.test_nav, param7: this.position
            });
        }
        if (this.testcategory == 1574) {
            console.log(" Verbal synomyme");
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_20__verbalsynonyme_verbalsynonyme__["a" /* VerbalsynonymePage */], { param1: this.parameter1, param2: this.parameter3, param4: this.testcategory, param5: this.testduree, param6: this.test_nav, param7: this.position
            });
        }
        if (this.testcategory == 713) {
            console.log(" Verbal definition");
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_12__verbaldefinition_verbaldefinition__["a" /* VerbaldefinitionPage */], { param1: this.parameter1, param2: this.parameter3, param4: this.testcategory, param5: this.testduree, param6: this.test_nav, param7: this.position
            });
        }
        if (this.testcategory == 142) {
            console.log(" Verbal elimination");
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_13__verbalelimination_verbalelimination__["a" /* VerbaleliminationPage */], { param1: this.parameter1, param2: this.parameter3, param4: this.testcategory, param5: this.testduree, param6: this.test_nav, param7: this.position
            });
        }
        if (this.testcategory == 533) {
            console.log(" Imagination");
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_17__imagination_imagination__["a" /* ImaginationPage */], { param1: this.parameter1, param2: this.parameter3, param4: this.testcategory, param5: this.testduree, param6: this.test_nav, param7: this.position
            });
        }
        if (this.testcategory == 14) {
            console.log(" Valeurs");
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__valeurs_valeurs__["a" /* ValeursPage */], { param1: this.parameter1, param2: this.parameter3, param4: this.testcategory, param5: this.testduree, param6: this.test_nav, param7: this.position,
            });
        }
        if (this.testcategory == 13) {
            console.log(" Jugement");
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_18__jugement_jugement__["a" /* JugementPage */], { param1: this.parameter1, param2: this.parameter3, param4: this.testcategory, param5: this.testduree, param6: this.test_nav, param7: this.position,
            });
        }
        if (this.testcategory == 122) {
            console.log(" Faciale");
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_19__faciale_faciale__["a" /* FacialePage */], { param1: this.parameter1, param2: this.parameter3, param4: this.testcategory, param5: this.testduree, param6: this.test_nav, param7: this.position,
            });
        }
        if (this.testcategory == 255 || this.testcategory == 345 || this.testcategory == 1283 || this.testcategory == 1388 || this.testcategory == 1193 || this.testcategory == 1013 || this.testcategory == 818 || this.testcategory == 923 || this.testcategory == 1103
            || this.testcategory == 1872) {
            console.log("Choix multiples");
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_9__questionnaire_questionnaire__["a" /* QuestionnairePage */], { param1: this.parameter1, param2: this.parameter3, param4: this.testcategory, param5: this.testduree, param6: this.test_nav, param7: this.position,
            });
        }
    };
    StarttestPage.prototype.storeQuestions = function () {
        var usersStringifiedObj = JSON.stringify(this.questions);
        this.storage.set("questions", usersStringifiedObj);
    };
    StarttestPage.prototype.logOut = function () {
        this.authService.logout();
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__login_login__["a" /* Login */]);
    };
    StarttestPage.prototype.timerprogress = function () {
        var _this = this;
        setTimeout(function () {
            _this.charcterstic1 = _this.charcterstic1 + 1;
            console.log(_this.charcterstic1);
        }, 3000);
    };
    StarttestPage.prototype.StartTimer = function () {
        console.log(this.charcterstic1);
        this.timer = this.timer + 1;
        console.log(this.timer);
        console.log(this.charcterstic1);
        if (this.timer <= this.maxTime) {
            setTimeout(function () {
                this.charcterstic1 = this.charcterstic1 + 1;
                this.StartTimer();
            }, 100);
        }
        else {
        }
    };
    StarttestPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["Component"])({
            selector: 'page-starttest',template:/*ion-inline-start:"/Users/Maxime/Desktop/passation/src/pages/starttest/starttest.html"*/'<ion-header>\n    <ion-toolbar  color="secondary" hide-back-button >\n          <ion-title> Valeurs </ion-title>\n          <ion-buttons end>\n            <button ion-button icon-only (click)="logOut()">\n                <ion-icon ios="ios-log-out" md="md-log-out"></ion-icon>\n            </button>\n          </ion-buttons>\n    </ion-toolbar>  \n  </ion-header>\n\n<ion-content color="light2" >\n      \n    <ion-grid >\n        <ion-row responsive-sm  >\n            <ion-col col-2 > \n            </ion-col> \n            <ion-col col-8 >  \n  \n      <ion-list no-lines *ngFor="let item of data">       \n          \n            <ion-card>\n                    <ion-card-header>\n                     <b> {{item.test_label}}</b>\n                    </ion-card-header>\n                    <ion-card-content>\n                    <div [innerHtml]="item.test_intro_fr"  *ngIf="parameter2==2" ></div>\n                    <div [innerHtml]="item.test_intro_gr"  *ngIf="parameter2==3" ></div>\n                    <div [innerHtml]="item.test_intro_en"  *ngIf="parameter2==1" ></div>\n                    </ion-card-content>\n                  </ion-card>\n            \n\n\n                  <button ion-button  full ion-button  full class="bottom" (click)="goTest()">  {{ \'next\' | translate }}</button>\n      </ion-list>\n </ion-col>\n <ion-col col-2 > \n          </ion-col> \n        </ion-row>\n      </ion-grid>\n  </ion-content>\n  \n'/*ion-inline-end:"/Users/Maxime/Desktop/passation/src/pages/starttest/starttest.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5_ionic_angular__["f" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_8__providers_AuthService__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["a" /* ActionSheetController */],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["m" /* ToastController */], __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["k" /* Platform */], __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["f" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_3__providers_reddit_service__["a" /* RedditService */],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_2__angular_core__["NgZone"], __WEBPACK_IMPORTED_MODULE_21__ngx_translate_core__["c" /* TranslateService */]])
    ], StarttestPage);
    return StarttestPage;
}());

//# sourceMappingURL=starttest.js.map

/***/ }),

/***/ 16:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Login; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_reddit_service__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__forgotpassword_forgotpassword__ = __webpack_require__(264);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ts_md5__ = __webpack_require__(265);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ts_md5___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_ts_md5__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_AuthService__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_forms__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__sessions_sessions__ = __webpack_require__(266);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ngx_translate_core__ = __webpack_require__(53);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};













var Login = /** @class */ (function () {
    function Login(translate, loadingController, authService, http, menu, alertCtrl, navCtrl, navParams, redditService, formbuilder, formBuilder, storage) {
        var _this = this;
        this.translate = translate;
        this.loadingController = loadingController;
        this.authService = authService;
        this.http = http;
        this.menu = menu;
        this.alertCtrl = alertCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.redditService = redditService;
        this.formbuilder = formbuilder;
        this.formBuilder = formBuilder;
        this.storage = storage;
        this.email = "";
        this.password = "";
        this.validation_messages = {
            'email': [
                { type: 'required', message: 'Email est requis.' },
                { type: 'pattern', message: 'Entrez un email valide' }
            ],
        };
        this.storage.get('numpassation').then(function (numpass) {
            _this.numpassation = numpass;
        });
        this.lg = translate.currentLang;
    }
    Login.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.menu.enable(false);
        this.validations_form = this.formBuilder.group({
            email: new __WEBPACK_IMPORTED_MODULE_9__angular_forms__["FormControl"]('', __WEBPACK_IMPORTED_MODULE_9__angular_forms__["Validators"].compose([
                __WEBPACK_IMPORTED_MODULE_9__angular_forms__["Validators"].required,
                __WEBPACK_IMPORTED_MODULE_9__angular_forms__["Validators"].pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
            ])),
        });
        this.storage.get('email').then(function (email) {
            _this.email = email;
        });
        this.storage.get('password').then(function (password) {
            _this.password = password;
        });
    };
    Login.prototype.doLogin = function () {
        this.Login(this.email, this.password);
    };
    Login.prototype.Login = function (email, password) {
        var _this = this;
        this.password2 = __WEBPACK_IMPORTED_MODULE_7_ts_md5__["Md5"].hashStr(this.password);
        var data = JSON.stringify({
            email: this.email,
            password: this.password2,
        });
        this.storage.set('email', this.email);
        this.storage.set('password', this.password);
        this.redditService.login(data)
            .toPromise()
            .then(function (response) {
            console.log(response);
            if (response.status == 'success') {
                var loading_1 = _this.loadingController.create({ content: "Chargement..." });
                loading_1.present();
                setTimeout(function () {
                    _this.storage.set('customer_id', response.data[0].customer_id);
                    _this.storage.set('firstname', response.data[0].firstname);
                    _this.storage.set('lastname', response.data[0].lastname);
                    _this.storage.set('genre', response.data[0].genre);
                    _this.storage.set('birthday', response.data[0].birthday);
                    console.log("------NUM PASSATION-------");
                    console.log(_this.numpassation);
                    _this.authService.login();
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_10__sessions_sessions__["a" /* SessionsPage */]);
                    loading_1.dismissAll();
                }, 3000);
            }
            else if (response.status == 'error') {
                var alert_1 = _this.alertCtrl.create({
                    title: 'Erreur',
                    subTitle: 'Identifiant ou mot de passe incorrect .',
                    buttons: ['Fermer']
                });
                alert_1.present();
            }
        });
    };
    ;
    Login.prototype.doSave = function () {
        var _this = this;
        var data = JSON.stringify({
            email: this.emailr,
        });
        console.log(data);
        this.redditService.userexist(data)
            .toPromise()
            .then(function (response) {
            console.log(response);
            if (response.nbrow >= 1) {
                var alert_2 = _this.alertCtrl.create({
                    title: 'Attention !',
                    subTitle: 'L utilisateur existe dejà, vous devez choisir une autre adresse email.',
                    buttons: ['Ok']
                });
                alert_2.present();
            }
            else {
                _this.adduser();
            }
        })
            .catch(function (error) { console.log(error); });
    };
    Login.prototype.adduser = function () {
        var _this = this;
        this.password2 = __WEBPACK_IMPORTED_MODULE_7_ts_md5__["Md5"].hashStr(this.passwordr);
        var data = JSON.stringify({
            userid: this.parameter1,
            lastname: this.lastname,
            firstname: this.firstname,
            email: this.emailr,
            address: this.address,
            city: this.city,
            cp: this.cp,
            genre: this.genre,
            diplome: this.diplome,
            time: 0,
            certificat: this.certificat,
            birthday: this.birthday,
            password: this.password2,
        });
        console.log(data);
        this.redditService.register(data)
            .toPromise()
            .then(function (response) {
            if (response.status == 'success') {
                _this.Login(_this.emailr, _this.passwordr);
            }
        });
    };
    Login.prototype.doForgotpassword = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__forgotpassword_forgotpassword__["a" /* Forgotpassword */]);
    };
    Login.prototype.changeLanguage = function () {
        this.translate.use(this.lg);
    };
    Login = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-login',template:/*ion-inline-start:"/Users/Maxime/Desktop/passation/src/pages/login/login.html"*/'\n    <ion-content scroll="false" >\n\n            <ion-grid class="login-container">\n\n                    <div class="version">  version 1.6</div>\n\n                    <ion-row responsive-sm  >\n\n                            <ion-col col-12 col-12 > \n                 <img src="assets/imgs/logo-admin.png" width="300px" class="logo" item-start  />\n                                </ion-col>\n\n\n                    </ion-row>\n\n                    <ion-row responsive-sm  >\n                         \n                            <ion-col col-12 col-12 >         \n        <section >\n          <ion-list  padding>\n                <h3 style="color: white;">  {{ \'login\' | translate }}</h3>\n                <ion-item>\n                        <ion-label floating> {{ \'email\' | translate }}</ion-label>\n                        <ion-input [(ngModel)] = "email" type="text"></ion-input>\n                    </ion-item>\n                    <ion-item>\n                        <ion-label floating> {{ \'password\' | translate }} </ion-label>\n                        <ion-input [(ngModel)] = "password" type="password"></ion-input>\n                    </ion-item>\n          \n            \n                <button ion-button  full ion-button  full color="red" class="bottom" (click)="doLogin()">{{ \'login\' | translate }}</button>\n                  <br />\n                  \n          <button style="display:none" ion-button block  color="secondary" (click)="doRegister()"> Inscription</button>\n          <br />   \n\n        </ion-list>\n    \n         \n    </section>\n</ion-col>\n<ion-col col-12 col-12 > \n        <h3 style="color: white;">  {{ \'singup\' | translate }}</h3>\n  <ion-item>\n        <ion-label color="primary" stacked> {{ \'lastname\' | translate }} </ion-label>\n      <ion-input type="text" placeholder="{{ \'lastname\' | translate }}" [(ngModel)]="lastname"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label color="primary" stacked>{{ \'firstname\' | translate }} </ion-label>\n        <ion-input type="text" placeholder="{{ \'firstname\' | translate }} " [(ngModel)]="firstname"></ion-input>\n      </ion-item>\n       <ion-item>\n          <ion-label color="primary" stacked> Email  </ion-label>\n         <ion-input type="email" placeholder="Email" [(ngModel)]="emailr"></ion-input>\n       </ion-item>\n       <ion-item>\n          <ion-label color="primary" stacked> {{ \'city\' | translate }}  </ion-label>\n         <ion-input type="text" placeholder="{{ \'city\' | translate }} " [(ngModel)]="city"></ion-input>\n       </ion-item>   \n        <ion-item>\n            <ion-label color="primary" stacked>{{ \'birthday\' | translate }}  </ion-label>\n            <ion-input type="date" placeholder="DD/MM/YYYY" [(ngModel)]="birthday" ></ion-input>\n            </ion-item>\n      \n      \n       <ion-item>\n          <ion-label  color="primary" stacked>{{ \'kind\' | translate }} </ion-label>\n          <ion-select [(ngModel)]="genre">\n            <ion-option value="f">{{ \'women\' | translate }} </ion-option>\n            <ion-option value="m">{{ \'man\' | translate }} </ion-option>\n          </ion-select>\n        </ion-item>\n      \n        <ion-item>\n            <ion-label  color="primary" stacked>Dîplome</ion-label>\n            <ion-select [(ngModel)]="diplome" >\n              <ion-option value="matuspe">Matu spé</ion-option>\n              <ion-option value="matupro">Matu Pro</ion-option>\n              <ion-option value="matugym">Matu gym</ion-option>\n              <ion-option value="autres">{{ \'autres\' | translate }} </ion-option>\n              <ion-option value="titresetrangers">{{ \'titles\' | translate }}</ion-option>\n            </ion-select>\n          </ion-item>\n        \n          <ion-item>\n                <ion-label color="primary" stacked>{{ \'password\' | translate }}</ion-label>\n                <ion-input type="password" placeholder="{{ \'password\' | translate }}" [(ngModel)]="passwordr" ></ion-input>\n                </ion-item>\n            \n              <button ion-button  full ion-button color="red"  (click)="doSave()">{{ \'singup\' | translate }}</button>\n                        \n    </ion-col>\n</ion-row>\n</ion-grid>\n  </ion-content>\n\n  <ion-footer>\n      <ion-toolbar  color="light2"  >\n          <ion-grid>\n              <ion-row>\n                <ion-col col-3>\n            \n                </ion-col>\n                <ion-col col-6>\n                </ion-col>\n                <ion-col col-1>\n                    <p>      {{ \'chooselanguage\' | translate }}</p>\n                  </ion-col>\n                <ion-col col-2>\n                    <ion-item>\n                    <ion-select (ionChange)="changeLanguage()" [(ngModel)]="lg" selected="fr" >\n                        <ion-option  value="fr" >Français</ion-option>\n                        <ion-option  value="de" >Deutsch</ion-option>\n                        <ion-option  value="en" >English</ion-option>\n                    </ion-select>\n                  </ion-item>\n                </ion-col>\n              </ion-row>\n            </ion-grid>\n          </ion-toolbar>\n   </ion-footer>\n\n\n'/*ion-inline-end:"/Users/Maxime/Desktop/passation/src/pages/login/login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_11__ngx_translate_core__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_8__providers_AuthService__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_5__providers_reddit_service__["a" /* RedditService */],
            __WEBPACK_IMPORTED_MODULE_9__angular_forms__["FormBuilder"], __WEBPACK_IMPORTED_MODULE_9__angular_forms__["FormBuilder"], __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */]])
    ], Login);
    return Login;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 164:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login_login__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_AuthService__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, authService) {
        this.navCtrl = navCtrl;
        this.authService = authService;
    }
    HomePage.prototype.Logout = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__login_login__["a" /* Login */]);
    };
    HomePage.prototype.dologout = function () {
        this.authService.login();
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__login_login__["a" /* Login */]);
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-home',template:/*ion-inline-start:"/Users/Maxime/Desktop/passation/src/pages/home/home.html"*/'<ion-header>\n  <ion-toolbar  color="secondary" >\n \n            \n          </ion-toolbar>\n          </ion-header>\n\n<ion-content color="light2" >\n    \n  <ion-grid >\n      <ion-row responsive-sm  >\n          <ion-col col-2 > \n          </ion-col> \n          <ion-col col-8 >  \n\n    <ion-list no-lines >       \n        \n          <ion-card>\n                  <ion-card-header>\n                   <b> {{ \'thk\' | translate }} </b>\n                  </ion-card-header>\n                  <ion-card-content>\n                  \n                  </ion-card-content>\n                </ion-card>\n                <button ion-button  full ion-button  full class="bottom" (click)="Logout()"> {{ \'logout\' | translate }}</button>\n    </ion-list>\n</ion-col>\n<ion-col col-2 > \n        </ion-col> \n      </ion-row>\n    </ion-grid>\n</ion-content>\n'/*ion-inline-end:"/Users/Maxime/Desktop/passation/src/pages/home/home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__providers_AuthService__["a" /* AuthService */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 165:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LogiquePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_add_operator_map__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_reddit_service__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng2_dragula__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__endtest_endtest__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_AuthService__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__login_login__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_forms__ = __webpack_require__(20);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var LogiquePage = /** @class */ (function () {
    function LogiquePage(fb, loadingController, authService, dragulaService, loadingCtrl, navCtrl, storage, redditService, navParams) {
        var _this = this;
        this.loadingController = loadingController;
        this.authService = authService;
        this.dragulaService = dragulaService;
        this.loadingCtrl = loadingCtrl;
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.redditService = redditService;
        this.navParams = navParams;
        this.editing = true;
        this.dataq = [];
        this.indextest = 0;
        this.indexdisplay = 1;
        this.posts2 = [];
        this.checkedIdx = -1;
        this.options = [];
        this.reponse = [];
        this.reponsesave = [];
        this.responsestring = "";
        this.urlimage = "https://www.selectionetconseils.ch/examen/image/";
        this.datareponse = [];
        this.progressPercent = 0;
        this.idtest = navParams.get('param1');
        this.testlenght = navParams.get('param2');
        this.testcategory = navParams.get('param4');
        this.timeInSeconds = navParams.get('param5');
        this.test_nav = navParams.get('param6');
        this.position = navParams.get('param7');
        this.form = fb.group({
            cbox: [false, LogiquePage_1.mustBeTruthy]
        });
        this.storage.get('idresponse').then(function (idresponse) {
            _this.idresponse = idresponse;
        });
    }
    LogiquePage_1 = LogiquePage;
    LogiquePage.prototype.ionViewWillEnter = function () {
        var _this = this;
        if (this.timeInSeconds > 0) {
            this.startTimer();
        }
        this.indexdisplay = 1;
        //Données i=0
        this.storage.get("questions").then(function (users) {
            _this.dataq = JSON.parse(users);
            console.log(_this.dataq);
            _this.nbquestionbytest = _this.dataq.length;
            _this.question_label = _this.dataq[0].question_label;
            _this.q_prop1 = _this.dataq[0].q_prop1;
            _this.q_prop2 = _this.dataq[0].q_prop2;
            _this.q_prop3 = _this.dataq[0].q_prop3;
            _this.q_prop4 = _this.dataq[0].q_prop4;
            _this.q_prop5 = _this.dataq[0].q_prop5;
            _this.q_img1 = _this.urlimage + _this.dataq[0].q_img1;
            _this.q_img2 = _this.urlimage + _this.dataq[0].q_img2;
            _this.q_img3 = _this.urlimage + _this.dataq[0].q_img3;
            _this.q_img4 = _this.urlimage + _this.dataq[0].q_img4;
            _this.q_img5 = _this.urlimage + _this.dataq[0].q_img5;
            _this.q_h = _this.dataq[0].q_h;
            _this.q_f = _this.dataq[0].q_f;
            _this.question_num = _this.dataq[0].question_num;
            _this.options = [_this.q_img1, _this.q_img2, _this.q_img3, _this.q_img4, _this.q_img5];
            _this.checkedIdx = -1;
            //Array reponse
            for (var i = 0; i < _this.nbquestionbytest; i++) {
                _this.reponsesave[i] = -1;
            }
        });
    };
    LogiquePage.prototype.next = function () {
        this.reponsesave[this.indextest] = this.checkedIdx;
        // new question
        this.indextest = this.indextest + 1;
        // toutes les réponses
        console.log("réponse");
        console.log(this.reponsesave);
        if (this.indextest < this.nbquestionbytest) {
            this.indexdisplay = this.indexdisplay + 1;
            this.retrieveQuestions();
        }
        else {
            this.pauseTimer();
            for (var i = 0; i < this.testlenght; i++) {
                if (this.reponsesave[i] == "-1") {
                    this.responsezero = "0";
                }
                else {
                    this.responsezero = this.reponsesave[i] + 1;
                }
                this.responsestring = this.responsestring + this.responsezero;
            }
            /////
            var data = JSON.stringify({
                name: "Logique",
                idresponse: this.idresponse,
                resp: this.responsestring,
            });
            this.redditService.addresponses(data)
                .toPromise()
                .then(function (response) {
                console.log(response);
            });
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__endtest_endtest__["a" /* EndtestPage */], {
                param1: this.idtest,
                param2: this.position,
            });
        }
    };
    LogiquePage.prototype.prev = function () {
        if (this.indextest > 0) {
            this.indextest = this.indextest - 1;
            this.indexdisplay = this.indexdisplay - 1;
            this.reviewQuestions();
        }
        else {
        }
    };
    LogiquePage.prototype.retrieveQuestions = function () {
        var _this = this;
        this.storage.get("questions").then(function (users) {
            _this.dataq = JSON.parse(users);
            _this.question_label = _this.dataq[_this.indextest].question_label;
            _this.propositions = _this.dataq[_this.indextest];
            _this.q_prop1 = _this.dataq[_this.indextest].q_prop1;
            _this.q_prop2 = _this.dataq[_this.indextest].q_prop2;
            _this.q_prop3 = _this.dataq[_this.indextest].q_prop3;
            _this.q_prop4 = _this.dataq[_this.indextest].q_prop4;
            _this.q_prop5 = _this.dataq[_this.indextest].q_prop5;
            _this.q_img1 = _this.urlimage + _this.dataq[_this.indextest].q_img1;
            _this.q_img2 = _this.urlimage + _this.dataq[_this.indextest].q_img2;
            _this.q_img3 = _this.urlimage + _this.dataq[_this.indextest].q_img3;
            _this.q_img4 = _this.urlimage + _this.dataq[_this.indextest].q_img4;
            _this.q_img5 = _this.urlimage + _this.dataq[_this.indextest].q_img5;
            _this.q_h = _this.dataq[_this.indextest].q_h;
            _this.q_f = _this.dataq[_this.indextest].q_f;
            _this.checkedIdx = _this.reponsesave[_this.indextest];
            _this.options = [_this.q_img1, _this.q_img2, _this.q_img3, _this.q_img4, _this.q_img5];
            _this.question_num = _this.dataq[_this.indextest].question_num;
        });
    };
    LogiquePage.prototype.reviewQuestions = function () {
        var _this = this;
        this.storage.get("questions").then(function (users) {
            _this.dataq = JSON.parse(users);
            _this.question_label = _this.dataq[_this.indextest].question_label;
            _this.propositions = _this.dataq[_this.indextest];
            _this.q_prop1 = _this.dataq[_this.indextest].q_prop1;
            _this.q_prop2 = _this.dataq[_this.indextest].q_prop2;
            _this.q_prop3 = _this.dataq[_this.indextest].q_prop3;
            _this.q_prop4 = _this.dataq[_this.indextest].q_prop4;
            _this.q_prop5 = _this.dataq[_this.indextest].q_prop5;
            _this.q_img1 = _this.urlimage + _this.dataq[_this.indextest].q_img1;
            _this.q_img2 = _this.urlimage + _this.dataq[_this.indextest].q_img2;
            _this.q_img3 = _this.urlimage + _this.dataq[_this.indextest].q_img3;
            _this.q_img4 = _this.urlimage + _this.dataq[_this.indextest].q_img4;
            _this.q_img5 = _this.urlimage + _this.dataq[_this.indextest].q_img5;
            _this.q_h = _this.dataq[_this.indextest].q_h;
            _this.q_f = _this.dataq[_this.indextest].q_f;
            _this.checkedIdx = _this.reponsesave[_this.indextest];
            _this.options = [_this.q_img1, _this.q_img2, _this.q_img3, _this.q_img4, _this.q_img5];
            _this.question_num = _this.dataq[_this.indextest].question_num;
        });
    };
    LogiquePage.mustBeTruthy = function (c) {
        var rv = {};
        if (!c.value) {
            rv['notChecked'] = true;
        }
        return rv;
    };
    LogiquePage.prototype.reorderData = function (indexes) {
        this.posts2 = Object(__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["o" /* reorderArray */])(this.posts2, indexes);
        console.log(this.posts2);
        console.log(indexes);
        //this.dataq[this.indextest] = reorderArray(this.dataq[this.indextest], indexes);
        // console.log(this.dataq[this.indextest]);
        // console.log(this.dataq[this.indextest]);
    };
    LogiquePage.prototype.logOut = function () {
        this.authService.logout();
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__login_login__["a" /* Login */]);
    };
    LogiquePage.prototype.ngOnInit = function () {
        this.initTimer();
    };
    LogiquePage.prototype.initTimer = function () {
        this.time = this.timeInSeconds;
        this.runTimer = false;
        this.hasStarted = false;
        this.hasFinished = false;
        this.remainingTime = this.timeInSeconds;
        this.displayTime = this.getSecondsAsDigitalClock(this.remainingTime);
    };
    LogiquePage.prototype.startTimer = function () {
        this.runTimer = true;
        this.hasStarted = true;
        this.timerTick();
    };
    LogiquePage.prototype.pauseTimer = function () {
        this.runTimer = false;
    };
    LogiquePage.prototype.resumeTimer = function () {
        this.startTimer();
    };
    LogiquePage.prototype.timerTick = function () {
        var _this = this;
        setTimeout(function () {
            if (!_this.runTimer) {
                return;
            }
            _this.remainingTime--;
            _this.displayTime = _this.getSecondsAsDigitalClock(_this.remainingTime);
            _this.progressTimer = (_this.remainingTime * 100) / _this.time;
            _this.progressPercent = (100 - _this.progressTimer).toFixed(0);
            if (_this.remainingTime > 0) {
                _this.timerTick();
            }
            else {
                _this.hasFinished = true;
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__endtest_endtest__["a" /* EndtestPage */], {
                    param1: _this.idtest,
                    param2: _this.position,
                });
            }
        }, 1000);
    };
    LogiquePage.prototype.getSecondsAsDigitalClock = function (inputSeconds) {
        var sec_num = parseInt(inputSeconds.toString(), 10); // don't forget the second param
        var hours = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);
        var hoursString = '';
        var minutesString = '';
        var secondsString = '';
        hoursString = (hours < 10) ? "0" + hours : hours.toString();
        minutesString = (minutes < 10) ? "0" + minutes : minutes.toString();
        secondsString = (seconds < 10) ? "0" + seconds : seconds.toString();
        return hoursString + ':' + minutesString + ':' + secondsString;
    };
    LogiquePage = LogiquePage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Component"])({
            providers: [__WEBPACK_IMPORTED_MODULE_5_ng2_dragula__["b" /* DragulaService */]],
            selector: 'page-logique',template:/*ion-inline-start:"/Users/Maxime/Desktop/passation/src/pages/logique/logique.html"*/'<ion-header>\n    <ion-toolbar  color="secondary" hide-back-button >\n          <ion-title>  </ion-title>\n            <h3 text-center *ngIf="displayTime!==0">{{displayTime}}</h3>\n          <ion-buttons end>\n            <button ion-button icon-only (click)="logOut()">\n                <ion-icon ios="ios-log-out" md="md-log-out"></ion-icon>\n            </button>\n          </ion-buttons>\n    </ion-toolbar>  \n  </ion-header>\n  \n  <ion-content color="light2" >\n  \n      <ion-grid >\n    \n          <ion-row responsive-sm  >\n              <ion-col col-2 > \n              </ion-col> \n              <ion-col col-8 >  \n                \n                    <div *ngIf="test_nav==1">\n                            <ion-list >  \n                              <ion-card>\n                                <ion-card-header>\n                                 <b> {{question_label}}</b>\n                                   {{indexdisplay}}/{{nbquestionbytest}}\n                                </ion-card-header>\n                                <ion-card-content>\n                                    <p [innerHTML]="q_h"></p>  \n                                </ion-card-content>\n                              </ion-card>\n                            </ion-list>\n                            <ion-list padding>  \n                              <ion-scroll scrollX="true" style="width:100vw; height:200px" >\n                                  <ion-row >\n                                    <div *ngFor="let item of options; let i=index" style="padding-right: 20px;">\n                                      <img  [src]="item" style="width:100px;height:100px"/>\n                                      <ion-checkbox item-right [ngModel]="checkedIdx == i"  (ngModelChange)="$event ? checkedIdx = i : checkedIdx = -1" [disabled]="checkedIdx >= 0 && checkedIdx != i"></ion-checkbox>\n                                    </div>\n                                  </ion-row>\n                                </ion-scroll>\n                            </ion-list>\n                            <ion-list padding>  \n                                    <ion-row responsive-sm  >\n                                        <ion-col col-2 > \n                                        <button ion-button full (click)="prev()" >\n                                        Précedent\n                                        </button>\n                                        </ion-col> \n                                        <ion-col col-8 >  \n                                        </ion-col> \n                                        <ion-col col-2 > \n                                        <button ion-button  full (click)="next()" >\n                                        Suivant\n                                        </button>\n                                        </ion-col> \n                                    </ion-row>\n                                </ion-list>\n                          </div>\n  \n                <div *ngIf="test_nav==2">\n                        <ion-list >  \n                          <ion-card>\n                            <ion-card-header>\n                             <b> {{question_label}}</b>\n                               {{indexdisplay}}/{{nbquestionbytest}}\n                            </ion-card-header>\n                            <ion-card-content>\n                                <p [innerHTML]="q_h"></p>  \n                            </ion-card-content>\n                          </ion-card>\n                        </ion-list>\n                        <ion-list padding>  \n                          <ion-scroll scrollX="true" style="width:100vw; height:200px" >\n                              <ion-row >\n                                <div *ngFor="let item of options; let i=index" style="padding-right: 20px;">\n                                  <img  [src]="item" style="width:100px;height:100px"/>\n                                  <ion-checkbox item-right [ngModel]="checkedIdx == i"  (ngModelChange)="$event ? checkedIdx = i : checkedIdx = -1" [disabled]="checkedIdx >= 0 && checkedIdx != i"></ion-checkbox>\n                                </div>\n                              </ion-row>\n                            </ion-scroll>\n                        </ion-list>\n                        <ion-list padding>  \n                                <ion-row responsive-sm  >\n                                    <ion-col col-2 > \n                                    </ion-col> \n                                    <ion-col col-8 >  \n                                    </ion-col> \n                                    <ion-col col-2 > \n                                    <button ion-button  full (click)="next()" >\n                                    Suivant\n                                    </button>\n                                    </ion-col> \n                                </ion-row>\n                            </ion-list>\n                      </div>\n                <div *ngIf="test_nav==3">\n                  <ion-list >  \n                    <ion-card>\n                      <ion-card-header>\n                       <b> {{question_label}}</b>\n                         {{indexdisplay}}/{{nbquestionbytest}}\n                      </ion-card-header>\n                      <ion-card-content >\n                          <p [innerHTML]="q_h"></p>  \n                      </ion-card-content>\n                    </ion-card>\n                  </ion-list>\n                  <ion-list padding>  \n                    <ion-scroll scrollX="true" style="width:100vw; height:200px" >\n                        <ion-row >\n                          <div *ngFor="let item of options; let i=index" style="padding-right: 20px;">\n                            <img  [src]="item" style="width:100px;height:100px"/>\n                            <ion-checkbox item-right [ngModel]="checkedIdx == i"  (ngModelChange)="$event ? checkedIdx = i : checkedIdx = -1" [disabled]="checkedIdx >= 0 && checkedIdx != i"></ion-checkbox>\n                          </div>\n                        </ion-row>\n                      </ion-scroll>\n                  </ion-list>\n                  <ion-list padding>  \n                          <ion-row responsive-sm  >\n                              <ion-col col-2 > \n                              <button ion-button full (click)="prev()" [disabled]="checkedIdx == -1 ">\n                              Précedent\n                              </button>\n                              </ion-col> \n                              <ion-col col-8 >  \n                              </ion-col> \n                              <ion-col col-2 > \n                              <button ion-button  full (click)="next()" [disabled]="checkedIdx == -1 ">\n                              Suivant\n                              </button>\n                              </ion-col> \n                          </ion-row>\n                      </ion-list>\n                </div>\n                <div *ngIf="test_nav==4">\n                        <ion-list >  \n                          <ion-card>\n                            <ion-card-header>\n                             <b> {{question_label}}</b>\n                               {{indexdisplay}}/{{nbquestionbytest}}\n                            </ion-card-header>\n                            <ion-card-content>\n                                <p [innerHTML]="q_h"></p>  \n                            </ion-card-content>\n                          </ion-card>\n                        </ion-list>\n                        <ion-list padding>  \n                          <ion-scroll scrollX="true" style="width:100vw; height:200px" >\n                              <ion-row >\n                                <div *ngFor="let item of options; let i=index" style="padding-right: 20px;">\n                                  <img  [src]="item" style="width:100px;height:100px"/>\n                                  <ion-checkbox item-right [ngModel]="checkedIdx == i"  (ngModelChange)="$event ? checkedIdx = i : checkedIdx = -1" [disabled]="checkedIdx >= 0 && checkedIdx != i"></ion-checkbox>\n                                </div>\n                              </ion-row>\n                            </ion-scroll>\n                        </ion-list>\n                        <ion-list padding>  \n                                <ion-row responsive-sm  >\n                                    <ion-col col-2 > \n                                  \n                                    </ion-col> \n                                    <ion-col col-8 >  \n                                    </ion-col> \n                                    <ion-col col-2 > \n                                    <button ion-button  full (click)="next()" [disabled]="checkedIdx == -1 ">\n                                    Suivant\n                                    </button>\n                                    </ion-col> \n                                </ion-row>\n                            </ion-list>\n                      </div>\n\n   </ion-col>\n   <ion-col col-2 > \n        {{question_num}}\n            </ion-col> \n          </ion-row>\n\n \n          <div style="width:70%; text-align: center; margin: 0 auto;" >\n              <div class="progress-outer" style="margin: 1px 1px;padding: 1px;text-align: center;background-color: #cfcfcf;border: 1px solid #dcdcdc;color: #fff;border-radius: 20px;">\n              <div class="progress-inner" [style.width]="progressPercent + \'%\'" style="width: 96%;height:20px;margin: 2px 2px;text-align: center;background-color: #84bdf1;border: 1px solid #dcdcdc;color: #fff;border-radius: 20px;">\n                 {{progressPercent}}%\n              </div>\n              </div>\n              </div> \n\n   \n        </ion-grid>\n    \n    </ion-content>\n    \n  '/*ion-inline-end:"/Users/Maxime/Desktop/passation/src/pages/logique/logique.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_9__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["f" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_7__providers_AuthService__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_5_ng2_dragula__["b" /* DragulaService */], __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["f" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_2__providers_reddit_service__["a" /* RedditService */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["j" /* NavParams */]])
    ], LogiquePage);
    return LogiquePage;
    var LogiquePage_1;
}());

//# sourceMappingURL=logique.js.map

/***/ }),

/***/ 17:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AuthService = /** @class */ (function () {
    function AuthService() {
        this.isLoggedIn = false;
    }
    // Login a user
    // Normally make a server request and store
    // e.g. the auth token
    AuthService.prototype.login = function () {
        this.isLoggedIn = true;
    };
    // Logout a user, destroy token and remove
    // every information related to a user
    AuthService.prototype.logout = function () {
        this.isLoggedIn = false;
    };
    // Returns whether the user is currently authenticated
    // Could check if current token is still valid
    AuthService.prototype.authenticated = function () {
        return this.isLoggedIn;
    };
    AuthService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], AuthService);
    return AuthService;
}());

//# sourceMappingURL=AuthService.js.map

/***/ }),

/***/ 176:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 176;

/***/ }),

/***/ 220:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 220;

/***/ }),

/***/ 264:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Forgotpassword; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_reddit_service__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__login_login__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var Forgotpassword = /** @class */ (function () {
    function Forgotpassword(http, navCtrl, navParams, redditService, storage) {
        this.http = http;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.redditService = redditService;
        this.storage = storage;
        this.email;
    }
    Forgotpassword.prototype.ionViewDidLoad = function () {
    };
    Forgotpassword.prototype.do = function () {
        if (this.email == "") {
            alert("Vous devez ajouter un email valide !");
        }
        else {
            /*
         this.redditService.forgotpassword(this.email).subscribe(data=>{
         this.data=data;
         console.log(data);
         });
         
         */
            alert(" Merci ! Votre nouveau mot de passe a bien été crée. Vérifiez votre boite email");
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__login_login__["a" /* Login */]);
        }
    };
    Forgotpassword = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-forgotpassword',template:/*ion-inline-start:"/Users/Maxime/Desktop/passation/src/pages/forgotpassword/forgotpassword.html"*/'\n<ion-header>\n\n  <ion-navbar color="secondary">\n\n      <div style=" font-size: 15px;padding-left:15px;color:aliceblue;font-weight:bold;">\n          Mot de passe oublié </div>\n\n  </ion-navbar>\n\n</ion-header>\n\n  <ion-content padding>\n\n      <ion-list>\n\n    \n\n      \n          <ion-item>\n              <ion-label floating>Email</ion-label>\n              <ion-input [(ngModel)] = "email" type="text"></ion-input>\n          </ion-item>\n\n      </ion-list>\n      <button ion-button full color="secondary" (click)="do()">Envoyer </button>\n\n\n  </ion-content>\n\n'/*ion-inline-end:"/Users/Maxime/Desktop/passation/src/pages/forgotpassword/forgotpassword.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_5__providers_reddit_service__["a" /* RedditService */], __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */]])
    ], Forgotpassword);
    return Forgotpassword;
}());

//# sourceMappingURL=forgotpassword.js.map

/***/ }),

/***/ 266:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SessionsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_reddit_service__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__starttest_starttest__ = __webpack_require__(147);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_AuthService__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__login_login__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ngx_translate_core__ = __webpack_require__(53);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var SessionsPage = /** @class */ (function () {
    function SessionsPage(translate, navCtrl, authService, popoverCtrl, alertCtrl, menu, loadingController, redditService, storage, navParams, viewCtrl) {
        var _this = this;
        this.translate = translate;
        this.navCtrl = navCtrl;
        this.authService = authService;
        this.popoverCtrl = popoverCtrl;
        this.alertCtrl = alertCtrl;
        this.menu = menu;
        this.loadingController = loadingController;
        this.redditService = redditService;
        this.storage = storage;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.word = "";
        this.wordid = "";
        this.lenghttest = 0;
        this.urlupload = "http://api.selectionetconseils.ch/uploads/";
        this.page = 1;
        this.storage.get('customer_id').then(function (userid) {
            _this.customer_id = userid;
        });
        this.storage.get('lastname').then(function (lastname) {
            _this.lastname = lastname;
        });
        this.storage.get('firstname').then(function (firstname) {
            _this.firstname = firstname;
        });
        this.storage.get('birthday').then(function (birthday) {
            _this.birthday = birthday;
        });
        this.storage.get('genre').then(function (genre) {
            _this.genre = genre;
        });
        this.storage.get('numpassation').then(function (numpass) {
            _this.numpassation = numpass;
            console.log("------Numero de passation sauvegarde -----------");
            console.log(_this.numpassation);
        });
    }
    SessionsPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.page = 1;
        this.storage.get('customer_id').then(function (userid) {
            _this.userid = userid;
            _this.redditService.getSession(_this.userid).subscribe(function (data) {
                _this.session = data.listing;
                console.log(_this.session);
            });
        });
        this.redditService.getAllPassations(this.page).subscribe(function (data) {
            console.log(data);
            _this.pass = data.listing;
        });
    };
    SessionsPage.prototype.doRefresh = function (refresher) {
        var _this = this;
        setTimeout(function () {
            _this.page = 1;
            _this.redditService.getAllPassations(_this.page).subscribe(function (data) {
                _this.posts = data.listing;
                console.log(_this.posts);
                _this.items = data.items;
                _this.pages = data.page;
                _this.currentpage = data.currentpage;
            });
            refresher.complete();
        }, 1100);
    };
    SessionsPage.prototype.goRestore = function (event, item) {
        ////get session order =i
        var _this = this;
        console.log(item.sessionorder);
        this.i = item.sessionorder;
        this.storage.set('sessionorder', 0);
        this.idsession = item.idsession;
        this.storage.set('idsession', this.idsession);
        this.parameter1 = item.passation_num;
        this.redditService.getFkpass(this.numpassation).subscribe(function (data) {
            console.log(data);
            _this.test = data.listing;
            _this.lenghttest = data.length;
            _this.storage.set('longueurpassation', _this.lenghttest);
            _this.idtest = _this.test[_this.i].idtest;
            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__starttest_starttest__["a" /* StarttestPage */], {
                param1: _this.idtest,
                param2: _this.lenghttest,
            });
        });
    };
    SessionsPage.prototype.gotest = function (event, item) {
        var _this = this;
        this.parameter1 = item.passation_num;
        this.redditService.getFkpass(this.numpassation).subscribe(function (data) {
            _this.test = data.listing;
            _this.lenghttest = data.length;
            console.log("longeur passation");
            console.log(_this.lenghttest);
            _this.idtest = _this.test[0].idtest;
            var data3 = JSON.stringify({
                firstname: _this.firstname,
                lastname: _this.lastname,
                idpassation: _this.numpassation,
                genre: _this.genre,
                birthday: _this.birthday,
            });
            _this.redditService.addnewresponse(data3)
                .toPromise()
                .then(function (response) {
                _this.idresponse = response.id;
                _this.storage.set('idresponse', _this.idresponse);
                console.log(response.id);
                var data2 = JSON.stringify({
                    iduser: _this.customer_id,
                    idpassation: _this.numpassation,
                    sessionorder: 0,
                    finished: 0,
                    idresponse: _this.idresponse,
                });
                _this.redditService.addsession(data2)
                    .toPromise()
                    .then(function (response) {
                    _this.storage.set('idsession', response.id);
                    if (response.status == 'success') {
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__starttest_starttest__["a" /* StarttestPage */], {
                            param1: _this.idtest,
                            param2: _this.lenghttest,
                            param6: 0,
                        });
                    }
                });
            });
        });
    };
    SessionsPage.prototype.logOut = function () {
        this.authService.logout();
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__login_login__["a" /* Login */]);
    };
    SessionsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-sessions',template:/*ion-inline-start:"/Users/Maxime/Desktop/passation/src/pages/sessions/sessions.html"*/'<ion-header>\n    <ion-toolbar  color="secondary" hide-back-button >\n          <ion-title> {{ \'session\' | translate }} </ion-title>\n          <ion-buttons end>\n            <button ion-button icon-only (click)="logOut()">\n                <ion-icon ios="ios-log-out" md="md-log-out"></ion-icon>\n            </button>\n          </ion-buttons>\n    </ion-toolbar>  \n</ion-header>\n<ion-content>\n    <ion-grid  >\n      <ion-row responsive-sm  >\n<ion-col col-12 col-2>  </ion-col>\n<ion-col col-12 col-8>  <ion-refresher (ionRefresh)="doRefresh($event)">\n  <ion-refresher-content></ion-refresher-content>\n</ion-refresher>\n\n<ion-list>\n\n   \n    <ion-item *ngFor="let item of pass" >\n        <ion-thumbnail item-start>\n            <img  [src]="item.image" style="width:100px;height:100px"/>\n            </ion-thumbnail>   \n            <b>{{item.title}} </b> {{item.sessionorder}}\n            <h5>{{item.subtitle}}</h5>\n       \n            <div class="buttons" item-right>\n                <button ion-button  (click)="gotest($event, item)"> {{ \'start\' | translate }} </button>\n              </div>\n              </ion-item>\n\n              <ion-item *ngFor="let item of session">\n                    <ion-thumbnail item-start>\n                        <img  [src]="item.image" style="width:100px;height:100px"/>\n                        </ion-thumbnail>   \n                        <b>{{item.title}} {{item.sessionorder}} </b> \n                        <h5> <b>Date de création : {{item.modified| date: \'dd/MM/yyyy\'}} </b></h5>\n                   \n                        <div class="buttons" item-right>\n                            <button ion-button color="green" (click)="goRestore($event, item)"> {{ \'restart\' | translate }} </button>\n                          </div>\n                          </ion-item>\n      </ion-list>\n</ion-col>\n<ion-col col-12 col-2>  </ion-col>\n</ion-row>\n</ion-grid>\n</ion-content>\n  \n  '/*ion-inline-end:"/Users/Maxime/Desktop/passation/src/pages/sessions/sessions.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_7__ngx_translate_core__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_5__providers_AuthService__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* PopoverController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* MenuController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_2__providers_reddit_service__["a" /* RedditService */], __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */]])
    ], SessionsPage);
    return SessionsPage;
}());

//# sourceMappingURL=sessions.js.map

/***/ }),

/***/ 267:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ValeursPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_add_operator_map__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_reddit_service__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng2_dragula__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__endtest_endtest__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_AuthService__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__login_login__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var ValeursPage = /** @class */ (function () {
    function ValeursPage(loadingController, authService, dragulaService, loadingCtrl, navCtrl, storage, redditService, navParams) {
        var _this = this;
        this.loadingController = loadingController;
        this.authService = authService;
        this.dragulaService = dragulaService;
        this.loadingCtrl = loadingCtrl;
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.redditService = redditService;
        this.navParams = navParams;
        this.editing = true;
        this.dataq = [];
        this.indextest = 0;
        this.indexdisplay = 1;
        this.posts2 = [];
        this.nbchecked = 0;
        this.checkedIdx = -1;
        this.options = [];
        this.reponse = [];
        this.reponsesave = [];
        this.reponsetosave = [];
        this.myDataArray = [];
        this.datareponse = [];
        this.responsestring = "";
        this.progressPercent = 0;
        this.idtest = navParams.get('param1');
        this.testlenght = navParams.get('param2');
        this.testcategory = navParams.get('param4');
        this.timeInSeconds = navParams.get('param5');
        this.test_nav = navParams.get('param6');
        this.position = navParams.get('param7');
        this.storage.get('idresponse').then(function (idresponse) {
            _this.idresponse = idresponse;
        });
        this.storage.get("questions").then(function (users) {
            _this.dataq = JSON.parse(users);
            for (var i = 0; i < _this.dataq.length; i++) {
                _this.question_label = _this.dataq[i].question_label;
                _this.propositions = _this.dataq[i];
                _this.q_prop1 = _this.dataq[i].q_prop1;
                _this.q_prop2 = _this.dataq[i].q_prop2;
                _this.q_prop3 = _this.dataq[i].q_prop3;
                _this.q_prop4 = _this.dataq[i].q_prop4;
                _this.q_prop5 = _this.dataq[i].q_prop5;
                _this.q_h = _this.dataq[i].q_h;
                _this.q_f = _this.dataq[i].q_f;
                _this.checkedIdx = _this.reponsesave[i];
                _this.question_num = _this.dataq[i].question_num;
                _this.options = [{ id: '2', prop: _this.q_prop2, order: 2, isChecked: false }, { id: '5', prop: _this.q_prop5, order: 5, isChecked: false },
                    { id: '3', prop: _this.q_prop3, order: 3, isChecked: false }, { id: '1', prop: _this.q_prop1, order: 1, isChecked: false }, { id: '4', prop: _this.q_prop4, order: 4, isChecked: false }];
                _this.datareponse.push(_this.options);
                console.log(_this.datareponse);
            }
        });
    }
    ValeursPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        if (this.timeInSeconds > 0) {
            this.startTimer();
        }
        this.indexdisplay = 1;
        //Données i=0
        this.storage.get("questions").then(function (users) {
            _this.dataq = JSON.parse(users);
            _this.nbquestionbytest = _this.dataq.length;
            _this.question_label = _this.dataq[0].question_label;
            _this.q_h = _this.dataq[0].q_h;
            _this.q_f = _this.dataq[0].q_f;
            _this.question_num = _this.dataq[0].question_num;
            _this.datarep = _this.datareponse[0];
        });
    };
    ValeursPage.prototype.next = function () {
        for (var i = 0; i < this.datareponse[this.indextest]; i++) {
            this.datareponse.push(this.datareponse[this.indextest]);
        }
        // new question
        this.indextest = this.indextest + 1;
        if (this.indextest < this.nbquestionbytest) {
            this.indexdisplay = this.indexdisplay + 1;
            this.retrieveQuestions();
        }
        else {
            this.pauseTimer();
            ////// SAVE DATA
            console.log(this.testlenght);
            for (var i = 0; i <= this.testlenght; i++) {
                this.responsezero = this.datareponse[i][0].id;
                this.responsestring = this.responsestring + this.responsezero;
            }
            /////
            var data = JSON.stringify({
                name: "Projections",
                idresponse: this.idresponse,
                resp: this.responsestring,
            });
            this.redditService.addresponses(data)
                .toPromise()
                .then(function (response) {
                console.log(response);
            });
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__endtest_endtest__["a" /* EndtestPage */], {
                param1: this.idtest,
                param2: this.position,
            });
        }
    };
    ValeursPage.prototype.prev = function () {
        if (this.indextest > 0) {
            this.indextest = this.indextest - 1;
            this.indexdisplay = this.indexdisplay - 1;
            this.reviewQuestions();
        }
        else {
        }
    };
    ValeursPage.prototype.retrieveQuestions = function () {
        var _this = this;
        this.storage.get("questions").then(function (users) {
            _this.dataq = JSON.parse(users);
            _this.question_label = _this.dataq[_this.indextest].question_label;
            _this.q_h = _this.dataq[_this.indextest].q_h;
            _this.q_f = _this.dataq[_this.indextest].q_f;
            _this.question_num = _this.dataq[_this.indextest].question_num;
            _this.datarep = _this.datareponse[_this.indextest];
            _this.nbchecked = 0;
            for (var i = 0; i < 5; i++) {
                if (_this.datarep[i].isChecked == true) {
                    _this.nbchecked = _this.nbchecked + 1;
                }
            }
        });
    };
    ValeursPage.prototype.reviewQuestions = function () {
        var _this = this;
        this.storage.get("questions").then(function (users) {
            _this.dataq = JSON.parse(users);
            _this.question_label = _this.dataq[_this.indextest].question_label;
            _this.q_h = _this.dataq[_this.indextest].q_h;
            _this.q_f = _this.dataq[_this.indextest].q_f;
            _this.question_num = _this.dataq[_this.indextest].question_num;
            _this.datarep = _this.datareponse[_this.indextest];
            // nb true reponse
            _this.nbchecked = 0;
            for (var i = 0; i < 5; i++) {
                if (_this.datarep[i].isChecked == true) {
                    _this.nbchecked = _this.nbchecked + 1;
                }
            }
        });
    };
    ValeursPage.mustBeTruthy = function (c) {
        var rv = {};
        if (!c.value) {
            rv['notChecked'] = true;
        }
        return rv;
    };
    ValeursPage.prototype.reorderData = function (indexes) {
        this.datarep = Object(__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["o" /* reorderArray */])(this.datarep, indexes);
        if (this.datarep[indexes.to].isChecked == false) {
            this.datarep[indexes.to].isChecked = true;
            this.nbchecked = this.nbchecked + 1;
        }
    };
    ValeursPage.prototype.logOut = function () {
        this.authService.logout();
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__login_login__["a" /* Login */]);
    };
    ValeursPage.prototype.ngOnInit = function () {
        this.initTimer();
    };
    ValeursPage.prototype.initTimer = function () {
        this.time = this.timeInSeconds;
        this.runTimer = false;
        this.hasStarted = false;
        this.hasFinished = false;
        this.remainingTime = this.timeInSeconds;
        this.displayTime = this.getSecondsAsDigitalClock(this.remainingTime);
        this.progressTimer = (this.remainingTime * 100) / this.time;
        this.progressPercent = (100 - this.progressTimer).toFixed(0);
    };
    ValeursPage.prototype.startTimer = function () {
        this.runTimer = true;
        this.hasStarted = true;
        this.timerTick();
    };
    ValeursPage.prototype.pauseTimer = function () {
        this.runTimer = false;
    };
    ValeursPage.prototype.resumeTimer = function () {
        this.startTimer();
    };
    ValeursPage.prototype.timerTick = function () {
        var _this = this;
        setTimeout(function () {
            if (!_this.runTimer) {
                return;
            }
            _this.remainingTime--;
            _this.displayTime = _this.getSecondsAsDigitalClock(_this.remainingTime);
            if (_this.remainingTime > 0) {
                _this.timerTick();
            }
            else {
                _this.hasFinished = true;
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__endtest_endtest__["a" /* EndtestPage */], {
                    param1: _this.idtest,
                    param2: _this.position,
                });
            }
        }, 1000);
    };
    ValeursPage.prototype.getSecondsAsDigitalClock = function (inputSeconds) {
        var sec_num = parseInt(inputSeconds.toString(), 10); // don't forget the second param
        var hours = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);
        var hoursString = '';
        var minutesString = '';
        var secondsString = '';
        hoursString = (hours < 10) ? "0" + hours : hours.toString();
        minutesString = (minutes < 10) ? "0" + minutes : minutes.toString();
        secondsString = (seconds < 10) ? "0" + seconds : seconds.toString();
        return hoursString + ':' + minutesString + ':' + secondsString;
    };
    ValeursPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Component"])({
            providers: [__WEBPACK_IMPORTED_MODULE_5_ng2_dragula__["b" /* DragulaService */]],
            selector: 'page-valeurs',template:/*ion-inline-start:"/Users/Maxime/Desktop/passation/src/pages/valeurs/valeurs.html"*/'<ion-header>\n  <ion-toolbar  color="secondary" hide-back-button >\n\n        <ion-buttons end>\n          <button ion-button icon-only (click)="logOut()">\n              <ion-icon ios="ios-log-out" md="md-log-out"></ion-icon>\n          </button>\n        </ion-buttons>\n  </ion-toolbar>  \n</ion-header>\n\n<ion-content color="light2" >\n      \n    <ion-grid >\n        <ion-row responsive-sm  >\n            <ion-col col-2 > \n            </ion-col> \n            <ion-col col-8 >  \n              \n                <div *ngIf="test_nav==1">\n                    <ion-list >     \n                      <ion-card>\n                        <ion-card-header>\n                         <b> {{question_label}}</b>\n                         {{indexdisplay}}/{{nbquestionbytest}}\n                        </ion-card-header>\n                        <ion-card-content>\n                            <p [innerHTML]="q_h"></p>  \n                        </ion-card-content>\n                      </ion-card>\n                    </ion-list>\n                       \n                    <ion-list padding>   \n                        <ion-item-group [reorder]="editing" (ionItemReorder)="reorderData($event)" >\n                                <ion-item *ngFor="let item of datarep " [color]="item.isChecked ? \'blue\' : \'secondary\'"  >\n                                  <b>{{item.prop}}</b>\n                                </ion-item>\n                              </ion-item-group>\n                 </ion-list>\n                    <ion-list padding>  \n                        <ion-row responsive-sm  >\n                            <ion-col col-2 > \n                            <button ion-button full (click)="prev()">\n                                    {{ \'prev\' | translate }}\n                            </button>\n                            </ion-col> \n                            <ion-col col-8 >  \n                            </ion-col> \n                            <ion-col col-2 > \n                            <button ion-button  full (click)="next()">\n                                    {{ \'next\' | translate }}\n                            </button>\n                            </ion-col> \n                        </ion-row>\n                    </ion-list>\n                  </div>\n                  <div *ngIf="test_nav==2">\n                      <ion-list >     \n                        <ion-card>\n                          <ion-card-header>\n                           <b> {{question_label}}</b>\n                           {{indexdisplay}}/{{nbquestionbytest}}\n                          </ion-card-header>\n                          <ion-card-content>\n                              <p [innerHTML]="q_h"></p>  \n                          </ion-card-content>\n                        </ion-card>\n                      </ion-list>\n                         \n                      <ion-list padding>   \n                          <ion-item-group [reorder]="editing" (ionItemReorder)="reorderData($event)" >\n                                  <ion-item *ngFor="let item of datarep " [color]="item.isChecked ? \'blue\' : \'secondary\'" >\n                                    <b>{{item.prop}}</b>\n                                  </ion-item>\n                                </ion-item-group>\n                   </ion-list>\n                      <ion-list padding>  \n                          <ion-row responsive-sm  >\n                              <ion-col col-2 > \n                           \n                              </ion-col> \n                              <ion-col col-8 >  \n                              </ion-col> \n                              <ion-col col-2 > \n                              <button ion-button  full (click)="next()" >\n                                    {{ \'next\' | translate }}\n                              </button>\n                              </ion-col> \n                          </ion-row>\n                      </ion-list>\n                    </div>\n                    <div *ngIf="test_nav==3">\n                        <ion-list >     \n                          <ion-card>\n                            <ion-card-header>\n                             <b> {{question_label}}</b>\n                             {{indexdisplay}}/{{nbquestionbytest}}\n                            </ion-card-header>\n                            <ion-card-content>\n                                <p [innerHTML]="q_h"></p>  \n                            </ion-card-content>\n                          </ion-card>\n                        </ion-list>\n                           \n                        <ion-list padding>   \n                            <ion-item-group [reorder]="editing" (ionItemReorder)="reorderData($event)" >\n                                    <ion-item *ngFor="let item of datarep " [color]="item.isChecked ? \'blue\' : \'secondary\'" >\n                                      <b>{{item.prop}}</b>\n                                    </ion-item>\n                                  </ion-item-group>\n                     </ion-list>\n                        <ion-list padding>  \n                            <ion-row responsive-sm  >\n                                <ion-col col-2 > \n                                <button ion-button full (click)="prev()"[disabled]="nbchecked < 5">\n                                        {{ \'prev\' | translate }}\n                                </button>\n                                </ion-col> \n                                <ion-col col-8 >  \n                                </ion-col> \n                                <ion-col col-2 > \n                                <button ion-button  full (click)="next()" [disabled]="nbchecked < 5">\n                                        {{ \'next\' | translate }}\n                                </button>\n                                </ion-col> \n                            </ion-row>\n                        </ion-list>\n                      </div>\n                      <div *ngIf="test_nav==4">\n                          <ion-list >     \n                            <ion-card>\n                              <ion-card-header>\n                               <b> {{question_label}}</b>\n                               {{indexdisplay}}/{{nbquestionbytest}}\n                              </ion-card-header>\n                              <ion-card-content>\n                                  <p [innerHTML]="q_h"></p>  \n                              </ion-card-content>\n                            </ion-card>\n                          </ion-list>\n                             \n                          <ion-list padding>   \n                              <ion-item-group [reorder]="editing" (ionItemReorder)="reorderData($event)" >\n                                      <ion-item *ngFor="let item of datarep " [color]="item.isChecked ? \'blue\' : \'secondary\'" >\n                                        <b>{{item.prop}}</b>\n                                      </ion-item>\n                                    </ion-item-group>\n                       </ion-list>\n                          <ion-list padding>  \n                              <ion-row responsive-sm  >\n                                  <ion-col col-2 > \n                              \n                                  </ion-col> \n                                  <ion-col col-8 >  \n                                  </ion-col> \n                                  <ion-col col-2 > \n                                  <button ion-button  full (click)="next()" [disabled]="nbchecked < 5">\n                                        {{ \'next\' | translate }}\n                                  </button>\n                                  </ion-col> \n                              </ion-row>\n                          </ion-list>\n                        </div>\n </ion-col>\n <ion-col col-2 > \n    {{question_num}}\n          </ion-col> \n        </ion-row>\n\n        <div style="width:70%; text-align: center; margin: 0 auto;" >\n                <div class="progress-outer" style="margin: 1px 1px;padding: 1px;text-align: center;background-color: #cfcfcf;border: 1px solid #dcdcdc;color: #fff;border-radius: 20px;">\n                <div class="progress-inner" [style.width]="progressPercent + \'%\'" style="width: 96%;height:20px;margin: 2px 2px;text-align: center;background-color: #84bdf1;border: 1px solid #dcdcdc;color: #fff;border-radius: 20px;">\n                   {{progressPercent}}%\n                </div>\n                </div>\n                </div> \n      </ion-grid>\n  </ion-content>\n  \n'/*ion-inline-end:"/Users/Maxime/Desktop/passation/src/pages/valeurs/valeurs.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["f" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_7__providers_AuthService__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_5_ng2_dragula__["b" /* DragulaService */], __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["f" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_2__providers_reddit_service__["a" /* RedditService */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["j" /* NavParams */]])
    ], ValeursPage);
    return ValeursPage;
}());

//# sourceMappingURL=valeurs.js.map

/***/ }),

/***/ 29:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EndtestPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_reddit_service__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__starttest_starttest__ = __webpack_require__(147);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__home_home__ = __webpack_require__(164);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__login_login__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_AuthService__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ngx_translate_core__ = __webpack_require__(53);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var EndtestPage = /** @class */ (function () {
    function EndtestPage(loadingController, authService, actionSheetCtrl, toastCtrl, platform, loadingCtrl, navCtrl, storage, redditService, navParams, http, zone, translate) {
        var _this = this;
        this.loadingController = loadingController;
        this.authService = authService;
        this.actionSheetCtrl = actionSheetCtrl;
        this.toastCtrl = toastCtrl;
        this.platform = platform;
        this.loadingCtrl = loadingCtrl;
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.redditService = redditService;
        this.navParams = navParams;
        this.http = http;
        this.zone = zone;
        this.translate = translate;
        this.parameter1 = navParams.get('param1');
        this.position = navParams.get('param2');
        this.storage.get('customer_id').then(function (userid) {
            _this.customer_id = userid;
            console.log(_this.numpassation);
        });
        this.storage.get('numpassation').then(function (numpass) {
            _this.numpassation = numpass;
            console.log(_this.numpassation);
        });
        this.storage.get('longueurpassation').then(function (longpass) {
            _this.longueurpassation = longpass;
            console.log("----------  longueur passation -------");
            console.log(_this.longueurpassation);
        });
        this.storage.get('idsession').then(function (sesid) {
            _this.idsession = sesid;
            console.log(_this.idsession);
        });
        this.lg = translate.currentLang;
        switch (this.lg) {
            case this.lg = "fr": {
                this.parameter2 = 2;
                break;
            }
            case this.lg = "de": {
                console.log("allemand break");
                this.parameter2 = 3;
                break;
            }
            case this.lg = "en": {
                console.log("english break");
                this.parameter2 = 1;
                break;
            }
        }
    }
    EndtestPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.redditService.getTest(this.parameter1).subscribe(function (data) {
            console.log(data);
            _this.data = data.items;
            _this.test_final = data.items[0].test_final_fr;
        });
    };
    EndtestPage.prototype.saveSession = function () {
        var _this = this;
        console.log("Longueur passation");
        console.log(this.longueurpassation);
        console.log("Session  NUMERO ACTUEL !!!!!!!!!!!");
        console.log(this.position);
        if (this.longueurpassation >= this.position) {
            this.position = this.position + 1;
            var data = JSON.stringify({
                idsession: this.idsession,
                sessionorder: this.position,
                finished: 0,
            });
            this.redditService.updatesession(data)
                .toPromise()
                .then(function (response) {
                if (response.status == 'success') {
                    console.log("session enregistré - test suivant ");
                    _this.redditService.getFkpass(_this.numpassation).subscribe(function (data) {
                        _this.test = data.listing;
                        _this.lenghttest = data.length;
                        _this.storage.set('longueurpassation', _this.lenghttest);
                        _this.idtest = _this.test[_this.position].idtest;
                        console.log("id nouveau test");
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__starttest_starttest__["a" /* StarttestPage */], {
                            param1: _this.idtest,
                            param2: _this.lenghttest,
                            param6: _this.position,
                        });
                    });
                }
            });
        }
        else {
            console.log("FIN DE PASSATION BRAVO ");
            var data = JSON.stringify({
                idsession: this.idsession,
                sessionorder: this.sessionorder,
                finished: 1,
            });
            this.redditService.updatesession(data)
                .toPromise()
                .then(function (response) {
                if (response.status == 'success') {
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__home_home__["a" /* HomePage */]);
                }
            });
        }
    };
    EndtestPage.prototype.logOut = function () {
        this.authService.logout();
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__login_login__["a" /* Login */]);
    };
    EndtestPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["Component"])({
            selector: 'page-endtest',template:/*ion-inline-start:"/Users/Maxime/Desktop/passation/src/pages/endtest /endtest.html"*/'<ion-header>\n        <ion-toolbar  color="secondary" hide-back-button >\n              <ion-buttons end>\n                <button ion-button icon-only (click)="logOut()">\n                    <ion-icon ios="ios-log-out" md="md-log-out"></ion-icon>\n                </button>\n              </ion-buttons>\n        </ion-toolbar>  \n      </ion-header>\n\n<ion-content color="light2" >\n      \n    <ion-grid >\n        <ion-row responsive-sm  >\n            <ion-col col-2 > \n            </ion-col> \n            <ion-col col-8 >  \n  \n      <ion-list no-lines *ngFor="let item of data">       \n          \n            <ion-card>\n                    <ion-card-header>\n                     <b> {{item.test_label}}</b>\n                    </ion-card-header>\n                    <ion-card-content>\n                        <div [innerHtml]="item.test_final_fr"  *ngIf="parameter2==2" ></div>\n                        <div [innerHtml]="item.test_final_gr"  *ngIf="parameter2==3" ></div>\n                        <div [innerHtml]="item.test_final_en"  *ngIf="parameter2==1" ></div>\n                    </ion-card-content>\n                  </ion-card>\n                  <button ion-button  full ion-button  full class="bottom" (click)="saveSession()"> {{ \'next\' | translate }}</button>\n      </ion-list>\n </ion-col>\n <ion-col col-2 > \n          </ion-col> \n        </ion-row>\n      </ion-grid>\n  </ion-content>\n  \n'/*ion-inline-end:"/Users/Maxime/Desktop/passation/src/pages/endtest /endtest.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5_ionic_angular__["f" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_9__providers_AuthService__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["a" /* ActionSheetController */],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["m" /* ToastController */], __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["k" /* Platform */], __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["f" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_3__providers_reddit_service__["a" /* RedditService */],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_2__angular_core__["NgZone"], __WEBPACK_IMPORTED_MODULE_10__ngx_translate_core__["c" /* TranslateService */]])
    ], EndtestPage);
    return EndtestPage;
}());

//# sourceMappingURL=endtest.js.map

/***/ }),

/***/ 359:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return QuestionnairePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_add_operator_map__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_reddit_service__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng2_dragula__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__endtest_endtest__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_AuthService__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__login_login__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_forms__ = __webpack_require__(20);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var QuestionnairePage = /** @class */ (function () {
    function QuestionnairePage(fb, loadingController, authService, dragulaService, loadingCtrl, navCtrl, storage, redditService, navParams) {
        var _this = this;
        this.loadingController = loadingController;
        this.authService = authService;
        this.dragulaService = dragulaService;
        this.loadingCtrl = loadingCtrl;
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.redditService = redditService;
        this.navParams = navParams;
        this.editing = true;
        this.dataq = [];
        this.indextest = 0;
        this.indexdisplay = 1;
        this.posts2 = [];
        this.checkedIdx = -1;
        this.options = [];
        this.reponse = [];
        this.reponsesave = [];
        this.responsestring = "";
        this.response1 = "";
        this.response2 = "";
        this.response3 = "";
        this.response4 = "";
        this.response5 = "";
        this.progressPercent = 0;
        this.idtest = navParams.get('param1');
        this.testlenght = navParams.get('param2');
        this.testcategory = navParams.get('param4');
        this.timeInSeconds = navParams.get('param5');
        this.test_nav = navParams.get('param6');
        this.position = navParams.get('param7');
        this.storage.get('idresponse').then(function (idresponse) {
            _this.idresponse = idresponse;
        });
        this.form = fb.group({
            cbox: [false, QuestionnairePage_1.mustBeTruthy]
        });
    }
    QuestionnairePage_1 = QuestionnairePage;
    QuestionnairePage.prototype.ionViewWillEnter = function () {
        var _this = this;
        if (this.timeInSeconds > 0) {
            this.startTimer();
        }
        this.indexdisplay = 1;
        //Données i=0
        this.storage.get("questions").then(function (users) {
            _this.dataq = JSON.parse(users);
            _this.nbquestionbytest = _this.dataq.length;
            _this.question_label = _this.dataq[0].question_label;
            _this.q_prop1 = _this.dataq[0].q_prop1;
            _this.q_prop2 = _this.dataq[0].q_prop2;
            _this.q_prop3 = _this.dataq[0].q_prop3;
            _this.q_prop4 = _this.dataq[0].q_prop4;
            _this.q_prop5 = _this.dataq[0].q_prop5;
            _this.q_h = _this.dataq[0].q_h;
            _this.q_f = _this.dataq[0].q_f;
            _this.options = [_this.q_prop1, _this.q_prop2, _this.q_prop3, _this.q_prop4, _this.q_prop5];
            _this.question_num = _this.dataq[0].question_num;
            _this.checkedIdx = -1;
            //Array reponse
            for (var i = 0; i < _this.nbquestionbytest; i++) {
                _this.reponsesave[i] = -1;
            }
        });
    };
    QuestionnairePage.prototype.next = function () {
        this.reponsesave[this.indextest] = this.checkedIdx;
        // new question
        this.indextest = this.indextest + 1;
        // toutes les réponses
        console.log("réponse");
        console.log(this.reponsesave);
        if (this.indextest < this.nbquestionbytest) {
            this.indexdisplay = this.indexdisplay + 1;
            this.retrieveQuestions();
        }
        else {
            this.pauseTimer();
            ////// SAVE DATA
            for (var i = 0; i < this.testlenght; i++) {
                this.responsezero = this.reponsesave[i];
                this.responsestring = this.responsestring + this.responsezero;
            }
            /////
            var data = JSON.stringify({
                name: "questionnaire",
                idresponse: this.idresponse,
                resp: this.responsestring,
            });
            console.log(data);
            this.redditService.addresponses(data)
                .toPromise()
                .then(function (response) {
                console.log(response);
            });
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__endtest_endtest__["a" /* EndtestPage */], {
                param1: this.idtest,
                param2: this.position,
            });
        }
    };
    QuestionnairePage.prototype.prev = function () {
        if (this.indextest > 0) {
            this.indextest = this.indextest - 1;
            this.indexdisplay = this.indexdisplay - 1;
            this.reviewQuestions();
        }
        else {
        }
    };
    QuestionnairePage.prototype.retrieveQuestions = function () {
        var _this = this;
        this.storage.get("questions").then(function (users) {
            _this.dataq = JSON.parse(users);
            _this.question_label = _this.dataq[_this.indextest].question_label;
            _this.propositions = _this.dataq[_this.indextest];
            _this.q_prop1 = _this.dataq[_this.indextest].q_prop1;
            _this.q_prop2 = _this.dataq[_this.indextest].q_prop2;
            _this.q_prop3 = _this.dataq[_this.indextest].q_prop3;
            _this.q_prop4 = _this.dataq[_this.indextest].q_prop4;
            _this.q_prop5 = _this.dataq[_this.indextest].q_prop5;
            _this.q_h = _this.dataq[_this.indextest].q_h;
            _this.q_f = _this.dataq[_this.indextest].q_f;
            _this.checkedIdx = _this.reponsesave[_this.indextest];
            _this.question_num = _this.dataq[_this.indextest].question_num;
        });
    };
    QuestionnairePage.prototype.reviewQuestions = function () {
        var _this = this;
        this.storage.get("questions").then(function (users) {
            _this.dataq = JSON.parse(users);
            _this.question_label = _this.dataq[_this.indextest].question_label;
            _this.propositions = _this.dataq[_this.indextest];
            _this.q_prop1 = _this.dataq[_this.indextest].q_prop1;
            _this.q_prop2 = _this.dataq[_this.indextest].q_prop2;
            _this.q_prop3 = _this.dataq[_this.indextest].q_prop3;
            _this.q_prop4 = _this.dataq[_this.indextest].q_prop4;
            _this.q_prop5 = _this.dataq[_this.indextest].q_prop5;
            _this.q_h = _this.dataq[_this.indextest].q_h;
            _this.q_f = _this.dataq[_this.indextest].q_f;
            _this.checkedIdx = _this.reponsesave[_this.indextest];
            _this.question_num = _this.dataq[_this.indextest].question_num;
        });
    };
    QuestionnairePage.mustBeTruthy = function (c) {
        var rv = {};
        if (!c.value) {
            rv['notChecked'] = true;
        }
        return rv;
    };
    QuestionnairePage.prototype.reorderData = function (indexes) {
        this.posts2 = Object(__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["o" /* reorderArray */])(this.posts2, indexes);
        console.log(this.posts2);
        console.log(indexes);
        //this.dataq[this.indextest] = reorderArray(this.dataq[this.indextest], indexes);
        // console.log(this.dataq[this.indextest]);
        // console.log(this.dataq[this.indextest]);
    };
    QuestionnairePage.prototype.logOut = function () {
        this.authService.logout();
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__login_login__["a" /* Login */]);
    };
    QuestionnairePage.prototype.ngOnInit = function () {
        this.initTimer();
    };
    QuestionnairePage.prototype.initTimer = function () {
        this.time = this.timeInSeconds;
        this.runTimer = false;
        this.hasStarted = false;
        this.hasFinished = false;
        this.remainingTime = this.timeInSeconds;
        this.displayTime = this.getSecondsAsDigitalClock(this.remainingTime);
    };
    QuestionnairePage.prototype.startTimer = function () {
        this.runTimer = true;
        this.hasStarted = true;
        this.timerTick();
    };
    QuestionnairePage.prototype.pauseTimer = function () {
        this.runTimer = false;
    };
    QuestionnairePage.prototype.resumeTimer = function () {
        this.startTimer();
    };
    QuestionnairePage.prototype.timerTick = function () {
        var _this = this;
        setTimeout(function () {
            if (!_this.runTimer) {
                return;
            }
            _this.remainingTime--;
            _this.displayTime = _this.getSecondsAsDigitalClock(_this.remainingTime);
            _this.progressTimer = (_this.remainingTime * 100) / _this.time;
            _this.progressPercent = (100 - _this.progressTimer).toFixed(0);
            if (_this.remainingTime > 0) {
                _this.timerTick();
            }
            else {
                _this.hasFinished = true;
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__endtest_endtest__["a" /* EndtestPage */], {
                    param1: _this.idtest,
                    param2: _this.position,
                });
            }
        }, 1000);
    };
    QuestionnairePage.prototype.getSecondsAsDigitalClock = function (inputSeconds) {
        var sec_num = parseInt(inputSeconds.toString(), 10); // don't forget the second param
        var hours = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);
        var hoursString = '';
        var minutesString = '';
        var secondsString = '';
        hoursString = (hours < 10) ? "0" + hours : hours.toString();
        minutesString = (minutes < 10) ? "0" + minutes : minutes.toString();
        secondsString = (seconds < 10) ? "0" + seconds : seconds.toString();
        return hoursString + ':' + minutesString + ':' + secondsString;
    };
    QuestionnairePage = QuestionnairePage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Component"])({
            providers: [__WEBPACK_IMPORTED_MODULE_5_ng2_dragula__["b" /* DragulaService */]],
            selector: 'page-questionnaire',template:/*ion-inline-start:"/Users/Maxime/Desktop/passation/src/pages/questionnaire/questionnaire.html"*/'<ion-header>\n  <ion-toolbar  color="secondary" hide-back-button >\n        <ion-title>  </ion-title>\n          <h3 text-center *ngIf="displayTime!==0">{{displayTime}}</h3>\n        <ion-buttons end>\n          <button ion-button icon-only (click)="logOut()">\n              <ion-icon ios="ios-log-out" md="md-log-out"></ion-icon>\n          </button>\n        </ion-buttons>\n  </ion-toolbar>  \n</ion-header>\n\n<ion-content color="light2" >\n\n    <ion-grid >\n  \n        <ion-row responsive-sm  >\n            <ion-col col-2 > \n            </ion-col> \n            <ion-col col-8 >  \n              \n              <div *ngIf="test_nav==1">\n                <ion-list >  \n                  <ion-card>\n                    <ion-card-header>\n                     <b> {{question_label}}</b>\n                       {{indexdisplay}}/{{nbquestionbytest}}\n                    </ion-card-header>\n                    <ion-card-content>\n                        <p [innerHTML]="q_h"></p>  \n                    </ion-card-content>\n                  </ion-card>\n                </ion-list>\n                <ion-list padding>  \n\n              \n                                <ion-item *ngFor="let item of options; let i=index">\n                                  <ion-label>{{item}}</ion-label>\n                                  <ion-checkbox item-right [ngModel]="checkedIdx == i"  (ngModelChange)="$event ? checkedIdx = i : checkedIdx = -1" [disabled]="checkedIdx >= 0 && checkedIdx != i"></ion-checkbox>\n                                </ion-item>\n                    \n             \n                </ion-list>\n\n                <ion-list padding>  \n                        <ion-row responsive-sm  >\n                            <ion-col col-2 > \n                            <button ion-button full (click)="prev()" >\n                            Précedent\n                            </button>\n                            </ion-col> \n                            <ion-col col-8 >  \n                            </ion-col> \n                            <ion-col col-2 > \n                            <button ion-button  full (click)="next()"  >\n                            Suivant\n                            </button>\n                            </ion-col> \n                        </ion-row>\n                    </ion-list>\n              </div>\n\n              <div *ngIf="test_nav==2">\n                <ion-list >  \n                  <ion-card>\n                    <ion-card-header>\n                     <b> {{question_label}}</b>\n                       {{indexdisplay}}/{{nbquestionbytest}}\n                    </ion-card-header>\n                    <ion-card-content>\n                        <p [innerHTML]="q_h"></p>  \n                    </ion-card-content>\n                  </ion-card>\n                </ion-list>\n                <ion-list padding>  \n\n              \n                                <ion-item *ngFor="let item of options; let i=index">\n                                  <ion-label>{{item}}</ion-label>\n                                  <ion-checkbox item-right [ngModel]="checkedIdx == i"  (ngModelChange)="$event ? checkedIdx = i : checkedIdx = -1" [disabled]="checkedIdx >= 0 && checkedIdx != i"></ion-checkbox>\n                                </ion-item>\n                    \n             \n                </ion-list>\n\n                <ion-list padding>  \n                        <ion-row responsive-sm  >\n                            <ion-col col-2 > \n                         \n                            </ion-col> \n                            <ion-col col-8 >  \n                            </ion-col> \n                            <ion-col col-2 > \n                            <button ion-button  full (click)="next()" >\n                            Suivant\n                            </button>\n                            </ion-col> \n                        </ion-row>\n                    </ion-list>\n              </div>\n              <div *ngIf="test_nav==3">\n                <ion-list >  \n                  <ion-card>\n                    <ion-card-header>\n                     <b> {{question_label}}</b>\n                       {{indexdisplay}}/{{nbquestionbytest}}\n                    </ion-card-header>\n                    <ion-card-content>\n                        <p [innerHTML]="q_h"></p>  \n                    </ion-card-content>\n                  </ion-card>\n                </ion-list>\n                <ion-list padding>  \n\n              \n                                <ion-item *ngFor="let item of options; let i=index">\n                                  <ion-label>{{item}}</ion-label>\n                                  <ion-checkbox item-right [ngModel]="checkedIdx == i"  (ngModelChange)="$event ? checkedIdx = i : checkedIdx = -1" [disabled]="checkedIdx >= 0 && checkedIdx != i"></ion-checkbox>\n                                </ion-item>\n                    \n             \n                </ion-list>\n\n                <ion-list padding>  \n                        <ion-row responsive-sm  >\n                            <ion-col col-2 > \n                            <button ion-button full (click)="prev()" [disabled]="checkedIdx == -1 ">\n                            Précedent\n                            </button>\n                            </ion-col> \n                            <ion-col col-8 >  \n                            </ion-col> \n                            <ion-col col-2 > \n                            <button ion-button  full (click)="next()" [disabled]="checkedIdx == -1 ">\n                            Suivant\n                            </button>\n                            </ion-col> \n                        </ion-row>\n                    </ion-list>\n              </div>\n              <div *ngIf="test_nav==4">\n                <ion-list >  \n                  <ion-card>\n                    <ion-card-header>\n                     <b> {{question_label}}</b>\n                       {{indexdisplay}}/{{nbquestionbytest}}\n                    </ion-card-header>\n                    <ion-card-content>\n                        <p [innerHTML]="q_h"></p>  \n                    </ion-card-content>\n                  </ion-card>\n                </ion-list>\n                <ion-list padding>  \n\n              \n                                <ion-item *ngFor="let item of options; let i=index">\n                                  <ion-label>{{item}}</ion-label>\n                                  <ion-checkbox item-right [ngModel]="checkedIdx == i"  (ngModelChange)="$event ? checkedIdx = i : checkedIdx = -1" [disabled]="checkedIdx >= 0 && checkedIdx != i"></ion-checkbox>\n                                </ion-item>\n                    \n             \n                </ion-list>\n\n                <ion-list padding>  \n                        <ion-row responsive-sm  >\n                            <ion-col col-2 > \n                        \n                            </ion-col> \n                            <ion-col col-8 >  \n                            </ion-col> \n                            <ion-col col-2 > \n                            <button ion-button  full (click)="next()" [disabled]="checkedIdx == -1 ">\n                            Suivant\n                            </button>\n                            </ion-col> \n                        </ion-row>\n                    </ion-list>\n              </div>\n </ion-col>\n <ion-col col-2 > \n    {{question_num}}\n          </ion-col> \n        </ion-row>\n\n        <div style="width:70%; text-align: center; margin: 0 auto;" >\n            <div class="progress-outer" style="margin: 1px 1px;padding: 1px;text-align: center;background-color: #cfcfcf;border: 1px solid #dcdcdc;color: #fff;border-radius: 20px;">\n            <div class="progress-inner" [style.width]="progressPercent + \'%\'" style="width: 96%;height:20px;margin: 2px 2px;text-align: center;background-color: #84bdf1;border: 1px solid #dcdcdc;color: #fff;border-radius: 20px;">\n               {{progressPercent}}%\n            </div>\n            </div>\n            </div> \n      </ion-grid>\n  </ion-content>\n  \n'/*ion-inline-end:"/Users/Maxime/Desktop/passation/src/pages/questionnaire/questionnaire.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_9__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["f" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_7__providers_AuthService__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_5_ng2_dragula__["b" /* DragulaService */], __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["f" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_2__providers_reddit_service__["a" /* RedditService */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["j" /* NavParams */]])
    ], QuestionnairePage);
    return QuestionnairePage;
    var QuestionnairePage_1;
}());

//# sourceMappingURL=questionnaire.js.map

/***/ }),

/***/ 360:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ScientifiquePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_add_operator_map__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_reddit_service__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng2_dragula__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__endtest_endtest__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_AuthService__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__login_login__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var ScientifiquePage = /** @class */ (function () {
    function ScientifiquePage(loadingController, authService, dragulaService, loadingCtrl, navCtrl, storage, redditService, navParams) {
        var _this = this;
        this.loadingController = loadingController;
        this.authService = authService;
        this.dragulaService = dragulaService;
        this.loadingCtrl = loadingCtrl;
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.redditService = redditService;
        this.navParams = navParams;
        this.editing = true;
        this.dataq = [];
        this.indextest = 0;
        this.indexdisplay = 1;
        this.posts2 = [];
        this.nbchecked = 0;
        this.checkedIdx = -1;
        this.options = [];
        this.reponse = [];
        this.reponsesave = [];
        this.myDataArray = [];
        this.datareponse = [];
        this.responsestring = "";
        this.response1 = "";
        this.response2 = "";
        this.response3 = "";
        this.response4 = "";
        this.response5 = "";
        this.progressPercent = 0;
        this.idtest = navParams.get('param1');
        this.testlenght = navParams.get('param2');
        this.testcategory = navParams.get('param4');
        this.timeInSeconds = navParams.get('param5');
        this.test_nav = navParams.get('param6');
        this.position = navParams.get('param7');
        this.storage.get('idresponse').then(function (idresponse) {
            _this.idresponse = idresponse;
        });
        this.storage.get("questions").then(function (users) {
            _this.dataq = JSON.parse(users);
            for (var i = 0; i < _this.dataq.length; i++) {
                _this.question_label = _this.dataq[i].question_label;
                _this.propositions = _this.dataq[i];
                _this.q_prop1 = _this.dataq[i].q_prop1;
                _this.q_prop2 = _this.dataq[i].q_prop2;
                _this.q_prop3 = _this.dataq[i].q_prop3;
                _this.q_prop4 = _this.dataq[i].q_prop4;
                _this.q_prop5 = _this.dataq[i].q_prop5;
                _this.q_h = _this.dataq[i].q_h;
                _this.q_f = _this.dataq[i].q_f;
                _this.checkedIdx = _this.reponsesave[i];
                _this.question_num = _this.dataq[i].question_num;
                _this.options = [{ id: '1', prop: _this.q_prop1, isChecked: false }, { id: '2', prop: _this.q_prop2, isChecked: false },
                    { id: '3', prop: _this.q_prop3, isChecked: false }, { id: '4', prop: _this.q_prop4, isChecked: false }, { id: '5', prop: _this.q_prop5, isChecked: false }];
                _this.datareponse.push(_this.options);
            }
        });
    }
    ScientifiquePage.prototype.ionViewWillEnter = function () {
        var _this = this;
        if (this.timeInSeconds > 0) {
            this.startTimer();
        }
        this.indexdisplay = 1;
        //Données i=0
        this.storage.get("questions").then(function (users) {
            _this.dataq = JSON.parse(users);
            _this.nbquestionbytest = _this.dataq.length;
            _this.question_label = _this.dataq[0].question_label;
            _this.q_h = _this.dataq[0].q_h;
            _this.q_f = _this.dataq[0].q_f;
            _this.question_num = _this.dataq[0].question_num;
            _this.datarep = _this.datareponse[0];
        });
    };
    ScientifiquePage.prototype.next = function () {
        for (var i = 0; i < this.datareponse[this.indextest]; i++) {
            this.datareponse.push(this.datareponse[this.indextest]);
            console.log(this.datareponse[this.indextest]);
        }
        console.log(this.datareponse);
        /*this.datareponse[this.indextest] =  this.datarep.filter(value => {
      
          console.log(value.isChecked );
          return value.isChecked && !value.isChecked ;
        });
      */
        console.log("enregistrement des questions");
        console.log(this.datareponse);
        // new question
        this.indextest = this.indextest + 1;
        if (this.indextest < this.nbquestionbytest) {
            this.indexdisplay = this.indexdisplay + 1;
            this.retrieveQuestions();
        }
        else {
            this.pauseTimer();
            ////// SAVE DATA
            for (var i = 0; i < this.testlenght; i++) {
                if (this.datareponse[i][0].isChecked == true) {
                    this.response1 = this.datareponse[i][0].id;
                }
                else if (this.datareponse[i][0].isChecked == false) {
                    this.response1 = "";
                }
                if (this.datareponse[i][1].isChecked == true) {
                    this.response2 = this.datareponse[i][1].id;
                }
                else if (this.datareponse[i][1].isChecked == false) {
                    this.response2 = "";
                }
                if (this.datareponse[i][2].isChecked == true) {
                    this.response3 = this.datareponse[i][2].id;
                }
                else if (this.datareponse[i][2].isChecked == false) {
                    this.response3 = "";
                }
                if (this.datareponse[i][3].isChecked == true) {
                    this.response4 = this.datareponse[i][3].id;
                }
                else if (this.datareponse[i][3].isChecked == false) {
                    this.response4 = "";
                }
                if (this.datareponse[i][4].isChecked == true) {
                    this.response5 = this.datareponse[i][4].id;
                }
                else if (this.datareponse[i][4].isChecked == false) {
                    this.response5 = "";
                }
                this.responsezero = this.response1 + this.response2 + this.response3 + this.response4 + ",";
                this.responsestring = this.responsestring + this.responsezero;
            }
            /////
            var data = JSON.stringify({
                name: "ConnSci6RepMult",
                idresponse: this.idresponse,
                resp: this.responsestring,
            });
            this.redditService.addresponses(data)
                .toPromise()
                .then(function (response) {
                console.log(response);
            });
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__endtest_endtest__["a" /* EndtestPage */], {
                param1: this.idtest,
                param2: this.position,
            });
        }
    };
    ScientifiquePage.prototype.prev = function () {
        if (this.indextest > 0) {
            this.indextest = this.indextest - 1;
            this.indexdisplay = this.indexdisplay - 1;
            this.reviewQuestions();
        }
        else {
        }
    };
    ScientifiquePage.prototype.retrieveQuestions = function () {
        var _this = this;
        this.storage.get("questions").then(function (users) {
            _this.dataq = JSON.parse(users);
            _this.question_label = _this.dataq[_this.indextest].question_label;
            _this.q_h = _this.dataq[_this.indextest].q_h;
            _this.q_f = _this.dataq[_this.indextest].q_f;
            _this.question_num = _this.dataq[_this.indextest].question_num;
            _this.datarep = _this.datareponse[_this.indextest];
            // nb true reponse
            _this.nbchecked = 0;
            for (var i = 0; i < 5; i++) {
                if (_this.datarep[i].isChecked == true) {
                    _this.nbchecked = _this.nbchecked + 1;
                }
            }
        });
    };
    ScientifiquePage.prototype.reviewQuestions = function () {
        var _this = this;
        this.storage.get("questions").then(function (users) {
            _this.dataq = JSON.parse(users);
            _this.question_label = _this.dataq[_this.indextest].question_label;
            _this.q_h = _this.dataq[_this.indextest].q_h;
            _this.q_f = _this.dataq[_this.indextest].q_f;
            _this.question_num = _this.dataq[_this.indextest].question_num;
            _this.datarep = _this.datareponse[_this.indextest];
            // nb true reponse
            _this.nbchecked = 0;
            for (var i = 0; i < 5; i++) {
                if (_this.datarep[i].isChecked == true) {
                    _this.nbchecked = _this.nbchecked + 1;
                }
            }
        });
    };
    ScientifiquePage.prototype.updateCheck = function ($event) {
        console.log($event.checked);
        if ($event.checked == true) {
            this.nbchecked = this.nbchecked + 1;
            console.log(this.nbchecked);
        }
        else if ($event.checked == false) {
            this.nbchecked = this.nbchecked - 1;
            console.log(this.nbchecked);
        }
    };
    ScientifiquePage.mustBeTruthy = function (c) {
        var rv = {};
        if (!c.value) {
            rv['notChecked'] = true;
        }
        return rv;
    };
    ScientifiquePage.prototype.reorderData = function (indexes) {
        this.posts2 = Object(__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["o" /* reorderArray */])(this.posts2, indexes);
        console.log(this.posts2);
        console.log(indexes);
        //this.dataq[this.indextest] = reorderArray(this.dataq[this.indextest], indexes);
        // console.log(this.dataq[this.indextest]);
        // console.log(this.dataq[this.indextest]);
    };
    ScientifiquePage.prototype.logOut = function () {
        this.authService.logout();
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__login_login__["a" /* Login */]);
    };
    ScientifiquePage.prototype.ngOnInit = function () {
        this.initTimer();
    };
    ScientifiquePage.prototype.initTimer = function () {
        this.time = this.timeInSeconds;
        this.runTimer = false;
        this.hasStarted = false;
        this.hasFinished = false;
        this.remainingTime = this.timeInSeconds;
        this.displayTime = this.getSecondsAsDigitalClock(this.remainingTime);
    };
    ScientifiquePage.prototype.startTimer = function () {
        this.runTimer = true;
        this.hasStarted = true;
        this.timerTick();
    };
    ScientifiquePage.prototype.pauseTimer = function () {
        this.runTimer = false;
    };
    ScientifiquePage.prototype.resumeTimer = function () {
        this.startTimer();
    };
    ScientifiquePage.prototype.timerTick = function () {
        var _this = this;
        setTimeout(function () {
            if (!_this.runTimer) {
                return;
            }
            _this.remainingTime--;
            _this.displayTime = _this.getSecondsAsDigitalClock(_this.remainingTime);
            _this.progressTimer = (_this.remainingTime * 100) / _this.time;
            _this.progressPercent = (100 - _this.progressTimer).toFixed(0);
            if (_this.remainingTime > 0) {
                _this.timerTick();
            }
            else {
                _this.hasFinished = true;
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__endtest_endtest__["a" /* EndtestPage */], {
                    param1: _this.idtest,
                    param2: _this.position,
                });
            }
        }, 1000);
    };
    ScientifiquePage.prototype.getSecondsAsDigitalClock = function (inputSeconds) {
        var sec_num = parseInt(inputSeconds.toString(), 10); // don't forget the second param
        var hours = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);
        var hoursString = '';
        var minutesString = '';
        var secondsString = '';
        hoursString = (hours < 10) ? "0" + hours : hours.toString();
        minutesString = (minutes < 10) ? "0" + minutes : minutes.toString();
        secondsString = (seconds < 10) ? "0" + seconds : seconds.toString();
        return hoursString + ':' + minutesString + ':' + secondsString;
    };
    ScientifiquePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Component"])({
            providers: [__WEBPACK_IMPORTED_MODULE_5_ng2_dragula__["b" /* DragulaService */]],
            selector: 'page-scientifique',template:/*ion-inline-start:"/Users/Maxime/Desktop/passation/src/pages/scientifique/scientifique.html"*/'<ion-header>\n  <ion-toolbar  color="secondary" hide-back-button >\n        <ion-title>  </ion-title>\n          <h3 text-center *ngIf="displayTime!==0">{{displayTime}}</h3>\n        <ion-buttons end>\n          <button ion-button icon-only (click)="logOut()">\n              <ion-icon ios="ios-log-out" md="md-log-out"></ion-icon>\n          </button>\n        </ion-buttons>\n  </ion-toolbar>  \n</ion-header>\n\n<ion-content color="light2" >\n\n    <ion-grid >\n  \n        <ion-row responsive-sm  >\n            <ion-col col-2 > \n            </ion-col> \n            <ion-col col-8 >  \n              \n              <div *ngIf="test_nav==1">\n                <ion-list >  \n                  <ion-card>\n                    <ion-card-header>\n                     <b> {{question_label}}</b>\n                       {{indexdisplay}}/{{nbquestionbytest}}\n                    </ion-card-header>\n                    <ion-card-content>\n                        <p [innerHTML]="q_h"></p>  \n                    </ion-card-content>\n                  </ion-card>\n                </ion-list>\n                <ion-list padding>  \n\n              \n                                <div *ngFor="let item of datarep; let i=index">\n\n\n                                  <p>  {{item.prop}}  {{item.id}}</p>\n                                  <ion-checkbox item-right [(ngModel)]="item.isChecked"  (ionChange)="updateCheck($event)"></ion-checkbox>\n                                </div>\n                    \n                </ion-list>\n\n                <ion-list padding>  \n                        <ion-row responsive-sm  >\n                            \n                            <ion-col col-2 > \n                            <button ion-button full (click)="prev()" >\n                            Précedent\n                            </button>\n                            </ion-col> \n                            <ion-col col-8 >  \n                            </ion-col> \n                            <ion-col col-2 > \n                            <button ion-button  full (click)="next()" >\n                            Suivant\n                            </button>\n                            </ion-col> \n                          \n                        </ion-row>\n                    </ion-list>\n              </div>\n              <div *ngIf="test_nav==2">\n                <ion-list >  \n                  <ion-card>\n                    <ion-card-header>\n                     <b> {{question_label}}</b>\n                       {{indexdisplay}}/{{nbquestionbytest}}\n                    </ion-card-header>\n                    <ion-card-content>\n                        <p [innerHTML]="q_h"></p>  \n                    </ion-card-content>\n                  </ion-card>\n                </ion-list>\n                <ion-list padding>  \n\n                              \n                                <div *ngFor="let item of datarep; let i=index">\n                                  <p>  {{item.prop}}  {{item.id}}</p>\n                                  <ion-checkbox item-right [(ngModel)]="item.isChecked"  (ionChange)="updateCheck($event)"></ion-checkbox>\n                                </div>\n                    \n                </ion-list>\n\n                <ion-list padding>  \n                        <ion-row responsive-sm  >\n                            \n                            <ion-col col-2 > \n                          \n                            </ion-col> \n                            <ion-col col-8 >  \n                            </ion-col> \n                            <ion-col col-2 > \n                            <button ion-button  full (click)="next()" >\n                            Suivant\n                            </button>\n                            </ion-col> \n                          \n                        </ion-row>\n                    </ion-list>\n              </div>\n                    <div *ngIf="test_nav==3">\n                        <ion-list >  \n                          <ion-card>\n                            <ion-card-header>\n                             <b> {{question_label}}</b>\n                               {{indexdisplay}}/{{nbquestionbytest}}\n                            </ion-card-header>\n                            <ion-card-content>\n                                <p [innerHTML]="q_h"></p>  \n                            </ion-card-content>\n                          </ion-card>\n                        </ion-list>\n                        <ion-list padding>  \n        \n                      \n                                        <div *ngFor="let item of datarep; let i=index">\n                                          <p>  {{item.prop}}  {{item.id}}</p>\n                                          <ion-checkbox item-right [(ngModel)]="item.isChecked"  (ionChange)="updateCheck($event)"></ion-checkbox>\n                                        </div>\n                            \n                        </ion-list>\n        \n                        <ion-list padding>  \n                                <ion-row responsive-sm  >\n                                    \n                                    <ion-col col-2 > \n                                    <button ion-button full (click)="prev()" >\n                                    Précedent\n                                    </button>\n                                    </ion-col> \n                                    <ion-col col-8 >  \n                                    </ion-col> \n                                    <ion-col col-2 > \n                                    <button ion-button  full (click)="next()"  [disabled]="nbchecked ==0 ">\n                                    Suivant\n                                    </button>\n                                    </ion-col> \n                                  \n                                </ion-row>\n                            </ion-list>\n                      </div>\n                      <div *ngIf="test_nav==4">\n                        <ion-list >  \n                          <ion-card>\n                            <ion-card-header>\n                             <b> {{question_label}}</b>\n                               {{indexdisplay}}/{{nbquestionbytest}}\n                            </ion-card-header>\n                            <ion-card-content>\n                                <p [innerHTML]="q_h"></p>  \n                            </ion-card-content>\n                          </ion-card>\n                        </ion-list>\n                        <ion-list padding>  \n        \n                      \n                                        <div *ngFor="let item of datarep; let i=index">\n                                          <p>  {{item.prop}}  {{item.id}}</p>\n                                          <ion-checkbox item-right [(ngModel)]="item.isChecked"  (ionChange)="updateCheck($event)"></ion-checkbox>\n                                        </div>\n                            \n                        </ion-list>\n        \n                        <ion-list padding>  \n                                <ion-row responsive-sm  >\n                                    \n                                    <ion-col col-2 > \n                                \n                                    </ion-col> \n                                    <ion-col col-8 >  \n                                    </ion-col> \n                                    <ion-col col-2 > \n                                    <button ion-button  full (click)="next()"  [disabled]="nbchecked ==0 ">\n                                    Suivant\n                                    </button>\n                                    </ion-col> \n                                  \n                                </ion-row>\n                            </ion-list>\n                      </div>\n </ion-col>\n <ion-col col-2 > \n    {{question_num}}\n          </ion-col> \n        </ion-row>\n\n        <div style="width:70%; text-align: center; margin: 0 auto;" >\n            <div class="progress-outer" style="margin: 1px 1px;padding: 1px;text-align: center;background-color: #cfcfcf;border: 1px solid #dcdcdc;color: #fff;border-radius: 20px;">\n            <div class="progress-inner" [style.width]="progressPercent + \'%\'" style="width: 96%;height:20px;margin: 2px 2px;text-align: center;background-color: #84bdf1;border: 1px solid #dcdcdc;color: #fff;border-radius: 20px;">\n               {{progressPercent}}%\n            </div>\n            </div>\n            </div> \n      </ion-grid>\n\n  </ion-content>\n  \n'/*ion-inline-end:"/Users/Maxime/Desktop/passation/src/pages/scientifique/scientifique.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["f" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_7__providers_AuthService__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_5_ng2_dragula__["b" /* DragulaService */], __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["f" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_2__providers_reddit_service__["a" /* RedditService */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["j" /* NavParams */]])
    ], ScientifiquePage);
    return ScientifiquePage;
}());

//# sourceMappingURL=scientifique.js.map

/***/ }),

/***/ 361:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VerbaldefinitionPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_add_operator_map__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_reddit_service__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng2_dragula__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__endtest_endtest__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_AuthService__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__login_login__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_forms__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__angular_platform_browser__ = __webpack_require__(39);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var VerbaldefinitionPage = /** @class */ (function () {
    function VerbaldefinitionPage(fb, loadingController, authService, dragulaService, loadingCtrl, navCtrl, storage, redditService, navParams, sanitizer) {
        var _this = this;
        this.loadingController = loadingController;
        this.authService = authService;
        this.dragulaService = dragulaService;
        this.loadingCtrl = loadingCtrl;
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.redditService = redditService;
        this.navParams = navParams;
        this.sanitizer = sanitizer;
        this.editing = true;
        this.dataq = [];
        this.indextest = 0;
        this.indexdisplay = 1;
        this.posts2 = [];
        this.checked = false;
        this.checkedIdx = -1;
        this.options = [];
        this.reponse = [];
        this.reponsesave = [];
        this.responsestring = "";
        this.datareponse = [];
        this.progressPercent = 0;
        this.idtest = navParams.get('param1');
        this.testlenght = navParams.get('param2');
        this.testcategory = navParams.get('param4');
        this.timeInSeconds = navParams.get('param5');
        this.test_nav = navParams.get('param6');
        this.position = navParams.get('param7');
        this.form = fb.group({
            cbox: [false, VerbaldefinitionPage_1.mustBeTruthy]
        });
        this.storage.get('idresponse').then(function (idresponse) {
            _this.idresponse = idresponse;
        });
        console.log(this.checked);
    }
    VerbaldefinitionPage_1 = VerbaldefinitionPage;
    VerbaldefinitionPage.prototype.transform = function (value) {
        return this.sanitizer.bypassSecurityTrustHtml(value);
    };
    VerbaldefinitionPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        if (this.timeInSeconds > 0) {
            this.startTimer();
        }
        this.indexdisplay = 1;
        //Données i=0
        this.storage.get("questions").then(function (users) {
            _this.dataq = JSON.parse(users);
            _this.nbquestionbytest = _this.dataq.length;
            _this.question_label = _this.dataq[0].question_label;
            _this.q_prop1 = _this.dataq[0].q_prop1;
            _this.q_prop2 = _this.dataq[0].q_prop2;
            _this.q_prop3 = _this.dataq[0].q_prop3;
            _this.q_prop4 = _this.dataq[0].q_prop4;
            _this.q_prop5 = _this.dataq[0].q_prop5;
            _this.q_h = _this.dataq[0].q_h;
            _this.q_f = _this.dataq[0].q_f;
            _this.options = [{ id: '1', prop: _this.q_prop1 }, { id: '2', prop: _this.q_prop2 }, { id: '3', prop: _this.q_prop3 }, { id: '4', prop: _this.q_prop4 },
                { id: '5', prop: _this.q_prop5 }];
            _this.question_num = _this.dataq[0].question_num;
            _this.checkedIdx = -1;
            //Array reponse
            for (var i = 0; i < _this.nbquestionbytest; i++) {
                _this.reponsesave[i] = [{ result1: '0' }, { result2: '0' }, { result3: '0' }, { result4: '0' }, { result5: '0' }];
            }
            console.log(_this.reponsesave);
        });
    };
    VerbaldefinitionPage.prototype.update1 = function () {
        if (this.result1 > 0 && this.result2 > 0 && this.result3 > 0 && this.result4 > 0 && this.result5 > 0) {
            this.checked = true;
        }
        else {
            this.checked = false;
        }
    };
    VerbaldefinitionPage.prototype.next = function () {
        this.reponsesave[this.indextest] = [{ result1: this.result1 }, { result2: this.result2 }, { result3: this.result3 }, { result4: this.result4 }, { result5: this.result5 }];
        // new question
        this.indextest = this.indextest + 1;
        // toutes les réponses
        console.log("réponse");
        console.log(this.reponsesave);
        if (this.indextest < this.nbquestionbytest) {
            this.indexdisplay = this.indexdisplay + 1;
            this.retrieveQuestions();
        }
        else {
            this.pauseTimer();
            ////// SAVE DATA
            console.log(this.testlenght);
            for (var i = 0; i < this.testlenght; i++) {
                this.response1 = this.reponsesave[i][0].result1;
                console.log(this.response1);
                this.response2 = this.reponsesave[i][1].result2;
                console.log(this.response2);
                this.response3 = this.reponsesave[i][2].result3;
                this.response4 = this.reponsesave[i][3].result4;
                this.response5 = this.reponsesave[i][4].result5;
                this.responsestring = this.responsestring + this.response1 + this.response2 + this.response3 + this.response4 + this.response5;
                console.log(this.responsestring);
            }
            /////
            var data = JSON.stringify({
                name: "VerbalDefinition",
                idresponse: this.idresponse,
                resp: this.responsestring,
            });
            console.log(data);
            this.redditService.addresponses(data)
                .toPromise()
                .then(function (response) {
                console.log(response);
            });
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__endtest_endtest__["a" /* EndtestPage */], {
                param1: this.idtest,
                param2: this.position,
            });
        }
    };
    VerbaldefinitionPage.prototype.prev = function () {
        if (this.indextest > 0) {
            this.indextest = this.indextest - 1;
            this.indexdisplay = this.indexdisplay - 1;
            this.reviewQuestions();
        }
        else {
        }
    };
    VerbaldefinitionPage.prototype.retrieveQuestions = function () {
        var _this = this;
        this.storage.get("questions").then(function (users) {
            _this.dataq = JSON.parse(users);
            _this.question_label = _this.dataq[_this.indextest].question_label;
            _this.propositions = _this.dataq[_this.indextest];
            _this.q_prop1 = _this.dataq[_this.indextest].q_prop1;
            _this.q_prop2 = _this.dataq[_this.indextest].q_prop2;
            _this.q_prop3 = _this.dataq[_this.indextest].q_prop3;
            _this.q_prop4 = _this.dataq[_this.indextest].q_prop4;
            _this.q_prop5 = _this.dataq[_this.indextest].q_prop5;
            _this.q_h = _this.dataq[_this.indextest].q_h;
            _this.q_f = _this.dataq[_this.indextest].q_f;
            _this.checkedIdx = _this.reponsesave[_this.indextest];
            _this.question_num = _this.dataq[_this.indextest].question_num;
            _this.options = [{ id: '1', prop: _this.q_prop1 }, { id: '2', prop: _this.q_prop2 }, { id: '3', prop: _this.q_prop3 }, { id: '4', prop: _this.q_prop4 },
                { id: '5', prop: _this.q_prop5 }];
            _this.result1 = _this.reponsesave[_this.indextest][0].result1;
            _this.result2 = _this.reponsesave[_this.indextest][1].result2;
            _this.result3 = _this.reponsesave[_this.indextest][2].result3;
            _this.result4 = _this.reponsesave[_this.indextest][3].result4;
            _this.result5 = _this.reponsesave[_this.indextest][4].result5;
            if (_this.result1 > 0 || _this.result2 > 0 || _this.result3 > 0 || _this.result4 > 0 || _this.result5 > 0) {
                _this.checked = true;
            }
            else {
                _this.checked = false;
            }
        });
    };
    VerbaldefinitionPage.prototype.reviewQuestions = function () {
        var _this = this;
        this.storage.get("questions").then(function (users) {
            _this.dataq = JSON.parse(users);
            _this.question_label = _this.dataq[_this.indextest].question_label;
            _this.propositions = _this.dataq[_this.indextest];
            _this.q_prop1 = _this.dataq[_this.indextest].q_prop1;
            _this.q_prop2 = _this.dataq[_this.indextest].q_prop2;
            _this.q_prop3 = _this.dataq[_this.indextest].q_prop3;
            _this.q_prop4 = _this.dataq[_this.indextest].q_prop4;
            _this.q_prop5 = _this.dataq[_this.indextest].q_prop5;
            _this.q_h = _this.dataq[_this.indextest].q_h;
            _this.q_f = _this.dataq[_this.indextest].q_f;
            _this.question_num = _this.dataq[_this.indextest].question_num;
            _this.options = [{ id: '1', prop: _this.q_prop1 }, { id: '2', prop: _this.q_prop2 }, { id: '3', prop: _this.q_prop3 }, { id: '4', prop: _this.q_prop4 },
                { id: '5', prop: _this.q_prop5 }];
            _this.result1 = _this.reponsesave[_this.indextest][0].result1;
            _this.result2 = _this.reponsesave[_this.indextest][1].result2;
            _this.result3 = _this.reponsesave[_this.indextest][2].result3;
            _this.result4 = _this.reponsesave[_this.indextest][3].result4;
            _this.result5 = _this.reponsesave[_this.indextest][4].result5;
            if (_this.result1 > 0 || _this.result2 > 0 || _this.result3 > 0 || _this.result4 > 0 || _this.result5 > 0) {
                _this.checked = true;
            }
            else {
                _this.checked = false;
            }
        });
    };
    VerbaldefinitionPage.mustBeTruthy = function (c) {
        var rv = {};
        if (!c.value) {
            rv['notChecked'] = true;
        }
        return rv;
    };
    VerbaldefinitionPage.prototype.reorderData = function (indexes) {
        this.posts2 = Object(__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["o" /* reorderArray */])(this.posts2, indexes);
        console.log(this.posts2);
        console.log(indexes);
        //this.dataq[this.indextest] = reorderArray(this.dataq[this.indextest], indexes);
        // console.log(this.dataq[this.indextest]);
        // console.log(this.dataq[this.indextest]);
    };
    VerbaldefinitionPage.prototype.logOut = function () {
        this.authService.logout();
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__login_login__["a" /* Login */]);
    };
    VerbaldefinitionPage.prototype.ngOnInit = function () {
        this.initTimer();
    };
    VerbaldefinitionPage.prototype.initTimer = function () {
        this.time = this.timeInSeconds;
        this.runTimer = false;
        this.hasStarted = false;
        this.hasFinished = false;
        this.remainingTime = this.timeInSeconds;
        this.displayTime = this.getSecondsAsDigitalClock(this.remainingTime);
        this.progressTimer = (this.remainingTime * 100) / this.time;
        this.progressPercent = (100 - this.progressTimer).toFixed(0);
    };
    VerbaldefinitionPage.prototype.startTimer = function () {
        this.runTimer = true;
        this.hasStarted = true;
        this.timerTick();
    };
    VerbaldefinitionPage.prototype.pauseTimer = function () {
        this.runTimer = false;
    };
    VerbaldefinitionPage.prototype.resumeTimer = function () {
        this.startTimer();
    };
    VerbaldefinitionPage.prototype.timerTick = function () {
        var _this = this;
        setTimeout(function () {
            if (!_this.runTimer) {
                return;
            }
            _this.remainingTime--;
            _this.displayTime = _this.getSecondsAsDigitalClock(_this.remainingTime);
            if (_this.remainingTime > 0) {
                _this.timerTick();
            }
            else {
                _this.hasFinished = true;
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__endtest_endtest__["a" /* EndtestPage */], {
                    param1: _this.idtest,
                    param2: _this.position,
                });
            }
        }, 1000);
    };
    VerbaldefinitionPage.prototype.getSecondsAsDigitalClock = function (inputSeconds) {
        var sec_num = parseInt(inputSeconds.toString(), 10); // don't forget the second param
        var hours = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);
        var hoursString = '';
        var minutesString = '';
        var secondsString = '';
        hoursString = (hours < 10) ? "0" + hours : hours.toString();
        minutesString = (minutes < 10) ? "0" + minutes : minutes.toString();
        secondsString = (seconds < 10) ? "0" + seconds : seconds.toString();
        return hoursString + ':' + minutesString + ':' + secondsString;
    };
    VerbaldefinitionPage = VerbaldefinitionPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Component"])({
            providers: [__WEBPACK_IMPORTED_MODULE_5_ng2_dragula__["b" /* DragulaService */]],
            selector: 'page-verbaldefinition',template:/*ion-inline-start:"/Users/Maxime/Desktop/passation/src/pages/verbaldefinition/verbaldefinition.html"*/'<ion-header>\n  <ion-toolbar  color="secondary" hide-back-button >\n        <ion-title>  </ion-title>\n          <h3 text-center *ngIf="displayTime!==0">{{displayTime}}</h3>\n        <ion-buttons end>\n          <button ion-button icon-only (click)="logOut()">\n              <ion-icon ios="ios-log-out" md="md-log-out"></ion-icon>\n          </button>\n        </ion-buttons>\n  </ion-toolbar>  \n</ion-header>\n\n<ion-content color="light2" >\n\n    <ion-grid >\n  \n        <ion-row responsive-sm  >\n            <ion-col col-2 > \n            </ion-col> \n            <ion-col col-8 >  \n              \n                <div *ngIf="test_nav==1">\n                    <ion-list >  \n                      <ion-card>\n                        <ion-card-header>\n                         <b> {{question_label}}</b>\n                           {{indexdisplay}}/{{nbquestionbytest}}\n                        </ion-card-header>\n                        <ion-card-content>\n                            <p [innerHTML]="q_h"></p>  \n                          \n                        </ion-card-content>\n                      </ion-card>\n                    </ion-list>\n                    <ion-list padding>  \n                            \n    \n                           \n                            <ion-label color="primary"> Définition 1 </ion-label>\n                            <p>Choisir la bonne réponse </p>\n                            <select [(ngModel)]="result1" (click)="update1()">\n                                <option value="{{item.id}}" *ngFor="let item of options" >{{item.prop}}</option>\n                            </select>\n                          \n                            <ion-label color="primary"> Définition 2 </ion-label>\n                            <p>Choisir la bonne réponse </p>\n                            <select [(ngModel)]="result2" (click)="update1()">\n                                <option value="{{item.id}}" *ngFor="let item of options" >{{item.prop}}</option>\n                            </select>\n    \n                            <ion-label color="primary"> Définition 3 </ion-label>\n                            <p>Choisir la bonne réponse </p>\n                            <select [(ngModel)]="result3" (click)="update1()">\n                                <option value="{{item.id}}" *ngFor="let item of options" >{{item.prop}}</option>\n                            </select>\n    \n                            <ion-label color="primary"> Définition 4 </ion-label>\n                            <p>Choisir la bonne réponse </p>\n                            <select [(ngModel)]="result4" (click)="update1()">\n                                <option value="{{item.id}}" *ngFor="let item of options" >{{item.prop}}</option>\n                            </select>\n    \n                            <ion-label color="primary"> Définition 5 </ion-label>\n                            <p>Choisir la bonne réponse </p>\n                            <select [(ngModel)]="result5" (click)="update1()">\n                                <option value="{{item.id}}" *ngFor="let item of options" >{{item.prop}}</option>\n                            </select>\n                                    \n                 \n                    </ion-list>\n    \n                    <ion-list padding>  \n                            <ion-row responsive-sm  >\n                                <ion-col col-2 > \n                                <button ion-button full (click)="prev()" >\n                                Précedent\n                                </button>\n                                </ion-col> \n                                <ion-col col-8 >  \n                                </ion-col> \n                                <ion-col col-2 > \n                                <button ion-button  full (click)="next()">\n                                Suivant\n                                </button>\n                                </ion-col> \n                            </ion-row>\n                        </ion-list>\n                  </div>\n                  <div *ngIf="test_nav==2">\n                      <ion-list >  \n                        <ion-card>\n                          <ion-card-header>\n                           <b> {{question_label}}</b>\n                             {{indexdisplay}}/{{nbquestionbytest}}\n                          </ion-card-header>\n                          <ion-card-content>\n                              <p [innerHTML]="q_h"></p>  \n                            \n                          </ion-card-content>\n                        </ion-card>\n                      </ion-list>\n                      <ion-list padding>  \n                              \n      \n                             \n                              <ion-label color="primary"> Définition 1 </ion-label>\n                              <p>Choisir la bonne réponse </p>\n                              <select [(ngModel)]="result1" (click)="update1()">\n                                  <option value="{{item.id}}" *ngFor="let item of options" >{{item.prop}}</option>\n                              </select>\n                            \n                              <ion-label color="primary"> Définition 2 </ion-label>\n                              <p>Choisir la bonne réponse </p>\n                              <select [(ngModel)]="result2" (click)="update1()">\n                                  <option value="{{item.id}}" *ngFor="let item of options" >{{item.prop}}</option>\n                              </select>\n      \n                              <ion-label color="primary"> Définition 3 </ion-label>\n                              <p>Choisir la bonne réponse </p>\n                              <select [(ngModel)]="result3" (click)="update1()">\n                                  <option value="{{item.id}}" *ngFor="let item of options" >{{item.prop}}</option>\n                              </select>\n      \n                              <ion-label color="primary"> Définition 4 </ion-label>\n                              <p>Choisir la bonne réponse </p>\n                              <select [(ngModel)]="result4" (click)="update1()">\n                                  <option value="{{item.id}}" *ngFor="let item of options" >{{item.prop}}</option>\n                              </select>\n      \n                              <ion-label color="primary"> Définition 5 </ion-label>\n                              <p>Choisir la bonne réponse </p>\n                              <select [(ngModel)]="result5" (click)="update1()">\n                                  <option value="{{item.id}}" *ngFor="let item of options" >{{item.prop}}</option>\n                              </select>\n                                      \n                   \n                      </ion-list>\n      \n                      <ion-list padding>  \n                              <ion-row responsive-sm  >\n                                  <ion-col col-2 > \n                               \n                                  </ion-col> \n                                  <ion-col col-8 >  \n                                  </ion-col> \n                                  <ion-col col-2 > \n                                  <button ion-button  full (click)="next()" >\n                                  Suivant\n                                  </button>\n                                  </ion-col> \n                              </ion-row>\n                          </ion-list>\n                    </div>\n                    <div *ngIf="test_nav==3">\n                        <ion-list >  \n                          <ion-card>\n                            <ion-card-header>\n                             <b> {{question_label}}</b>\n                               {{indexdisplay}}/{{nbquestionbytest}}\n                            </ion-card-header>\n                            <ion-card-content>\n                                <p [innerHTML]="q_h"></p>  \n                              <p>  Repérage d’intrus (5 mots, il faut en sélectionner 2) </p>\n                            </ion-card-content>\n                          </ion-card>\n                        </ion-list>\n                        <ion-list padding>  \n                                \n        \n                               \n                                <ion-label color="primary"> Définition 1 </ion-label>\n                                <p>Choisir la bonne réponse </p>\n                                <select [(ngModel)]="result1" (click)="update1()">\n                                    <option value="{{item.id}}" *ngFor="let item of options" >{{item.prop}}</option>\n                                </select>\n                              \n                                <ion-label color="primary"> Définition 2 </ion-label>\n                                <p>Choisir la bonne réponse </p>\n                                <select [(ngModel)]="result2" (click)="update1()">\n                                    <option value="{{item.id}}" *ngFor="let item of options" >{{item.prop}}</option>\n                                </select>\n        \n                                <ion-label color="primary"> Définition 3 </ion-label>\n                                <p>Choisir la bonne réponse </p>\n                                <select [(ngModel)]="result3" (click)="update1()">\n                                    <option value="{{item.id}}" *ngFor="let item of options" >{{item.prop}}</option>\n                                </select>\n        \n                                <ion-label color="primary"> Définition 4 </ion-label>\n                                <p>Choisir la bonne réponse </p>\n                                <select [(ngModel)]="result4" (click)="update1()">\n                                    <option value="{{item.id}}" *ngFor="let item of options" >{{item.prop}}</option>\n                                </select>\n        \n                                <ion-label color="primary"> Définition 5 </ion-label>\n                                <p>Choisir la bonne réponse </p>\n                                <select [(ngModel)]="result5" (click)="update1()">\n                                    <option value="{{item.id}}" *ngFor="let item of options" >{{item.prop}}</option>\n                                </select>\n                                        \n                     \n                        </ion-list>\n        \n                        <ion-list padding>  \n                                <ion-row responsive-sm  >\n                                    <ion-col col-2 > \n                                    <button ion-button full (click)="prev()" >\n                                    Précedent\n                                    </button>\n                                    </ion-col> \n                                    <ion-col col-8 >  \n                                    </ion-col> \n                                    <ion-col col-2 > \n                                    <button ion-button  full (click)="next()" [disabled]="checked==false">\n                                    Suivant\n                                    </button>\n                                    </ion-col> \n                                </ion-row>\n                            </ion-list>\n                      </div>\n                      <div *ngIf="test_nav==4">\n                          <ion-list >  \n                            <ion-card>\n                              <ion-card-header>\n                               <b> {{question_label}}</b>\n                                 {{indexdisplay}}/{{nbquestionbytest}}\n                              </ion-card-header>\n                              <ion-card-content>\n                                  <p [innerHTML]="q_h"></p>  \n                                \n                              </ion-card-content>\n                            </ion-card>\n                          </ion-list>\n                          <ion-list padding>  \n                                  \n          \n                                 \n                                  <ion-label color="primary"> Définition 1 </ion-label>\n                                  <p>Choisir la bonne réponse </p>\n                                  <select [(ngModel)]="result1" (click)="update1()">\n                                      <option value="{{item.id}}" *ngFor="let item of options" >{{item.prop}}</option>\n                                  </select>\n                                \n                                  <ion-label color="primary"> Définition 2 </ion-label>\n                                  <p>Choisir la bonne réponse </p>\n                                  <select [(ngModel)]="result2" (click)="update1()">\n                                      <option value="{{item.id}}" *ngFor="let item of options" >{{item.prop}}</option>\n                                  </select>\n          \n                                  <ion-label color="primary"> Définition 3 </ion-label>\n                                  <p>Choisir la bonne réponse </p>\n                                  <select [(ngModel)]="result3" (click)="update1()">\n                                      <option value="{{item.id}}" *ngFor="let item of options" >{{item.prop}}</option>\n                                  </select>\n          \n                                  <ion-label color="primary"> Définition 4 </ion-label>\n                                  <p>Choisir la bonne réponse </p>\n                                  <select [(ngModel)]="result4" (click)="update1()">\n                                      <option value="{{item.id}}" *ngFor="let item of options" >{{item.prop}}</option>\n                                  </select>\n          \n                                  <ion-label color="primary"> Définition 5 </ion-label>\n                                  <p>Choisir la bonne réponse </p>\n                                  <select [(ngModel)]="result5" (click)="update1()">\n                                      <option value="{{item.id}}" *ngFor="let item of options" >{{item.prop}}</option>\n                                  </select>\n                                          \n                       \n                          </ion-list>\n          \n                          <ion-list padding>  \n                                  <ion-row responsive-sm  >\n                                      <ion-col col-2 > \n                                    \n                                      </ion-col> \n                                      <ion-col col-8 >  \n                                      </ion-col> \n                                      <ion-col col-2 > \n                                      <button ion-button  full (click)="next()" [disabled]="checked==false">\n                                      Suivant\n                                      </button>\n                                      </ion-col> \n                                  </ion-row>\n                              </ion-list>\n                        </div>\n </ion-col>\n <ion-col col-2 > \n    {{question_num}}\n          </ion-col> \n        </ion-row>\n\n        <div style="width:70%; text-align: center; margin: 0 auto;" >\n                <div class="progress-outer" style="margin: 1px 1px;padding: 1px;text-align: center;background-color: #cfcfcf;border: 1px solid #dcdcdc;color: #fff;border-radius: 20px;">\n                <div class="progress-inner" [style.width]="progressPercent + \'%\'" style="width: 96%;height:20px;margin: 2px 2px;text-align: center;background-color: #84bdf1;border: 1px solid #dcdcdc;color: #fff;border-radius: 20px;">\n                   {{progressPercent}}%\n                </div>\n                </div>\n                </div> \n      </ion-grid>\n  </ion-content>\n  \n'/*ion-inline-end:"/Users/Maxime/Desktop/passation/src/pages/verbaldefinition/verbaldefinition.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_9__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["f" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_7__providers_AuthService__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_5_ng2_dragula__["b" /* DragulaService */], __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["f" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_2__providers_reddit_service__["a" /* RedditService */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_10__angular_platform_browser__["c" /* DomSanitizer */]])
    ], VerbaldefinitionPage);
    return VerbaldefinitionPage;
    var VerbaldefinitionPage_1;
}());

//# sourceMappingURL=verbaldefinition.js.map

/***/ }),

/***/ 362:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VerbaleliminationPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_add_operator_map__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_reddit_service__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng2_dragula__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__endtest_endtest__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_AuthService__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__login_login__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_forms__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__angular_platform_browser__ = __webpack_require__(39);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var VerbaleliminationPage = /** @class */ (function () {
    function VerbaleliminationPage(fb, loadingController, authService, dragulaService, loadingCtrl, navCtrl, storage, redditService, navParams, sanitizer) {
        var _this = this;
        this.loadingController = loadingController;
        this.authService = authService;
        this.dragulaService = dragulaService;
        this.loadingCtrl = loadingCtrl;
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.redditService = redditService;
        this.navParams = navParams;
        this.sanitizer = sanitizer;
        this.editing = true;
        this.dataq = [];
        this.indextest = 0;
        this.indexdisplay = 1;
        this.posts2 = [];
        this.checked = false;
        this.checkedIdx = -1;
        this.options = [];
        this.reponse = [];
        this.reponsesave = [];
        this.responsestring = "";
        this.progressPercent = 0;
        this.storage.get('idresponse').then(function (idresponse) {
            _this.idresponse = idresponse;
        });
        this.idtest = navParams.get('param1');
        this.testlenght = navParams.get('param2');
        this.testcategory = navParams.get('param4');
        this.timeInSeconds = navParams.get('param5');
        this.test_nav = navParams.get('param6');
        this.position = navParams.get('param7');
        this.form = fb.group({
            cbox: [false, VerbaleliminationPage_1.mustBeTruthy]
        });
        console.log(this.checked);
    }
    VerbaleliminationPage_1 = VerbaleliminationPage;
    VerbaleliminationPage.prototype.transform = function (value) {
        return this.sanitizer.bypassSecurityTrustHtml(value);
    };
    VerbaleliminationPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        if (this.timeInSeconds > 0) {
            this.startTimer();
        }
        this.indexdisplay = 1;
        //Données i=0
        this.storage.get("questions").then(function (users) {
            _this.dataq = JSON.parse(users);
            _this.nbquestionbytest = _this.dataq.length;
            _this.question_label = _this.dataq[0].question_label;
            _this.q_prop1 = _this.dataq[0].q_prop1;
            _this.q_prop2 = _this.dataq[0].q_prop2;
            _this.q_prop3 = _this.dataq[0].q_prop3;
            _this.q_prop4 = _this.dataq[0].q_prop4;
            _this.q_prop5 = _this.dataq[0].q_prop5;
            _this.q_h = _this.dataq[0].q_h;
            _this.q_f = _this.dataq[0].q_f;
            _this.options = [{ id: '1', prop: _this.q_prop1 }, { id: '2', prop: _this.q_prop2 }, { id: '3', prop: _this.q_prop3 }, { id: '4', prop: _this.q_prop4 },
                { id: '5', prop: _this.q_prop5 }];
            _this.question_num = _this.dataq[0].question_num;
            _this.checkedIdx = -1;
            //Array reponse
            for (var i = 0; i < _this.nbquestionbytest; i++) {
                _this.reponsesave[i] = [{ result1: '0' }, { result2: '0' }, { result3: '0' }, { result4: '0' }, { result5: '0' }];
            }
            console.log(_this.reponsesave);
        });
    };
    VerbaleliminationPage.prototype.update1 = function () {
        if (this.result1 > 0 && this.result2 > 0) {
            this.checked = true;
        }
        else {
            this.checked = false;
        }
    };
    VerbaleliminationPage.prototype.next = function () {
        this.reponsesave[this.indextest] = [{ result1: this.result1 }, { result2: this.result2 }, { result3: this.result3 }, { result4: this.result4 }, { result5: this.result5 }];
        // new question
        this.indextest = this.indextest + 1;
        // toutes les réponses
        console.log("réponse");
        console.log(this.reponsesave);
        if (this.indextest < this.nbquestionbytest) {
            this.indexdisplay = this.indexdisplay + 1;
            this.retrieveQuestions();
        }
        else {
            this.pauseTimer();
            console.log(this.reponsesave);
            ////// SAVE DATA
            ////// SAVE DATA
            console.log(this.testlenght);
            for (var i = 0; i < this.testlenght; i++) {
                this.rep1 = this.reponsesave[i][0].result1;
                this.rep2 = this.reponsesave[i][1].result2;
                this.responsezero = this.rep1 + this.rep2;
                this.responsestring = this.responsestring + this.responsezero + ",";
                console.log(this.responsestring);
            }
            /////
            var data = JSON.stringify({
                name: "VerbalElimination",
                idresponse: this.idresponse,
                resp: this.responsestring,
            });
            this.redditService.addresponses(data)
                .toPromise()
                .then(function (response) {
                console.log(response);
            });
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__endtest_endtest__["a" /* EndtestPage */], {
                param1: this.idtest,
                param2: this.position,
            });
        }
    };
    VerbaleliminationPage.prototype.prev = function () {
        if (this.indextest > 0) {
            this.indextest = this.indextest - 1;
            this.indexdisplay = this.indexdisplay - 1;
            this.reviewQuestions();
        }
        else {
        }
    };
    VerbaleliminationPage.prototype.retrieveQuestions = function () {
        var _this = this;
        this.storage.get("questions").then(function (users) {
            _this.dataq = JSON.parse(users);
            _this.question_label = _this.dataq[_this.indextest].question_label;
            _this.propositions = _this.dataq[_this.indextest];
            _this.q_prop1 = _this.dataq[_this.indextest].q_prop1;
            _this.q_prop2 = _this.dataq[_this.indextest].q_prop2;
            _this.q_prop3 = _this.dataq[_this.indextest].q_prop3;
            _this.q_prop4 = _this.dataq[_this.indextest].q_prop4;
            _this.q_prop5 = _this.dataq[_this.indextest].q_prop5;
            _this.q_h = _this.dataq[_this.indextest].q_h;
            _this.q_f = _this.dataq[_this.indextest].q_f;
            _this.checkedIdx = _this.reponsesave[_this.indextest];
            _this.question_num = _this.dataq[_this.indextest].question_num;
            _this.options = [{ id: '1', prop: _this.q_prop1 }, { id: '2', prop: _this.q_prop2 }, { id: '3', prop: _this.q_prop3 }, { id: '4', prop: _this.q_prop4 },
                { id: '5', prop: _this.q_prop5 }];
            _this.result1 = _this.reponsesave[_this.indextest][0].result1;
            _this.result2 = _this.reponsesave[_this.indextest][1].result2;
            _this.result3 = _this.reponsesave[_this.indextest][2].result3;
            _this.result4 = _this.reponsesave[_this.indextest][3].result4;
            _this.result5 = _this.reponsesave[_this.indextest][4].result5;
            if (_this.result1 > 0 || _this.result2 > 0) {
                _this.checked = true;
            }
            else {
                _this.checked = false;
            }
        });
    };
    VerbaleliminationPage.prototype.reviewQuestions = function () {
        var _this = this;
        this.storage.get("questions").then(function (users) {
            _this.dataq = JSON.parse(users);
            _this.question_label = _this.dataq[_this.indextest].question_label;
            _this.propositions = _this.dataq[_this.indextest];
            _this.q_prop1 = _this.dataq[_this.indextest].q_prop1;
            _this.q_prop2 = _this.dataq[_this.indextest].q_prop2;
            _this.q_prop3 = _this.dataq[_this.indextest].q_prop3;
            _this.q_prop4 = _this.dataq[_this.indextest].q_prop4;
            _this.q_prop5 = _this.dataq[_this.indextest].q_prop5;
            _this.q_h = _this.dataq[_this.indextest].q_h;
            _this.q_f = _this.dataq[_this.indextest].q_f;
            _this.question_num = _this.dataq[_this.indextest].question_num;
            _this.options = [{ id: '1', prop: _this.q_prop1 }, { id: '2', prop: _this.q_prop2 }, { id: '3', prop: _this.q_prop3 }, { id: '4', prop: _this.q_prop4 },
                { id: '5', prop: _this.q_prop5 }];
            _this.result1 = _this.reponsesave[_this.indextest][0].result1;
            _this.result2 = _this.reponsesave[_this.indextest][1].result2;
            _this.result3 = _this.reponsesave[_this.indextest][2].result3;
            _this.result4 = _this.reponsesave[_this.indextest][3].result4;
            _this.result5 = _this.reponsesave[_this.indextest][4].result5;
            if (_this.result1 > 0 || _this.result2 > 0) {
                _this.checked = true;
            }
            else {
                _this.checked = false;
            }
        });
    };
    VerbaleliminationPage.mustBeTruthy = function (c) {
        var rv = {};
        if (!c.value) {
            rv['notChecked'] = true;
        }
        return rv;
    };
    VerbaleliminationPage.prototype.reorderData = function (indexes) {
        this.posts2 = Object(__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["o" /* reorderArray */])(this.posts2, indexes);
        console.log(this.posts2);
        console.log(indexes);
        //this.dataq[this.indextest] = reorderArray(this.dataq[this.indextest], indexes);
        // console.log(this.dataq[this.indextest]);
        // console.log(this.dataq[this.indextest]);
    };
    VerbaleliminationPage.prototype.logOut = function () {
        this.authService.logout();
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__login_login__["a" /* Login */]);
    };
    VerbaleliminationPage.prototype.ngOnInit = function () {
        this.initTimer();
    };
    VerbaleliminationPage.prototype.initTimer = function () {
        this.time = this.timeInSeconds;
        this.runTimer = false;
        this.hasStarted = false;
        this.hasFinished = false;
        this.remainingTime = this.timeInSeconds;
        this.displayTime = this.getSecondsAsDigitalClock(this.remainingTime);
        this.progressTimer = (this.remainingTime * 100) / this.time;
        this.progressPercent = (100 - this.progressTimer).toFixed(0);
    };
    VerbaleliminationPage.prototype.startTimer = function () {
        this.runTimer = true;
        this.hasStarted = true;
        this.timerTick();
    };
    VerbaleliminationPage.prototype.pauseTimer = function () {
        this.runTimer = false;
    };
    VerbaleliminationPage.prototype.resumeTimer = function () {
        this.startTimer();
    };
    VerbaleliminationPage.prototype.timerTick = function () {
        var _this = this;
        setTimeout(function () {
            if (!_this.runTimer) {
                return;
            }
            _this.remainingTime--;
            _this.displayTime = _this.getSecondsAsDigitalClock(_this.remainingTime);
            if (_this.remainingTime > 0) {
                _this.timerTick();
            }
            else {
                _this.hasFinished = true;
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__endtest_endtest__["a" /* EndtestPage */], {
                    param1: _this.idtest,
                    param2: _this.position,
                });
            }
        }, 1000);
    };
    VerbaleliminationPage.prototype.getSecondsAsDigitalClock = function (inputSeconds) {
        var sec_num = parseInt(inputSeconds.toString(), 10); // don't forget the second param
        var hours = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);
        var hoursString = '';
        var minutesString = '';
        var secondsString = '';
        hoursString = (hours < 10) ? "0" + hours : hours.toString();
        minutesString = (minutes < 10) ? "0" + minutes : minutes.toString();
        secondsString = (seconds < 10) ? "0" + seconds : seconds.toString();
        return hoursString + ':' + minutesString + ':' + secondsString;
    };
    VerbaleliminationPage = VerbaleliminationPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Component"])({
            providers: [__WEBPACK_IMPORTED_MODULE_5_ng2_dragula__["b" /* DragulaService */]],
            selector: 'page-verbalelimination',template:/*ion-inline-start:"/Users/Maxime/Desktop/passation/src/pages/verbalelimination/verbalelimination.html"*/'<ion-header>\n  <ion-toolbar  color="secondary" hide-back-button >\n        <ion-title>  </ion-title>\n          <h3 text-center *ngIf="displayTime!==0">{{displayTime}}</h3>\n        <ion-buttons end>\n          <button ion-button icon-only (click)="logOut()">\n              <ion-icon ios="ios-log-out" md="md-log-out"></ion-icon>\n          </button>\n        </ion-buttons>\n  </ion-toolbar>  \n</ion-header>\n\n<ion-content color="light2" >\n\n    <ion-grid >\n  \n        <ion-row responsive-sm  >\n            <ion-col col-2 > \n            </ion-col> \n            <ion-col col-8 >  \n              \n                    <div *ngIf="test_nav==1">\n                            <ion-list >  \n                                    <ion-card>\n                                      <ion-card-header>\n                                       <b> {{question_label}}</b>\n                                         {{indexdisplay}}/{{nbquestionbytest}}\n                                      </ion-card-header>\n                                      <ion-card-content>\n                                          <p [innerHTML]="q_h"></p>  \n                                        <p>  Repérage d’intrus (5 mots, il faut en sélectionner 2) </p>\n          \n                                        <b>  {{q_prop1}} / {{q_prop2}} / {{q_prop3}} / {{q_prop4}} / {{q_prop5}}</b>\n                                      </ion-card-content>\n                                    </ion-card>\n                                  </ion-list>\n                                  <ion-list padding>  \n                                          \n                  \n                                         \n                                          <ion-label color="primary"> Choisir le premier intrus </ion-label>\n                                        \n                                          <select [(ngModel)]="result1" (click)="update1()">\n                                              <option value="{{item.id}}" *ngFor="let item of options" >{{item.prop}}</option>\n                                          </select>\n                                        \n                                          <ion-label color="primary"> Choisir le second intus</ion-label>\n                                     \n                                          <select [(ngModel)]="result2" (click)="update1()">\n                                              <option value="{{item.id}}" *ngFor="let item of options" >{{item.prop}}</option>\n                                          </select>\n                  \n                                                  \n                               \n                                  </ion-list>\n        \n                        <ion-list padding>  \n                                <ion-row responsive-sm  >\n                                    <ion-col col-2 > \n                                    <button ion-button full (click)="prev()" >\n                                    Précedent\n                                    </button>\n                                    </ion-col> \n                                    <ion-col col-8 >  \n                                    </ion-col> \n                                    <ion-col col-2 > \n                                    <button ion-button  full (click)="next()" >\n                                    Suivant\n                                    </button>\n                                    </ion-col> \n                                </ion-row>\n                            </ion-list>\n                      </div>\n                      <div *ngIf="test_nav==2">\n                            <ion-list >  \n                                    <ion-card>\n                                      <ion-card-header>\n                                       <b> {{question_label}}</b>\n                                         {{indexdisplay}}/{{nbquestionbytest}}\n                                      </ion-card-header>\n                                      <ion-card-content>\n                                          <p [innerHTML]="q_h"></p>  \n                                        <p>  Repérage d’intrus (5 mots, il faut en sélectionner 2) </p>\n          \n                                        <b>  {{q_prop1}} / {{q_prop2}} / {{q_prop3}} / {{q_prop4}} / {{q_prop5}}</b>\n                                      </ion-card-content>\n                                    </ion-card>\n                                  </ion-list>\n                                  <ion-list padding>  \n                                          \n                  \n                                         \n                                          <ion-label color="primary"> Choisir le premier intrus </ion-label>\n                                        \n                                          <select [(ngModel)]="result1" (click)="update1()">\n                                              <option value="{{item.id}}" *ngFor="let item of options" >{{item.prop}}</option>\n                                          </select>\n                                        \n                                          <ion-label color="primary"> Choisir le second intus</ion-label>\n                                     \n                                          <select [(ngModel)]="result2" (click)="update1()">\n                                              <option value="{{item.id}}" *ngFor="let item of options" >{{item.prop}}</option>\n                                          </select>\n                  \n                                                  \n                               \n                                  </ion-list>\n        \n                        <ion-list padding>  \n                                <ion-row responsive-sm  >\n                                    <ion-col col-2 > \n                                  \n                                    </ion-col> \n                                    <ion-col col-8 >  \n                                    </ion-col> \n                                    <ion-col col-2 > \n                                    <button ion-button  full (click)="next()" >\n                                    Suivant\n                                    </button>\n                                    </ion-col> \n                                </ion-row>\n                            </ion-list>\n                      </div>\n                      <div *ngIf="test_nav==3">\n                            <ion-list >  \n                                    <ion-card>\n                                      <ion-card-header>\n                                       <b> {{question_label}}</b>\n                                         {{indexdisplay}}/{{nbquestionbytest}}\n                                      </ion-card-header>\n                                      <ion-card-content>\n                                          <p [innerHTML]="q_h"></p>  \n                                        <p>  Repérage d’intrus (5 mots, il faut en sélectionner 2) </p>\n          \n                                        <b>  {{q_prop1}} / {{q_prop2}} / {{q_prop3}} / {{q_prop4}} / {{q_prop5}}</b>\n                                      </ion-card-content>\n                                    </ion-card>\n                                  </ion-list>\n                                  <ion-list padding>  \n                                          \n                  \n                                         \n                                          <ion-label color="primary"> Choisir le premier intrus </ion-label>\n                                        \n                                          <select [(ngModel)]="result1" (click)="update1()">\n                                              <option value="{{item.id}}" *ngFor="let item of options" >{{item.prop}}</option>\n                                          </select>\n                                        \n                                          <ion-label color="primary"> Choisir le second intus</ion-label>\n                                     \n                                          <select [(ngModel)]="result2" (click)="update1()">\n                                              <option value="{{item.id}}" *ngFor="let item of options" >{{item.prop}}</option>\n                                          </select>\n                  \n                                                  \n                               \n                                  </ion-list>\n        \n                        <ion-list padding>  \n                                <ion-row responsive-sm  >\n                                    <ion-col col-2 > \n                                    <button ion-button full (click)="prev()" >\n                                    Précedent\n                                    </button>\n                                    </ion-col> \n                                    <ion-col col-8 >  \n                                    </ion-col> \n                                    <ion-col col-2 > \n                                    <button ion-button  full (click)="next()" [disabled]="checked==false">\n                                    Suivant\n                                    </button>\n                                    </ion-col> \n                                </ion-row>\n                            </ion-list>\n                      </div>\n                      <div *ngIf="test_nav==4">\n                            <ion-list >  \n                                    <ion-card>\n                                      <ion-card-header>\n                                       <b> {{question_label}}</b>\n                                         {{indexdisplay}}/{{nbquestionbytest}}\n                                      </ion-card-header>\n                                      <ion-card-content>\n                                          <p [innerHTML]="q_h"></p>  \n                                        <p>  Repérage d’intrus (5 mots, il faut en sélectionner 2) </p>\n          \n                                        <b>  {{q_prop1}} / {{q_prop2}} / {{q_prop3}} / {{q_prop4}} / {{q_prop5}}</b>\n                                      </ion-card-content>\n                                    </ion-card>\n                                  </ion-list>\n                                  <ion-list padding>  \n                                          \n                  \n                                         \n                                          <ion-label color="primary"> Choisir le premier intrus </ion-label>\n                                        \n                                          <select [(ngModel)]="result1" (click)="update1()">\n                                              <option value="{{item.id}}" *ngFor="let item of options" >{{item.prop}}</option>\n                                          </select>\n                                        \n                                          <ion-label color="primary"> Choisir le second intus</ion-label>\n                                     \n                                          <select [(ngModel)]="result2" (click)="update1()">\n                                              <option value="{{item.id}}" *ngFor="let item of options" >{{item.prop}}</option>\n                                          </select>\n                  \n                                                  \n                               \n                                  </ion-list>\n        \n                        <ion-list padding>  \n                                <ion-row responsive-sm  >\n                                    <ion-col col-2 > \n                                 \n                                    </ion-col> \n                                    <ion-col col-8 >  \n                                    </ion-col> \n                                    <ion-col col-2 > \n                                    <button ion-button  full (click)="next()" [disabled]="checked==false">\n                                    Suivant\n                                    </button>\n                                    </ion-col> \n                                </ion-row>\n                            </ion-list>\n                      </div>\n </ion-col>\n <ion-col col-2 > \n    {{question_num}}\n          </ion-col> \n        </ion-row>\n\n        <div style="width:70%; text-align: center; margin: 0 auto;" >\n                <div class="progress-outer" style="margin: 1px 1px;padding: 1px;text-align: center;background-color: #cfcfcf;border: 1px solid #dcdcdc;color: #fff;border-radius: 20px;">\n                <div class="progress-inner" [style.width]="progressPercent + \'%\'" style="width: 96%;height:20px;margin: 2px 2px;text-align: center;background-color: #84bdf1;border: 1px solid #dcdcdc;color: #fff;border-radius: 20px;">\n                   {{progressPercent}}%\n                </div>\n                </div>\n                </div> \n      </ion-grid>\n  </ion-content>\n  \n'/*ion-inline-end:"/Users/Maxime/Desktop/passation/src/pages/verbalelimination/verbalelimination.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_9__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["f" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_7__providers_AuthService__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_5_ng2_dragula__["b" /* DragulaService */], __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["f" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_2__providers_reddit_service__["a" /* RedditService */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_10__angular_platform_browser__["c" /* DomSanitizer */]])
    ], VerbaleliminationPage);
    return VerbaleliminationPage;
    var VerbaleliminationPage_1;
}());

//# sourceMappingURL=verbalelimination.js.map

/***/ }),

/***/ 363:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MemoirePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_add_operator_map__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_reddit_service__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng2_dragula__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__endtest_endtest__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_AuthService__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__login_login__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_forms__ = __webpack_require__(20);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var MemoirePage = /** @class */ (function () {
    function MemoirePage(fb, loadingController, authService, dragulaService, loadingCtrl, navCtrl, storage, redditService, navParams) {
        var _this = this;
        this.loadingController = loadingController;
        this.authService = authService;
        this.dragulaService = dragulaService;
        this.loadingCtrl = loadingCtrl;
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.redditService = redditService;
        this.navParams = navParams;
        this.editing = true;
        this.dataq = [];
        this.indextest = 0;
        this.indexdisplay = 1;
        this.posts2 = [];
        this.checkedIdx = -1;
        this.options = [];
        this.reponse = [];
        this.reponsesave = [];
        this.reponsesave2 = [];
        this.reponsesave3 = [];
        this.hide = 3;
        this.responsestring = "";
        this.response1 = "";
        this.response2 = "";
        this.response3 = "";
        this.response4 = "";
        this.response5 = "";
        this.urlimage = "https://api.selectionetconseils.ch/uploads/image/";
        this.progressPercent = 0;
        this.testnumber = 0;
        this.idtest = navParams.get('param1');
        this.testlenght = navParams.get('param2');
        this.testcategory = navParams.get('param4');
        this.timeInSeconds = navParams.get('param5');
        this.test_nav = navParams.get('param6');
        this.position = navParams.get('param7');
        this.storage.get('idresponse').then(function (idresponse) {
            _this.idresponse = idresponse;
        });
        this.form = fb.group({
            cbox: [false, MemoirePage_1.mustBeTruthy]
        });
    }
    MemoirePage_1 = MemoirePage;
    MemoirePage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.hide = 1;
        if (this.timeInSeconds > 0) {
            this.startTimer();
        }
        this.indexdisplay = 1;
        //Données i=0
        this.storage.get("questions").then(function (users) {
            _this.dataq = JSON.parse(users);
            _this.nbquestionbytest = _this.dataq.length;
            //Array reponse
            for (var i = 0; i < _this.nbquestionbytest; i++) {
                _this.reponsesave[i] = -1;
            }
            for (var i = 0; i < _this.nbquestionbytest; i++) {
                _this.reponsesave2[i] = -1;
            }
            for (var i = 0; i < _this.nbquestionbytest; i++) {
                _this.reponsesave3[i] = -1;
            }
        });
    };
    MemoirePage.prototype.displayimages = function () {
        this.hide = 0;
        console.log(this.hide);
        this.indexdisplay = 0;
        this.indextest = -1;
        this.next();
    };
    MemoirePage.prototype.displayimages2 = function () {
        this.hide = 0;
        console.log(this.hide);
        this.indexdisplay = 0;
        this.indextest = -1;
        this.next3();
    };
    MemoirePage.prototype.displayimage3 = function () {
        this.hide = 0;
        console.log(this.hide);
        this.indexdisplay = 0;
        this.indextest = -1;
        this.next3();
    };
    MemoirePage.prototype.displayintro3 = function () {
        this.hide = 7;
        console.log(this.hide);
    };
    MemoirePage.prototype.next = function () {
        this.reponsesave[this.indextest] = this.checkedIdx;
        // new question
        this.indextest = this.indextest + 1;
        if (this.indextest < this.nbquestionbytest) {
            this.indexdisplay = this.indexdisplay + 1;
            this.retrieveQuestions();
        }
        else {
            this.hide = 2;
            this.indextest = -1;
            this.indexdisplay = 0;
            this.next2();
        }
    };
    MemoirePage.prototype.next2 = function () {
        console.log(this.hide);
        this.testnumber = this.testnumber + 1;
        console.log(this.testnumber);
        this.reponsesave2[this.indextest] = this.checkedIdx;
        this.indextest = this.indextest + 1;
        if (this.indextest < this.nbquestionbytest) {
            this.indexdisplay = this.indexdisplay + 1;
            this.retrieveQuestions2();
        }
        else {
            this.hide = 4;
            for (var i = 0; i < this.testlenght; i++) {
                this.responseun = this.reponsesave2[i];
                this.responsestring = this.responsestring + this.responseun;
                console.log(this.responsestring);
            }
        }
    };
    MemoirePage.prototype.next3 = function () {
        console.log(this.hide);
        this.testnumber = this.testnumber + 1;
        console.log(this.testnumber);
        this.reponsesave2[this.indextest] = this.checkedIdx;
        this.indextest = this.indextest + 1;
        if (this.indextest < this.nbquestionbytest) {
            this.indexdisplay = this.indexdisplay + 1;
            this.retrieveQuestions2();
        }
        else {
            this.hide = 5;
            for (var i = 0; i < this.testlenght; i++) {
                this.responsdeux = this.reponsesave2[i];
                this.responsestring2 = this.responsestring + this.responsdeux + ",";
                console.log(this.responsestring);
            }
        }
    };
    MemoirePage.prototype.next5 = function () {
        this.testnumber = this.testnumber + 1;
        console.log(this.testnumber);
        this.reponsesave3[this.indextest] = this.checkedIdx;
        this.indextest = this.indextest + 1;
        if (this.indextest < this.nbquestionbytest) {
            this.indexdisplay = this.indexdisplay + 1;
            this.retrieveQuestions2();
        }
        else {
            this.hide = 6;
            ////// SAVE DATA
            for (var i = 0; i < this.testlenght; i++) {
                this.responsetrois = this.reponsesave3[i];
                this.responsestring3 = this.responsestring + this.responsetrois + ",";
            }
            var data = JSON.stringify({
                name: "Memoire",
                idresponse: this.idresponse,
                resp: this.responsestring + "," + this.responsestring2 + "," + this.responsestring3 + ",",
            });
            console.log(data);
            this.redditService.addresponses(data)
                .toPromise()
                .then(function (response) {
                console.log(response);
            });
        }
    };
    MemoirePage.prototype.finish = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__endtest_endtest__["a" /* EndtestPage */], {
            param1: this.idtest,
            param2: this.position,
        });
    };
    MemoirePage.prototype.prev = function () {
        if (this.indextest > 0) {
            this.indextest = this.indextest - 1;
            this.indexdisplay = this.indexdisplay - 1;
            this.reviewQuestions();
        }
        else {
        }
    };
    MemoirePage.prototype.retrieveQuestions = function () {
        var _this = this;
        this.storage.get("questions").then(function (users) {
            _this.dataq = JSON.parse(users);
            _this.question_label = _this.dataq[_this.indextest].question_label;
            _this.propositions = _this.dataq[_this.indextest];
            _this.q_prop1 = _this.dataq[_this.indextest].q_prop1;
            _this.q_prop2 = _this.dataq[_this.indextest].q_prop2;
            _this.q_prop3 = _this.dataq[_this.indextest].q_prop3;
            _this.q_prop4 = _this.dataq[_this.indextest].q_prop4;
            _this.q_prop5 = _this.dataq[_this.indextest].q_prop5;
            _this.q_img1 = _this.urlimage + _this.dataq[_this.indextest].q_img1;
            _this.q_h = _this.dataq[_this.indextest].q_h;
            _this.q_f = _this.dataq[_this.indextest].q_f;
            _this.checkedIdx = _this.reponsesave[_this.indextest];
            _this.options = [_this.q_img1];
            _this.options2 = [_this.q_prop1, _this.q_prop2, _this.q_prop3, _this.q_prop4, _this.q_prop5];
            _this.question_num = _this.dataq[_this.indextest].question_num;
            setTimeout(function () {
                _this.next();
            }, 2500);
        });
    };
    MemoirePage.prototype.retrieveQuestions2 = function () {
        var _this = this;
        this.storage.get("questions").then(function (users) {
            _this.dataq = JSON.parse(users);
            _this.question_label = _this.dataq[_this.indextest].question_label;
            _this.propositions = _this.dataq[_this.indextest];
            _this.q_prop1 = _this.dataq[_this.indextest].q_prop1;
            _this.q_prop2 = _this.dataq[_this.indextest].q_prop2;
            _this.q_prop3 = _this.dataq[_this.indextest].q_prop3;
            _this.q_prop4 = _this.dataq[_this.indextest].q_prop4;
            _this.q_prop5 = _this.dataq[_this.indextest].q_prop5;
            _this.q_img1 = _this.urlimage + _this.dataq[_this.indextest].q_img1;
            _this.q_h = _this.dataq[_this.indextest].q_h;
            _this.q_f = _this.dataq[_this.indextest].q_f;
            _this.checkedIdx = _this.reponsesave[_this.indextest];
            _this.options = [_this.q_img1];
            _this.options2 = [_this.q_prop1, _this.q_prop2, _this.q_prop3, _this.q_prop4, _this.q_prop5];
            _this.question_num = _this.dataq[_this.indextest].question_num;
            setTimeout(function () {
                _this.next2();
            }, 2500);
        });
    };
    MemoirePage.prototype.reviewQuestions = function () {
        var _this = this;
        this.storage.get("questions").then(function (users) {
            _this.dataq = JSON.parse(users);
            _this.question_label = _this.dataq[_this.indextest].question_label;
            _this.propositions = _this.dataq[_this.indextest];
            _this.q_prop1 = _this.dataq[_this.indextest].q_prop1;
            _this.q_prop2 = _this.dataq[_this.indextest].q_prop2;
            _this.q_prop3 = _this.dataq[_this.indextest].q_prop3;
            _this.q_prop4 = _this.dataq[_this.indextest].q_prop4;
            _this.q_prop5 = _this.dataq[_this.indextest].q_prop5;
            _this.q_img1 = _this.urlimage + _this.dataq[_this.indextest].q_img1;
            _this.q_img2 = _this.urlimage + _this.dataq[_this.indextest].q_img2;
            _this.q_img3 = _this.urlimage + _this.dataq[_this.indextest].q_img3;
            _this.q_img4 = _this.urlimage + _this.dataq[_this.indextest].q_img4;
            _this.q_img5 = _this.urlimage + _this.dataq[_this.indextest].q_img5;
            _this.q_h = _this.dataq[_this.indextest].q_h;
            _this.q_f = _this.dataq[_this.indextest].q_f;
            _this.checkedIdx = _this.reponsesave[_this.indextest];
            _this.options = [_this.q_img1];
            _this.options2 = [_this.q_prop1, _this.q_prop2, _this.q_prop3, _this.q_prop4, _this.q_prop5];
            _this.question_num = _this.dataq[_this.indextest].question_num;
        });
    };
    MemoirePage.mustBeTruthy = function (c) {
        var rv = {};
        if (!c.value) {
            rv['notChecked'] = true;
        }
        return rv;
    };
    MemoirePage.prototype.logOut = function () {
        this.authService.logout();
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__login_login__["a" /* Login */]);
    };
    MemoirePage.prototype.ngOnInit = function () {
        this.initTimer();
    };
    MemoirePage.prototype.initTimer = function () {
        this.time = this.timeInSeconds;
        this.runTimer = false;
        this.hasStarted = false;
        this.hasFinished = false;
        this.remainingTime = this.timeInSeconds;
        this.displayTime = this.getSecondsAsDigitalClock(this.remainingTime);
    };
    MemoirePage.prototype.startTimer = function () {
        this.runTimer = true;
        this.hasStarted = true;
        this.timerTick();
    };
    MemoirePage.prototype.pauseTimer = function () {
        this.runTimer = false;
    };
    MemoirePage.prototype.resumeTimer = function () {
        this.startTimer();
    };
    MemoirePage.prototype.timerTick = function () {
        var _this = this;
        setTimeout(function () {
            if (!_this.runTimer) {
                return;
            }
            _this.remainingTime--;
            _this.progressTimer = (_this.remainingTime * 100) / _this.time;
            _this.progressPercent = (100 - _this.progressTimer).toFixed(0);
            _this.displayTime = _this.getSecondsAsDigitalClock(_this.remainingTime);
            if (_this.remainingTime > 0) {
                _this.timerTick();
            }
            else {
                _this.hasFinished = true;
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__endtest_endtest__["a" /* EndtestPage */], {
                    param1: _this.idtest,
                    param2: _this.position,
                });
            }
        }, 1000);
    };
    MemoirePage.prototype.getSecondsAsDigitalClock = function (inputSeconds) {
        var sec_num = parseInt(inputSeconds.toString(), 10); // don't forget the second param
        var hours = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);
        var hoursString = '';
        var minutesString = '';
        var secondsString = '';
        hoursString = (hours < 10) ? "0" + hours : hours.toString();
        minutesString = (minutes < 10) ? "0" + minutes : minutes.toString();
        secondsString = (seconds < 10) ? "0" + seconds : seconds.toString();
        return hoursString + ':' + minutesString + ':' + secondsString;
    };
    MemoirePage = MemoirePage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Component"])({
            providers: [__WEBPACK_IMPORTED_MODULE_5_ng2_dragula__["b" /* DragulaService */]],
            selector: 'page-memoire',template:/*ion-inline-start:"/Users/Maxime/Desktop/passation/src/pages/memoire/memoire.html"*/'<ion-header>\n    <ion-toolbar  color="secondary" hide-back-button >\n          <ion-title>  </ion-title>\n            <h3 text-center *ngIf="displayTime!==0">{{displayTime}}</h3>\n          <ion-buttons end>\n            <button ion-button icon-only (click)="logOut()">\n                <ion-icon ios="ios-log-out" md="md-log-out"></ion-icon>\n            </button>\n          </ion-buttons>\n    </ion-toolbar>  \n  </ion-header>\n  \n  <ion-content color="light2" >\n  \n      <ion-grid  >\n    \n          <ion-row responsive-sm  >\n              <ion-col col-2 > \n              </ion-col> \n              <ion-col col-8 >  \n                \n                  <div *ngIf="hide==0">\n                      <ion-list >  \n                        <ion-card>\n                          <ion-card-header>\n                           <b> {{question_label}}</b>\n                             {{indexdisplay}}/{{nbquestionbytest}}\n                          </ion-card-header>\n                          <ion-card-content >\n                              <p [innerHTML]="q_h"></p>  \n                          </ion-card-content>\n                        </ion-card>\n                      </ion-list>\n                      <ion-list padding>  \n                        \n                            <ion-row style="text-align: center">\n                \n                              <div *ngFor="let item of options; let i=index" >\n                                <img  [src]="item" style="width:300px;height:300px;text-align: center"/> \n                              </div>\n\n                          \n                            </ion-row>\n                      </ion-list>\n                      <ion-list padding>  \n                          <b>{{question_label}}</b>\n                          </ion-list>\n                        </div>\n                          <ion-list padding>  \n\n\n                              <div *ngIf="hide==1" style="width:99%">\n                               \n                                  <p>     Dans cette épreuve, vous devez mémoriser une série de 25 pairs image/mot qui vous sont présentées une à une pendant un court laps de temps.</p>\n                                  <button ion-button  full ion-button  full class="bottom" (click)="displayimages()">  {{ \'next\' | translate }}</button>\n                              \n                               </div> \n\n                            <div *ngIf="hide==2" style="width:99%">\n\n                                <ion-list padding>  \n                        \n                                    <ion-row style="text-align: center">\n                        \n                                      <div *ngFor="let item of options; let i=index" >\n                                        <img  [src]="item" style="width:300px;height:300px;text-align: center"/> \n                                      </div>\n        \n                                  \n                                    </ion-row>\n                              </ion-list>\n                              <ion-item *ngFor="let item of options2; let i=index" >\n                                <ion-label>{{item}}</ion-label>\n                                <ion-checkbox item-right [ngModel]="checkedIdx == i"  (ngModelChange)="$event ? checkedIdx = i : checkedIdx = -1" \n                                [disabled]="checkedIdx >= 0 && checkedIdx != i"></ion-checkbox>\n                              </ion-item>\n                              <button ion-button  full (click)="next2()" [disabled]="checkedIdx == -1">{{ \'next\' | translate }}</button>\n                            </div> \n\n\n                             <div *ngIf="hide==4" style="width:99%">\n                               \n                                <p>    Vous avez terminé la première partie. Vous allez maintenant revoir une à une, les 25 images et leur mot. Quans vous êtes prêt, cliquez sur suivant</p>\n                                <button ion-button  full ion-button  full class="bottom" (click)="displayimages2()">  {{ \'next\' | translate }}</button>\n                            \n                             </div> \n                             <div *ngIf="hide==5" style="width:99%">\n                               \n                                <p>    Vous avez terminé la seconde partie du test de mémoire. Pour avancer, cliquez sur Suivant</p>\n                                <button ion-button  full ion-button  full class="bottom" (click)="displayintro3()">  {{ \'next\' | translate }}</button>\n                            \n                             </div> \n\n                             <div *ngIf="hide==6" style="width:99%">\n                               \n                                <p>   Vous maintenant terminé la 3ème et dernière phase du test de mémoire</p>\n                                <button ion-button  full ion-button  full class="bottom" (click)="finish()">  {{ \'next\' | translate }}</button>\n                            \n                             </div> \n                            \n\n                             <div *ngIf="hide==7" style="width:99%">\n                               \n                                <p>    Nous voulons tester le souvenir qu\'il vous reste du test de mémoire. Veuillez à nouveau choisir parmi les 5 propositions, le mot associé à l\'image</p>\n                                <button ion-button  full ion-button  full class="bottom" (click)="displayimage3()">  {{ \'next\' | translate }}</button>\n                            \n                             </div> \n\n\n\n\n\n              </ion-list>\n                  \n                \n                    \n     </ion-col>\n     <ion-col col-2 > \n          {{question_num}}\n              </ion-col> \n            </ion-row>\n\n\n           <div style="width:70%; text-align: center; margin: 0 auto;" >\n              <div class="progress-outer" style="margin: 1px 1px;padding: 1px;text-align: center;background-color: #cfcfcf;border: 1px solid #dcdcdc;color: #fff;border-radius: 20px;">\n              <div class="progress-inner" [style.width]="progressPercent + \'%\'" style="width: 96%;height:20px;margin: 2px 2px;text-align: center;background-color: #84bdf1;border: 1px solid #dcdcdc;color: #fff;border-radius: 20px;">\n                 {{progressPercent}}%\n              </div>\n              </div>\n              </div> \n       \n          </ion-grid>\n      \n    </ion-content>\n    \n  '/*ion-inline-end:"/Users/Maxime/Desktop/passation/src/pages/memoire/memoire.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_9__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["f" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_7__providers_AuthService__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_5_ng2_dragula__["b" /* DragulaService */], __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["f" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_2__providers_reddit_service__["a" /* RedditService */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["j" /* NavParams */]])
    ], MemoirePage);
    return MemoirePage;
    var MemoirePage_1;
}());

//# sourceMappingURL=memoire.js.map

/***/ }),

/***/ 364:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EspacePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_add_operator_map__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_reddit_service__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng2_dragula__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__endtest_endtest__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_AuthService__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__login_login__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_forms__ = __webpack_require__(20);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var EspacePage = /** @class */ (function () {
    function EspacePage(fb, loadingController, authService, dragulaService, loadingCtrl, navCtrl, storage, redditService, navParams) {
        var _this = this;
        this.loadingController = loadingController;
        this.authService = authService;
        this.dragulaService = dragulaService;
        this.loadingCtrl = loadingCtrl;
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.redditService = redditService;
        this.navParams = navParams;
        this.editing = true;
        this.dataq = [];
        this.indextest = 0;
        this.indexdisplay = 1;
        this.posts2 = [];
        this.checkedIdx = -1;
        this.options = [];
        this.reponse = [];
        this.reponsesave = [];
        this.responsestring = "";
        this.progressPercent = 0;
        this.urlimage = "https://www.selectionetconseils.ch/examen/image/";
        this.idtest = navParams.get('param1');
        this.testlenght = navParams.get('param2');
        this.testcategory = navParams.get('param4');
        this.timeInSeconds = navParams.get('param5');
        this.test_nav = navParams.get('param6');
        this.position = navParams.get('param7');
        this.form = fb.group({
            cbox: [false, EspacePage_1.mustBeTruthy]
        });
        this.storage.get('idresponse').then(function (idresponse) {
            _this.idresponse = idresponse;
        });
    }
    EspacePage_1 = EspacePage;
    EspacePage.prototype.ionViewWillEnter = function () {
        var _this = this;
        if (this.timeInSeconds > 0) {
            this.startTimer();
        }
        this.indexdisplay = 1;
        //Données i=0
        this.storage.get("questions").then(function (users) {
            _this.dataq = JSON.parse(users);
            console.log(_this.dataq);
            _this.nbquestionbytest = _this.dataq.length;
            _this.question_label = _this.dataq[0].question_label;
            _this.q_prop1 = _this.dataq[0].q_prop1;
            _this.q_prop2 = _this.dataq[0].q_prop2;
            _this.q_prop3 = _this.dataq[0].q_prop3;
            _this.q_prop4 = _this.dataq[0].q_prop4;
            _this.q_prop5 = _this.dataq[0].q_prop5;
            _this.q_img1 = _this.urlimage + _this.dataq[0].q_img1;
            _this.q_img2 = _this.urlimage + _this.dataq[0].q_img2;
            _this.q_img3 = _this.urlimage + _this.dataq[0].q_img3;
            _this.q_img4 = _this.urlimage + _this.dataq[0].q_img4;
            _this.q_img5 = _this.urlimage + _this.dataq[0].q_img5;
            _this.q_h = _this.dataq[0].q_h;
            _this.q_f = _this.dataq[0].q_f;
            _this.question_num = _this.dataq[0].question_num;
            _this.options = [_this.q_prop1, _this.q_prop2, _this.q_prop3, _this.q_prop4, _this.q_prop5];
            _this.checkedIdx = -1;
            //Array reponse
            for (var i = 0; i < _this.nbquestionbytest; i++) {
                _this.reponsesave[i] = -1;
            }
        });
    };
    EspacePage.prototype.next = function () {
        this.reponsesave[this.indextest] = this.checkedIdx;
        // new question
        this.indextest = this.indextest + 1;
        // toutes les réponses
        console.log("réponse");
        console.log(this.reponsesave);
        if (this.indextest < this.nbquestionbytest) {
            this.indexdisplay = this.indexdisplay + 1;
            this.retrieveQuestions();
        }
        else {
            this.pauseTimer();
            ////// SAVE DATA
            for (var i = 0; i < this.testlenght; i++) {
                if (this.reponsesave[i] == "-1") {
                    this.responsezero = "0";
                }
                else {
                    this.responsezero = this.reponsesave[i] + 1;
                }
                this.responsestring = this.responsestring + this.responsezero;
            }
            /////
            var data = JSON.stringify({
                name: "Espace",
                idresponse: this.idresponse,
                resp: this.responsestring,
            });
            this.redditService.addresponses(data)
                .toPromise()
                .then(function (response) {
                console.log(response);
            });
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__endtest_endtest__["a" /* EndtestPage */], {
                param1: this.idtest,
                param2: this.position,
            });
        }
    };
    EspacePage.prototype.prev = function () {
        if (this.indextest > 0) {
            this.indextest = this.indextest - 1;
            this.indexdisplay = this.indexdisplay - 1;
            this.reviewQuestions();
        }
        else {
        }
    };
    EspacePage.prototype.retrieveQuestions = function () {
        var _this = this;
        this.storage.get("questions").then(function (users) {
            _this.dataq = JSON.parse(users);
            _this.question_label = _this.dataq[_this.indextest].question_label;
            _this.propositions = _this.dataq[_this.indextest];
            _this.q_prop1 = _this.dataq[_this.indextest].q_prop1;
            _this.q_prop2 = _this.dataq[_this.indextest].q_prop2;
            _this.q_prop3 = _this.dataq[_this.indextest].q_prop3;
            _this.q_prop4 = _this.dataq[_this.indextest].q_prop4;
            _this.q_prop5 = _this.dataq[_this.indextest].q_prop5;
            _this.q_img1 = _this.urlimage + _this.dataq[_this.indextest].q_img1;
            _this.q_img2 = _this.urlimage + _this.dataq[_this.indextest].q_img2;
            _this.q_img3 = _this.urlimage + _this.dataq[_this.indextest].q_img3;
            _this.q_img4 = _this.urlimage + _this.dataq[_this.indextest].q_img4;
            _this.q_img5 = _this.urlimage + _this.dataq[_this.indextest].q_img5;
            _this.q_h = _this.dataq[_this.indextest].q_h;
            _this.q_f = _this.dataq[_this.indextest].q_f;
            _this.checkedIdx = _this.reponsesave[_this.indextest];
            _this.options = [_this.q_prop1, _this.q_prop2, _this.q_prop3, _this.q_prop4, _this.q_prop5];
            _this.question_num = _this.dataq[_this.indextest].question_num;
        });
    };
    EspacePage.prototype.reviewQuestions = function () {
        var _this = this;
        this.storage.get("questions").then(function (users) {
            _this.dataq = JSON.parse(users);
            _this.question_label = _this.dataq[_this.indextest].question_label;
            _this.propositions = _this.dataq[_this.indextest];
            _this.q_prop1 = _this.dataq[_this.indextest].q_prop1;
            _this.q_prop2 = _this.dataq[_this.indextest].q_prop2;
            _this.q_prop3 = _this.dataq[_this.indextest].q_prop3;
            _this.q_prop4 = _this.dataq[_this.indextest].q_prop4;
            _this.q_prop5 = _this.dataq[_this.indextest].q_prop5;
            _this.q_img1 = _this.urlimage + _this.dataq[_this.indextest].q_img1;
            _this.q_img2 = _this.urlimage + _this.dataq[_this.indextest].q_img2;
            _this.q_img3 = _this.urlimage + _this.dataq[_this.indextest].q_img3;
            _this.q_img4 = _this.urlimage + _this.dataq[_this.indextest].q_img4;
            _this.q_img5 = _this.urlimage + _this.dataq[_this.indextest].q_img5;
            _this.q_h = _this.dataq[_this.indextest].q_h;
            _this.q_f = _this.dataq[_this.indextest].q_f;
            _this.checkedIdx = _this.reponsesave[_this.indextest];
            _this.options = [_this.q_prop1, _this.q_prop2, _this.q_prop3, _this.q_prop4, _this.q_prop5];
            _this.question_num = _this.dataq[_this.indextest].question_num;
        });
    };
    EspacePage.mustBeTruthy = function (c) {
        var rv = {};
        if (!c.value) {
            rv['notChecked'] = true;
        }
        return rv;
    };
    EspacePage.prototype.reorderData = function (indexes) {
        this.posts2 = Object(__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["o" /* reorderArray */])(this.posts2, indexes);
        console.log(this.posts2);
        console.log(indexes);
        //this.dataq[this.indextest] = reorderArray(this.dataq[this.indextest], indexes);
        // console.log(this.dataq[this.indextest]);
        // console.log(this.dataq[this.indextest]);
    };
    EspacePage.prototype.logOut = function () {
        this.authService.logout();
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__login_login__["a" /* Login */]);
    };
    EspacePage.prototype.ngOnInit = function () {
        this.initTimer();
    };
    EspacePage.prototype.initTimer = function () {
        this.time = this.timeInSeconds;
        this.runTimer = false;
        this.hasStarted = false;
        this.hasFinished = false;
        this.remainingTime = this.timeInSeconds;
        this.displayTime = this.getSecondsAsDigitalClock(this.remainingTime);
    };
    EspacePage.prototype.startTimer = function () {
        this.runTimer = true;
        this.hasStarted = true;
        this.timerTick();
    };
    EspacePage.prototype.pauseTimer = function () {
        this.runTimer = false;
    };
    EspacePage.prototype.resumeTimer = function () {
        this.startTimer();
    };
    EspacePage.prototype.timerTick = function () {
        var _this = this;
        setTimeout(function () {
            if (!_this.runTimer) {
                return;
            }
            _this.remainingTime--;
            _this.displayTime = _this.getSecondsAsDigitalClock(_this.remainingTime);
            _this.progressTimer = (_this.remainingTime * 100) / _this.time;
            _this.progressPercent = (100 - _this.progressTimer).toFixed(0);
            if (_this.remainingTime > 0) {
                _this.timerTick();
            }
            else {
                _this.hasFinished = true;
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__endtest_endtest__["a" /* EndtestPage */], {
                    param1: _this.idtest,
                    param2: _this.position,
                });
            }
        }, 1000);
    };
    EspacePage.prototype.getSecondsAsDigitalClock = function (inputSeconds) {
        var sec_num = parseInt(inputSeconds.toString(), 10); // don't forget the second param
        var hours = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);
        var hoursString = '';
        var minutesString = '';
        var secondsString = '';
        hoursString = (hours < 10) ? "0" + hours : hours.toString();
        minutesString = (minutes < 10) ? "0" + minutes : minutes.toString();
        secondsString = (seconds < 10) ? "0" + seconds : seconds.toString();
        return hoursString + ':' + minutesString + ':' + secondsString;
    };
    EspacePage = EspacePage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Component"])({
            providers: [__WEBPACK_IMPORTED_MODULE_5_ng2_dragula__["b" /* DragulaService */]],
            selector: 'page-espace',template:/*ion-inline-start:"/Users/Maxime/Desktop/passation/src/pages/espace/espace.html"*/'<ion-header>\n    <ion-toolbar  color="secondary" hide-back-button >\n          <ion-title>  </ion-title>\n            <h3 text-center *ngIf="displayTime!==0">{{displayTime}}</h3>\n          <ion-buttons end>\n            <button ion-button icon-only (click)="logOut()">\n                <ion-icon ios="ios-log-out" md="md-log-out"></ion-icon>\n            </button>\n          </ion-buttons>\n    </ion-toolbar>  \n  </ion-header>\n  \n  <ion-content color="light2" >\n  \n      <ion-grid >\n    \n          <ion-row responsive-sm  >\n              <ion-col col-2 > \n              </ion-col> \n              <ion-col col-8 >  \n                  <div *ngIf="test_nav==1">\n                      <ion-list >  \n                        <ion-card>\n                          <ion-card-header>\n                           <b> {{question_label}}</b>\n                             {{indexdisplay}}/{{nbquestionbytest}}\n                          </ion-card-header>\n                          <ion-card-content >\n                              <p [innerHTML]="q_h"></p>  \n                          </ion-card-content>\n                        </ion-card>\n                      </ion-list>\n                      <ion-list padding>  \n                        <ion-scroll scrollX="true" style="width:100vw; height:200px" >\n                            <ion-row >\n                              <div *ngFor="let item of options; let i=index" style="padding-right: 20px;">\n                              {{item}}\n                                <ion-checkbox item-right [ngModel]="checkedIdx == i"  (ngModelChange)="$event ? checkedIdx = i : checkedIdx = -1" [disabled]="checkedIdx >= 0 && checkedIdx != i"></ion-checkbox>\n                              </div>\n                            </ion-row>\n                          </ion-scroll>\n                      </ion-list>\n                      <ion-list padding>  \n                              <ion-row responsive-sm  >\n                                  <ion-col col-2 > \n                                  <button ion-button full (click)="prev()" >\n                                  Précedent\n                                  </button>\n                                  </ion-col> \n                                  <ion-col col-8 >  \n                                  </ion-col> \n                                  <ion-col col-2 > \n                                  <button ion-button  full (click)="next()">\n                                  Suivant\n                                  </button>\n                                  </ion-col> \n                              </ion-row>\n                          </ion-list>\n                    </div>\n                    <div *ngIf="test_nav==2">\n                        <ion-list >  \n                          <ion-card>\n                            <ion-card-header>\n                             <b> {{question_label}}</b>\n                               {{indexdisplay}}/{{nbquestionbytest}}\n                            </ion-card-header>\n                            <ion-card-content >\n                                <p [innerHTML]="q_h"></p>  \n                            </ion-card-content>\n                          </ion-card>\n                        </ion-list>\n                        <ion-list padding>  \n                          <ion-scroll scrollX="true" style="width:100vw; height:200px" >\n                              <ion-row >\n                                <div *ngFor="let item of options; let i=index" style="padding-right: 20px;">\n                                {{item}}\n                                  <ion-checkbox item-right [ngModel]="checkedIdx == i"  (ngModelChange)="$event ? checkedIdx = i : checkedIdx = -1" [disabled]="checkedIdx >= 0 && checkedIdx != i"></ion-checkbox>\n                                </div>\n                              </ion-row>\n                            </ion-scroll>\n                        </ion-list>\n                        <ion-list padding>  \n                                <ion-row responsive-sm  >\n                                    <ion-col col-2 > \n                                \n                                    </ion-col> \n                                    <ion-col col-8 >  \n                                    </ion-col> \n                                    <ion-col col-2 > \n                                    <button ion-button  full (click)="next()" >\n                                    Suivant\n                                    </button>\n                                    </ion-col> \n                                </ion-row>\n                            </ion-list>\n                      </div>\n                      <div *ngIf="test_nav==3">\n                          <ion-list >  \n                            <ion-card>\n                              <ion-card-header>\n                               <b> {{question_label}}</b>\n                                 {{indexdisplay}}/{{nbquestionbytest}}\n                              </ion-card-header>\n                              <ion-card-content >\n                                  <p [innerHTML]="q_h"></p>  \n                              </ion-card-content>\n                            </ion-card>\n                          </ion-list>\n                          <ion-list padding>  \n                            <ion-scroll scrollX="true" style="width:100vw; height:200px" >\n                                <ion-row >\n                                  <div *ngFor="let item of options; let i=index" style="padding-right: 20px;">\n                                  {{item}}\n                                    <ion-checkbox item-right [ngModel]="checkedIdx == i"  (ngModelChange)="$event ? checkedIdx = i : checkedIdx = -1" [disabled]="checkedIdx >= 0 && checkedIdx != i"></ion-checkbox>\n                                  </div>\n                                </ion-row>\n                              </ion-scroll>\n                          </ion-list>\n                          <ion-list padding>  \n                                  <ion-row responsive-sm  >\n                                      <ion-col col-2 > \n                                      <button ion-button full (click)="prev()" [disabled]="checkedIdx == -1 ">\n                                      Précedent\n                                      </button>\n                                      </ion-col> \n                                      <ion-col col-8 >  \n                                      </ion-col> \n                                      <ion-col col-2 > \n                                      <button ion-button  full (click)="next()" [disabled]="checkedIdx == -1 ">\n                                      Suivant\n                                      </button>\n                                      </ion-col> \n                                  </ion-row>\n                              </ion-list>\n                        </div>\n                        <div *ngIf="test_nav==4">\n                            <ion-list >  \n                              <ion-card>\n                                <ion-card-header>\n                                 <b> {{question_label}}</b>\n                                   {{indexdisplay}}/{{nbquestionbytest}}\n                                </ion-card-header>\n                                <ion-card-content >\n                                    <p [innerHTML]="q_h"></p>  \n                                </ion-card-content>\n                              </ion-card>\n                            </ion-list>\n                            <ion-list padding>  \n                              <ion-scroll scrollX="true" style="width:100vw; height:200px" >\n                                  <ion-row >\n                                    <div *ngFor="let item of options; let i=index" style="padding-right: 20px;">\n                                    {{item}}\n                                      <ion-checkbox item-right [ngModel]="checkedIdx == i"  (ngModelChange)="$event ? checkedIdx = i : checkedIdx = -1" [disabled]="checkedIdx >= 0 && checkedIdx != i"></ion-checkbox>\n                                    </div>\n                                  </ion-row>\n                                </ion-scroll>\n                            </ion-list>\n                            <ion-list padding>  \n                                    <ion-row responsive-sm  >\n                                        <ion-col col-2 > \n                                      \n                                        </ion-col> \n                                        <ion-col col-8 >  \n                                        </ion-col> \n                                        <ion-col col-2 > \n                                        <button ion-button  full (click)="next()" [disabled]="checkedIdx == -1 ">\n                                        Suivant\n                                        </button>\n                                        </ion-col> \n                                    </ion-row>\n                                </ion-list>\n                          </div>\n   </ion-col>\n   <ion-col col-2 > \n        {{question_num}}\n            </ion-col> \n          </ion-row>\n\n\n          <div style="width:70%; text-align: center; margin: 0 auto;">\n              <div class="progress-outer" style="margin: 1px 1px;padding: 1px;text-align: center;background-color: #cfcfcf;border: 1px solid #dcdcdc;color: #fff;border-radius: 20px;">\n              <div class="progress-inner" [style.width]="progressPercent + \'%\'" style="width: 96%;height:20px;margin: 2px 2px;text-align: center;background-color: #84bdf1;border: 1px solid #dcdcdc;color: #fff;border-radius: 20px;">\n                 {{progressPercent}}%\n              </div>\n              </div>\n              </div> \n       \n        </ion-grid>\n    \n    </ion-content>\n    \n  '/*ion-inline-end:"/Users/Maxime/Desktop/passation/src/pages/espace/espace.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_9__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["f" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_7__providers_AuthService__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_5_ng2_dragula__["b" /* DragulaService */], __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["f" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_2__providers_reddit_service__["a" /* RedditService */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["j" /* NavParams */]])
    ], EspacePage);
    return EspacePage;
    var EspacePage_1;
}());

//# sourceMappingURL=espace.js.map

/***/ }),

/***/ 365:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StressPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_add_operator_map__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_reddit_service__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng2_dragula__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__endtest_endtest__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_AuthService__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__login_login__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var StressPage = /** @class */ (function () {
    function StressPage(loadingController, authService, dragulaService, loadingCtrl, navCtrl, storage, redditService, navParams) {
        var _this = this;
        this.loadingController = loadingController;
        this.authService = authService;
        this.dragulaService = dragulaService;
        this.loadingCtrl = loadingCtrl;
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.redditService = redditService;
        this.navParams = navParams;
        this.editing = true;
        this.dataq = [];
        this.indextest = 0;
        this.indexdisplay = 1;
        this.posts2 = [];
        this.nbchecked = 0;
        this.checkedIdx = -1;
        this.options = [];
        this.reponse = [];
        this.reponsesave = [];
        this.myDataArray = [];
        this.datareponse = [];
        this.points = [];
        this.result = [];
        this.test = [];
        this.can3 = [];
        this.cpt3 = 0;
        this.progressPercent = 0;
        this.idtest = navParams.get('param1');
        this.testlenght = navParams.get('param2');
        this.testcategory = navParams.get('param4');
        this.timeInSeconds = navParams.get('param5');
        this.test_nav = navParams.get('param6');
        this.position = navParams.get('param7');
        this.storage.get("questions").then(function (users) {
            _this.dataq = JSON.parse(users);
            for (var i = 0; i < _this.dataq.length; i++) {
                _this.question_label = _this.dataq[i].question_label;
                _this.propositions = _this.dataq[i];
                _this.q_prop1 = _this.dataq[i].q_prop1;
                _this.q_prop2 = _this.dataq[i].q_prop2;
                _this.q_prop3 = _this.dataq[i].q_prop3;
                _this.q_prop4 = _this.dataq[i].q_prop4;
                _this.q_prop5 = _this.dataq[i].q_prop5;
                _this.q_h = _this.dataq[i].q_h;
                _this.q_f = _this.dataq[i].q_f;
                _this.checkedIdx = _this.reponsesave[i];
                _this.question_num = _this.dataq[i].question_num;
                _this.options = [{ id: '1', prop: _this.q_prop1, isChecked: false }, { id: '2', prop: _this.q_prop2, isChecked: false },
                    { id: '3', prop: _this.q_prop3, isChecked: false }, { id: '4', prop: _this.q_prop4, isChecked: false }, { id: '5', prop: _this.q_prop5, isChecked: false }];
                _this.datareponse.push(_this.options);
            }
        });
    }
    StressPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.canvas1 = document.getElementById('canvas1');
        this.canvas2 = document.getElementById('canvas2');
        this.canvas3 = document.getElementById('canvas3');
        if (this.timeInSeconds > 0) {
            this.startTimer();
        }
        this.indexdisplay = 1;
        //Données i=0
        this.storage.get("questions").then(function (users) {
            _this.dataq = JSON.parse(users);
            _this.nbquestionbytest = _this.dataq.length;
            _this.question_label = _this.dataq[0].question_label;
            _this.q_h = _this.dataq[0].q_h;
            _this.q_f = _this.dataq[0].q_f;
            _this.q_prop1 = _this.dataq[0].q_prop1;
            console.log(_this.q_prop1);
            _this.q_prop2 = _this.dataq[0].q_prop2;
            _this.question_num = _this.dataq[0].question_num;
            _this.datarep = _this.datareponse[0];
            console.log(_this.datareponse[0][0].prop);
            _this.pts1 = _this.datareponse[0][0].prop;
            _this.pts2 = _this.datareponse[0][1].prop;
            _this.pts3 = _this.datareponse[0][2].prop;
            _this.markPoint(_this.pts1);
            _this.markPoint2(_this.pts1);
            _this.markPoint3(_this.pts3);
            console.log("-------------PTS2--------");
            console.log(_this.pts2);
        });
    };
    StressPage.prototype.markPoint = function (pts1) {
        var context1 = this.canvas1.getContext("2d");
        var canvas1 = document.getElementById('canvas1');
        context1.beginPath();
        this.points = this.remplace(this.pts1);
        console.log("PTS 1");
        console.log(this.points);
        for (var i = 0; i < 21; i++) {
            this.x = this.points[i];
            this.y = this.points[i + 1];
            console.log(this.x, this.y);
            i = i + 1;
            context1.fillRect(this.x, this.y, 5, 5);
        }
        context1.fillStyle = "#92B901;  width: 20";
        context1.stroke();
        context1.closePath();
    };
    StressPage.prototype.markPoint2 = function (pts2) {
        var context2 = this.canvas2.getContext("2d");
        var canvas2 = document.getElementById('canvas2');
        context2.beginPath();
        this.pts2 = this.remplace(this.pts2);
        console.log("PTS 2");
        console.log(this.pts2);
        for (var i = 0; i < 21; i++) {
            this.x = this.pts2[i];
            this.y = this.pts2[i + 1];
            console.log(this.x, this.y);
            i = i + 1;
            context2.fillRect(this.x, this.y, 5, 5);
        }
        context2.fillStyle = "#92B901;  width: 20";
        context2.stroke();
        context2.closePath();
    };
    StressPage.prototype.markPoint3 = function (pts3) {
        var context3 = this.canvas3.getContext("2d");
        var canvas3 = document.getElementById('canvas3');
        context3.beginPath();
        this.pts3 = this.remplace(this.pts3);
        console.log("PTS 3");
        console.log(this.pts3);
        for (var i = 0; i < 21; i++) {
            this.x = this.pts3[i];
            this.y = this.pts3[i + 1];
            context3.fillRect(this.x, this.y, 5, 5);
        }
        context3.fillStyle = "#92B901;  width: 20";
        context3.stroke();
        context3.closePath();
        canvas3.addEventListener('click', function (evt) {
            var rect = canvas3.getBoundingClientRect();
            // this.xdata = (evt.x - rect.left).toFixed(0);
            //  this.ydata = (evt.y - rect.top).toFixed(0);
            //console.log(this.ctp3);
            //  if(this.ctp3=0){
            //     console.log("pts 0");
            // context3.beginPath();
            // context3.lineWidth = 2;
            // context3.strokeStyle="#FF0000";
            // context3.lineJoin = "round";
            // context3.moveTo(evt.x - rect.left ,evt.y - rect.top); 
            //   this.ctp3=this.ctp3+1;
            //  }else if (this.ctp3<6 && this.ctp3>0){
            // console.log("pts");
            //    context3.lineTo(evt.x - rect.left ,evt.y - rect.top);
            //    context3.stroke();
            //  this.ctp3=this.ctp3+1;
            //  }
        });
        context3.stroke();
        context3.closePath();
    };
    StressPage.prototype.getPosition = function (event) {
        var rect = this.canvas3.getBoundingClientRect();
        var x = event.clientX - rect.left;
        var y = event.clientY - rect.top;
        console.log(x);
    };
    StressPage.prototype.remplace = function (data) {
        var ptsarray = data;
        var r1 = /\,/gi;
        var re = /\;/gi;
        var r2 = /\[/gi;
        var r3 = /\]/gi;
        var result = ptsarray.replace(r1, ",").replace(re, ",").replace(r2, "").replace(r3, "");
        this.points = result.split(',');
        return this.points;
    };
    StressPage.prototype.next = function () {
        for (var i = 0; i < this.datareponse[this.indextest]; i++) {
            this.datareponse.push(this.datareponse[this.indextest]);
            console.log(this.datareponse[this.indextest]);
        }
        console.log(this.datareponse);
        /*this.datareponse[this.indextest] =  this.datarep.filter(value => {
      
          console.log(value.isChecked );
          return value.isChecked && !value.isChecked ;
        });
      */
        console.log("enregistrement des questions");
        console.log(this.datareponse);
        // new question
        this.indextest = this.indextest + 1;
        if (this.indextest < this.nbquestionbytest) {
            this.indexdisplay = this.indexdisplay + 1;
            this.retrieveQuestions();
        }
        else {
            this.pauseTimer();
            ////// SAVE DATA
            /*console.log("réponses");
            console.log(this.reponsesave);
          
            for(let i=0;i<this.nbquestionbytest[i];i++){
              this.datasave=this.datasave+this.reponsesave[i];
              console.log(this.datasave);
            }*/
            /////
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__endtest_endtest__["a" /* EndtestPage */], {
                param1: this.idtest,
                param2: this.position,
            });
        }
    };
    StressPage.prototype.prev = function () {
        if (this.indextest > 0) {
            this.indextest = this.indextest - 1;
            this.indexdisplay = this.indexdisplay - 1;
            this.reviewQuestions();
        }
        else {
        }
    };
    StressPage.prototype.retrieveQuestions = function () {
        var _this = this;
        this.storage.get("questions").then(function (users) {
            _this.dataq = JSON.parse(users);
            _this.question_label = _this.dataq[_this.indextest].question_label;
            _this.q_h = _this.dataq[_this.indextest].q_h;
            _this.q_f = _this.dataq[_this.indextest].q_f;
            _this.question_num = _this.dataq[_this.indextest].question_num;
            _this.datarep = _this.datareponse[_this.indextest];
            // nb true reponse
            _this.nbchecked = 0;
            for (var i = 0; i < 5; i++) {
                if (_this.datarep[i].isChecked == true) {
                    _this.nbchecked = _this.nbchecked + 1;
                }
            }
        });
    };
    StressPage.prototype.reviewQuestions = function () {
        var _this = this;
        this.storage.get("questions").then(function (users) {
            _this.dataq = JSON.parse(users);
            _this.question_label = _this.dataq[_this.indextest].question_label;
            _this.q_h = _this.dataq[_this.indextest].q_h;
            _this.q_f = _this.dataq[_this.indextest].q_f;
            _this.question_num = _this.dataq[_this.indextest].question_num;
            _this.datarep = _this.datareponse[_this.indextest];
            // nb true reponse
            _this.nbchecked = 0;
            for (var i = 0; i < 5; i++) {
                if (_this.datarep[i].isChecked == true) {
                    _this.nbchecked = _this.nbchecked + 1;
                }
            }
        });
    };
    StressPage.prototype.updateCheck = function ($event) {
        console.log($event.checked);
        if ($event.checked == true) {
            this.nbchecked = this.nbchecked + 1;
            console.log(this.nbchecked);
        }
        else if ($event.checked == false) {
            this.nbchecked = this.nbchecked - 1;
            console.log(this.nbchecked);
        }
    };
    StressPage.mustBeTruthy = function (c) {
        var rv = {};
        if (!c.value) {
            rv['notChecked'] = true;
        }
        return rv;
    };
    StressPage.prototype.reorderData = function (indexes) {
        this.posts2 = Object(__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["o" /* reorderArray */])(this.posts2, indexes);
        console.log(this.posts2);
        console.log(indexes);
        //this.dataq[this.indextest] = reorderArray(this.dataq[this.indextest], indexes);
        // console.log(this.dataq[this.indextest]);
        // console.log(this.dataq[this.indextest]);
    };
    StressPage.prototype.logOut = function () {
        this.authService.logout();
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__login_login__["a" /* Login */]);
    };
    StressPage.prototype.ngOnInit = function () {
        this.initTimer();
    };
    StressPage.prototype.initTimer = function () {
        this.time = this.timeInSeconds;
        this.runTimer = false;
        this.hasStarted = false;
        this.hasFinished = false;
        this.remainingTime = this.timeInSeconds;
        this.displayTime = this.getSecondsAsDigitalClock(this.remainingTime);
    };
    StressPage.prototype.startTimer = function () {
        this.runTimer = true;
        this.hasStarted = true;
        this.timerTick();
    };
    StressPage.prototype.pauseTimer = function () {
        this.runTimer = false;
    };
    StressPage.prototype.resumeTimer = function () {
        this.startTimer();
    };
    StressPage.prototype.timerTick = function () {
        var _this = this;
        setTimeout(function () {
            if (!_this.runTimer) {
                return;
            }
            _this.remainingTime--;
            _this.displayTime = _this.getSecondsAsDigitalClock(_this.remainingTime);
            _this.progressTimer = (_this.remainingTime * 100) / _this.time;
            _this.progressPercent = (100 - _this.progressTimer).toFixed(0);
            if (_this.remainingTime > 0) {
                _this.timerTick();
            }
            else {
                _this.hasFinished = true;
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__endtest_endtest__["a" /* EndtestPage */], {
                    param1: _this.idtest,
                    param2: _this.position,
                });
            }
        }, 1000);
    };
    StressPage.prototype.getSecondsAsDigitalClock = function (inputSeconds) {
        var sec_num = parseInt(inputSeconds.toString(), 10); // don't forget the second param
        var hours = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);
        var hoursString = '';
        var minutesString = '';
        var secondsString = '';
        hoursString = (hours < 10) ? "0" + hours : hours.toString();
        minutesString = (minutes < 10) ? "0" + minutes : minutes.toString();
        secondsString = (seconds < 10) ? "0" + seconds : seconds.toString();
        return hoursString + ':' + minutesString + ':' + secondsString;
    };
    StressPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Component"])({
            providers: [__WEBPACK_IMPORTED_MODULE_5_ng2_dragula__["b" /* DragulaService */]],
            selector: 'page-stress',template:/*ion-inline-start:"/Users/Maxime/Desktop/passation/src/pages/stress/stress.html"*/'\n                    \n                            \n                     \n\n                                        <ion-header>\n                                            <ion-toolbar  color="secondary" hide-back-button >\n                                                  <ion-title>  </ion-title>\n                                                    <h3 text-center *ngIf="displayTime!==0">{{displayTime}}</h3>\n                                                  <ion-buttons end>\n                                                    <button ion-button icon-only (click)="logOut()">\n                                                        <ion-icon ios="ios-log-out" md="md-log-out"></ion-icon>\n                                                    </button>\n                                                  </ion-buttons>\n                                            </ion-toolbar>  \n                                          </ion-header>\n                                          \n                                          <ion-content color="light2" >\n                                          \n                                              <ion-grid >\n                                            \n                                                  <ion-row responsive-sm  >\n                                                      <ion-col col-2 > \n                                                          EN DEVELLOPPEMENT\n                                                      </ion-col> \n                                                      <ion-col col-8 >  \n                                                        \n                                                            <div>\n                                                                    <ion-list >  \n                                                                      <ion-card>\n                                                                        <ion-card-header>\n                                                                         <b> {{question_label}}</b>\n                                                                           {{indexdisplay}}/{{nbquestionbytest}}\n                                                                        </ion-card-header>\n                                                                        <ion-card-content>\n                                                                            <p [innerHTML]="q_h"></p>  \n                                                                        </ion-card-content>\n                                                                      </ion-card>\n                                                                    </ion-list>\n  \n                                                                    \n                                                                    \n                                                                    \n                                                                    <ion-list padding>  \n                                                    \n                                                                      <ion-row>\n                                                                          <ion-col col-3 > \n  \n                                                                         <canvas id="canvas1" style="border:1px solid" class="cont" width=200 height=200></canvas>\n                                                                    \n                                                                          </ion-col> \n                                                                          <ion-col col-3 >  \n  \n                                                                \n                                                                            <canvas id="canvas2" style="border:1px solid" class="cont" width=200 height=200></canvas>\n                                                                          </ion-col> \n                                                                          <ion-col col-3 > \n  \n                                                                            <canvas id="canvas3" style="border:1px solid" class="cont" width=200 height=200></canvas>\n                                                                            <button ion-button (click)="getPosition(event)">Comments</button>\n                                                                        \n                                                                        \n                                                                        </ion-col> \n                                                                          <ion-col col-3 > \n  \n                                                             \n                                                                    \n                                                                          </ion-col> \n                                                                      </ion-row>\n                             \n                              \n                           \n                                                                     \n                                                                        \n                                                                    </ion-list>\n                                                    \n                                                                    <ion-list padding>  \n                                                                            <ion-row responsive-sm  >\n                                                                                \n                                                                                <ion-col col-2 > \n                                                                                <button ion-button full (click)="prev()" >\n                                                                                Précedent\n                                                                                </button>\n                                                                                </ion-col> \n                                                                                <ion-col col-8 >  \n                                                                                </ion-col> \n                                                                                <ion-col col-2 > \n                                                                                <button ion-button  full (click)="next()"  >\n                                                                                Suivant\n                                                                                </button>\n                                                                                </ion-col> \n                                                                              \n                                                                            </ion-row>\n                                                                        </ion-list>\n                                                                  </div>\n                                                                  <div *ngIf="test_nav==2">\n                                                                      <ion-list >  \n                                                                        <ion-card>\n                                                                          <ion-card-header>\n                                                                           <b> {{question_label}}</b>\n                                                                             {{indexdisplay}}/{{nbquestionbytest}}\n                                                                          </ion-card-header>\n                                                                          <ion-card-content>\n                                                                              <p [innerHTML]="q_h"></p>  \n                                                                          </ion-card-content>\n                                                                        </ion-card>\n                                                                      </ion-list>\n    \n                                                                      \n                                                                      \n                                                                      \n                                                                      <ion-list padding>  \n                                                      \n                                                                        <ion-row>\n                                                                            <ion-col col-3 > \n    \n                                                                      <canvas id="canvas" style="border:1px solid" class="cont" width=200 height=200></canvas>\n                                                                      \n                                                                            </ion-col> \n                                                                            <ion-col col-3 >  \n    \n                                                                  \n                                                                      \n                                                                            </ion-col> \n                                                                            <ion-col col-3 > \n    \n                                                                      \n                                                                            </ion-col> \n                                                                            <ion-col col-3 > \n    \n                                                               \n                                                                      \n                                                                            </ion-col> \n                                                                        </ion-row>\n                               \n                                \n                             \n                                                                       \n                                                                          \n                                                                      </ion-list>\n                                                      \n                                                                      <ion-list padding>  \n                                                                              <ion-row responsive-sm  >\n                                                                                  \n                                                                                  <ion-col col-2 > \n                                                                                  <button ion-button full (click)="prev()" >\n                                                                                  Précedent\n                                                                                  </button>\n                                                                                  </ion-col> \n                                                                                  <ion-col col-8 >  \n                                                                                  </ion-col> \n                                                                                  <ion-col col-2 > \n                                                                                  <button ion-button  full (click)="next()" >\n                                                                                  Suivant\n                                                                                  </button>\n                                                                                  </ion-col> \n                                                                                \n                                                                              </ion-row>\n                                                                          </ion-list>\n                                                                    </div>\n                                                                    <div *ngIf="test_nav==3">\n                                                                        <ion-list >  \n                                                                          <ion-card>\n                                                                            <ion-card-header>\n                                                                             <b> {{question_label}}</b>\n                                                                               {{indexdisplay}}/{{nbquestionbytest}}\n                                                                            </ion-card-header>\n                                                                            <ion-card-content>\n                                                                                <p [innerHTML]="q_h"></p>  \n                                                                            </ion-card-content>\n                                                                          </ion-card>\n                                                                        </ion-list>\n      \n                                                                        \n                                                                        \n                                                                        \n                                                                        <ion-list padding>  \n                                                        \n                                                                          <ion-row>\n                                                                              <ion-col col-3 > \n      \n                                                                        <canvas id="canvas1" style="border:1px solid" class="cont" width=200 height=200></canvas>\n                                                                       \n                                                                        <canvas id="canvas2" style="border:1px solid" class="cont" width=200 height=200></canvas>\n                                                                   \n                                                                              </ion-col> \n                                                                              <ion-col col-3 >  \n      \n                                                                    \n                                                                        \n                                                                              </ion-col> \n                                                                              <ion-col col-3 > \n      \n                                                                        \n                                                                              </ion-col> \n                                                                              <ion-col col-3 > \n      \n                                                                 \n                                                                        \n                                                                              </ion-col> \n                                                                          </ion-row>\n                                 \n                                  \n                               \n                                                                         \n                                                                            \n                                                                        </ion-list>\n                                                        \n                                                                        <ion-list padding>  \n                                                                                <ion-row responsive-sm  >\n                                                                                    \n                                                                                    <ion-col col-2 > \n                                                                                    <button ion-button full (click)="prev()" >\n                                                                                    Précedent\n                                                                                    </button>\n                                                                                    </ion-col> \n                                                                                    <ion-col col-8 >  \n                                                                                    </ion-col> \n                                                                                    <ion-col col-2 > \n                                                                                    <button ion-button  full (click)="next()"  >\n                                                                                    Suivant\n                                                                                    </button>\n                                                                                    </ion-col> \n                                                                                  \n                                                                                </ion-row>\n                                                                            </ion-list>\n                                                                      </div>\n                                                                      <div *ngIf="test_nav==4">\n                                                                          <ion-list >  \n                                                                            <ion-card>\n                                                                              <ion-card-header>\n                                                                               <b> {{question_label}}</b>\n                                                                                 {{indexdisplay}}/{{nbquestionbytest}}\n                                                                              </ion-card-header>\n                                                                              <ion-card-content>\n                                                                                  <p [innerHTML]="q_h"></p>  \n                                                                              </ion-card-content>\n                                                                            </ion-card>\n                                                                          </ion-list>\n        \n                                                                          \n                                                                          \n                                                                          \n                                                                          <ion-list padding>  \n                                                          \n                                                                            <ion-row>\n                                                                                <ion-col col-3 > \n        \n                                                                          <canvas id="canvas" style="border:1px solid" class="cont" width=200 height=200></canvas>\n                                                                          \n                                                                                </ion-col> \n                                                                                <ion-col col-3 >  \n        \n                                                                      \n                                                                          \n                                                                                </ion-col> \n                                                                                <ion-col col-3 > \n        \n                                                                          \n                                                                                </ion-col> \n                                                                                <ion-col col-3 > \n        \n                                                                   \n                                                                          \n                                                                                </ion-col> \n                                                                            </ion-row>\n                                   \n                                    \n                                 \n                                                                           \n                                                                              \n                                                                          </ion-list>\n                                                          \n                                                                          <ion-list padding>  \n                                                                                  <ion-row responsive-sm  >\n                                                                                      \n                                                                                      <ion-col col-2 > \n                                                                                      <button ion-button full (click)="prev()" >\n                                                                                      Précedent\n                                                                                      </button>\n                                                                                      </ion-col> \n                                                                                      <ion-col col-8 >  \n                                                                                      </ion-col> \n                                                                                      <ion-col col-2 > \n                                                                                      <button ion-button  full (click)="next()"  [disabled]="nbchecked ==0 ">\n                                                                                      Suivant\n                                                                                      </button>\n                                                                                      </ion-col> \n                                                                                    \n                                                                                  </ion-row>\n                                                                              </ion-list>\n                                                                        </div>\n                                                       \n                                \n                                                              \n                                           </ion-col>\n                                           <ion-col col-2 > \n                                              {{question_num}}\n                                                    </ion-col> \n                                                  </ion-row>\n\n                                                  <div style="width:70%; text-align: center; margin: 0 auto;" >\n                                                        <div class="progress-outer" style="margin: 1px 1px;padding: 1px;text-align: center;background-color: #cfcfcf;border: 1px solid #dcdcdc;color: #fff;border-radius: 20px;">\n                                                        <div class="progress-inner" [style.width]="progressPercent + \'%\'" style="width: 96%;height:20px;margin: 2px 2px;text-align: center;background-color: #84bdf1;border: 1px solid #dcdcdc;color: #fff;border-radius: 20px;">\n                                                           {{progressPercent}}%\n                                                        </div>\n                                                        </div>\n                                                        </div> \n                                                </ion-grid>\n                                            </ion-content>'/*ion-inline-end:"/Users/Maxime/Desktop/passation/src/pages/stress/stress.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["f" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_7__providers_AuthService__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_5_ng2_dragula__["b" /* DragulaService */], __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["f" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_2__providers_reddit_service__["a" /* RedditService */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["j" /* NavParams */]])
    ], StressPage);
    return StressPage;
}());

//# sourceMappingURL=stress.js.map

/***/ }),

/***/ 366:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ImaginationPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_add_operator_map__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_reddit_service__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng2_dragula__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__endtest_endtest__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_AuthService__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__login_login__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_forms__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__logique_logique__ = __webpack_require__(165);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var ImaginationPage = /** @class */ (function () {
    function ImaginationPage(fb, loadingController, authService, dragulaService, loadingCtrl, navCtrl, storage, redditService, navParams) {
        var _this = this;
        this.loadingController = loadingController;
        this.authService = authService;
        this.dragulaService = dragulaService;
        this.loadingCtrl = loadingCtrl;
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.redditService = redditService;
        this.navParams = navParams;
        this.editing = true;
        this.dataq = [];
        this.indextest = 0;
        this.indexdisplay = 1;
        this.posts2 = [];
        this.checkedIdx = -1;
        this.options = [];
        this.reponse = [];
        this.reponsesave = [];
        this.urlimage = "https://www.selectionetconseils.ch/examen/image/";
        this.responsestring = "";
        this.datareponse = [];
        this.idtest = navParams.get('param1');
        this.testlenght = navParams.get('param2');
        this.testcategory = navParams.get('param4');
        this.timeInSeconds = navParams.get('param5');
        this.test_nav = navParams.get('param6');
        this.position = navParams.get('param7');
        this.form = fb.group({
            cbox: [false, __WEBPACK_IMPORTED_MODULE_10__logique_logique__["a" /* LogiquePage */].mustBeTruthy]
        });
        this.storage.get('idresponse').then(function (idresponse) {
            _this.idresponse = idresponse;
        });
    }
    ImaginationPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        if (this.timeInSeconds > 0) {
            this.startTimer();
        }
        this.indexdisplay = 1;
        //Données i=0
        this.storage.get("questions").then(function (users) {
            _this.dataq = JSON.parse(users);
            console.log(_this.dataq);
            _this.nbquestionbytest = _this.dataq.length;
            _this.question_label = _this.dataq[0].question_label;
            _this.q_prop1 = _this.dataq[0].q_prop1;
            _this.q_prop2 = _this.dataq[0].q_prop2;
            _this.q_prop3 = _this.dataq[0].q_prop3;
            _this.q_prop4 = _this.dataq[0].q_prop4;
            _this.q_prop5 = _this.dataq[0].q_prop5;
            _this.q_img1 = _this.urlimage + _this.dataq[0].q_img1;
            _this.q_img2 = _this.urlimage + _this.dataq[0].q_img2;
            _this.q_img3 = _this.urlimage + _this.dataq[0].q_img3;
            _this.q_img4 = _this.urlimage + _this.dataq[0].q_img4;
            _this.q_img5 = _this.urlimage + _this.dataq[0].q_img5;
            _this.q_h = _this.dataq[0].q_h;
            _this.q_f = _this.dataq[0].q_f;
            _this.question_num = _this.dataq[0].question_num;
            _this.options = [_this.q_img1];
            _this.checkedIdx = -1;
            _this.reponseimg = "";
            //Array reponse
            for (var i = 0; i < _this.nbquestionbytest; i++) {
                _this.reponsesave[i] = "";
            }
            console.log(_this.reponsesave);
        });
    };
    ImaginationPage.prototype.next = function () {
        this.reponsesave[this.indextest] = this.reponseimg;
        // new question
        this.indextest = this.indextest + 1;
        // toutes les réponses
        console.log("réponse");
        console.log(this.reponsesave);
        this.reponseimg = this.reponsesave[this.indextest];
        if (this.indextest < this.nbquestionbytest) {
            this.indexdisplay = this.indexdisplay + 1;
            this.retrieveQuestions();
        }
        else {
            this.pauseTimer();
            ////// SAVE DATA
            console.log(this.testlenght);
            for (var i = 0; i < this.testlenght; i++) {
                this.response1 = this.reponsesave[i];
                console.log(this.response1);
                this.responsestring = this.responsestring + this.response1 + ";";
                console.log(this.responsestring);
            }
            /////
            var data = JSON.stringify({
                name: "InterpretationA",
                idresponse: this.idresponse,
                resp: this.responsestring,
            });
            console.log(data);
            this.redditService.addresponses(data)
                .toPromise()
                .then(function (response) {
                console.log(response);
            });
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__endtest_endtest__["a" /* EndtestPage */], {
                param1: this.idtest,
                param2: this.position,
            });
        }
    };
    ImaginationPage.prototype.prev = function () {
        if (this.indextest > 0) {
            this.indextest = this.indextest - 1;
            this.indexdisplay = this.indexdisplay - 1;
            this.reponseimg = this.reponsesave[this.indextest];
            this.reviewQuestions();
        }
        else {
        }
    };
    ImaginationPage.prototype.retrieveQuestions = function () {
        var _this = this;
        this.storage.get("questions").then(function (users) {
            _this.dataq = JSON.parse(users);
            _this.question_label = _this.dataq[_this.indextest].question_label;
            _this.propositions = _this.dataq[_this.indextest];
            _this.q_prop1 = _this.dataq[_this.indextest].q_prop1;
            _this.q_prop2 = _this.dataq[_this.indextest].q_prop2;
            _this.q_prop3 = _this.dataq[_this.indextest].q_prop3;
            _this.q_prop4 = _this.dataq[_this.indextest].q_prop4;
            _this.q_prop5 = _this.dataq[_this.indextest].q_prop5;
            _this.q_img1 = _this.urlimage + _this.dataq[_this.indextest].q_img1;
            _this.q_img2 = _this.urlimage + _this.dataq[_this.indextest].q_img2;
            _this.q_img3 = _this.urlimage + _this.dataq[_this.indextest].q_img3;
            _this.q_img4 = _this.urlimage + _this.dataq[_this.indextest].q_img4;
            _this.q_img5 = _this.urlimage + _this.dataq[_this.indextest].q_img5;
            _this.q_h = _this.dataq[_this.indextest].q_h;
            _this.q_f = _this.dataq[_this.indextest].q_f;
            _this.reponseimg = _this.reponsesave[_this.indextest];
            _this.options = [_this.q_img1];
            _this.question_num = _this.dataq[_this.indextest].question_num;
        });
    };
    ImaginationPage.prototype.reviewQuestions = function () {
        var _this = this;
        this.storage.get("questions").then(function (users) {
            _this.dataq = JSON.parse(users);
            _this.question_label = _this.dataq[_this.indextest].question_label;
            _this.propositions = _this.dataq[_this.indextest];
            _this.q_prop1 = _this.dataq[_this.indextest].q_prop1;
            _this.q_prop2 = _this.dataq[_this.indextest].q_prop2;
            _this.q_prop3 = _this.dataq[_this.indextest].q_prop3;
            _this.q_prop4 = _this.dataq[_this.indextest].q_prop4;
            _this.q_prop5 = _this.dataq[_this.indextest].q_prop5;
            _this.q_img1 = _this.urlimage + _this.dataq[_this.indextest].q_img1;
            _this.q_img2 = _this.urlimage + _this.dataq[_this.indextest].q_img2;
            _this.q_img3 = _this.urlimage + _this.dataq[_this.indextest].q_img3;
            _this.q_img4 = _this.urlimage + _this.dataq[_this.indextest].q_img4;
            _this.q_img5 = _this.urlimage + _this.dataq[_this.indextest].q_img5;
            _this.q_h = _this.dataq[_this.indextest].q_h;
            _this.q_f = _this.dataq[_this.indextest].q_f;
            _this.reponseimg = _this.reponsesave[_this.indextest];
            _this.options = [_this.q_img1];
            _this.question_num = _this.dataq[_this.indextest].question_num;
        });
    };
    ImaginationPage.mustBeTruthy = function (c) {
        var rv = {};
        if (!c.value) {
            rv['notChecked'] = true;
        }
        return rv;
    };
    ImaginationPage.prototype.reorderData = function (indexes) {
        this.posts2 = Object(__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["o" /* reorderArray */])(this.posts2, indexes);
        console.log(this.posts2);
        console.log(indexes);
        //this.dataq[this.indextest] = reorderArray(this.dataq[this.indextest], indexes);
        // console.log(this.dataq[this.indextest]);
        // console.log(this.dataq[this.indextest]);
    };
    ImaginationPage.prototype.logOut = function () {
        this.authService.logout();
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__login_login__["a" /* Login */]);
    };
    ImaginationPage.prototype.ngOnInit = function () {
        this.initTimer();
    };
    ImaginationPage.prototype.initTimer = function () {
        this.time = this.timeInSeconds;
        this.runTimer = false;
        this.hasStarted = false;
        this.hasFinished = false;
        this.remainingTime = this.timeInSeconds;
        this.displayTime = this.getSecondsAsDigitalClock(this.remainingTime);
    };
    ImaginationPage.prototype.startTimer = function () {
        this.runTimer = true;
        this.hasStarted = true;
        this.timerTick();
    };
    ImaginationPage.prototype.pauseTimer = function () {
        this.runTimer = false;
    };
    ImaginationPage.prototype.resumeTimer = function () {
        this.startTimer();
    };
    ImaginationPage.prototype.timerTick = function () {
        var _this = this;
        setTimeout(function () {
            if (!_this.runTimer) {
                return;
            }
            _this.remainingTime--;
            _this.displayTime = _this.getSecondsAsDigitalClock(_this.remainingTime);
            if (_this.remainingTime > 0) {
                _this.timerTick();
            }
            else {
                _this.hasFinished = true;
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__endtest_endtest__["a" /* EndtestPage */], {
                    param1: _this.idtest,
                    param2: _this.position,
                });
            }
        }, 1000);
    };
    ImaginationPage.prototype.getSecondsAsDigitalClock = function (inputSeconds) {
        var sec_num = parseInt(inputSeconds.toString(), 10); // don't forget the second param
        var hours = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);
        var hoursString = '';
        var minutesString = '';
        var secondsString = '';
        hoursString = (hours < 10) ? "0" + hours : hours.toString();
        minutesString = (minutes < 10) ? "0" + minutes : minutes.toString();
        secondsString = (seconds < 10) ? "0" + seconds : seconds.toString();
        return hoursString + ':' + minutesString + ':' + secondsString;
    };
    ImaginationPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Component"])({
            providers: [__WEBPACK_IMPORTED_MODULE_5_ng2_dragula__["b" /* DragulaService */]],
            selector: 'page-imagination',template:/*ion-inline-start:"/Users/Maxime/Desktop/passation/src/pages/imagination/imagination.html"*/'<ion-header>\n    <ion-toolbar  color="secondary" hide-back-button >\n          <ion-title>  </ion-title>\n            <h3 text-center *ngIf="displayTime!==0">{{displayTime}}</h3>\n          <ion-buttons end>\n            <button ion-button icon-only (click)="logOut()">\n                <ion-icon ios="ios-log-out" md="md-log-out"></ion-icon>\n            </button>\n          </ion-buttons>\n    </ion-toolbar>  \n  </ion-header>\n  \n  <ion-content color="light2" >\n  \n      <ion-grid >\n    \n          <ion-row responsive-sm  >\n              <ion-col col-2 > \n              </ion-col> \n              <ion-col col-8 >  \n                  <div *ngIf="test_nav==1">\n                      <ion-list >  \n                        <ion-card>\n                          <ion-card-header>\n                           <b> {{question_label}}</b>\n                             {{indexdisplay}}/{{nbquestionbytest}}\n                          </ion-card-header>\n                          <ion-card-content >\n                           \n                          </ion-card-content>\n                        </ion-card>\n                      </ion-list>\n                      <ion-list padding>  \n                          <ion-row >\n                          <ion-col col-6 > \n                             \n                                  <div *ngFor="let item of options; let i=index" style="padding-right: 20px;">\n                                    <img  [src]="item" style="width:400px;height:400px"/>\n                                  </div>\n                           \n                            </ion-col> \n                            <ion-col col-6 >  \n    \n                                <ion-list  padding>\n                                    <h3> Ajoutez une réponse sous forme de mot ou texte.</h3>\n                                    <ion-item>\n                                            <ion-label floating>Votre réponse</ion-label>\n                                            <ion-input [(ngModel)] = "reponseimg" type="text"></ion-input>\n                                        </ion-item>\n                                     \n                                \n                                   \n                    \n                            </ion-list>\n                            </ion-col> \n                          </ion-row>\n                          \n                      </ion-list>\n                      <ion-list padding>  \n                              <ion-row responsive-sm  >\n                                  <ion-col col-2 > \n                                  <button ion-button full (click)="prev()" >\n                                  Précedent\n                                  </button>\n                                  </ion-col> \n                                  <ion-col col-8 >  \n                                  </ion-col> \n                                  <ion-col col-2 > \n                                  <button ion-button  full (click)="next()" >\n                                  Suivant\n                                  </button>\n                                  </ion-col> \n                              </ion-row>\n                          </ion-list>\n                    </div>\n                    <div *ngIf="test_nav==2">\n                        <ion-list >  \n                          <ion-card>\n                            <ion-card-header>\n                             <b> {{question_label}}</b>\n                               {{indexdisplay}}/{{nbquestionbytest}}\n                            </ion-card-header>\n                            <ion-card-content >\n                             \n                            </ion-card-content>\n                          </ion-card>\n                        </ion-list>\n                        <ion-list padding>  \n                            <ion-row >\n                            <ion-col col-6 > \n                               \n                                    <div *ngFor="let item of options; let i=index" style="padding-right: 20px;">\n                                      <img  [src]="item" style="width:400px;height:400px"/>\n                                    </div>\n                             \n                              </ion-col> \n                              <ion-col col-6 >  \n      \n                                  <ion-list  padding>\n                                      <h3> Ajoutez une réponse sous forme de mot ou texte.</h3>\n                                      <ion-item>\n                                              <ion-label floating>Votre réponse</ion-label>\n                                              <ion-input [(ngModel)] = "reponseimg" type="text"></ion-input>\n                                          </ion-item>\n                                       \n                                  \n                                     \n                      \n                              </ion-list>\n                              </ion-col> \n                            </ion-row>\n                            \n                        </ion-list>\n                        <ion-list padding>  \n                                <ion-row responsive-sm  >\n                                    <ion-col col-2 > \n                               \n                                    </ion-col> \n                                    <ion-col col-8 >  \n                                    </ion-col> \n                                    <ion-col col-2 > \n                                    <button ion-button  full (click)="next()" >\n                                    Suivant\n                                    </button>\n                                    </ion-col> \n                                </ion-row>\n                            </ion-list>\n                      </div>\n                <div *ngIf="test_nav==3">\n                  <ion-list >  \n                    <ion-card>\n                      <ion-card-header>\n                       <b> {{question_label}}</b>\n                         {{indexdisplay}}/{{nbquestionbytest}}\n                      </ion-card-header>\n                      <ion-card-content >\n                       \n                      </ion-card-content>\n                    </ion-card>\n                  </ion-list>\n                  <ion-list padding>  \n                      <ion-row >\n                      <ion-col col-6 > \n                         \n                              <div *ngFor="let item of options; let i=index" style="padding-right: 20px;">\n                                <img  [src]="item" style="width:400px;height:400px"/>\n                              </div>\n                       \n                        </ion-col> \n                        <ion-col col-6 >  \n\n                            <ion-list  padding>\n                                <h3> Ajoutez une réponse sous forme de mot ou texte.</h3>\n                                <ion-item>\n                                        <ion-label floating>Votre réponse</ion-label>\n                                        <ion-input [(ngModel)] = "reponseimg" type="text"></ion-input>\n                                    </ion-item>\n                                 \n                            \n                               \n                \n                        </ion-list>\n                        </ion-col> \n                      </ion-row>\n                      \n                  </ion-list>\n                  <ion-list padding>  \n                          <ion-row responsive-sm  >\n                              <ion-col col-2 > \n                              <button ion-button full (click)="prev()" [disabled]="reponseimg==\'\'">\n                              Précedent\n                              </button>\n                              </ion-col> \n                              <ion-col col-8 >  \n                              </ion-col> \n                              <ion-col col-2 > \n                              <button ion-button  full (click)="next()" [disabled]="reponseimg==\'\'">\n                              Suivant\n                              </button>\n                              </ion-col> \n                          </ion-row>\n                      </ion-list>\n                </div>\n                <div *ngIf="test_nav==4">\n                    <ion-list >  \n                      <ion-card>\n                        <ion-card-header>\n                         <b> {{question_label}}</b>\n                           {{indexdisplay}}/{{nbquestionbytest}}\n                        </ion-card-header>\n                        <ion-card-content >\n                         \n                        </ion-card-content>\n                      </ion-card>\n                    </ion-list>\n                    <ion-list padding>  \n                        <ion-row >\n                        <ion-col col-6 > \n                           \n                                <div *ngFor="let item of options; let i=index" style="padding-right: 20px;">\n                                  <img  [src]="item" style="width:400px;height:400px"/>\n                                </div>\n                         \n                          </ion-col> \n                          <ion-col col-6 >  \n  \n                              <ion-list  padding>\n                                  <h3> Ajoutez une réponse sous forme de mot ou texte.</h3>\n                                  <ion-item>\n                                          <ion-label floating>Votre réponse</ion-label>\n                                          <ion-input [(ngModel)] = "reponseimg" type="text"></ion-input>\n                                      </ion-item>\n                                   \n                              \n                                 \n                  \n                          </ion-list>\n                          </ion-col> \n                        </ion-row>\n                        \n                    </ion-list>\n                    <ion-list padding>  \n                            <ion-row responsive-sm  >\n                                <ion-col col-2 > \n                             \n                                </ion-col> \n                                <ion-col col-8 >  \n                                </ion-col> \n                                <ion-col col-2 > \n                                <button ion-button  full (click)="next()" [disabled]="reponseimg==\'\'">\n                                Suivant\n                                </button>\n                                </ion-col> \n                            </ion-row>\n                        </ion-list>\n                  </div>\n   </ion-col>\n   <ion-col col-2 > \n        {{question_num}}\n            </ion-col> \n          </ion-row>\n\n\n          <div style="width:70%; text-align: center; margin: 0 auto;">\n                <div class="progress-outer" style="margin: 1px 1px;padding: 1px;text-align: center;background-color: #cfcfcf;border: 1px solid #dcdcdc;color: #fff;border-radius: 20px;">\n                <div class="progress-inner" [style.width]="progressPercent + \'%\'" style="width: 96%;height:20px;margin: 2px 2px;text-align: center;background-color: #84bdf1;border: 1px solid #dcdcdc;color: #fff;border-radius: 20px;">\n                   {{progressPercent}}%\n                </div>\n                </div>\n                </div> \n         \n        </ion-grid>\n    \n    </ion-content>\n    \n  '/*ion-inline-end:"/Users/Maxime/Desktop/passation/src/pages/imagination/imagination.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_9__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["f" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_7__providers_AuthService__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_5_ng2_dragula__["b" /* DragulaService */], __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["f" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_2__providers_reddit_service__["a" /* RedditService */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["j" /* NavParams */]])
    ], ImaginationPage);
    return ImaginationPage;
}());

//# sourceMappingURL=imagination.js.map

/***/ }),

/***/ 367:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return JugementPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_add_operator_map__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_reddit_service__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng2_dragula__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__endtest_endtest__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_AuthService__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__login_login__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var JugementPage = /** @class */ (function () {
    function JugementPage(loadingController, authService, dragulaService, loadingCtrl, navCtrl, storage, redditService, navParams) {
        var _this = this;
        this.loadingController = loadingController;
        this.authService = authService;
        this.dragulaService = dragulaService;
        this.loadingCtrl = loadingCtrl;
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.redditService = redditService;
        this.navParams = navParams;
        this.editing = true;
        this.dataq = [];
        this.indextest = 0;
        this.indexdisplay = 1;
        this.posts2 = [];
        this.nbchecked = 0;
        this.checkedIdx = -1;
        this.options = [];
        this.reponse = [];
        this.reponsesave = [];
        this.myDataArray = [];
        this.datareponse = [];
        this.responsestring = "";
        this.reponsetosave = [];
        this.idtest = navParams.get('param1');
        this.testlenght = navParams.get('param2');
        this.testcategory = navParams.get('param4');
        this.timeInSeconds = navParams.get('param5');
        this.test_nav = navParams.get('param6');
        this.position = navParams.get('param7');
        this.storage.get('idresponse').then(function (idresponse) {
            _this.idresponse = idresponse;
        });
        this.storage.get("questions").then(function (users) {
            _this.dataq = JSON.parse(users);
            for (var i = 0; i < _this.dataq.length; i++) {
                _this.question_label = _this.dataq[i].question_label;
                _this.propositions = _this.dataq[i];
                _this.q_prop1 = _this.dataq[i].q_prop1;
                _this.q_prop2 = _this.dataq[i].q_prop2;
                _this.q_prop3 = _this.dataq[i].q_prop3;
                _this.q_prop4 = _this.dataq[i].q_prop4;
                _this.q_prop5 = _this.dataq[i].q_prop5;
                _this.q_h = _this.dataq[i].q_h;
                _this.q_f = _this.dataq[i].q_f;
                _this.checkedIdx = _this.reponsesave[i];
                _this.question_num = _this.dataq[i].question_num;
                _this.options = [{ id: '2', prop: _this.q_prop2, order: 2, isChecked: false }, { id: '5', prop: _this.q_prop5, order: 5, isChecked: false },
                    { id: '3', prop: _this.q_prop3, order: 3, isChecked: false }, { id: '1', prop: _this.q_prop1, order: 1, isChecked: false }, { id: '4', prop: _this.q_prop4, order: 4, isChecked: false }];
                _this.datareponse.push(_this.options);
                console.log(_this.datareponse);
            }
        });
    }
    JugementPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        if (this.timeInSeconds > 0) {
            this.startTimer();
        }
        this.indexdisplay = 1;
        //Données i=0
        this.storage.get("questions").then(function (users) {
            _this.dataq = JSON.parse(users);
            _this.nbquestionbytest = _this.dataq.length;
            _this.question_label = _this.dataq[0].question_label;
            _this.q_h = _this.dataq[0].q_h;
            _this.q_f = _this.dataq[0].q_f;
            _this.question_num = _this.dataq[0].question_num;
            _this.datarep = _this.datareponse[0];
        });
    };
    JugementPage.prototype.next = function () {
        for (var i = 0; i < this.datareponse[this.indextest]; i++) {
            this.datareponse.push(this.datareponse[this.indextest]);
        }
        // new question
        this.indextest = this.indextest + 1;
        if (this.indextest < this.nbquestionbytest) {
            this.indexdisplay = this.indexdisplay + 1;
            this.retrieveQuestions();
        }
        else {
            this.pauseTimer();
            ////// SAVE DATA
            for (var i = 0; i < this.testlenght; i++) {
                this.responsezero = this.datareponse[i][0].id;
                console.log(this.responsezero);
                this.responsestring = this.responsestring + this.responsezero;
                console.log(this.responsestring);
            }
            /////
            var data = JSON.stringify({
                name: "MaturitéJugement",
                idresponse: this.idresponse,
                resp: this.responsestring,
            });
            this.redditService.addresponses(data)
                .toPromise()
                .then(function (response) {
                console.log(response);
            });
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__endtest_endtest__["a" /* EndtestPage */], {
                param1: this.idtest,
                param2: this.position,
            });
        }
    };
    JugementPage.prototype.prev = function () {
        if (this.indextest > 0) {
            this.indextest = this.indextest - 1;
            this.indexdisplay = this.indexdisplay - 1;
            this.reviewQuestions();
        }
        else {
        }
    };
    JugementPage.prototype.retrieveQuestions = function () {
        var _this = this;
        this.storage.get("questions").then(function (users) {
            _this.dataq = JSON.parse(users);
            _this.question_label = _this.dataq[_this.indextest].question_label;
            _this.q_h = _this.dataq[_this.indextest].q_h;
            _this.q_f = _this.dataq[_this.indextest].q_f;
            _this.question_num = _this.dataq[_this.indextest].question_num;
            _this.datarep = _this.datareponse[_this.indextest];
            _this.nbchecked = 0;
            for (var i = 0; i < 5; i++) {
                if (_this.datarep[i].isChecked == true) {
                    _this.nbchecked = _this.nbchecked + 1;
                }
            }
        });
    };
    JugementPage.prototype.reviewQuestions = function () {
        var _this = this;
        this.storage.get("questions").then(function (users) {
            _this.dataq = JSON.parse(users);
            _this.question_label = _this.dataq[_this.indextest].question_label;
            _this.q_h = _this.dataq[_this.indextest].q_h;
            _this.q_f = _this.dataq[_this.indextest].q_f;
            _this.question_num = _this.dataq[_this.indextest].question_num;
            _this.datarep = _this.datareponse[_this.indextest];
            // nb true reponse
            _this.nbchecked = 0;
            for (var i = 0; i < 5; i++) {
                if (_this.datarep[i].isChecked == true) {
                    _this.nbchecked = _this.nbchecked + 1;
                }
            }
        });
    };
    JugementPage.mustBeTruthy = function (c) {
        var rv = {};
        if (!c.value) {
            rv['notChecked'] = true;
        }
        return rv;
    };
    JugementPage.prototype.reorderData = function (indexes) {
        this.datarep = Object(__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["o" /* reorderArray */])(this.datarep, indexes);
        if (this.datarep[indexes.to].isChecked == false) {
            this.datarep[indexes.to].isChecked = true;
            this.nbchecked = this.nbchecked + 1;
        }
    };
    JugementPage.prototype.logOut = function () {
        this.authService.logout();
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__login_login__["a" /* Login */]);
    };
    JugementPage.prototype.ngOnInit = function () {
        this.initTimer();
    };
    JugementPage.prototype.initTimer = function () {
        this.time = this.timeInSeconds;
        this.runTimer = false;
        this.hasStarted = false;
        this.hasFinished = false;
        this.remainingTime = this.timeInSeconds;
        this.displayTime = this.getSecondsAsDigitalClock(this.remainingTime);
    };
    JugementPage.prototype.startTimer = function () {
        this.runTimer = true;
        this.hasStarted = true;
        this.timerTick();
    };
    JugementPage.prototype.pauseTimer = function () {
        this.runTimer = false;
    };
    JugementPage.prototype.resumeTimer = function () {
        this.startTimer();
    };
    JugementPage.prototype.timerTick = function () {
        var _this = this;
        setTimeout(function () {
            if (!_this.runTimer) {
                return;
            }
            _this.remainingTime--;
            _this.displayTime = _this.getSecondsAsDigitalClock(_this.remainingTime);
            if (_this.remainingTime > 0) {
                _this.timerTick();
            }
            else {
                _this.hasFinished = true;
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__endtest_endtest__["a" /* EndtestPage */], {
                    param1: _this.idtest,
                    param2: _this.position,
                });
            }
        }, 1000);
    };
    JugementPage.prototype.getSecondsAsDigitalClock = function (inputSeconds) {
        var sec_num = parseInt(inputSeconds.toString(), 10); // don't forget the second param
        var hours = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);
        var hoursString = '';
        var minutesString = '';
        var secondsString = '';
        hoursString = (hours < 10) ? "0" + hours : hours.toString();
        minutesString = (minutes < 10) ? "0" + minutes : minutes.toString();
        secondsString = (seconds < 10) ? "0" + seconds : seconds.toString();
        return hoursString + ':' + minutesString + ':' + secondsString;
    };
    JugementPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Component"])({
            providers: [__WEBPACK_IMPORTED_MODULE_5_ng2_dragula__["b" /* DragulaService */]],
            selector: 'page-jugement',template:/*ion-inline-start:"/Users/Maxime/Desktop/passation/src/pages/jugement/jugement.html"*/'<ion-header>\n  <ion-toolbar  color="secondary" hide-back-button >\n\n        <ion-buttons end>\n          <button ion-button icon-only (click)="logOut()">\n              <ion-icon ios="ios-log-out" md="md-log-out"></ion-icon>\n          </button>\n        </ion-buttons>\n  </ion-toolbar>  \n</ion-header>\n\n<ion-content color="light2" >\n      \n    <ion-grid >\n        <ion-row responsive-sm  >\n            <ion-col col-2 > \n            </ion-col> \n            <ion-col col-8 >  \n              \n                <div *ngIf="test_nav==1">\n                    <ion-list >     \n                      <ion-card>\n                        <ion-card-header>\n                         <b> {{question_label}}</b>\n                         {{indexdisplay}}/{{nbquestionbytest}}\n                        </ion-card-header>\n                        <ion-card-content>\n                            <p [innerHTML]="q_h"></p>  \n                        </ion-card-content>\n                      </ion-card>\n                    </ion-list>\n                       \n                    <ion-list padding>   \n                        <ion-item-group [reorder]="editing" (ionItemReorder)="reorderData($event)" >\n                                <ion-item *ngFor="let item of datarep " [color]="item.isChecked ? \'violet\' : \'secondary\'"  >\n                                  <b>{{item.prop}}</b>\n                                </ion-item>\n                              </ion-item-group>\n                 </ion-list>\n                    <ion-list padding>  \n                        <ion-row responsive-sm  >\n                            <ion-col col-2 > \n                            <button ion-button full (click)="prev()">\n                                    {{ \'prev\' | translate }}\n                            </button>\n                            </ion-col> \n                            <ion-col col-8 >  \n                            </ion-col> \n                            <ion-col col-2 > \n                            <button ion-button  full (click)="next()">\n                                    {{ \'next\' | translate }}\n                            </button>\n                            </ion-col> \n                        </ion-row>\n                    </ion-list>\n                  </div>\n                  <div *ngIf="test_nav==2">\n                      <ion-list >     \n                        <ion-card>\n                          <ion-card-header>\n                           <b> {{question_label}}</b>\n                           {{indexdisplay}}/{{nbquestionbytest}}\n                          </ion-card-header>\n                          <ion-card-content>\n                              <p [innerHTML]="q_h"></p>  \n                          </ion-card-content>\n                        </ion-card>\n                      </ion-list>\n                         \n                      <ion-list padding>   \n                          <ion-item-group [reorder]="editing" (ionItemReorder)="reorderData($event)" >\n                                  <ion-item *ngFor="let item of datarep " [color]="item.isChecked ? \'violet\' : \'secondary\'" >\n                                    <b>{{item.prop}}</b>\n                                  </ion-item>\n                                </ion-item-group>\n                   </ion-list>\n                      <ion-list padding>  \n                          <ion-row responsive-sm  >\n                              <ion-col col-2 > \n                           \n                              </ion-col> \n                              <ion-col col-8 >  \n                              </ion-col> \n                              <ion-col col-2 > \n                              <button ion-button  full (click)="next()" >\n                                    {{ \'next\' | translate }}\n                              </button>\n                              </ion-col> \n                          </ion-row>\n                      </ion-list>\n                    </div>\n                    <div *ngIf="test_nav==3">\n                        <ion-list >     \n                          <ion-card>\n                            <ion-card-header>\n                             <b> {{question_label}}</b>\n                             {{indexdisplay}}/{{nbquestionbytest}}\n                            </ion-card-header>\n                            <ion-card-content>\n                                <p [innerHTML]="q_h"></p>  \n                            </ion-card-content>\n                          </ion-card>\n                        </ion-list>\n                           \n                        <ion-list padding>   \n                            <ion-item-group [reorder]="editing" (ionItemReorder)="reorderData($event)" >\n                                    <ion-item *ngFor="let item of datarep " [color]="item.isChecked ? \'violet\' : \'secondary\'" >\n                                      <b>{{item.prop}}</b>\n                                    </ion-item>\n                                  </ion-item-group>\n                     </ion-list>\n                        <ion-list padding>  \n                            <ion-row responsive-sm  >\n                                <ion-col col-2 > \n                                <button ion-button full (click)="prev()"[disabled]="nbchecked < 5">\n                                        {{ \'prev\' | translate }}\n                                </button>\n                                </ion-col> \n                                <ion-col col-8 >  \n                                </ion-col> \n                                <ion-col col-2 > \n                                <button ion-button  full (click)="next()" [disabled]="nbchecked < 5">\n                                        {{ \'next\' | translate }}\n                                </button>\n                                </ion-col> \n                            </ion-row>\n                        </ion-list>\n                      </div>\n                      <div *ngIf="test_nav==4">\n                          <ion-list >     \n                            <ion-card>\n                              <ion-card-header>\n                               <b> {{question_label}}</b>\n                               {{indexdisplay}}/{{nbquestionbytest}}\n                              </ion-card-header>\n                              <ion-card-content>\n                                  <p [innerHTML]="q_h"></p>  \n                              </ion-card-content>\n                            </ion-card>\n                          </ion-list>\n                             \n                          <ion-list padding>   \n                              <ion-item-group [reorder]="editing" (ionItemReorder)="reorderData($event)" >\n                                      <ion-item *ngFor="let item of datarep " [color]="item.isChecked ? \'violet\' : \'secondary\'" >\n                                        <b>{{item.prop}}</b>\n                                      </ion-item>\n                                    </ion-item-group>\n                       </ion-list>\n                          <ion-list padding>  \n                              <ion-row responsive-sm  >\n                                  <ion-col col-2 > \n                              \n                                  </ion-col> \n                                  <ion-col col-8 >  \n                                  </ion-col> \n                                  <ion-col col-2 > \n                                  <button ion-button  full (click)="next()" [disabled]="nbchecked < 5">\n                                        {{ \'next\' | translate }}\n                                  </button>\n                                  </ion-col> \n                              </ion-row>\n                          </ion-list>\n                        </div>\n </ion-col>\n <ion-col col-2 > \n    {{question_num}}\n          </ion-col> \n        </ion-row>\n\n        <div style="width:70%; text-align: center; margin: 0 auto;">\n                <div class="progress-outer" style="margin: 1px 1px;padding: 1px;text-align: center;background-color: #cfcfcf;border: 1px solid #dcdcdc;color: #fff;border-radius: 20px;">\n                <div class="progress-inner" [style.width]="progressPercent + \'%\'" style="width: 96%;height:20px;margin: 2px 2px;text-align: center;background-color: #84bdf1;border: 1px solid #dcdcdc;color: #fff;border-radius: 20px;">\n                   {{progressPercent}}%\n                </div>\n                </div>\n                </div> \n         \n      </ion-grid>\n  </ion-content>\n  \n'/*ion-inline-end:"/Users/Maxime/Desktop/passation/src/pages/jugement/jugement.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["f" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_7__providers_AuthService__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_5_ng2_dragula__["b" /* DragulaService */], __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["f" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_2__providers_reddit_service__["a" /* RedditService */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["j" /* NavParams */]])
    ], JugementPage);
    return JugementPage;
}());

//# sourceMappingURL=jugement.js.map

/***/ }),

/***/ 368:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FacialePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_add_operator_map__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_reddit_service__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng2_dragula__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__endtest_endtest__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_AuthService__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__login_login__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var FacialePage = /** @class */ (function () {
    function FacialePage(loadingController, authService, dragulaService, loadingCtrl, navCtrl, storage, redditService, navParams) {
        var _this = this;
        this.loadingController = loadingController;
        this.authService = authService;
        this.dragulaService = dragulaService;
        this.loadingCtrl = loadingCtrl;
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.redditService = redditService;
        this.navParams = navParams;
        this.editing = true;
        this.dataq = [];
        this.indextest = 0;
        this.indexdisplay = 1;
        this.posts2 = [];
        this.nbchecked = 0;
        this.checkedIdx = -1;
        this.options = [];
        this.reponse = [];
        this.reponsesave = [];
        this.myDataArray = [];
        this.datareponse = [];
        this.responsestring = "";
        this.urlimage = "https://www.selectionetconseils.ch/examen/image/";
        this.idtest = navParams.get('param1');
        this.testlenght = navParams.get('param2');
        this.testcategory = navParams.get('param4');
        this.timeInSeconds = navParams.get('param5');
        this.test_nav = navParams.get('param6');
        this.position = navParams.get('param7');
        this.storage.get('idresponse').then(function (idresponse) {
            _this.idresponse = idresponse;
        });
        this.storage.get("questions").then(function (users) {
            _this.dataq = JSON.parse(users);
            for (var i = 0; i < _this.dataq.length; i++) {
                _this.question_label = _this.dataq[i].question_label;
                _this.propositions = _this.dataq[i];
                _this.q_prop1 = _this.dataq[i].q_prop1;
                _this.q_prop2 = _this.dataq[i].q_prop2;
                _this.q_prop3 = _this.dataq[i].q_prop3;
                _this.q_prop4 = _this.dataq[i].q_prop4;
                _this.q_prop5 = _this.dataq[i].q_prop5;
                _this.q_h = _this.dataq[i].q_h;
                _this.q_f = _this.dataq[i].q_f;
                _this.checkedIdx = _this.reponsesave[i];
                _this.question_num = _this.dataq[i].question_num;
                _this.options = [{ id: '2', prop: _this.q_prop2, order: 2, isChecked: false }, { id: '5', prop: _this.q_prop5, order: 5, isChecked: false },
                    { id: '3', prop: _this.q_prop3, order: 3, isChecked: false }, { id: '1', prop: _this.q_prop1, order: 1, isChecked: false }, { id: '4', prop: _this.q_prop4, order: 4, isChecked: false }];
                _this.datareponse.push(_this.options);
                console.log(_this.datareponse);
            }
        });
    }
    FacialePage.prototype.ionViewWillEnter = function () {
        var _this = this;
        if (this.timeInSeconds > 0) {
            this.startTimer();
        }
        this.indexdisplay = 1;
        //Données i=0
        this.storage.get("questions").then(function (users) {
            _this.dataq = JSON.parse(users);
            _this.nbquestionbytest = _this.dataq.length;
            _this.question_label = _this.dataq[0].question_label;
            _this.q_h = _this.dataq[0].q_h;
            _this.q_f = _this.dataq[0].q_f;
            _this.q_img1 = _this.urlimage + _this.dataq[_this.indextest].q_img1;
            _this.question_num = _this.dataq[0].question_num;
            _this.datarep = _this.datareponse[0];
        });
    };
    FacialePage.prototype.next = function () {
        for (var i = 0; i < this.datareponse[this.indextest]; i++) {
            this.datareponse.push(this.datareponse[this.indextest]);
        }
        // new question
        this.indextest = this.indextest + 1;
        if (this.indextest < this.nbquestionbytest) {
            this.indexdisplay = this.indexdisplay + 1;
            this.retrieveQuestions();
        }
        else {
            this.pauseTimer();
            ////// SAVE DATA
            console.log(this.testlenght);
            for (var i = 0; i < this.testlenght; i++) {
                this.responsezero = this.datareponse[i][0].id;
                this.responsestring = this.responsestring + this.responsezero;
            }
            /////
            var data = JSON.stringify({
                name: "ReconFaciale1",
                idresponse: this.idresponse,
                resp: this.responsestring,
            });
            console.log(data);
            this.redditService.addresponses(data)
                .toPromise()
                .then(function (response) {
                console.log(response);
            });
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__endtest_endtest__["a" /* EndtestPage */], {
                param1: this.idtest,
                param2: this.position,
            });
        }
    };
    FacialePage.prototype.prev = function () {
        if (this.indextest > 0) {
            this.indextest = this.indextest - 1;
            this.indexdisplay = this.indexdisplay - 1;
            this.reviewQuestions();
        }
        else {
        }
    };
    FacialePage.prototype.retrieveQuestions = function () {
        var _this = this;
        this.storage.get("questions").then(function (users) {
            _this.dataq = JSON.parse(users);
            _this.question_label = _this.dataq[_this.indextest].question_label;
            _this.q_h = _this.dataq[_this.indextest].q_h;
            _this.q_f = _this.dataq[_this.indextest].q_f;
            _this.q_img1 = _this.urlimage + _this.dataq[_this.indextest].q_img1;
            _this.question_num = _this.dataq[_this.indextest].question_num;
            _this.datarep = _this.datareponse[_this.indextest];
            _this.nbchecked = 0;
            for (var i = 0; i < 5; i++) {
                if (_this.datarep[i].isChecked == true) {
                    _this.nbchecked = _this.nbchecked + 1;
                }
            }
        });
    };
    FacialePage.prototype.reviewQuestions = function () {
        var _this = this;
        this.storage.get("questions").then(function (users) {
            _this.dataq = JSON.parse(users);
            _this.question_label = _this.dataq[_this.indextest].question_label;
            _this.q_h = _this.dataq[_this.indextest].q_h;
            _this.q_f = _this.dataq[_this.indextest].q_f;
            _this.q_img1 = _this.urlimage + _this.dataq[_this.indextest].q_img1;
            _this.question_num = _this.dataq[_this.indextest].question_num;
            _this.datarep = _this.datareponse[_this.indextest];
            // nb true reponse
            _this.nbchecked = 0;
            for (var i = 0; i < 5; i++) {
                if (_this.datarep[i].isChecked == true) {
                    _this.nbchecked = _this.nbchecked + 1;
                }
            }
        });
    };
    FacialePage.mustBeTruthy = function (c) {
        var rv = {};
        if (!c.value) {
            rv['notChecked'] = true;
        }
        return rv;
    };
    FacialePage.prototype.reorderData = function (indexes) {
        this.datarep = Object(__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["o" /* reorderArray */])(this.datarep, indexes);
        if (this.datarep[indexes.to].isChecked == false) {
            this.datarep[indexes.to].isChecked = true;
            this.nbchecked = this.nbchecked + 1;
        }
    };
    FacialePage.prototype.logOut = function () {
        this.authService.logout();
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__login_login__["a" /* Login */]);
    };
    FacialePage.prototype.ngOnInit = function () {
        this.initTimer();
    };
    FacialePage.prototype.initTimer = function () {
        this.time = this.timeInSeconds;
        this.runTimer = false;
        this.hasStarted = false;
        this.hasFinished = false;
        this.remainingTime = this.timeInSeconds;
        this.displayTime = this.getSecondsAsDigitalClock(this.remainingTime);
    };
    FacialePage.prototype.startTimer = function () {
        this.runTimer = true;
        this.hasStarted = true;
        this.timerTick();
    };
    FacialePage.prototype.pauseTimer = function () {
        this.runTimer = false;
    };
    FacialePage.prototype.resumeTimer = function () {
        this.startTimer();
    };
    FacialePage.prototype.timerTick = function () {
        var _this = this;
        setTimeout(function () {
            if (!_this.runTimer) {
                return;
            }
            _this.remainingTime--;
            _this.displayTime = _this.getSecondsAsDigitalClock(_this.remainingTime);
            if (_this.remainingTime > 0) {
                _this.timerTick();
            }
            else {
                _this.hasFinished = true;
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__endtest_endtest__["a" /* EndtestPage */], {
                    param1: _this.idtest,
                    param2: _this.position,
                });
            }
        }, 1000);
    };
    FacialePage.prototype.getSecondsAsDigitalClock = function (inputSeconds) {
        var sec_num = parseInt(inputSeconds.toString(), 10); // don't forget the second param
        var hours = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);
        var hoursString = '';
        var minutesString = '';
        var secondsString = '';
        hoursString = (hours < 10) ? "0" + hours : hours.toString();
        minutesString = (minutes < 10) ? "0" + minutes : minutes.toString();
        secondsString = (seconds < 10) ? "0" + seconds : seconds.toString();
        return hoursString + ':' + minutesString + ':' + secondsString;
    };
    FacialePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Component"])({
            providers: [__WEBPACK_IMPORTED_MODULE_5_ng2_dragula__["b" /* DragulaService */]],
            selector: 'page-faciale',template:/*ion-inline-start:"/Users/Maxime/Desktop/passation/src/pages/faciale/faciale.html"*/'<ion-header>\n  <ion-toolbar  color="secondary" hide-back-button >\n\n        <ion-buttons end>\n          <button ion-button icon-only (click)="logOut()">\n              <ion-icon ios="ios-log-out" md="md-log-out"></ion-icon>\n          </button>\n        </ion-buttons>\n  </ion-toolbar>  \n</ion-header>\n\n<ion-content color="light2" >\n      \n    <ion-grid >\n        <ion-row responsive-sm  >\n            <ion-col col-2 > \n            </ion-col> \n            <ion-col col-8 >  \n              \n                    <div *ngIf="test_nav==1">\n                            <ion-list >     \n                              <ion-card>\n                                <ion-card-header>\n                                 <b> {{question_label}}</b>\n                                 {{indexdisplay}}/{{nbquestionbytest}}\n                                </ion-card-header>\n                                <ion-card-content>\n                                    <p [innerHTML]="q_h"></p>  \n                                </ion-card-content>\n                              </ion-card>\n                            </ion-list>\n                               \n                            <ion-row >\n                                    <ion-col col-6 > \n                                            <div style="padding-right: 20px;padding-top: 40px;">\n                                              <img src="{{q_img1}}" style="width:300px;height:300px"/>\n                                            </div>\n                                      </ion-col> \n                                      <ion-col col-6 >  \n              \n                                            <ion-list padding>   \n                                                    <ion-item-group [reorder]="editing" (ionItemReorder)="reorderData($event)" >\n                                                            <ion-item *ngFor="let item of datarep " [color]="item.isChecked ? \'marron\' : \'secondary\'" >\n                                                              <b>{{item.prop}}</b>\n                                                            </ion-item>\n                                                          </ion-item-group>\n                                             </ion-list>\n                                      </ion-col> \n                                    </ion-row>\n                                    \n                            <ion-list padding>  \n                                <ion-row responsive-sm  >\n                                    <ion-col col-2 > \n                                    <button ion-button full (click)="prev()">\n                                    Précedent\n                                    </button>\n                                    </ion-col> \n                                    <ion-col col-8 >  \n                                    </ion-col> \n                                    <ion-col col-2 > \n                                    <button ion-button  full (click)="next()" >\n                                    Suivant\n                                    </button>\n                                    </ion-col> \n                                </ion-row>\n                            </ion-list>\n                          </div>\n                          <div *ngIf="test_nav==2">\n                                <ion-list >     \n                                  <ion-card>\n                                    <ion-card-header>\n                                     <b> {{question_label}}</b>\n                                     {{indexdisplay}}/{{nbquestionbytest}}\n                                    </ion-card-header>\n                                    <ion-card-content>\n                                        <p [innerHTML]="q_h"></p>  \n                                    </ion-card-content>\n                                  </ion-card>\n                                </ion-list>\n                                   \n                                <ion-row >\n                                        <ion-col col-6 > \n                                                <div style="padding-right: 20px;padding-top: 40px;">\n                                                  <img src="{{q_img1}}" style="width:300px;height:300px"/>\n                                                </div>\n                                          </ion-col> \n                                          <ion-col col-6 >  \n                  \n                                                <ion-list padding>   \n                                                        <ion-item-group [reorder]="editing" (ionItemReorder)="reorderData($event)" >\n                                                                <ion-item *ngFor="let item of datarep " [color]="item.isChecked ? \'marron\' : \'secondary\'" >\n                                                                  <b>{{item.prop}}</b>\n                                                                </ion-item>\n                                                              </ion-item-group>\n                                                 </ion-list>\n                                          </ion-col> \n                                        </ion-row>\n                                        \n                                <ion-list padding>  \n                                    <ion-row responsive-sm  >\n                                        <ion-col col-2 > \n                                      \n                                        </ion-col> \n                                        <ion-col col-8 >  \n                                        </ion-col> \n                                        <ion-col col-2 > \n                                        <button ion-button  full (click)="next()" >\n                                        Suivant\n                                        </button>\n                                        </ion-col> \n                                    </ion-row>\n                                </ion-list>\n                              </div>\n                              <div *ngIf="test_nav==3">\n                                    <ion-list >     \n                                      <ion-card>\n                                        <ion-card-header>\n                                         <b> {{question_label}}</b>\n                                         {{indexdisplay}}/{{nbquestionbytest}}\n                                        </ion-card-header>\n                                        <ion-card-content>\n                                            <p [innerHTML]="q_h"></p>  \n                                        </ion-card-content>\n                                      </ion-card>\n                                    </ion-list>\n                                       \n                                    <ion-row >\n                                            <ion-col col-6 > \n                                                    <div style="padding-right: 20px;padding-top: 40px;">\n                                                      <img src="{{q_img1}}" style="width:300px;height:300px"/>\n                                                    </div>\n                                              </ion-col> \n                                              <ion-col col-6 >  \n                      \n                                                    <ion-list padding>   \n                                                            <ion-item-group [reorder]="editing" (ionItemReorder)="reorderData($event)" >\n                                                                    <ion-item *ngFor="let item of datarep " [color]="item.isChecked ? \'marron\' : \'secondary\'" >\n                                                                      <b>{{item.prop}}</b>\n                                                                    </ion-item>\n                                                                  </ion-item-group>\n                                                     </ion-list>\n                                              </ion-col> \n                                            </ion-row>\n                                            \n                                    <ion-list padding>  \n                                        <ion-row responsive-sm  >\n                                            <ion-col col-2 > \n                                            <button ion-button full (click)="prev()"[disabled]="nbchecked < 5">\n                                            Précedent\n                                            </button>\n                                            </ion-col> \n                                            <ion-col col-8 >  \n                                            </ion-col> \n                                            <ion-col col-2 > \n                                            <button ion-button  full (click)="next()" [disabled]="nbchecked < 5">\n                                            Suivant\n                                            </button>\n                                            </ion-col> \n                                        </ion-row>\n                                    </ion-list>\n                                  </div>\n                                  <div *ngIf="test_nav==4">\n                                        <ion-list >     \n                                          <ion-card>\n                                            <ion-card-header>\n                                             <b> {{question_label}}</b>\n                                             {{indexdisplay}}/{{nbquestionbytest}}\n                                            </ion-card-header>\n                                            <ion-card-content>\n                                                <p [innerHTML]="q_h"></p>  \n                                            </ion-card-content>\n                                          </ion-card>\n                                        </ion-list>\n                                           \n                                        <ion-row >\n                                                <ion-col col-6 > \n                                                        <div style="padding-right: 20px;padding-top: 40px;">\n                                                          <img src="{{q_img1}}" style="width:300px;height:300px"/>\n                                                        </div>\n                                                  </ion-col> \n                                                  <ion-col col-6 >  \n                          \n                                                        <ion-list padding>   \n                                                                <ion-item-group [reorder]="editing" (ionItemReorder)="reorderData($event)" >\n                                                                        <ion-item *ngFor="let item of datarep " [color]="item.isChecked ? \'marron\' : \'secondary\'" >\n                                                                          <b>{{item.prop}}</b>\n                                                                        </ion-item>\n                                                                      </ion-item-group>\n                                                         </ion-list>\n                                                  </ion-col> \n                                                </ion-row>\n                                                \n                                        <ion-list padding>  \n                                            <ion-row responsive-sm  >\n                                                <ion-col col-2 > \n                                            \n                                                </ion-col> \n                                                <ion-col col-8 >  \n                                                </ion-col> \n                                                <ion-col col-2 > \n                                                <button ion-button  full (click)="next()" [disabled]="nbchecked < 5">\n                                                Suivant\n                                                </button>\n                                                </ion-col> \n                                            </ion-row>\n                                        </ion-list>\n                                      </div>\n </ion-col>\n <ion-col col-2 > \n    {{question_num}}\n          </ion-col> \n        </ion-row>\n\n        <div style="width:70%; text-align: center; margin: 0 auto;">\n            <div class="progress-outer" style="margin: 1px 1px;padding: 1px;text-align: center;background-color: #cfcfcf;border: 1px solid #dcdcdc;color: #fff;border-radius: 20px;">\n            <div class="progress-inner" [style.width]="progressPercent + \'%\'" style="width: 96%;height:20px;margin: 2px 2px;text-align: center;background-color: #84bdf1;border: 1px solid #dcdcdc;color: #fff;border-radius: 20px;">\n               {{progressPercent}}%\n            </div>\n            </div>\n            </div> \n     \n      </ion-grid>\n  </ion-content>\n  \n'/*ion-inline-end:"/Users/Maxime/Desktop/passation/src/pages/faciale/faciale.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["f" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_7__providers_AuthService__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_5_ng2_dragula__["b" /* DragulaService */], __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["f" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_2__providers_reddit_service__["a" /* RedditService */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["j" /* NavParams */]])
    ], FacialePage);
    return FacialePage;
}());

//# sourceMappingURL=faciale.js.map

/***/ }),

/***/ 369:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VerbalsynonymePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_add_operator_map__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_reddit_service__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng2_dragula__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__endtest_endtest__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_AuthService__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__login_login__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var VerbalsynonymePage = /** @class */ (function () {
    function VerbalsynonymePage(loadingController, authService, dragulaService, loadingCtrl, navCtrl, storage, redditService, navParams) {
        var _this = this;
        this.loadingController = loadingController;
        this.authService = authService;
        this.dragulaService = dragulaService;
        this.loadingCtrl = loadingCtrl;
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.redditService = redditService;
        this.navParams = navParams;
        this.editing = true;
        this.dataq = [];
        this.indextest = 0;
        this.indexdisplay = 1;
        this.posts2 = [];
        this.nbchecked = 0;
        this.checkedIdx = -1;
        this.options = [];
        this.reponse = [];
        this.reponsesave = [];
        this.myDataArray = [];
        this.datareponse = [];
        this.responsestring = "";
        this.progressPercent = 0;
        this.idtest = navParams.get('param1');
        this.testlenght = navParams.get('param2');
        this.testcategory = navParams.get('param4');
        this.timeInSeconds = navParams.get('param5');
        this.test_nav = navParams.get('param6');
        this.position = navParams.get('param7');
        this.storage.get('idresponse').then(function (idresponse) {
            _this.idresponse = idresponse;
        });
        this.storage.get("questions").then(function (users) {
            _this.dataq = JSON.parse(users);
            for (var i = 0; i < _this.dataq.length; i++) {
                _this.question_label = _this.dataq[i].question_label;
                _this.propositions = _this.dataq[i];
                _this.q_prop1 = _this.dataq[i].q_prop1;
                _this.q_prop2 = _this.dataq[i].q_prop2;
                _this.q_prop3 = _this.dataq[i].q_prop3;
                _this.q_prop4 = _this.dataq[i].q_prop4;
                _this.q_h = _this.dataq[i].q_h;
                _this.q_f = _this.dataq[i].q_f;
                _this.checkedIdx = _this.reponsesave[i];
                _this.question_num = _this.dataq[i].question_num;
                _this.options = [{ id: '2', prop: _this.q_prop2, order: 2, isChecked: false },
                    { id: '3', prop: _this.q_prop3, order: 3, isChecked: false }, { id: '1', prop: _this.q_prop1, order: 1, isChecked: false }, { id: '4', prop: _this.q_prop4, order: 4, isChecked: false }];
                _this.datareponse.push(_this.options);
                console.log(_this.datareponse);
            }
        });
    }
    VerbalsynonymePage.prototype.ionViewWillEnter = function () {
        var _this = this;
        if (this.timeInSeconds > 0) {
            this.startTimer();
        }
        this.indexdisplay = 1;
        //Données i=0
        this.storage.get("questions").then(function (users) {
            _this.dataq = JSON.parse(users);
            _this.nbquestionbytest = _this.dataq.length;
            _this.question_label = _this.dataq[0].question_label;
            _this.q_h = _this.dataq[0].q_h;
            _this.q_f = _this.dataq[0].q_f;
            _this.question_num = _this.dataq[0].question_num;
            _this.datarep = _this.datareponse[0];
        });
    };
    VerbalsynonymePage.prototype.next = function () {
        for (var i = 0; i < this.datareponse[this.indextest]; i++) {
            this.datareponse.push(this.datareponse[this.indextest]);
        }
        // new question
        this.indextest = this.indextest + 1;
        if (this.indextest < this.nbquestionbytest) {
            this.indexdisplay = this.indexdisplay + 1;
            this.retrieveQuestions();
        }
        else {
            this.pauseTimer();
            ////// SAVE DATA
            console.log(this.testlenght);
            for (var i = 0; i < this.testlenght; i++) {
                this.rep1 = this.datareponse[i][0].id;
                this.rep2 = this.datareponse[i][1].id;
                this.rep3 = this.datareponse[i][2].id;
                this.rep4 = this.datareponse[i][3].id;
                this.responsezero = this.rep1 + this.rep2 + this.rep3 + this.rep4;
                this.responsestring = this.responsestring + this.responsezero;
                console.log(this.responsestring);
            }
            /////
            var data = JSON.stringify({
                name: "VerbalSynonyme",
                idresponse: this.idresponse,
                resp: this.responsestring,
            });
            this.redditService.addresponses(data)
                .toPromise()
                .then(function (response) {
                console.log(response);
            });
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__endtest_endtest__["a" /* EndtestPage */], {
                param1: this.idtest,
                param2: this.position,
            });
        }
    };
    VerbalsynonymePage.prototype.prev = function () {
        if (this.indextest > 0) {
            this.indextest = this.indextest - 1;
            this.indexdisplay = this.indexdisplay - 1;
            this.reviewQuestions();
        }
        else {
        }
    };
    VerbalsynonymePage.prototype.retrieveQuestions = function () {
        var _this = this;
        this.storage.get("questions").then(function (users) {
            _this.dataq = JSON.parse(users);
            _this.question_label = _this.dataq[_this.indextest].question_label;
            _this.q_h = _this.dataq[_this.indextest].q_h;
            _this.q_f = _this.dataq[_this.indextest].q_f;
            _this.question_num = _this.dataq[_this.indextest].question_num;
            _this.datarep = _this.datareponse[_this.indextest];
            _this.nbchecked = 0;
            for (var i = 0; i < 4; i++) {
                if (_this.datarep[i].isChecked == true) {
                    _this.nbchecked = _this.nbchecked + 1;
                }
            }
        });
    };
    VerbalsynonymePage.prototype.reviewQuestions = function () {
        var _this = this;
        this.storage.get("questions").then(function (users) {
            _this.dataq = JSON.parse(users);
            _this.question_label = _this.dataq[_this.indextest].question_label;
            _this.q_h = _this.dataq[_this.indextest].q_h;
            _this.q_f = _this.dataq[_this.indextest].q_f;
            _this.question_num = _this.dataq[_this.indextest].question_num;
            _this.datarep = _this.datareponse[_this.indextest];
            // nb true reponse
            _this.nbchecked = 0;
            for (var i = 0; i < 4; i++) {
                if (_this.datarep[i].isChecked == true) {
                    _this.nbchecked = _this.nbchecked + 1;
                }
            }
        });
    };
    VerbalsynonymePage.mustBeTruthy = function (c) {
        var rv = {};
        if (!c.value) {
            rv['notChecked'] = true;
        }
        return rv;
    };
    VerbalsynonymePage.prototype.reorderData = function (indexes) {
        this.datarep = Object(__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["o" /* reorderArray */])(this.datarep, indexes);
        if (this.datarep[indexes.to].isChecked == false) {
            this.datarep[indexes.to].isChecked = true;
            this.nbchecked = this.nbchecked + 1;
        }
    };
    VerbalsynonymePage.prototype.logOut = function () {
        this.authService.logout();
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__login_login__["a" /* Login */]);
    };
    VerbalsynonymePage.prototype.ngOnInit = function () {
        this.initTimer();
    };
    VerbalsynonymePage.prototype.initTimer = function () {
        this.time = this.timeInSeconds;
        this.runTimer = false;
        this.hasStarted = false;
        this.hasFinished = false;
        this.remainingTime = this.timeInSeconds;
        this.displayTime = this.getSecondsAsDigitalClock(this.remainingTime);
        this.progressTimer = (this.remainingTime * 100) / this.time;
        this.progressPercent = (100 - this.progressTimer).toFixed(0);
    };
    VerbalsynonymePage.prototype.startTimer = function () {
        this.runTimer = true;
        this.hasStarted = true;
        this.timerTick();
    };
    VerbalsynonymePage.prototype.pauseTimer = function () {
        this.runTimer = false;
    };
    VerbalsynonymePage.prototype.resumeTimer = function () {
        this.startTimer();
    };
    VerbalsynonymePage.prototype.timerTick = function () {
        var _this = this;
        setTimeout(function () {
            if (!_this.runTimer) {
                return;
            }
            _this.remainingTime--;
            _this.displayTime = _this.getSecondsAsDigitalClock(_this.remainingTime);
            if (_this.remainingTime > 0) {
                _this.timerTick();
            }
            else {
                _this.hasFinished = true;
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__endtest_endtest__["a" /* EndtestPage */], {
                    param1: _this.idtest,
                    param2: _this.position,
                });
            }
        }, 1000);
    };
    VerbalsynonymePage.prototype.getSecondsAsDigitalClock = function (inputSeconds) {
        var sec_num = parseInt(inputSeconds.toString(), 10); // don't forget the second param
        var hours = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);
        var hoursString = '';
        var minutesString = '';
        var secondsString = '';
        hoursString = (hours < 10) ? "0" + hours : hours.toString();
        minutesString = (minutes < 10) ? "0" + minutes : minutes.toString();
        secondsString = (seconds < 10) ? "0" + seconds : seconds.toString();
        return hoursString + ':' + minutesString + ':' + secondsString;
    };
    VerbalsynonymePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Component"])({
            providers: [__WEBPACK_IMPORTED_MODULE_5_ng2_dragula__["b" /* DragulaService */]],
            selector: 'page-verbalsynonyme',template:/*ion-inline-start:"/Users/Maxime/Desktop/passation/src/pages/verbalsynonyme/verbalsynonyme.html"*/'<ion-header>\n  <ion-toolbar  color="secondary" hide-back-button >\n\n        <ion-buttons end>\n          <button ion-button icon-only (click)="logOut()">\n              <ion-icon ios="ios-log-out" md="md-log-out"></ion-icon>\n          </button>\n        </ion-buttons>\n  </ion-toolbar>  \n</ion-header>\n\n<ion-content color="light2" >\n      \n    <ion-grid >\n        <ion-row responsive-sm  >\n            <ion-col col-3 > \n            </ion-col> \n            <ion-col col-6 >  \n              \n                <div *ngIf="test_nav==1">\n                    <ion-list >     \n                      <ion-card>\n                        <ion-card-header>\n                         <b> {{question_label}}</b>\n                         {{indexdisplay}}/{{nbquestionbytest}}\n                        </ion-card-header>\n                        <ion-card-content>\n                            <p [innerHTML]="q_h"></p>  \n                        </ion-card-content>\n                      </ion-card>\n                    </ion-list>\n                       \n                    <ion-list padding>   \n                        <ion-item-group [reorder]="editing" (ionItemReorder)="reorderData($event)" >\n                                <ion-item *ngFor="let item of datarep " [color]="item.isChecked ? \'green\' : \'secondary\'"  >\n                                  <b>{{item.prop}}</b>\n                                </ion-item>\n                              </ion-item-group>\n                 </ion-list>\n                    <ion-list padding>  \n                        <ion-row responsive-sm  >\n                            <ion-col col-2 > \n                            <button ion-button full (click)="prev()">\n                            Précedent\n                            </button>\n                            </ion-col> \n                            <ion-col col-8 >  \n                            </ion-col> \n                            <ion-col col-2 > \n                            <button ion-button  full (click)="next()">\n                            Suivant\n                            </button>\n                            </ion-col> \n                        </ion-row>\n                    </ion-list>\n                  </div>\n                  <div *ngIf="test_nav==2">\n                      <ion-list >     \n                        <ion-card>\n                          <ion-card-header>\n                           <b> {{question_label}}</b>\n                           {{indexdisplay}}/{{nbquestionbytest}}\n                          </ion-card-header>\n                          <ion-card-content>\n                              <p [innerHTML]="q_h"></p>  \n                          </ion-card-content>\n                        </ion-card>\n                      </ion-list>\n                         \n                      <ion-list padding>   \n                          <ion-item-group [reorder]="editing" (ionItemReorder)="reorderData($event)" >\n                                  <ion-item *ngFor="let item of datarep " [color]="item.isChecked ? \'green\' : \'secondary\'" >\n                                    <b>{{item.prop}}</b>\n                                  </ion-item>\n                                </ion-item-group>\n                   </ion-list>\n                      <ion-list padding>  \n                          <ion-row responsive-sm  >\n                              <ion-col col-2 > \n                           \n                              </ion-col> \n                              <ion-col col-8 >  \n                              </ion-col> \n                              <ion-col col-2 > \n                              <button ion-button  full (click)="next()" >\n                              Suivant\n                              </button>\n                              </ion-col> \n                          </ion-row>\n                      </ion-list>\n                    </div>\n                    <div *ngIf="test_nav==3">\n                        <ion-list >     \n                          <ion-card>\n                            <ion-card-header>\n                             <b> {{question_label}}</b>\n                             {{indexdisplay}}/{{nbquestionbytest}}\n                            </ion-card-header>\n                            <ion-card-content>\n                                <p [innerHTML]="q_h"></p>  \n                            </ion-card-content>\n                          </ion-card>\n                        </ion-list>\n                           \n                        <ion-list padding>   \n                            <ion-item-group [reorder]="editing" (ionItemReorder)="reorderData($event)" >\n                                    <ion-item *ngFor="let item of datarep " [color]="item.isChecked ? \'green\' : \'secondary\'" >\n                                      <b>{{item.prop}}</b>\n                                    </ion-item>\n                                  </ion-item-group>\n                     </ion-list>\n                        <ion-list padding>  \n                            <ion-row responsive-sm  >\n                                <ion-col col-2 > \n                                <button ion-button full (click)="prev()"[disabled]="nbchecked < 4">\n                                Précedent\n                                </button>\n                                </ion-col> \n                                <ion-col col-8 >  \n                                </ion-col> \n                                <ion-col col-2 > \n                                <button ion-button  full (click)="next()" [disabled]="nbchecked < 4">\n                                Suivant\n                                </button>\n                                </ion-col> \n                            </ion-row>\n                        </ion-list>\n                      </div>\n                      <div *ngIf="test_nav==4">\n                          <ion-list >     \n                            <ion-card>\n                              <ion-card-header>\n                               <b> {{question_label}}</b>\n                               {{indexdisplay}}/{{nbquestionbytest}}\n                              </ion-card-header>\n                              <ion-card-content>\n                                  <p [innerHTML]="q_h"></p>  \n                              </ion-card-content>\n                            </ion-card>\n                          </ion-list>\n                             \n                          <ion-list padding>   \n                              <ion-item-group [reorder]="editing" (ionItemReorder)="reorderData($event)" >\n                                      <ion-item *ngFor="let item of datarep " [color]="item.isChecked ? \'green\' : \'secondary\'" >\n                                        <b>{{item.prop}}</b>\n                                      </ion-item>\n                                    </ion-item-group>\n                       </ion-list>\n                          <ion-list padding>  \n                              <ion-row responsive-sm  >\n                                  <ion-col col-2 > \n                              \n                                  </ion-col> \n                                  <ion-col col-8 >  \n                                  </ion-col> \n                                  <ion-col col-2 > \n                                  <button ion-button  full (click)="next()" [disabled]="nbchecked < 4">\n                                  Suivant\n                                  </button>\n                                  </ion-col> \n                              </ion-row>\n                          </ion-list>\n                        </div>\n </ion-col>\n <ion-col col-3 > \n    {{question_num}}\n          </ion-col> \n        </ion-row>\n\n        <div style="width:70%; text-align: center; margin: 0 auto;" >\n            <div class="progress-outer" style="margin: 1px 1px;padding: 1px;text-align: center;background-color: #cfcfcf;border: 1px solid #dcdcdc;color: #fff;border-radius: 20px;">\n            <div class="progress-inner" [style.width]="progressPercent + \'%\'" style="width: 96%;height:20px;margin: 2px 2px;text-align: center;background-color: #84bdf1;border: 1px solid #dcdcdc;color: #fff;border-radius: 20px;">\n               {{progressPercent}}%\n            </div>\n            </div>\n            </div> \n      </ion-grid>\n  </ion-content>\n\n  <ion-footer color="light2">\n    <ion-toolbar>\n     \n    </ion-toolbar>\n  </ion-footer>\n  \n'/*ion-inline-end:"/Users/Maxime/Desktop/passation/src/pages/verbalsynonyme/verbalsynonyme.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["f" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_7__providers_AuthService__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_5_ng2_dragula__["b" /* DragulaService */], __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["f" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_2__providers_reddit_service__["a" /* RedditService */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["j" /* NavParams */]])
    ], VerbalsynonymePage);
    return VerbalsynonymePage;
}());

//# sourceMappingURL=verbalsynonyme.js.map

/***/ }),

/***/ 372:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PassationsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_reddit_service__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_AuthService__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__login_login__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ngx_translate_core__ = __webpack_require__(53);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var PassationsPage = /** @class */ (function () {
    function PassationsPage(translate, navCtrl, authService, popoverCtrl, alertCtrl, menu, loadingController, redditService, storage, navParams, viewCtrl) {
        this.translate = translate;
        this.navCtrl = navCtrl;
        this.authService = authService;
        this.popoverCtrl = popoverCtrl;
        this.alertCtrl = alertCtrl;
        this.menu = menu;
        this.loadingController = loadingController;
        this.redditService = redditService;
        this.storage = storage;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.word = "";
        this.wordid = "";
        this.lenghttest = 0;
        this.urlupload = "http://api.selectionetconseils.ch/uploads/";
        this.lg = "fr";
        this.page = 1;
    }
    PassationsPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.page = 1;
        this.redditService.getAllPassations(this.page).subscribe(function (data) {
            console.log(data);
            _this.pass = data.listing;
        });
    };
    PassationsPage.prototype.doRefresh = function (refresher) {
        var _this = this;
        setTimeout(function () {
            _this.page = 1;
            _this.redditService.getAllPassations(_this.page).subscribe(function (data) {
                _this.posts = data.listing;
                console.log(_this.posts);
                _this.items = data.items;
                _this.pages = data.page;
                _this.currentpage = data.currentpage;
            });
            refresher.complete();
        }, 1100);
    };
    PassationsPage.prototype.gotest = function (event, item, passw) {
        this.code = item.passation_mdp;
        if (this.code == passw) {
            this.storage.set('numpassation', item.passation_num);
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__login_login__["a" /* Login */]);
        }
    };
    PassationsPage.prototype.doPrompt = function (event, item) {
        var _this = this;
        this.translate.get(['code', 'start', 'cancel']).subscribe(function (text) {
            var alert = _this.alertCtrl.create({
                title: text["code"],
                message: '',
                inputs: [
                    {
                        name: 'titre',
                        placeholder: 'CODE',
                        value: _this.passw
                    },
                ],
                buttons: [
                    {
                        text: text["cancel"],
                        handler: function (data) {
                        }
                    },
                    {
                        text: text["start"],
                        handler: function (data) {
                            _this.passw = data.titre;
                            _this.gotest(event, item, _this.passw);
                        }
                    }
                ]
            });
            alert.present();
        });
    };
    PassationsPage.prototype.changeLanguage = function () {
        this.translate.use(this.lg);
    };
    PassationsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-passations',template:/*ion-inline-start:"/Users/Maxime/Desktop/passation/src/pages/passations/passations.html"*/'<ion-header>\n    <ion-toolbar  color="secondary"  >\n        <ion-grid>\n            <ion-row>\n              <ion-col col-3>\n              <h3>      {{ \'passation\' | translate }}</h3>\n              </ion-col>\n              <ion-col col-6>\n              </ion-col>\n              <ion-col col-1>\n                  <p>      {{ \'chooselanguage\' | translate }}</p>\n                </ion-col>\n              <ion-col col-2>\n                  <ion-item>\n                  <ion-select (ionChange)="changeLanguage()" [(ngModel)]="lg" selected="fr" >\n                      <ion-option  value="fr" >Français</ion-option>\n                      <ion-option  value="de" >Deutsch</ion-option>\n                      <ion-option  value="en" >English</ion-option>\n                  </ion-select>\n                </ion-item>\n              </ion-col>\n            </ion-row>\n          </ion-grid>\n        </ion-toolbar>\n </ion-header>\n  <ion-content>\n\n      <ion-row responsive-sm  >\n          <ion-col col-6>  \n            <ion-card>\n            {{ \'bienvenue\' | translate }}\n\n            </ion-card>\n\n          </ion-col>\n          <ion-col col-6 > \n            \n              <ion-card>\n                \n              {{ \'descbienvenue\' | translate }}\n                  \n              </ion-card>\n          </ion-col>\n      </ion-row>\n    <ion-grid  >\n\n    \n      <ion-row responsive-sm  >\n<ion-col col-12 col-2>  </ion-col>\n<ion-col col-12 col-8>  <ion-refresher (ionRefresh)="doRefresh($event)">\n  <ion-refresher-content></ion-refresher-content>\n</ion-refresher>\n\n<ion-list>\n    <ion-item *ngFor="let item of pass">\n        <ion-thumbnail item-start>\n            <img  [src]="item.image" style="width:100px;height:100px"/>\n            </ion-thumbnail>   \n            <b>{{item.title}} </b> \n            <h5>{{item.subtitle}}</h5>\n       \n            <div class="buttons" item-right>\n                <button ion-button  (click)="doPrompt($event, item)"> {{ \'code\' | translate }} </button>\n              </div>\n              </ion-item>\n      </ion-list>\n \n\n   \n       \n</ion-col>\n<ion-col col-12 col-2>  </ion-col>\n</ion-row>\n</ion-grid>\n\n  </ion-content>\n  \n  '/*ion-inline-end:"/Users/Maxime/Desktop/passation/src/pages/passations/passations.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6__ngx_translate_core__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_4__providers_AuthService__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* PopoverController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* MenuController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_2__providers_reddit_service__["a" /* RedditService */], __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */]])
    ], PassationsPage);
    return PassationsPage;
}());

//# sourceMappingURL=passations.js.map

/***/ }),

/***/ 374:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OrdertestPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_add_operator_map__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_reddit_service__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_dragula__ = __webpack_require__(26);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var OrdertestPage = /** @class */ (function () {
    function OrdertestPage(navCtrl, navParams, loadingController, dragulaService, alertCtrl, redditService) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.loadingController = loadingController;
        this.dragulaService = dragulaService;
        this.alertCtrl = alertCtrl;
        this.redditService = redditService;
        this.items = [];
        this.posts2 = [];
        this.editing = true;
        this.word = "";
        this.wordid = "";
        this.parameter1 = navParams.get('param1');
        this.category = navParams.get('param2');
        this.page = 1;
        this.redditService.getTest(this.parameter1).subscribe(function (data) {
            _this.data = data.items;
        });
        this.getFk();
    }
    OrdertestPage_1 = OrdertestPage;
    OrdertestPage.prototype.ngOnInit = function () { };
    OrdertestPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.redditService.Questioncategory(this.page, this.category).subscribe(function (data) {
            _this.posts = data.listing;
            _this.items = data.items;
            _this.pages = data.page;
            _this.currentpage = data.currentpage;
        });
    };
    OrdertestPage.prototype.reorderData = function (indexes) {
        this.posts2 = Object(__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["o" /* reorderArray */])(this.posts2, indexes);
        console.log(this.posts2);
    };
    OrdertestPage.prototype.save = function () {
        var _this = this;
        var loading = this.loadingController.create({ content: "Enregistrement..." });
        loading.present();
        var data2 = JSON.stringify({
            idfktest: this.parameter1,
        });
        this.redditService.deletefkidtest(data2)
            .toPromise()
            .then(function (response) {
            if (response.status == 'success') {
                for (var i = 0; i < _this.posts2.length; i++) {
                    _this.qnum = _this.posts2[i].question_num;
                    _this.addfk(_this.qnum, i);
                }
            }
            setTimeout(function () {
                loading.dismissAll();
                _this.getFk();
            }, 3000);
        });
    };
    OrdertestPage.prototype.addfk = function (qnum, orderid) {
        var data = JSON.stringify({
            idquestion: qnum,
            idtest: this.parameter1,
            idorder: orderid,
        });
        this.redditService.addfktest(data)
            .toPromise()
            .then(function (response) {
            if (response.status == 'success') { }
        });
    };
    ;
    OrdertestPage.prototype.add = function (event, item) {
        var _this = this;
        var data = JSON.stringify({
            idquestion: item.question_num,
            idtest: this.parameter1,
            idorder: this.idorder,
        });
        this.redditService.addfktest(data)
            .toPromise()
            .then(function (response) {
            if (response.status == 'success') {
                _this.getFk();
            }
        });
    };
    ;
    OrdertestPage.prototype.delete = function (event, item) {
        var _this = this;
        var data = JSON.stringify({
            idfktest: item.idfktest,
        });
        this.redditService.deletefktest(data)
            .toPromise()
            .then(function (response) {
            if (response.status == 'success') {
                _this.getFk();
            }
        });
    };
    ;
    OrdertestPage.prototype.getFk = function () {
        var _this = this;
        this.redditService.getFktest(this.parameter1).subscribe(function (data) {
            _this.posts2 = data.listing;
        });
    };
    OrdertestPage.prototype.onInput = function (selectedValue) {
        var _this = this;
        this.page = 1;
        this.redditService.seachQuestioncategoryword(this.page, this.word, this.category).subscribe(function (data) {
            _this.posts = data.listing;
            _this.items = data.items;
            _this.pages = data.page;
            _this.currentpage = data.currentpage;
        });
    };
    OrdertestPage.prototype.onCancel = function (selectedValue) {
        this.wordid == "";
        this.word == "";
    };
    OrdertestPage.prototype.onInputid = function (selectedValue) {
        var _this = this;
        this.page = 1;
        this.redditService.seachQuestioncategoryid(this.page, this.wordid, this.category).subscribe(function (data) {
            _this.posts = data.listing;
            _this.items = data.items;
            _this.pages = data.page;
            _this.currentpage = data.currentpage;
        });
    };
    OrdertestPage.prototype.onCancelid = function (selectedValue) {
        this.wordid == "";
        this.word == "";
    };
    OrdertestPage.prototype.reset = function () {
        this.wordid == "";
        this.word == "";
        this.navCtrl.push(OrdertestPage_1, {
            param1: this.parameter1,
            param2: this.category,
        });
    };
    OrdertestPage.prototype.next = function () {
        var _this = this;
        if (this.page < this.pages) {
            this.page = this.page + 1;
            if (this.word == "" && this.wordid == "") {
                this.redditService.Questioncategory(this.page, this.category).subscribe(function (data) {
                    _this.posts = data.listing;
                    _this.items = data.items;
                    _this.pages = data.page;
                    _this.currentpage = data.currentpage;
                });
            }
            else if (this.word !== "") {
                this.redditService.seachQuestioncategoryword(this.page, this.word, this.category).subscribe(function (data) {
                    _this.posts = data.listing;
                    _this.items = data.items;
                    _this.pages = data.page;
                    _this.currentpage = data.currentpage;
                });
            }
            else if (this.wordid !== "") {
                this.redditService.seachQuestioncategoryid(this.page, this.wordid, this.category).subscribe(function (data) {
                    _this.posts = data.listing;
                    _this.items = data.items;
                    _this.pages = data.page;
                    _this.currentpage = data.currentpage;
                });
            }
        }
    };
    OrdertestPage.prototype.prev = function () {
        var _this = this;
        if (this.page > 1) {
            this.page = this.page - 1;
            if (this.word == "" && this.wordid == "") {
                this.redditService.Questioncategory(this.page, this.category).subscribe(function (data) {
                    _this.posts = data.listing;
                    _this.items = data.items;
                    _this.pages = data.page;
                    _this.currentpage = data.currentpage;
                });
            }
            else if (this.word !== "") {
                this.redditService.seachQuestioncategoryword(this.page, this.word, this.category).subscribe(function (data) {
                    _this.posts = data.listing;
                    _this.items = data.items;
                    _this.pages = data.page;
                    _this.currentpage = data.currentpage;
                });
            }
            else if (this.wordid !== "") {
                this.redditService.seachQuestioncategoryid(this.page, this.wordid, this.category).subscribe(function (data) {
                    _this.posts = data.listing;
                    _this.items = data.items;
                    _this.pages = data.page;
                    _this.currentpage = data.currentpage;
                });
            }
        }
    };
    OrdertestPage = OrdertestPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Component"])({
            providers: [__WEBPACK_IMPORTED_MODULE_4_ng2_dragula__["b" /* DragulaService */]],
            selector: 'page-ordertest',template:/*ion-inline-start:"/Users/Maxime/Desktop/passation/src/pages/ordertest/ordertest.html"*/'\n<ion-header>\n    <ion-toolbar  color="secondary" >\n    <button ion-button menuToggle>\n    <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title> Test (Ordre question)</ion-title>\n                  <ion-buttons end>             \n                  </ion-buttons>\n            </ion-toolbar>\n            </ion-header>\n\n<ion-content color="light2" >\n    <ion-grid>\n                 <div class="wrap">\n                    <div class="left box">\n\n                        \n\n                        <ion-row responsive-sm >\n                                    <ion-item *ngFor="let item of data"  class="back"> \n                                            <ion-icon ios="ios-contact" md="md-contact"></ion-icon>      \n                                            <ion-label><b> <ion-icon ios="ios-document" md="md-document"></ion-icon> {{item.test_label}} </b> /  Numéro : <b>{{item.test_num}} </b> /   Durée  <ion-icon ios="ios-alarm" md="md-alarm"></ion-icon>:    <b>{{item.test_duree}} minutes</b></ion-label>  \n                                          </ion-item>\n                                          <ion-item *ngFor="let item of data"  class="back"> \n                                                <ion-icon ios="ios-contact" md="md-contact"></ion-icon>      \n                                                <ion-label > {{item.test_titre}}</ion-label>  \n                                          </ion-item>\n                         </ion-row> \n                \n                         <ion-list >   \n                                <ion-item-group [reorder]="editing" (ionItemReorder)="reorderData($event)">\n                                        <ion-item *ngFor="let item of posts2">\n                                          <h2> {{item.question_num}}</h2>\n                                          <b>{{item.question_label}}</b>\n                                          <button ion-button item-end small color="light2" (click)="delete($event, item)"> <ion-icon ios="ios-trash" md="md-trash"></ion-icon></button>\n                                        </ion-item>\n                                      </ion-item-group>\n                         </ion-list>\n                 \n                    </div>\n\n                    <div class="boxsave">\n                    <button ion-button block  color="red" (click)="save()">Enregistrer </button>\n                    </div>\n                    <div class="right box">\n\n                            <ion-row responsive-sm class="backcolor" >\n  \n                                    <ion-col col-12 col-md-12>\n                                      <ion-searchbar\n                                      type=number\n                                      [(ngModel)]="wordid"\n                                      [showCancelButton]="shouldShowCancel"\n                                      (ionInput)="onInputid($event)"\n                                      (ionCancel)="onCancel($event)"\n                                      placeholder="Id num" >\n                                     </ion-searchbar>\n                                     </ion-col>\n                                    \n                                      <ion-col col-12 col-md-12>\n                                          <ion-searchbar\n                                          [(ngModel)]="word"\n                                          [showCancelButton]="shouldShowCancel"\n                                          (ionInput)="onInput($event)"\n                                          (ionCancel)="onCancel($event)"\n                                          placeholder="Titre">\n                                        </ion-searchbar>\n                                      </ion-col>\n                              \n                                    </ion-row>\n\n                              \n\n                                    <ion-row responsive-sm class="backcolor" >\n  \n                                        \n                                            <ion-col col-12 col-md-10 color="secondary">\n                                                <ion-item color="secondary">\n                                                    Nombre de résultats : <b>{{items}}</b>\n                                                    Page : <b>{{currentpage}} / {{pages}}</b>\n                                                </ion-item>\n                                            </ion-col>\n                                            <ion-col col-12 col-md-2>\n                                              <button ion-button color="light" (click)="reset()" > <ion-icon ios="ios-refresh" md="md-refresh"></ion-icon></button>\n                                            </ion-col>\n                                            </ion-row>\n\n                      \n                            <ion-list>\n                     <ion-item *ngFor="let item of posts"><b> {{item.question_num}} {{item.question_label}} </b>\n                    \n                        <button ion-button item-end small color="red" (click)="add($event, item)"> <ion-icon ios="ios-add" md="md-add"></ion-icon></button>\n                    \n                    </ion-item>\n\n                  \n                    <button ion-button small color="secondary" (click)="prev()"> Precédent</button>\n                    <button ion-button small color="secondary" (click)="next()"> Suivant</button>\n                   \n             </ion-list>\n                     \n                    </div>\n                </div>\n        <ion-row>\n          <ion-col col-12 col-sm> \n          </ion-col> \n        </ion-row>\n      </ion-grid>\n  </ion-content>\n  \n'/*ion-inline-end:"/Users/Maxime/Desktop/passation/src/pages/ordertest/ordertest.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["f" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_4_ng2_dragula__["b" /* DragulaService */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_2__providers_reddit_service__["a" /* RedditService */]])
    ], OrdertestPage);
    return OrdertestPage;
    var OrdertestPage_1;
}());

/*
this.posts2.forEach((data) => {
console.log(data.question_num);
this.qnum=data.question_num;
console.log(data.length);
    this.addfk(this.qnum, 4);
});*/ 
//# sourceMappingURL=ordertest.js.map

/***/ }),

/***/ 375:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(376);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(380);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 380:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* unused harmony export HttpLoaderFactory */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(417);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(164);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_diagnostic__ = __webpack_require__(370);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_status_bar__ = __webpack_require__(710);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_splash_screen__ = __webpack_require__(711);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_login_login__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_forgotpassword_forgotpassword__ = __webpack_require__(264);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_settings_settings__ = __webpack_require__(712);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__angular_http__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_storage__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__providers_reddit_service__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__ionic_native_geolocation__ = __webpack_require__(373);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__angular_forms__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_edit_edit__ = __webpack_require__(713);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__angular_common_http__ = __webpack_require__(714);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__ionic_native_file_transfer__ = __webpack_require__(715);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__ionic_native_file_path__ = __webpack_require__(716);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__ionic_native_file__ = __webpack_require__(717);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__ionic_native_transfer__ = __webpack_require__(718);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__providers_AuthService__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23_ngx_wig__ = __webpack_require__(719);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23_ngx_wig___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_23_ngx_wig__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_tests_tests__ = __webpack_require__(720);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__pages_questions_questions__ = __webpack_require__(721);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__pages_passations_passations__ = __webpack_require__(372);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__pages_ordertest_ordertest__ = __webpack_require__(374);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28_ng2_dragula__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__pages_orderpassation_orderpassation__ = __webpack_require__(722);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__pages_starttest_starttest__ = __webpack_require__(147);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__pages_valeurs_valeurs__ = __webpack_require__(267);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__pages_endtest_endtest__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__pages_sessions_sessions__ = __webpack_require__(266);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__pages_questionnaire_questionnaire__ = __webpack_require__(359);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__pages_scientifique_scientifique__ = __webpack_require__(360);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__pages_logique_logique__ = __webpack_require__(165);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__pages_verbaldefinition_verbaldefinition__ = __webpack_require__(361);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__pages_verbalelimination_verbalelimination__ = __webpack_require__(362);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__pages_memoire_memoire__ = __webpack_require__(363);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__pages_espace_espace__ = __webpack_require__(364);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__pages_stress_stress__ = __webpack_require__(365);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__pages_imagination_imagination__ = __webpack_require__(366);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__pages_jugement_jugement__ = __webpack_require__(367);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_44__pages_faciale_faciale__ = __webpack_require__(368);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_45__pages_verbalsynonyme_verbalsynonyme__ = __webpack_require__(369);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_46__ngx_translate_core__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_47__ngx_translate_http_loader__ = __webpack_require__(723);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
















































var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_login_login__["a" /* Login */],
                __WEBPACK_IMPORTED_MODULE_9__pages_forgotpassword_forgotpassword__["a" /* Forgotpassword */],
                __WEBPACK_IMPORTED_MODULE_10__pages_settings_settings__["a" /* SettingsPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_edit_edit__["a" /* Edit */],
                __WEBPACK_IMPORTED_MODULE_33__pages_sessions_sessions__["a" /* SessionsPage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_tests_tests__["a" /* TestsPage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_questions_questions__["a" /* QuestionsPage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_passations_passations__["a" /* PassationsPage */],
                __WEBPACK_IMPORTED_MODULE_30__pages_starttest_starttest__["a" /* StarttestPage */],
                __WEBPACK_IMPORTED_MODULE_32__pages_endtest_endtest__["a" /* EndtestPage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_ordertest_ordertest__["a" /* OrdertestPage */],
                __WEBPACK_IMPORTED_MODULE_29__pages_orderpassation_orderpassation__["a" /* OrderpassationPage */],
                __WEBPACK_IMPORTED_MODULE_31__pages_valeurs_valeurs__["a" /* ValeursPage */],
                __WEBPACK_IMPORTED_MODULE_34__pages_questionnaire_questionnaire__["a" /* QuestionnairePage */],
                __WEBPACK_IMPORTED_MODULE_35__pages_scientifique_scientifique__["a" /* ScientifiquePage */],
                __WEBPACK_IMPORTED_MODULE_36__pages_logique_logique__["a" /* LogiquePage */],
                __WEBPACK_IMPORTED_MODULE_37__pages_verbaldefinition_verbaldefinition__["a" /* VerbaldefinitionPage */],
                __WEBPACK_IMPORTED_MODULE_38__pages_verbalelimination_verbalelimination__["a" /* VerbaleliminationPage */],
                __WEBPACK_IMPORTED_MODULE_39__pages_memoire_memoire__["a" /* MemoirePage */],
                __WEBPACK_IMPORTED_MODULE_40__pages_espace_espace__["a" /* EspacePage */],
                __WEBPACK_IMPORTED_MODULE_41__pages_stress_stress__["a" /* StressPage */],
                __WEBPACK_IMPORTED_MODULE_42__pages_imagination_imagination__["a" /* ImaginationPage */],
                __WEBPACK_IMPORTED_MODULE_43__pages_jugement_jugement__["a" /* JugementPage */],
                __WEBPACK_IMPORTED_MODULE_44__pages_faciale_faciale__["a" /* FacialePage */],
                __WEBPACK_IMPORTED_MODULE_45__pages_verbalsynonyme_verbalsynonyme__["a" /* VerbalsynonymePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_17__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], {}, {
                    links: []
                }),
                __WEBPACK_IMPORTED_MODULE_11__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_12__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_23_ngx_wig__["NgxWigModule"],
                __WEBPACK_IMPORTED_MODULE_28_ng2_dragula__["a" /* DragulaModule */],
                __WEBPACK_IMPORTED_MODULE_46__ngx_translate_core__["b" /* TranslateModule */].forRoot({
                    loader: {
                        provide: __WEBPACK_IMPORTED_MODULE_46__ngx_translate_core__["a" /* TranslateLoader */],
                        useFactory: (HttpLoaderFactory),
                        deps: [__WEBPACK_IMPORTED_MODULE_17__angular_common_http__["a" /* HttpClient */]]
                    }
                }),
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_login_login__["a" /* Login */],
                __WEBPACK_IMPORTED_MODULE_33__pages_sessions_sessions__["a" /* SessionsPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_forgotpassword_forgotpassword__["a" /* Forgotpassword */],
                __WEBPACK_IMPORTED_MODULE_10__pages_settings_settings__["a" /* SettingsPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_edit_edit__["a" /* Edit */],
                __WEBPACK_IMPORTED_MODULE_24__pages_tests_tests__["a" /* TestsPage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_questions_questions__["a" /* QuestionsPage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_passations_passations__["a" /* PassationsPage */],
                __WEBPACK_IMPORTED_MODULE_30__pages_starttest_starttest__["a" /* StarttestPage */],
                __WEBPACK_IMPORTED_MODULE_32__pages_endtest_endtest__["a" /* EndtestPage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_ordertest_ordertest__["a" /* OrdertestPage */],
                __WEBPACK_IMPORTED_MODULE_29__pages_orderpassation_orderpassation__["a" /* OrderpassationPage */],
                __WEBPACK_IMPORTED_MODULE_31__pages_valeurs_valeurs__["a" /* ValeursPage */],
                __WEBPACK_IMPORTED_MODULE_34__pages_questionnaire_questionnaire__["a" /* QuestionnairePage */],
                __WEBPACK_IMPORTED_MODULE_35__pages_scientifique_scientifique__["a" /* ScientifiquePage */],
                __WEBPACK_IMPORTED_MODULE_36__pages_logique_logique__["a" /* LogiquePage */],
                __WEBPACK_IMPORTED_MODULE_37__pages_verbaldefinition_verbaldefinition__["a" /* VerbaldefinitionPage */],
                __WEBPACK_IMPORTED_MODULE_38__pages_verbalelimination_verbalelimination__["a" /* VerbaleliminationPage */],
                __WEBPACK_IMPORTED_MODULE_39__pages_memoire_memoire__["a" /* MemoirePage */],
                __WEBPACK_IMPORTED_MODULE_40__pages_espace_espace__["a" /* EspacePage */],
                __WEBPACK_IMPORTED_MODULE_41__pages_stress_stress__["a" /* StressPage */],
                __WEBPACK_IMPORTED_MODULE_42__pages_imagination_imagination__["a" /* ImaginationPage */],
                __WEBPACK_IMPORTED_MODULE_43__pages_jugement_jugement__["a" /* JugementPage */],
                __WEBPACK_IMPORTED_MODULE_44__pages_faciale_faciale__["a" /* FacialePage */],
                __WEBPACK_IMPORTED_MODULE_45__pages_verbalsynonyme_verbalsynonyme__["a" /* VerbalsynonymePage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_20__ionic_native_file__["a" /* File */],
                __WEBPACK_IMPORTED_MODULE_21__ionic_native_transfer__["a" /* Transfer */],
                __WEBPACK_IMPORTED_MODULE_19__ionic_native_file_path__["a" /* FilePath */],
                __WEBPACK_IMPORTED_MODULE_18__ionic_native_file_transfer__["a" /* FileTransfer */],
                __WEBPACK_IMPORTED_MODULE_14__ionic_native_geolocation__["a" /* Geolocation */],
                __WEBPACK_IMPORTED_MODULE_6__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_15__angular_forms__["FormsModule"],
                __WEBPACK_IMPORTED_MODULE_7__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_13__providers_reddit_service__["a" /* RedditService */],
                __WEBPACK_IMPORTED_MODULE_22__providers_AuthService__["a" /* AuthService */],
                __WEBPACK_IMPORTED_MODULE_5__ionic_native_diagnostic__["a" /* Diagnostic */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ErrorHandler"], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicErrorHandler */] },
            ]
        })
    ], AppModule);
    return AppModule;
}());

function HttpLoaderFactory(http) {
    return new __WEBPACK_IMPORTED_MODULE_47__ngx_translate_http_loader__["a" /* TranslateHttpLoader */](http, './assets/i18n/', '.json');
}
//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 417:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_login_login__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_diagnostic__ = __webpack_require__(370);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_home_home__ = __webpack_require__(164);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_AuthService__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_passations_passations__ = __webpack_require__(372);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ngx_translate_core__ = __webpack_require__(53);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var MyApp = /** @class */ (function () {
    function MyApp(translate, loadingController, authService, diagnostic, menu, storage, platform) {
        this.translate = translate;
        this.loadingController = loadingController;
        this.authService = authService;
        this.diagnostic = diagnostic;
        this.menu = menu;
        this.storage = storage;
        this.platform = platform;
        translate.addLangs(['en', 'gr', 'fr']);
        if (this.authService.authenticated()) {
            this.rootPage = __WEBPACK_IMPORTED_MODULE_7__pages_passations_passations__["a" /* PassationsPage */];
        }
        else {
            this.rootPage = __WEBPACK_IMPORTED_MODULE_7__pages_passations_passations__["a" /* PassationsPage */];
        }
        this.pages2 = [
            { title: 'Déconnexion', component: __WEBPACK_IMPORTED_MODULE_5__pages_home_home__["a" /* HomePage */], icon: 'log-out', logsOut: true }
        ];
        this.menu.enable(false);
        this.initializeApp();
    }
    MyApp.prototype.initializeApp = function () {
        this.platform.ready().then(function () {
        });
        this.initTranslate();
    };
    MyApp.prototype.initTranslate = function () {
        console.log("translate ini");
        // Set the default language for translation strings, and the current language.
        this.translate.setDefaultLang('fr');
        if (this.translate.getBrowserLang() !== undefined) {
            this.translate.use(this.translate.getBrowserLang());
        }
        else {
            this.translate.use('fr'); // Set your language here
        }
    };
    MyApp.prototype.dologout = function () {
        this.authService.login();
        this.menu.enable(false);
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_3__pages_login_login__["a" /* Login */]);
    };
    MyApp.prototype.isActive = function (page) {
        if (this.nav.getActive() && this.nav.getActive().component === page.component) {
            return 'light';
        }
        return 'secondary';
    };
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"/Users/Maxime/Desktop/passation/src/app/app.html"*/'\n\n<ion-menu [content]="content" >\n  <ion-header color="secondary">\n    <ion-toolbar color="secondary">\n      <ion-title>Menu</ion-title>\n    </ion-toolbar>\n  </ion-header>\n\n  <ion-content >\n    <ion-list>\n        <ion-item menuClose *ngFor="let p of pages"  [color]="isActive(p)" (click)="openPage(p)">\n            <ion-icon item-left small [name]="p.icon" width="10px"></ion-icon>\n       {{p.title}}\n        </ion-item>\n      </ion-list>\n        <ion-list  >\n        <button ion-item menuClose *ngFor="let p of pages2"  [color]="isActive(p)" (click)="dologout()">\n            <ion-icon item-left small [name]="p.icon"></ion-icon>\n          {{p.title}} \n        </button>\n   \n    </ion-list>\n\n\n  \n  </ion-content>\n\n</ion-menu>\n\n <!-- main navigation -->\n <ion-nav [root]="rootPage" #content swipeBackEnabled="false" main></ion-nav>\n'/*ion-inline-end:"/Users/Maxime/Desktop/passation/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_8__ngx_translate_core__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_6__providers_AuthService__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_diagnostic__["a" /* Diagnostic */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* MenuController */], __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 712:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SettingsPage = /** @class */ (function () {
    function SettingsPage(navCtrl, navParams, storage) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
    }
    SettingsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SettingsPage');
    };
    SettingsPage.prototype.delete = function () {
        this.storage.clear();
        alert("Deconnexion ");
    };
    SettingsPage.prototype.toggleNotifications = function () {
        if (this.enableNotifications) {
            this.toastCtrl.create('Notifications enabled.');
        }
        else {
            this.toastCtrl.create('Notifications disabled.');
        }
    };
    SettingsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-settings',template:/*ion-inline-start:"/Users/Maxime/Desktop/passation/src/pages/settings/settings.html"*/'<ion-header>\n    <ion-navbar color="secondary">\n      <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n  \n  \n      <ion-title>Réglages</ion-title>\n  \n    </ion-navbar>\n  \n  </ion-header>\n\n\n\n     \n  <ion-content>\n   \n      <ion-list no-border>\n        <ion-list-header>\n          General\n        </ion-list-header>\n \n        <ion-item>\n          <ion-toggle [(ngModel)]="enableNotifications" (click)="toggleNotifications()"></ion-toggle>\n          <ion-label class="label"> Activé notifications </ion-label>\n          <ion-icon name=\'notifications\' item-start></ion-icon>\n        </ion-item>\n      </ion-list>\n\n    </ion-content>'/*ion-inline-end:"/Users/Maxime/Desktop/passation/src/pages/settings/settings.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */]])
    ], SettingsPage);
    return SettingsPage;
}());

//# sourceMappingURL=settings.js.map

/***/ }),

/***/ 713:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Edit; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__ = __webpack_require__(373);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_reddit_service__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ts_md5__ = __webpack_require__(265);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ts_md5___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_ts_md5__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var Edit = /** @class */ (function () {
    function Edit(loadingController, actionSheetCtrl, toastCtrl, platform, loadingCtrl, navCtrl, storage, redditService, navParams, http, Geolocation, zone) {
        this.loadingController = loadingController;
        this.actionSheetCtrl = actionSheetCtrl;
        this.toastCtrl = toastCtrl;
        this.platform = platform;
        this.loadingCtrl = loadingCtrl;
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.redditService = redditService;
        this.navParams = navParams;
        this.http = http;
        this.Geolocation = Geolocation;
        this.zone = zone;
        this.parameter1 = navParams.get('param1');
        console.log(this.parameter1);
    }
    Edit_1 = Edit;
    Edit.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.redditService.getDetail(this.parameter1).subscribe(function (data) {
            _this.data = data.items;
            _this.birthday = data.items[0].birthday;
            _this.date = data.items[0].date;
            _this.genre = data.items[0].genre;
            _this.diplome = data.items[0].diplome;
            _this.time = data.items[0].time;
            _this.certificat = data.items[0].certificat;
        });
    };
    Edit.prototype.doSave = function () {
        var data = JSON.stringify({
            userid: this.parameter1,
            lastname: this.lastname,
            firstname: this.firstname,
            email: this.email,
            address: this.address,
            city: this.city,
            cp: this.cp,
            genre: this.genre,
            diplome: this.diplome,
            time: this.time,
            certificat: this.certificat,
            birthday: this.birthday,
        });
        this.redditService.updateuser(data)
            .toPromise()
            .then(function (response) {
            console.log(response);
        });
    };
    Edit.prototype.doSavepassword = function () {
        var _this = this;
        this.password2 = __WEBPACK_IMPORTED_MODULE_7_ts_md5__["Md5"].hashStr(this.password);
        var data = JSON.stringify({
            userid: this.parameter1,
            password: this.password2,
        });
        this.redditService.updateuserpassword(data)
            .toPromise()
            .then(function (response) {
            console.log(response);
        });
        setTimeout(function () {
            _this.navCtrl.push(Edit_1, {
                param1: _this.parameter1,
            });
        }, 500);
    };
    Edit = Edit_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["Component"])({
            selector: 'page-edit',template:/*ion-inline-start:"/Users/Maxime/Desktop/passation/src/pages/edit/edit.html"*/'\n<ion-header>\n<ion-toolbar  color="secondary" hide-back-button >\n<button ion-button menuToggle>\n<ion-icon name="menu"></ion-icon>\n</button>\n<ion-title> Modifier utilisateur</ion-title>\n              <ion-buttons end> \n        \n                 \n                <button ion-button icon-only (click)="doSave()">\n                        <ion-icon ios="ios-cloud-upload" md="md-cloud-upload"></ion-icon>Enregistrer\n              </button>\n               \n        \n              </ion-buttons>\n        </ion-toolbar>\n        </ion-header>\n  <ion-content cache-view="false" color="light2" >\n      \n  <ion-grid *ngFor="let item of data">\n      <ion-row responsive-sm  >\n     \n<ion-col col-12 col-sm>  \n\n              \n    <button ion-button padding full color="light"> Profil</button>\n \n\n        \n                 \n                        <ion-item>\n                       Date de création : {{item.date_added }}\n                        </ion-item>\n                            <ion-item>\n                                <ion-label color="primary" stacked>Prénom</ion-label>\n                                <ion-input type="text" placeholder="Prénom" value="{{item.firstname}}"[(ngModel)]="firstname">{{item.firstname}}</ion-input>\n                              </ion-item>\n                            <ion-item>\n                            <ion-label color="primary" stacked> Nom </ion-label>\n                          <ion-input type="text" placeholder="Nom"  value="{{item.lastname}}"[(ngModel)]="lastname">{{item.lastname}}</ion-input>\n                        </ion-item>\n            \n                           <ion-item>\n                              <ion-label color="primary" stacked> Email  </ion-label>\n                             <ion-input type="email" placeholder="Email" value="{{item.email}}"[(ngModel)]="email">{{item.email}</ion-input>\n                           </ion-item>\n                      \n                           <ion-item>\n                              <ion-label color="primary" stacked> Ville </ion-label>\n                             <ion-input type="text" placeholder="Ville" value="{{item.city}}"[(ngModel)]="city">{{item.city}}</ion-input>\n                           </ion-item>\n\n                         \n  <ion-item>\n      <ion-label color="primary" stacked>Date anniversaire </ion-label>\n      <ion-input type="date" placeholder="DD/MM/YYYY" [(ngModel)]="birthday" ></ion-input>\n      </ion-item>\n\n\n<ion-item>\n<ion-label  color="primary" stacked>Genre</ion-label>\n<ion-select [(ngModel)]="genre">\n<ion-option value="f">Femme</ion-option>\n<ion-option value="m">Homme</ion-option>\n</ion-select>\n</ion-item>\n\n  <ion-item>\n      <ion-label  color="primary" stacked>Dîplome</ion-label>\n      <ion-select [(ngModel)]="diplome" >\n        <ion-option value="matuspe">Matu spé</ion-option>\n        <ion-option value="matupro">Matu Pro</ion-option>\n        <ion-option value="matugym">Matu gym</ion-option>\n        <ion-option value="autres">Autres</ion-option>\n        <ion-option value="titresetrangers">Titres étrangers</ion-option>\n      </ion-select>\n    </ion-item>\n\n    <ion-item>\n        <ion-label color="primary" stacked>Temps supplémentaire </ion-label>\n      <ion-input type="text" placeholder="Nom"  value="{{item.time}}"[(ngModel)]="time">{{item.time}}</ion-input>\n    </ion-item>\n\n                      \n    <button ion-button  full ion-button color="red"  (click)="doSave()">Enregistrer</button>\n                              \n             \n\n      </ion-col>\n\n        <ion-col col-12 col-sm> \n         \n     \n     <button ion-button padding full color="light"> Changement de mot de passe </button>\n     <ion-item>\n         <ion-label color="primary" stacked> Nouveau mot de passe </ion-label>\n         <ion-input type="password" placeholder="Mot de passe" [(ngModel)]="password" ></ion-input>\n     </ion-item>\n\n     <button ion-button  full ion-button color="red"  (click)="doSavepassword()">Enregistrer</button>\n        </ion-col> \n\n\n\n      </ion-row>\n    </ion-grid>\n</ion-content>\n'/*ion-inline-end:"/Users/Maxime/Desktop/passation/src/pages/edit/edit.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6_ionic_angular__["f" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["a" /* ActionSheetController */],
            __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["m" /* ToastController */], __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["k" /* Platform */], __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["f" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_4__providers_reddit_service__["a" /* RedditService */],
            __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__["a" /* Geolocation */], __WEBPACK_IMPORTED_MODULE_2__angular_core__["NgZone"]])
    ], Edit);
    return Edit;
    var Edit_1;
}());

//# sourceMappingURL=edit.js.map

/***/ }),

/***/ 720:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TestsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_reddit_service__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ordertest_ordertest__ = __webpack_require__(374);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var TestsPage = /** @class */ (function () {
    function TestsPage(navCtrl, popoverCtrl, alertCtrl, menu, loadingController, redditService, storage, navParams, viewCtrl) {
        this.navCtrl = navCtrl;
        this.popoverCtrl = popoverCtrl;
        this.alertCtrl = alertCtrl;
        this.menu = menu;
        this.loadingController = loadingController;
        this.redditService = redditService;
        this.storage = storage;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.word = "";
        this.wordid = "";
        this.page = 1;
    }
    TestsPage_1 = TestsPage;
    TestsPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.page = 1;
        this.redditService.getAllTests(this.page).subscribe(function (data) {
            console.log(data);
            _this.posts = data.listing;
            _this.items = data.items;
            _this.pages = data.page;
            _this.currentpage = data.currentpage;
        });
    };
    TestsPage.prototype.doRefresh = function (refresher) {
        var _this = this;
        setTimeout(function () {
            _this.page = 1;
            _this.redditService.getAllTests(_this.page).subscribe(function (data) {
                _this.posts = data.listing;
                _this.items = data.items;
                _this.pages = data.page;
                _this.currentpage = data.currentpage;
            });
            refresher.complete();
        }, 1100);
    };
    TestsPage.prototype.order = function (event, item) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__ordertest_ordertest__["a" /* OrdertestPage */], {
            param1: item.test_num,
            param2: item.test_category,
        });
    };
    TestsPage.prototype.delete = function (event, item) {
        var _this = this;
        var data = JSON.stringify({
            idtest: item.test_num,
        });
        console.log(data);
        this.redditService.deleteTest(data)
            .toPromise()
            .then(function (response) { setTimeout(function () { _this.navCtrl.push(TestsPage_1); }, 500); });
    };
    TestsPage.prototype.next = function () {
        var _this = this;
        if (this.page < this.pages) {
            this.page = this.page + 1;
            if (this.word == "" && this.wordid == "") {
                this.redditService.getAllTests(this.page).subscribe(function (data) {
                    _this.posts = data.listing;
                    _this.items = data.items;
                    _this.pages = data.page;
                    _this.currentpage = data.currentpage;
                });
            }
            else if (this.word !== "") {
                this.redditService.seachTest(this.page, this.word).subscribe(function (data) {
                    _this.posts = data.listing;
                    _this.items = data.items;
                    _this.pages = data.page;
                    _this.currentpage = data.currentpage;
                });
            }
            else if (this.wordid !== "") {
                this.redditService.seachTestid(this.page, this.wordid).subscribe(function (data) {
                    _this.posts = data.listing;
                    _this.items = data.items;
                    _this.pages = data.page;
                    _this.currentpage = data.currentpage;
                });
            }
        }
    };
    TestsPage.prototype.prev = function () {
        var _this = this;
        if (this.page > 1) {
            this.page = this.page - 1;
            if (this.word == "" && this.wordid == "") {
                this.redditService.getAllTests(this.page).subscribe(function (data) {
                    _this.posts = data.listing;
                    _this.items = data.items;
                    _this.pages = data.page;
                    _this.currentpage = data.currentpage;
                });
            }
            else if (this.word !== "") {
                this.redditService.seachTest(this.page, this.word).subscribe(function (data) {
                    _this.posts = data.listing;
                    _this.items = data.items;
                    _this.pages = data.page;
                    _this.currentpage = data.currentpage;
                });
            }
            else if (this.wordid !== "") {
                this.redditService.seachTestid(this.page, this.wordid).subscribe(function (data) {
                    _this.posts = data.listing;
                    _this.items = data.items;
                    _this.pages = data.page;
                    _this.currentpage = data.currentpage;
                });
            }
        }
    };
    TestsPage.prototype.forward = function () {
        var _this = this;
        if (this.pages > 1) {
            this.page = this.pages;
            if (this.word == "" && this.wordid == "") {
                this.redditService.getAllTests(this.page).subscribe(function (data) {
                    _this.posts = data.listing;
                    _this.items = data.items;
                    _this.pages = data.page;
                    _this.currentpage = data.currentpage;
                });
            }
            else if (this.word !== "") {
                this.redditService.seachTest(this.page, this.word).subscribe(function (data) {
                    _this.posts = data.listing;
                    _this.items = data.items;
                    _this.pages = data.page;
                    _this.currentpage = data.currentpage;
                });
            }
            else if (this.wordid !== "") {
                this.redditService.seachTestid(this.page, this.wordid).subscribe(function (data) {
                    _this.posts = data.listing;
                    _this.items = data.items;
                    _this.pages = data.page;
                    _this.currentpage = data.currentpage;
                });
            }
        }
    };
    TestsPage.prototype.backward = function () {
        var _this = this;
        if (this.pages > 1) {
            this.page = 1;
            if (this.word == "" && this.wordid == "") {
                this.redditService.getAllTests(this.page).subscribe(function (data) {
                    _this.posts = data.listing;
                    _this.items = data.items;
                    _this.pages = data.page;
                    _this.currentpage = data.currentpage;
                });
            }
            else if (this.word !== "") {
                this.redditService.seachTest(this.page, this.word).subscribe(function (data) {
                    _this.posts = data.listing;
                    _this.items = data.items;
                    _this.pages = data.page;
                    _this.currentpage = data.currentpage;
                });
            }
            else if (this.wordid !== "") {
                this.redditService.seachTestid(this.page, this.wordid).subscribe(function (data) {
                    _this.posts = data.listing;
                    _this.items = data.items;
                    _this.pages = data.page;
                    _this.currentpage = data.currentpage;
                });
            }
        }
    };
    TestsPage.prototype.onInput = function (selectedValue) {
        var _this = this;
        this.page = 1;
        this.redditService.seachTest(this.page, this.word).subscribe(function (data) {
            console.log(data);
            _this.posts = data.listing;
            _this.items = data.items;
            _this.pages = data.page;
            _this.currentpage = data.currentpage;
        });
    };
    TestsPage.prototype.onCancel = function (selectedValue) {
        this.wordid == "";
        this.word == "";
    };
    TestsPage.prototype.onInputid = function (selectedValue) {
        var _this = this;
        this.page = 1;
        this.redditService.seachTestid(this.page, this.wordid).subscribe(function (data) {
            _this.posts = data.listing;
            _this.items = data.items;
            _this.pages = data.page;
            _this.currentpage = data.currentpage;
        });
    };
    TestsPage.prototype.onCancelid = function (selectedValue) {
        this.wordid == "";
        this.word == "";
    };
    TestsPage.prototype.reset = function () {
        this.wordid == "";
        this.word == "";
        this.navCtrl.setRoot(TestsPage_1);
    };
    TestsPage = TestsPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-tests',template:/*ion-inline-start:"/Users/Maxime/Desktop/passation/src/pages/tests/tests.html"*/'<ion-header>\n    <ion-toolbar  color="secondary" hide-back-button >\n        <button ion-button menuToggle class="hidden-lg-up">\n            <ion-icon name="menu"></ion-icon>\n          </button>\n          <ion-title> Tests</ion-title>\n          \n          <ion-buttons end> \n        <button ion-button icon-only (click)="goAdduser()">\n          <ion-icon ios="ios-add-circle" md="md-add-circle"></ion-icon> Ajouter \n        </button>\n        </ion-buttons>\n        \n      </ion-toolbar>  \n   \n    </ion-header>\n  \n  <ion-content>\n      <ion-row responsive-sm class="backcolor" >\n    \n        <ion-col col-12 col-md-2>\n          <ion-searchbar\n          type=number\n          [(ngModel)]="wordid"\n          [showCancelButton]="shouldShowCancel"\n          (ionInput)="onInputid($event)"\n          (ionCancel)="onCancel($event)"\n          placeholder="Id" >\n        </ion-searchbar>\n      </ion-col>\n        \n          <ion-col col-12 col-md-4>\n              <ion-searchbar\n              [(ngModel)]="word"\n              [showCancelButton]="shouldShowCancel"\n              (ionInput)="onInput($event)"\n              (ionCancel)="onCancel($event)"\n              placeholder="Titre">\n            </ion-searchbar>\n          </ion-col>\n  \n  \n          <ion-col col-12 col-md-1>\n          <button ion-button color="light" (click)="reset()" > <ion-icon ios="ios-refresh" md="md-refresh"></ion-icon></button>\n        </ion-col>\n          <ion-col col-12 col-md-5 color="secondary">\n            <ion-item color="secondary">\n                Nombre de résultats : <b>{{items}}</b>\n                Page : <b>{{currentpage}} / {{pages}}</b>\n            </ion-item>\n          </ion-col>\n  \n        </ion-row>\n  \n    <ion-refresher (ionRefresh)="doRefresh($event)">\n        <ion-refresher-content></ion-refresher-content>\n      </ion-refresher>\n      \n      <ion-list>\n\n          <ion-item *ngFor="let item of posts" > \n              <ion-icon ios="ios-contact" md="md-contact"></ion-icon>      \n              <ion-label><b>{{item.test_num}}<ion-icon ios="ios-document" md="md-document"></ion-icon> {{item.test_label}} </b> </ion-label>  \n              <ion-label   *ngIf="item.test_category==42" >     <ion-icon ios="ios-pricetag" md="md-pricetag">  </ion-icon> <b> Logique </b> </ion-label >\n                <ion-label   *ngIf="item.test_category==13" >     <ion-icon ios="ios-pricetag" md="md-pricetag">  </ion-icon> <b> Jugement </b></ion-label >\n                <ion-label   *ngIf="item.test_category==1552" >   <ion-icon ios="ios-pricetag" md="md-pricetag"></ion-icon> <b> Verbal ( Compréhension )</b> </ion-label >\n                <ion-label   *ngIf="item.test_category==713" >    <ion-icon ios="ios-pricetag" md="md-pricetag"></ion-icon> <b> Verbal ( Définition )</b> </ion-label >\n                <ion-label   *ngIf="item.test_category==1574" >   <ion-icon ios="ios-pricetag" md="md-pricetag"></ion-icon> <b> Verbal ( Synonyme )</b> </ion-label >\n                <ion-label   *ngIf="item.test_category==142" >    <ion-icon ios="ios-pricetag" md="md-pricetag"></ion-icon> <b> Verbal ( Elimination )</b> </ion-label >\n                <ion-label   *ngIf="item.test_category==14" >     <ion-icon ios="ios-pricetag" md="md-pricetag"></ion-icon> <b> Valeurs </b></ion-label >\n                <ion-label   *ngIf="item.test_category==22" >     <ion-icon ios="ios-pricetag" md="md-pricetag"></ion-icon> <b> Stress </b> </ion-label >\n                <ion-label   *ngIf="item.test_category==533" >    <ion-icon ios="ios-pricetag" md="md-pricetag"></ion-icon> <b> Imagination</b> </ion-label >\n                <ion-label   *ngIf="item.test_category==725" >    <ion-icon ios="ios-pricetag" md="md-pricetag"></ion-icon> <b> Données scientifiques</b> </ion-label >\n                <ion-label   *ngIf="item.test_category==394" >    <ion-icon ios="ios-pricetag" md="md-pricetag"></ion-icon> <b> Mémoire </b> </ion-label >\n                <ion-label   *ngIf="item.test_category==1616" >   <ion-icon ios="ios-pricetag" md="md-pricetag"></ion-icon> <b> Vidéo</b> </ion-label > \n                <ion-label   *ngIf="item.test_category==122" >    <ion-icon ios="ios-pricetag" md="md-pricetag"></ion-icon> <b> Expressions faciales</b> </ion-label >\n                <ion-label   *ngIf="item.test_category==1654" >   <ion-icon ios="ios-pricetag" md="md-pricetag"></ion-icon> <b> Représentation spatiale</b> </ion-label >\n                <ion-label   *ngIf="item.test_category==1609" >   <ion-icon ios="ios-pricetag" md="md-pricetag"></ion-icon> <b> Langue</b> </ion-label >\n                <ion-label   *ngIf="item.test_category==255" >    <ion-icon ios="ios-pricetag" md="md-pricetag"></ion-icon> <b> Questionnaire (Att.Personnelles)</b> </ion-label >\n                <ion-label   *ngIf="item.test_category==345" >    <ion-icon ios="ios-pricetag" md="md-pricetag"></ion-icon> <b> Questionnaire (EDUC) </b> </ion-label >\n                <ion-label   *ngIf="item.test_category==1283" >   <ion-icon ios="ios-pricetag" md="md-pricetag"></ion-icon> <b> Questionnaire (SF_GE) </b> </ion-label >\n                <ion-label   *ngIf="item.test_category==1388" >   <ion-icon ios="ios-pricetag" md="md-pricetag"></ion-icon> <b> Questionnaire (SF_VD) </b> </ion-label >\n                <ion-label   *ngIf="item.test_category==1193" >   <ion-icon ios="ios-pricetag" md="md-pricetag"></ion-icon> <b> Questionnaire (PHYSIO) </b> </ion-label >\n                <ion-label   *ngIf="item.test_category==1013" >   <ion-icon ios="ios-pricetag" md="md-pricetag"></ion-icon> <b> Questionnaire (HD) </b> </ion-label >\n                <ion-label   *ngIf="item.test_category==818" >    <ion-icon ios="ios-pricetag" md="md-pricetag"></ion-icon> <b> Questionnaire (DIETE) </b> </ion-label >\n                <ion-label   *ngIf="item.test_category==923" >    <ion-icon ios="ios-pricetag" md="md-pricetag"></ion-icon> <b> Questionnaire (ERGO)</b> </ion-label >\n                <ion-label   *ngIf="item.test_category==1103" >   <ion-icon ios="ios-pricetag" md="md-pricetag"></ion-icon> <b> Questionnaire (OSTEO)</b> </ion-label >\n                <ion-label   *ngIf="item.test_category==1872" >   <ion-icon ios="ios-pricetag" md="md-pricetag"></ion-icon> <b> Questionnaire (PODO) </b> </ion-label >\n              <ion-label> <ion-icon ios="ios-alarm" md="md-alarm"></ion-icon><b>{{item.test_duree}} </b></ion-label>  \n              <ion-label> {{item.test_titre}}</ion-label>  \n              <button ion-button item-end small color="red" (click)="order($event, item)"> <ion-icon ios="ios-list-box" md="md-list-box"></ion-icon></button>\n              <button ion-button item-end small color="secondary" (click)="edit($event, item)"><ion-icon ios="ios-brush" md="md-brush"></ion-icon></button>\n              <button ion-button outline item-end small color="secondary" (click)="delete($event, item)"><ion-icon ios="ios-trash" md="md-trash"></ion-icon></button>\n          </ion-item>\n\n      </ion-list>\n      <button ion-button small color="secondary" (click)="backward()"> <ion-icon ios="ios-skip-backward" md="md-skip-backward"></ion-icon></button>\n      <button ion-button small color="secondary" (click)="prev()"> Precédent</button>\n      <button ion-button small color="secondary" (click)="next()"> Suivant</button>\n      <button ion-button small color="secondary" (click)="forward()"> <ion-icon ios="ios-skip-forward" md="md-skip-forward"></ion-icon></button>\n  </ion-content>\n  \n  '/*ion-inline-end:"/Users/Maxime/Desktop/passation/src/pages/tests/tests.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* PopoverController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* MenuController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_2__providers_reddit_service__["a" /* RedditService */], __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */]])
    ], TestsPage);
    return TestsPage;
    var TestsPage_1;
}());

//# sourceMappingURL=tests.js.map

/***/ }),

/***/ 721:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return QuestionsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_reddit_service__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var QuestionsPage = /** @class */ (function () {
    function QuestionsPage(navCtrl, popoverCtrl, alertCtrl, menu, loadingController, redditService, storage, navParams, viewCtrl) {
        this.navCtrl = navCtrl;
        this.popoverCtrl = popoverCtrl;
        this.alertCtrl = alertCtrl;
        this.menu = menu;
        this.loadingController = loadingController;
        this.redditService = redditService;
        this.storage = storage;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.word = "";
        this.wordid = "";
        this.page = 1;
    }
    QuestionsPage_1 = QuestionsPage;
    QuestionsPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.page = 1;
        this.redditService.getAllQuestions(this.page).subscribe(function (data) {
            _this.posts = data.listing;
            _this.items = data.items;
            _this.pages = data.page;
            _this.currentpage = data.currentpage;
        });
        this.redditService.getCategory().subscribe(function (data) {
            _this.types = data.listing;
        });
    };
    QuestionsPage.prototype.doRefresh = function (refresher) {
        var _this = this;
        setTimeout(function () {
            _this.page = 1;
            _this.redditService.getAllQuestions(_this.page).subscribe(function (data) {
                _this.posts = data.listing;
                _this.items = data.items;
                _this.pages = data.page;
                _this.currentpage = data.currentpage;
            });
            refresher.complete();
        }, 1100);
    };
    QuestionsPage.prototype.next = function () {
        var _this = this;
        if (this.page < this.pages) {
            this.page = this.page + 1;
            if (this.word == "" && this.wordid == "") {
                this.redditService.getAllQuestions(this.page).subscribe(function (data) {
                    _this.posts = data.listing;
                    _this.items = data.items;
                    _this.pages = data.page;
                    _this.currentpage = data.currentpage;
                });
            }
            else if (this.word !== "") {
                this.redditService.seachQuestion(this.page, this.word).subscribe(function (data) {
                    _this.posts = data.listing;
                    _this.items = data.items;
                    _this.pages = data.page;
                    _this.currentpage = data.currentpage;
                });
            }
            else if (this.wordid !== "") {
                this.redditService.seachQuestionid(this.page, this.wordid).subscribe(function (data) {
                    _this.posts = data.listing;
                    _this.items = data.items;
                    _this.pages = data.page;
                    _this.currentpage = data.currentpage;
                });
            }
        }
    };
    QuestionsPage.prototype.prev = function () {
        var _this = this;
        if (this.page > 1) {
            this.page = this.page - 1;
            if (this.word == "" && this.wordid == "") {
                this.redditService.getAllQuestions(this.page).subscribe(function (data) {
                    _this.posts = data.listing;
                    _this.items = data.items;
                    _this.pages = data.page;
                    _this.currentpage = data.currentpage;
                });
            }
            else if (this.word !== "") {
                this.redditService.seachQuestion(this.page, this.word).subscribe(function (data) {
                    _this.posts = data.listing;
                    _this.items = data.items;
                    _this.pages = data.page;
                    _this.currentpage = data.currentpage;
                });
            }
            else if (this.wordid !== "") {
                this.redditService.seachQuestionid(this.page, this.wordid).subscribe(function (data) {
                    _this.posts = data.listing;
                    _this.items = data.items;
                    _this.pages = data.page;
                    _this.currentpage = data.currentpage;
                });
            }
        }
    };
    QuestionsPage.prototype.forward = function () {
        var _this = this;
        if (this.pages > 1) {
            this.page = this.pages;
            if (this.word == "" && this.wordid == "") {
                this.redditService.getAllQuestions(this.page).subscribe(function (data) {
                    _this.posts = data.listing;
                    _this.items = data.items;
                    _this.pages = data.page;
                    _this.currentpage = data.currentpage;
                });
            }
            else if (this.word !== "") {
                this.redditService.seachQuestion(this.page, this.word).subscribe(function (data) {
                    _this.posts = data.listing;
                    _this.items = data.items;
                    _this.pages = data.page;
                    _this.currentpage = data.currentpage;
                });
            }
            else if (this.wordid !== "") {
                this.redditService.seachQuestionid(this.page, this.wordid).subscribe(function (data) {
                    _this.posts = data.listing;
                    _this.items = data.items;
                    _this.pages = data.page;
                    _this.currentpage = data.currentpage;
                });
            }
        }
    };
    QuestionsPage.prototype.backward = function () {
        var _this = this;
        if (this.pages > 1) {
            this.page = 1;
            if (this.word == "" && this.wordid == "") {
                this.redditService.getAllQuestions(this.page).subscribe(function (data) {
                    _this.posts = data.listing;
                    _this.items = data.items;
                    _this.pages = data.page;
                    _this.currentpage = data.currentpage;
                });
            }
            else if (this.word !== "") {
                this.redditService.seachQuestion(this.page, this.word).subscribe(function (data) {
                    _this.posts = data.listing;
                    _this.items = data.items;
                    _this.pages = data.page;
                    _this.currentpage = data.currentpage;
                });
            }
            else if (this.wordid !== "") {
                this.redditService.seachQuestionid(this.page, this.wordid).subscribe(function (data) {
                    _this.posts = data.listing;
                    _this.items = data.items;
                    _this.pages = data.page;
                    _this.currentpage = data.currentpage;
                });
            }
        }
    };
    QuestionsPage.prototype.goUser = function () {
        this.navCtrl.push(QuestionsPage_1);
    };
    QuestionsPage.prototype.onInput = function (selectedValue) {
        var _this = this;
        this.page = 1;
        this.redditService.seachQuestion(this.page, this.word).subscribe(function (data) {
            _this.posts = data.listing;
            _this.items = data.items;
            _this.pages = data.page;
            _this.currentpage = data.currentpage;
        });
    };
    QuestionsPage.prototype.onCancel = function (selectedValue) {
        this.wordid == "";
        this.word == "";
    };
    QuestionsPage.prototype.onInputid = function (selectedValue) {
        var _this = this;
        this.page = 1;
        this.redditService.seachQuestionid(this.page, this.wordid).subscribe(function (data) {
            _this.posts = data.listing;
            _this.items = data.items;
            _this.pages = data.page;
            _this.currentpage = data.currentpage;
        });
    };
    QuestionsPage.prototype.onCancelid = function (selectedValue) {
        this.wordid == "";
        this.word == "";
    };
    QuestionsPage.prototype.reset = function () {
        this.wordid == "";
        this.word == "";
        this.navCtrl.setRoot(QuestionsPage_1);
    };
    QuestionsPage = QuestionsPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-questions',template:/*ion-inline-start:"/Users/Maxime/Desktop/passation/src/pages/questions/questions.html"*/'<ion-header>\n  <ion-toolbar  color="secondary" hide-back-button >\n      <button ion-button menuToggle class="hidden-lg-up">\n          <ion-icon name="menu"></ion-icon>\n        </button>\n        <ion-title> Questions </ion-title>\n        \n        <ion-buttons end> \n      <button ion-button icon-only (click)="goAdduser()">\n        <ion-icon ios="ios-add-circle" md="md-add-circle"></ion-icon> Ajouter \n      </button>\n      </ion-buttons>\n      \n    </ion-toolbar>  \n \n  </ion-header>\n\n<ion-content>\n    <ion-row responsive-sm class="backcolor" >\n  \n      <ion-col col-12 col-md-2>\n        <ion-searchbar\n        type=number\n        [(ngModel)]="wordid"\n        [showCancelButton]="shouldShowCancel"\n        (ionInput)="onInputid($event)"\n        (ionCancel)="onCancel($event)"\n        placeholder="Id num" >\n       </ion-searchbar>\n       </ion-col>\n      \n        <ion-col col-12 col-md-4>\n            <ion-searchbar\n            [(ngModel)]="word"\n            [showCancelButton]="shouldShowCancel"\n            (ionInput)="onInput($event)"\n            (ionCancel)="onCancel($event)"\n            placeholder="Titre">\n          </ion-searchbar>\n        </ion-col>\n\n\n        <ion-col col-12 col-md-1>\n        <button ion-button color="light" (click)="reset()" > <ion-icon ios="ios-refresh" md="md-refresh"></ion-icon></button>\n        </ion-col>\n\n      <ion-col col-12 col-md-5 color="secondary">\n          <ion-item color="secondary">\n              Nombre de résultats : <b>{{items}}</b>\n              Page : <b>{{currentpage}} / {{pages}}</b>\n          </ion-item>\n      </ion-col>\n\n      </ion-row>\n \n\n    \n    <ion-list>\n        <ion-item *ngFor="let item of posts" > \n            <ion-label><b>   <ion-icon ios="ios-document" md="md-document"></ion-icon>   {{item.question_num}} </b> type : {{item.question_groupe}}  </ion-label>            \n            <ion-label><b> {{item.question_label}} </b> </ion-label>   \n              <ion-label   *ngIf="item.question_groupe==42" >     <ion-icon ios="ios-pricetag" md="md-pricetag">  </ion-icon> <b> Logique </b> </ion-label >\n              <ion-label   *ngIf="item.question_groupe==13" >     <ion-icon ios="ios-pricetag" md="md-pricetag">  </ion-icon> <b> Jugement </b></ion-label >\n              <ion-label   *ngIf="item.question_groupe==1552" >   <ion-icon ios="ios-pricetag" md="md-pricetag"></ion-icon> <b> Verbal ( Compréhension )</b> </ion-label >\n              <ion-label   *ngIf="item.question_groupe==713" >    <ion-icon ios="ios-pricetag" md="md-pricetag"></ion-icon>  <b> Verbal ( Définition )</b> </ion-label >\n              <ion-label   *ngIf="item.question_groupe==1574" >   <ion-icon ios="ios-pricetag" md="md-pricetag"></ion-icon> <b> Verbal ( Synonyme )</b> </ion-label >\n              <ion-label   *ngIf="item.question_groupe==142" >    <ion-icon ios="ios-pricetag" md="md-pricetag"></ion-icon>  <b> Verbal ( Elimination )</b> </ion-label >\n              <ion-label   *ngIf="item.question_groupe==14" >     <ion-icon ios="ios-pricetag" md="md-pricetag">  </ion-icon> <b> Valeurs </b></ion-label >\n              <ion-label   *ngIf="item.question_groupe==22" >     <ion-icon ios="ios-pricetag" md="md-pricetag"></ion-icon>   <b> Stress </b> </ion-label >\n              <ion-label   *ngIf="item.question_groupe==533" >    <ion-icon ios="ios-pricetag" md="md-pricetag"></ion-icon>  <b> Imagination</b> </ion-label >\n              <ion-label   *ngIf="item.question_groupe==725" >    <ion-icon ios="ios-pricetag" md="md-pricetag"></ion-icon>  <b> Données scientifiques</b> </ion-label >\n              <ion-label   *ngIf="item.question_groupe==394" >    <ion-icon ios="ios-pricetag" md="md-pricetag"></ion-icon>  <b> Mémoire </b> </ion-label >\n              <ion-label   *ngIf="item.question_groupe==1616" >   <ion-icon ios="ios-pricetag" md="md-pricetag"></ion-icon> <b> Vidéo</b> </ion-label > \n              <ion-label   *ngIf="item.question_groupe==122" >    <ion-icon ios="ios-pricetag" md="md-pricetag"></ion-icon>  <b> Expressions faciales</b> </ion-label >\n              <ion-label   *ngIf="item.question_groupe==1654" >   <ion-icon ios="ios-pricetag" md="md-pricetag"></ion-icon> <b> Représentation spatiale</b> </ion-label >\n              <ion-label   *ngIf="item.question_groupe==1609" >   <ion-icon ios="ios-pricetag" md="md-pricetag"></ion-icon> <b> Langue</b> </ion-label >\n              <ion-label   *ngIf="item.question_groupe==255" >    <ion-icon ios="ios-pricetag" md="md-pricetag"></ion-icon>  <b> Questionnaire (Att.Personnelles)</b> </ion-label >\n              <ion-label   *ngIf="item.question_groupe==345" >    <ion-icon ios="ios-pricetag" md="md-pricetag"></ion-icon>  <b> Questionnaire (EDUC) </b> </ion-label >\n              <ion-label   *ngIf="item.question_groupe==1283" >   <ion-icon ios="ios-pricetag" md="md-pricetag"></ion-icon> <b> Questionnaire (SF_GE) </b> </ion-label >\n              <ion-label   *ngIf="item.question_groupe==1388" >   <ion-icon ios="ios-pricetag" md="md-pricetag"></ion-icon> <b> Questionnaire (SF_VD) </b> </ion-label >\n              <ion-label   *ngIf="item.question_groupe==1193" >   <ion-icon ios="ios-pricetag" md="md-pricetag"></ion-icon> <b> Questionnaire (PHYSIO) </b> </ion-label >\n              <ion-label   *ngIf="item.question_groupe==1013" >   <ion-icon ios="ios-pricetag" md="md-pricetag"></ion-icon> <b> Questionnaire (HD) </b> </ion-label >\n              <ion-label   *ngIf="item.question_groupe==818" >    <ion-icon ios="ios-pricetag" md="md-pricetag"></ion-icon>  <b> Questionnaire (DIETE) </b> </ion-label >\n              <ion-label   *ngIf="item.question_groupe==923" >    <ion-icon ios="ios-pricetag" md="md-pricetag"></ion-icon>  <b> Questionnaire (ERGO)</b> </ion-label >\n              <ion-label   *ngIf="item.question_groupe==1103" >   <ion-icon ios="ios-pricetag" md="md-pricetag"></ion-icon> <b> Questionnaire (OSTEO)</b> </ion-label >\n              <ion-label   *ngIf="item.question_groupe==1872" >   <ion-icon ios="ios-pricetag" md="md-pricetag"></ion-icon> <b> Questionnaire (PODO) </b> </ion-label >\n              <ion-label><b>{{item.question_date_modified}}</b></ion-label>  \n            <button ion-button item-end small color="secondary" (click)="edit($event, item)"><ion-icon ios="ios-brush" md="md-brush"></ion-icon></button>\n            <button ion-button outline item-end small color="secondary" (click)="delete($event, item)"><ion-icon ios="ios-trash" md="md-trash"></ion-icon></button>\n          </ion-item>\n    </ion-list>\n\n    <button ion-button small color="secondary" (click)="backward()"> <ion-icon ios="ios-skip-backward" md="md-skip-backward"></ion-icon></button>\n    <button ion-button small color="secondary" (click)="prev()"> Precédent</button>\n    <button ion-button small color="secondary" (click)="next()"> Suivant</button>\n    <button ion-button small color="secondary" (click)="forward()"> <ion-icon ios="ios-skip-forward" md="md-skip-forward"></ion-icon></button>\n</ion-content>\n\n'/*ion-inline-end:"/Users/Maxime/Desktop/passation/src/pages/questions/questions.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* PopoverController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* MenuController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_2__providers_reddit_service__["a" /* RedditService */], __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */]])
    ], QuestionsPage);
    return QuestionsPage;
    var QuestionsPage_1;
}());

//# sourceMappingURL=questions.js.map

/***/ }),

/***/ 722:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OrderpassationPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_add_operator_map__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_reddit_service__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_dragula__ = __webpack_require__(26);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var OrderpassationPage = /** @class */ (function () {
    function OrderpassationPage(navCtrl, navParams, loadingController, dragulaService, alertCtrl, redditService) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.loadingController = loadingController;
        this.dragulaService = dragulaService;
        this.alertCtrl = alertCtrl;
        this.redditService = redditService;
        this.items = [];
        this.posts2 = [];
        this.editing = true;
        this.word = "";
        this.wordid = "";
        this.parameter1 = navParams.get('param1');
        this.page = 1;
        this.redditService.getPassation(this.parameter1).subscribe(function (data) {
            _this.data = data.items;
        });
        this.getFk();
    }
    OrderpassationPage.prototype.ngOnInit = function () { };
    OrderpassationPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.redditService.getAllTests(this.page).subscribe(function (data) {
            _this.posts = data.listing;
            _this.items = data.items;
            _this.pages = data.page;
            _this.currentpage = data.currentpage;
        });
    };
    OrderpassationPage.prototype.reorderData = function (indexes) {
        this.posts2 = Object(__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["o" /* reorderArray */])(this.posts2, indexes);
        console.log(this.posts2);
    };
    OrderpassationPage.prototype.getFk = function () {
        var _this = this;
        this.redditService.getFkpass(this.parameter1).subscribe(function (data) {
            _this.posts2 = data.listing;
        });
    };
    OrderpassationPage.prototype.next = function () {
    };
    OrderpassationPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Component"])({
            providers: [__WEBPACK_IMPORTED_MODULE_4_ng2_dragula__["b" /* DragulaService */]],
            selector: 'page-orderpassation',template:/*ion-inline-start:"/Users/Maxime/Desktop/passation/src/pages/orderpassation/orderpassation.html"*/'\n<ion-header>\n    <ion-toolbar  color="secondary" >\n    <button ion-button menuToggle>\n    <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>  Intro </ion-title>\n                  <ion-buttons end>             \n                  </ion-buttons>\n            </ion-toolbar>\n            </ion-header>\n\n            <ion-content>\n              <ion-grid >\n                <ion-row responsive-sm  >\n          <ion-col col-12 col-sm>  </ion-col>\n          <ion-col col-12 col-sm>  <ion-item *ngFor="let item of posts2">\n            <ion-icon ios="ios-albums" md="md-albums"></ion-icon>   <b>{{item.test_label}}</b>\n\n          </ion-item> </ion-col>\n          <ion-col col-12 col-sm>  </ion-col>\n          </ion-row>\n          </ion-grid>\n  </ion-content>\n  \n'/*ion-inline-end:"/Users/Maxime/Desktop/passation/src/pages/orderpassation/orderpassation.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["f" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_4_ng2_dragula__["b" /* DragulaService */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_2__providers_reddit_service__["a" /* RedditService */]])
    ], OrderpassationPage);
    return OrderpassationPage;
}());

//# sourceMappingURL=orderpassation.js.map

/***/ })

},[375]);
//# sourceMappingURL=main.js.map